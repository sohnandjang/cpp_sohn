#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

long long int A = 0;
long long  B = 0;
long long  C = 0;

long long int Solve(long long a, long long b)
{
    if(b==0)
        return 1;
    //if(b==1)
    //    return a;
    if(b%2 > 0)
        return (a * (Solve(a,b-1)%C))%C;

    long long int temp = Solve(a,b/2) %C;
    return (temp*temp)%C;
}

long long pow(int A, int B, int C)
{
    if (B == 1)
        return A;

    else
    {
        long long temp = pow(A, B / 2, C);
        //B가 홀수면 A를 한번 더 곱해줘야한다
        if (B % 2)
            return ((temp * temp) % C * A) % C;
        else
            return (temp * temp) % C;
    }
}
long long  power(long long  n, long long  k){
    // 기저 사례: n^0 = 1
    if(k == 0) return 1;
 
    long long  temp = power(n, k/2);
    long long  result = temp * temp % C;
    // 홀수이면 n을 한 번 더 곱해준다.
    if(k%2) result = result * n % C;
    return result;
}
 

int main()
{
    cin >> A;
    cin >> B;
    cin >> C;

    //A^B % C
    //long long int ret1 = Solve(A, B);
    //long long int ret2 = pow(A, B, C);
    //printf("%d\n", ret1);
    //printf("%d\n", ret2);
    //cout << pow(A % C, B, C) << "\n";
    printf("%d\n", Solve(A, B));
    return 0;

}


#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_N = 51;
int N = 0;
int Array[MAX_N][MAX_N];
bool Visited[MAX_N][MAX_N] = {0};

int dx[4] = {1, -1, 0, 0};
int dy[4] = {0, 0, 1, -1};

// return num of Vertex
int DFS(int row, int col)
{
    if(row < 1 || row > N || col < 1 || col > N)
        return 0;

    if(!Array[row][col])
        return 0;

    Visited[row][col] = true;
    //printf("Visted[%d][%d] > ", row, col);

    int ret = 1;
    for(int i=0; i<4; ++i)
    {
        int next_row = row + dy[i];
        int next_col = col + dx[i];
        if(1 <= next_row && next_row <= N && 1<= next_col && next_col <= N)
        {
            if(!Visited[next_row][next_col] && Array[next_row][next_col])
            {
                ret += DFS(next_row, next_col); 
            }
        }
    }
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> N;
    for(int i=1; i<=N; ++i)
    {
        string line;
        cin >> line;
        for(int j=0; j<line.size(); ++j)
        {
            Array[i][j+1] = line[j] - '0';
        }
    }

    int numOfComplex = 0;
    int numOfHouse = 0;
    vector<int> numOfHouses;
    for(int i=1; i<=N; ++i)
    {
        numOfHouse = 0;
        for(int j=1; j<=N; ++j)
        {
            if(!Visited[i][j])
            {
                numOfHouse = DFS(i, j);
                if(numOfHouse != 0)
                {
                    ++numOfComplex;
                    numOfHouses.push_back(numOfHouse);
                }
            }
        }
    }
    printf("%d\n", numOfComplex);
    sort(numOfHouses.begin(), numOfHouses.end());
    for(int i=0; i<numOfHouses.size(); ++i)
    {
        printf("%d\n", numOfHouses[i]);
    }
    return 0;
}
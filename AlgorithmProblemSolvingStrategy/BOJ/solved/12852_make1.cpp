#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

constexpr int MAX_LEN = 1'000'001;

int N = 0;
int DP[MAX_LEN] = {0}; // DP[x] x값을 1로 만드는데 걸리는 횟수
int Before[MAX_LEN] = {0}; // Before[x] x수 바로 이전 수


void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
}

void Solve()
{
    DP[1] = 0;

    for(int i=2; i<=N; ++i)
    {
        DP[i] = DP[i-1] + 1;
        Before[i] = i-1;

        if (i % 2 == 0)
        {
            if (DP[i] > DP[i/2] + 1)
            {
                DP[i] = DP[i/2] + 1;
                Before[i] = i/2;
            }
        }
        if (i % 3 == 0)
        {
            if (DP[i] > DP[i/3] + 1)
            {
                DP[i] = DP[i/3] + 1;
                Before[i] = i/3;
            }
        }
    }
    int n = N;
    printf("%d\n", DP[N]);
    while(1)
    {
        if (n==1)
        {
            printf("%d ", n);
            break;
        }
        printf("%d ", n);
        n = Before[n];
    }
}

int main()
{
    Input();
    Solve();
    return 0;
}
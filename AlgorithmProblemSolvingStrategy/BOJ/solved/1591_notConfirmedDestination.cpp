#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>
#include <algorithm>

using namespace std;

#define MAX_V 2001
vector<pair<int, int>> VERTEX[MAX_V];
vector<int> Candidate;
int StartPoint = 0;
int G = 0;
int H = 0;

void Init()
{
    Candidate.clear();
    for(int i=0; i<MAX_V; ++i)
        VERTEX[i].clear();
}

void Input()
{
    int n, m, t;
    cin >> n >> m >> t;
    int s, g, h;
    cin >> s >> g >> h;
    StartPoint = s;
    G = g;
    H = h;

    for (int i = 0; i < m; ++i)
    {
        int a, b, d;
        cin >> a >> b >> d;
        VERTEX[a].push_back(make_pair(b, d));
        VERTEX[b].push_back(make_pair(a, d)); //bidirectional
    }

    for (int i = 0; i < t; ++i)
    {
        int _candidate;
        cin >> _candidate;
        Candidate.push_back(_candidate);
    }
}

vector<int> Dijkstra(int start)
{
    vector<int> _Distance(MAX_V, INT_MAX);
    priority_queue<pair<int,int>> pq;

    _Distance[start] = 0;
    pq.push(make_pair(0, start));

    while(!pq.empty())
    {
        int cost = pq.top().first;
        int current = pq.top().second;
        pq.pop();

        if (cost > _Distance[current])
            continue;
        
        for (int i=0; i<VERTEX[current].size(); ++i)
        {
            int neighbor = VERTEX[current][i].first;
            int neighborCost = VERTEX[current][i].second;
            int neighborDistance = _Distance[current] + neighborCost;
            if (neighborDistance < _Distance[neighbor])
            {
                _Distance[neighbor] = neighborDistance;
                pq.push(make_pair(-neighborDistance, neighbor));
            }
        }
    }
    return _Distance;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    int T = 0;
    cin >> T;
    for (int testCase = 0; testCase < T; ++testCase)
    {
        Init();
        Input();
        vector<int> DistanceFromStart = Dijkstra(StartPoint);
        vector<int> DistanceFromG = Dijkstra(G);
        vector<int> DistanceFromH = Dijkstra(H);
        vector<int> Result;

        for(int _candidate : Candidate)
        {
            int d_Start_to_End = DistanceFromStart[_candidate];
            int d_Start_G_H_End = DistanceFromStart[G] + DistanceFromG[H] + DistanceFromH[_candidate];
            int d_Start_H_G_End = DistanceFromStart[H] + DistanceFromH[G] + DistanceFromG[_candidate];
            if (d_Start_to_End == d_Start_G_H_End || d_Start_to_End == d_Start_H_G_End)
                Result.push_back(_candidate);
        }
        
        sort(Result.begin(), Result.end());
        for(int i : Result)
        {
            printf("%d ", i);
        }
        printf("\n");
    }

    return 0;
}
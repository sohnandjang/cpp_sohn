#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
using namespace std;

constexpr int MAX_LEN = 302;
int N =0;
int MAP[MAX_LEN] = {0};
int cache[MAX_LEN][MAX_LEN] = {0};

#if 0
6
10
20
15
25
10
20

75
#endif

int sum = 0;
int stair(int height, int numSeries)
{
    //printf(">> h:%d, s:%d\n", height, numSeries);
    if(height == N){
        return MAP[height];
    }
  
    int &ret = cache[height][numSeries];
    if(ret != -1){
        //printf("h:%d, s:%d, cache:%d\n", height, numSeries, ret);
        return ret;
    }
    //int ret = 0;
    int case1 = INT_MIN;
    int case2 = INT_MIN;
    if(height == 0)
    {
        if(height+1 <= N)
            case1 = stair(height+1, 0);
        if(height+2 <= N)
            case2 = stair(height+2, 0);

        ret = max(case1, case2);
    }
    else
    {    
        if(height+1 <= N && numSeries == 0)
            case1 = stair(height+1, numSeries+1)+MAP[height];
        if(height+2 <= N)
            case2 = stair(height+2, 0)+MAP[height];
        ret = max(case1, case2);
    }
        
    //printf("h:%d, s:%d, ret:%d, MAP[%d]=%d\n", height, numSeries, ret, height, MAP[height]);
    return ret;
}

int main()
{
    memset(cache, -1, sizeof(cache));
    cin >> N;
    for(int i=1; i<=N; ++i)
    {
        cin >> MAP[i];
    }

    printf("%d\n", stair(0,0));

    return 0;
}
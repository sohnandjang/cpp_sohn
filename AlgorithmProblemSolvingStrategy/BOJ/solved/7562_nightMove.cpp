#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;
constexpr int MAX_N = 301;
int T = 0;
int N = 0;
typedef struct
{
    int row;
    int col;
} POSITION_T;

POSITION_T Start;
POSITION_T End;

deque<POSITION_T> myQueue;
int Visited[MAX_N][MAX_N] = {0,};
int Board[MAX_N][MAX_N] = {0,};
int dx[] = {2, 2, 1, 1, -1, -1, -2, -2};
int dy[] = {1, -1, 2, -2, 2, -2, 1, -1};

bool isValid(int row, int col)
{
    if (0 <= row && row < N && 0 <= col && col < N)
        return true;
    else
        return false;
}

int BFS(int _row, int _col)
{
    myQueue.push_back({_row, _col});
    Visited[_row][_col] = 0; //init

    int row = 0;
    int col = 0;
    while (!myQueue.empty())
    {
        auto front = myQueue.front();
        //disp();
        myQueue.pop_front();
        row = front.row;
        col = front.col;
        //printf("%d,%d (%d) > ", row, col, brokenBefore);

        if (front.row == End.row && front.col == End.col)
        {
            //printf("[found]!!!\n");
            break;
        }

        for (int i = 0; i < 8; ++i)
        {
            int next_row = row + dy[i];
            int next_col = col + dx[i];
            if (!isValid(next_row, next_col))
                continue;
            
            if (!Visited[next_row][next_col])
            {
                myQueue.push_back({next_row, next_col});
                Visited[next_row][next_col] = Visited[row][col] + 1;
            }
        }
    }
    if(row == End.row && col == End.col)
        return Visited[row][col];
    else
    {
        return -1;
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> T;
    for(int i=0; i<T; ++i)
    {
        cin >> N;
        cin >> Start.row;
        cin >> Start.col;
        cin >> End.row;
        cin >> End.col;
        memset(Visited, 0, sizeof(Visited));
        myQueue.clear();
        printf("%d\n", BFS(Start.row, Start.col));
    }
    
    return 0;
}
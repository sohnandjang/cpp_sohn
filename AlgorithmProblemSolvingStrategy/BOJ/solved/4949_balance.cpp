#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
using namespace std;
#define LEFT_BRACE '('
#define RIGHT_BRACE ')'
#define LEFT_BRACET '['
#define RIGHT_BRACET ']'

int N = 0;


int main()
{
    cin >> N;
    stack<int> myStack;
    vector<int> myVector;
    for(int i=0; i<N; ++i)
    {
        int tmp;
        cin >> tmp;
        myVector.push_back(tmp);
    }   
    for(int i=0; i<16; ++i)
    {
        char tmp;
        cin >> tmp;
        if(tmp == '+')
            myStack.push(tmp);
        else if(tmp == '-')
            myStack.pop();
    }
    return 0;
}
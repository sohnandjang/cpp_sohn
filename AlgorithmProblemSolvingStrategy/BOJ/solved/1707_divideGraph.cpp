#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;
constexpr int MAX_V = 20001;
int K = 0;
int V = 0;
int E = 0;
typedef enum
{
    OFF = 0,
    RED,
    BLUE
}COLOR_T;

deque<int> myQueue;
int Color[MAX_V] = {0,};
vector<vector<int>> Adj;

void BFS(int _start)
{
    if(Color[_start]!=0)
        return;
    myQueue.push_back(_start);
    Color[_start] = RED; 

    while (!myQueue.empty())
    {
        auto front = myQueue.front();
        //disp();
        myQueue.pop_front();
        int s = front;

        for(int i=0; i<Adj[s].size(); ++i)
        {
            int e = Adj[s][i];
            if(Color[e]==0)
            {
                myQueue.push_back(e);
                Color[e] = (Color[s] == RED ? BLUE : RED);
            }
        }
    }
}

bool IsBiPartite()
{
    // check
    for(int i=1; i<=V; ++i)
    {
        for(int j=0; j<Adj[i].size(); ++j)
        {
            int s = i;
            int e = Adj[i][j];
            
            if(Color[s] == Color[e])
                return false;
        }
    }
    return true;
}
int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> K;
    for(int i=0; i<K; ++i)
    {
        cin >> V;
        cin >> E;
        Adj.clear();
        Adj.resize(V+1);
        memset(Color, 0, sizeof(Color));
        for(int i=0; i<E; ++i)
        {
            int s = 0;
            int e = 0;
            cin >> s >> e;
            Adj[s].push_back(e);
            Adj[e].push_back(s);
        }
        for(int i=1; i<=V; ++i)
            BFS(i);

        if(IsBiPartite())
            printf("YES\n");
        else
            printf("NO\n");
        
    }
    
    return 0;
}
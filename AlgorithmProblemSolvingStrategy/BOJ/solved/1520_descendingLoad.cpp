#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_LEN = 501;

typedef struct{
    int row;
    int col;
} MATRIX_T;

int N = 0;
int M = 0;
int cache[MAX_LEN][MAX_LEN] = {0};
int Array[MAX_LEN][MAX_LEN] = {0};
int rowDiff[4] = {1, -1, 0, 0}; // right, left, up, down
int colDiff[4] = {0, 0, -1, 1};
bool isValidPosition(int row, int col)
{
    if (0 <= row && row <= M-1 && 0<= col && col <= N-1)
        return true;
    else
    {
        return false;
    }
}
bool isValidNextPosition(int row, int col, int next_row, int next_col)
{
    if(Array[row][col] > Array[next_row][next_col])
        return true;
    else
    {
        return false;
    }
}
/**
 * return possible way to be able to go to the destination
 */
int solve(int row, int col)
{
    int& ret = cache[row][col];

    if(ret != -1)
        return ret;

    if (row == M-1 && col == N-1)
    {
        //printf("found\n");
        return 1; 
    }

    int numOfWays = 0;
    for(int i=0; i<4; ++i)
    {
        int next_row = row + rowDiff[i];
        int next_col = col + colDiff[i];
        if (isValidPosition(next_row, next_col))
        {
            if (isValidNextPosition(row, col, next_row, next_col))
            {
                //printf("%d %d -> %d %d (%d -> %d)\n", row, col, next_row, next_col, Array[row][col], Array[next_row][next_col]);
                numOfWays += solve(next_row, next_col);
            }
        }
    }
    ret = numOfWays;
    //printf("numOfWays = %d\n", numOfWays);
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> M >> N;
    memset(cache, -1, sizeof(cache));
    for (int i = 0; i < M; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            int temp = 0;
            cin >> temp;
            Array[i][j] = temp;
        }
    }
    // Logic
    printf("%d\n", solve(0, 0));

    return 0;
}

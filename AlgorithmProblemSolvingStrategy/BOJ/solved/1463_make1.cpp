#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
using namespace std;

constexpr int MAX_LEN = 1000001;
int N =0;
int cache[MAX_LEN] = {0};

int makeOne(int n)
{
    if(n == 1)
        return 0;

    int & ret = cache[n];
    if(ret != -1)
        return ret;

    int case1 = INT_MAX;
    int case2 = INT_MAX;
    int case3 = INT_MAX;
    if( n %3 == 0)
        case1 = 1+makeOne(n/3);
    if( n %2 == 0)
        case2 = 1+makeOne(n/2);
    
    case3 = makeOne(n-1)+1;

    ret = min(min(case1, case2), case3);
    return ret;
}

// 2-> 1, 10-> 3
int main()
{
    memset(cache, -1, sizeof(cache));
    cin >> N;
    
    printf("%d\n", makeOne(N));

    return 0;
}
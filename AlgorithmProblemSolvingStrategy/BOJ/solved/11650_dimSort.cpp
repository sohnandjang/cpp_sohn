#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
using namespace std;

typedef struct{
    int x;
    int y;
}COORDINATE_T;

vector<COORDINATE_T> vec;

bool cmp(const COORDINATE_T& a, const COORDINATE_T& b)
{
    if(a.x < b.x)
        return true;
    else if(a.x == b.x)
        if(a.y <= b.y)
            return true;

    return false;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    int N;
    cin >> N;
    
    for(int i=0; i<N; ++i)
    {
        int _x = 0;
        int _y = 0;
        cin >> _x;
        cin >> _y;
        vec.push_back({_x,_y});
    }

    sort(vec.begin(), vec.end(), cmp);

    for(int i=0; i<vec.size(); ++i)
    {
        printf("%d %d\n", vec[i].x, vec[i].y);
    }
        
    return 0;
}

#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
using namespace std;

int N = 0;

int main()
{
    cin >> N;
    stack<int> myStack;
    for(int i=0; i<N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        if (tmp != 0)
        {
            myStack.push(tmp);
        }
        else
        {
            myStack.pop();
        }
    }
    int sum = 0;
    while(1)
    {
        if (myStack.empty())
            break;
        int tmp = myStack.top();
        sum += tmp;
        myStack.pop();
    }
    printf("%d\n", sum);
    return 0;
}
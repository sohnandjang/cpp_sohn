#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
int N = 0;
deque<int> myDeque;

int main()
{
    
    cin >> N;
    
    for(int i=0; i<N; ++i)
    {
        string opcode;
        cin >> opcode;
        if(opcode == "push_front")
        {
            int num = 0;
            cin >> num;
            myDeque.push_front(num);
        }
        else if(opcode == "push_back")
        {
            int num = 0;
            cin >> num;
            myDeque.push_back(num);
        }
        else if(opcode == "pop_front")
        {
            if(myDeque.empty())
            {
                printf("-1\n");
                continue;
            }
            int num = myDeque.front();
            printf("%d\n", num);
            myDeque.pop_front();    
        }
        else if(opcode == "pop_back")
        {
            if(myDeque.empty())
            {
                printf("-1\n");
                continue;
            }
            int num = myDeque.back();
            printf("%d\n", num);
            myDeque.pop_back();    
        }
        else if(opcode == "size")
        {
            printf("%d\n", myDeque.size());
        }
        else if(opcode == "empty")
        {
            printf("%d\n", myDeque.empty() ? 1 : 0);
        }
        else if(opcode == "front")
        {
            if(myDeque.empty())
            {
                printf("-1\n");
                continue;
            }
            printf("%d\n", myDeque.front());
        }
        else if(opcode == "back")
        {
            if(myDeque.empty())
            {
                printf("-1\n");
                continue;
            }
            printf("%d\n", myDeque.back());
        }

    }


    return 0;
}


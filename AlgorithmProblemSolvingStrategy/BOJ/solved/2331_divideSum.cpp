#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> arr;

int elementSum(int n)
{
    int sum = 0;
    while(n != 0)
    {
        sum += n % 10;
        n = n / 10;
    }
    return sum;
}

int solve(int N)
{
    int ret = 0;
    int i = 0;
    for( i=1; i<=N; ++i)
    {
        if( N == (elementSum(i)+i) )
            return i;
    }
    return 0;
}

int main()
{
    int N=0;
    scanf("%d", &N);
    int ret = solve(N);
    printf("%d\n", ret);
    return 0;
}
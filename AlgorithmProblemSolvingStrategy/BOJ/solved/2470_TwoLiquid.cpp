#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

int N = 0;
vector<int> Arr;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Arr.push_back(temp);
    }
}

void Solve()
{
    sort(Arr.begin(), Arr.end());
    int left = 0;
    int right = Arr.size()-1;

    int result_left = left;
    int result_right = right;
    int result_sum = Arr[right] + Arr[left];

    while(left < right)
    {
        int sum = Arr[left] + Arr[right];
        if (abs(sum) < abs(result_sum))
        {
            result_left = left;
            result_right = right;
            result_sum = sum;
        }

        if (sum < 0)
        {
            ++left;
        }
        else
        {
            --right;
        }
    }
    printf("%d %d\n", Arr[result_left], Arr[result_right]);
}

int main()
{
    Input();
    Solve();
    return 0;
}
#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

int N = 0;
vector<int> Arr;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
}

void FindPrimeNumber(int n)
{
    vector<bool> Candidates(n+1);
    for(int i=2; i<=n; ++i)
        Candidates[i] = true;

    for(int i=2; i*i<=n; ++i)
    {
        if(Candidates[i]==false)
            continue;

        for(int j=i+i; j<=n; j=j+i)
        {
            Candidates[j] = false;
        }
    }
    for(int i=2; i<=n; ++i)
        if(Candidates[i])
            Arr.push_back(i);

}

void Solve()
{
    FindPrimeNumber(N);
 #if 1
    int low = 0;
    int high = 0;
    int count = 0;
    if(Arr.size() == 0)
    {
        printf("0\n");
        return;
    }
    int sum = Arr[0];
    int maxArrIndex = Arr.size() - 1;

    while(low <= maxArrIndex && high <= maxArrIndex)
    {
        if(sum == N)
        {
            ++count;
            if(low == maxArrIndex)
                break;
            sum-=Arr[low++];
        }
        else if(sum > N)
        {   
            if(low == maxArrIndex)
                break;
            sum-=Arr[low++];
        }
        else if(sum < N)
        {
            if(high == maxArrIndex)
                break;
            sum+=Arr[++high];
        } 
    }
    printf("%d\n", count);
#else
    int result = 0, sum = 0, lo = 0, hi = 0;
    while (1) {
        if (sum > N) {
            sum -= Arr[lo++];
        } else if (hi == Arr.size()) {
            break;
        } else {
            sum += Arr[hi++];
        }
        if(sum == N) result++;
    }

    printf("%d\n", result);
#endif
}

int main()
{
    Input();
    Solve();
    return 0;
}
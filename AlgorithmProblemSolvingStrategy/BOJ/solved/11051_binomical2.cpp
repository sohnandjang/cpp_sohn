#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

constexpr int MOD = 10007;

int N = 0;
int K = 0;
constexpr int MAX_SIZE = 1001; 
int cache[MAX_SIZE][MAX_SIZE];

int binomial(int n, int k)
{
    if(k > n)
        return 0;
    if(k == 0)
        return 1;

    int &ret = cache[n][k];
    if(ret != -1)
        return ret;

    if(k == 1){
        ret = n % MOD;
        return ret;
    }
        
    ret = (binomial(n-1, k-1) + binomial(n-1, k)) % MOD;
    return ret;
}


int main()
{
    cin >> N;
    cin >> K;
    memset(cache, -1, sizeof(cache));
    printf("%d\n", binomial(N, K));
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
using namespace std;
int N = 0;
deque<int> myDeque;

int main()
{
    cin >> N;
    for(int i=1; i<=N; ++i)
        myDeque.push_back(i);

    while(1)
    {
        if (myDeque.size() == 1)
            break;
        
        myDeque.pop_front();
        int temp = myDeque.front();
        myDeque.pop_front();
        myDeque.push_back(temp);
    }

    int result = myDeque.front();
    printf("%d\n", result);
    return 0;
}
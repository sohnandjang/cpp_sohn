#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
int N = 0;
vector<int> WaitTime;
int sum = 0;

int main()
{
    cin >> N;

    for(int i=0; i<N; ++i)
    {
        int tmp;
        cin >> tmp;
        WaitTime.push_back(tmp);
    }

    sort(WaitTime.begin(), WaitTime.end());

    for(int i=0; i<N; ++i)
    {
        sum += WaitTime[i]*(N-i);
    }

    printf("%d\n", sum);
    return 0;
}
#include <iostream>
#include <cmath>
using namespace std;

#define MAX_N 1000001
int arr[MAX_N] = {0,};

int main()
{
    int M = 0;
    int N = 0;
    cin >> M >> N;
    for(int i=2; i<MAX_N; ++i){
        arr[i] = 1;
    }

    int sqrtNum = (int)sqrt((double)N);
    for(int i=2; i<=sqrtNum; i++){
        for(int j=2; i*j <= N; j++){
            arr[i*j] = 0;
        }
    }
    
    for(int i=M; i<=N; ++i){
        if(arr[i] == 1)
            printf("%d\n", i);
    }

    return 0;
}
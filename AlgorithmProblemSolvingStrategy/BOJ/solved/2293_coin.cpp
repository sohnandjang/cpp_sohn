#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

#if 1
constexpr int MAX_N = 102;
constexpr int MAX_K = 10002;

int N = 0;
int K = 0;
int cache[MAX_N][MAX_K] = {0};
vector<int> Coins;


int solve(int order, int cost)
{
    if (cost > K)
        return 0;
    int& ret = cache[order][cost];

    if (ret != -1)
        return ret;

    if (order == N)
    {
        if (cost == K)
            return 1;
        else
            return 0; 
    }

    ret = 0;
    int num = 0;
    if ( K - cost >= 0 )
        num = (K - cost) / Coins[order];

    for(int i=0; i<=num; ++i)
    {
        int next_order = order + 1;
        int next_cost = cost + Coins[order] * i;
        ret += solve(next_order, next_cost);
    }
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N >> K;
    memset(cache, -1, sizeof(cache));
    for (int i = 0; i < N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Coins.push_back(temp);
    }
    sort(Coins.begin(), Coins.end());
    // Logic
    printf("%d\n", solve(0,0));

    return 0;
}
#endif

#if 0
int N = 0;
int K = 0;
int coins[102] = {0};
int dp[10002] = {0};


int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N >> K;
    for(int i=0; i<N; ++i)
        cin >> coins[i];

    dp[0] = 1;
    for(int i=0; i<N; ++i)
    {
        for(int j=coins[i]; j<=K; ++j)
        {
            dp[j] = dp[j] + dp[j-coins[i]];
        }
    }
    printf("%d\n", dp[K]); 
}

#endif
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
long long sum = 0; //int -> long long 
int N = 0;
vector<long long > price;
vector<long long > station;

int main()
{
    cin >> N;

    for(int i=0; i<N-1; ++i)
    {
        long long  tmp = 0;
        cin >> tmp;
        price.push_back(tmp);
    }
    for(int i=0; i<N; ++i)
    {
        long long tmp = 0;
        cin >> tmp;
        station.push_back(tmp);
    }

    long long  prePrice = INT_MAX;
    for(int i=0; i<N-1; ++i)
    {
        long long  curPrice = station[i];
        if (prePrice < curPrice)
        {
            sum += prePrice * price[i];
        }
        else
        {
            sum += curPrice * price[i];
            prePrice = curPrice; // copy
        }
    }

    printf("%lld\n", sum);
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

long long N = 0;
long long C = 0;
long long MAX_NUM = 0;
long long MIN_NUM = LONG_MAX;
vector<long long> SRC;
vector<long long> SELECTED;

void input()
{
    cin >> N;
    cin >> C;
    for(long long i=0; i<N; ++i)
    {
        long long temp = 0;
        cin >> temp;
        SRC.push_back(temp);
        MAX_NUM = max(MAX_NUM, temp);
        MIN_NUM = min(MIN_NUM, temp);
    }
}

long long GetWifiNumber(const vector<long long>& in, long long len)
{
    long long ret = 1; // include start point
    long long previous = in[0];
    for(long long i=1; i<in.size(); ++i)
    {
        if(in[i]-previous >= len)
        {
            ++ret;
            previous = in[i];
        }
    }
    return ret;
}

void solve()
{
    sort(SRC.begin(), SRC.end());
    long long lo = 1;//MIN_NUM;
    long long hi = SRC[N-1] - SRC[0];//MAX_NUM;
    long long result = 0;
    while(lo <= hi)
    {
        long long mid = (long long)(lo + hi) / 2;
        long long _wifiNum = GetWifiNumber(SRC, mid);
        if( _wifiNum >= C)
        {
            lo = mid + 1;
            result = max(result, mid);
            continue;
        }
        else
        {
            hi = mid - 1;
        }
    }
    printf("%lld\n", result);
}
int main()
{
    input();
    solve();
    return 0;
}


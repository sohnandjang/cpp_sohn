#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
constexpr int MAX = 101;
int  MAP1[MAX][MAX] = {0};
int  MAP2[MAX][MAX] = {0};
int RESULT[MAX][MAX] = {0};
int N = 0;
int M = 0;
int K = 0;

int main()
{
    cin >> N;
    cin >> M;
    for(int i=0; i<N; ++i)
        for(int j=0; j<M; ++j)
            cin >> MAP1[i][j];

    cin >> M;
    cin >> K;
    for(int i=0; i<M; ++i)
        for(int j=0; j<K; ++j)
            cin >> MAP2[i][j];

    for(int i=0; i<N; ++i)
    {
        for(int j=0; j<M; ++j)
        {
            for(int k=0; k<K; ++k)
            {
                RESULT[i][k] += (MAP1[i][j] * MAP2[j][k]);
            }
        }
    }

    for(int i=0; i<N; ++i)
    {
        for(int j=0; j<K; ++j)
        {
            printf("%d ", RESULT[i][j]);
        }
        printf("\n");
    }
    return 0;

}


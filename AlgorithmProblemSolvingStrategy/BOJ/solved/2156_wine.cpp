#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
using namespace std;

int N = 0; //1~10000
int WINE[10001] = {0};
int cache[10002][3] = {0};
#if 0
6
6
10
13
9
8
1

33

6
100
400
2
1
4
200

704

#endif

/**
 * \ret possible case number
 */
int solve(int n, int numSeries)
{
    if(n >= N)
        return 0;
    
    //int ret = 0;
    int &ret = cache[n+1][numSeries];
    if(ret != -1)
        return ret;

    int case1 = 0;
    int case2 = 0;
    int case3 = 0;
    if(numSeries+1<=2 && n+1 < N)
        case1 = solve(n+1, numSeries+1)+WINE[n+1];
    if(n+2 < N)
        case2 = solve(n+2, 1)+WINE[n+2];
    if(n+3 < N)
        case3 = solve(n+3, 1)+WINE[n+3];
    
    ret = max(max(case1, case2), case3);
    return ret;
}

int main()
{
    memset(cache, -1, sizeof(cache));
    cin >> N;
    for(int i=0; i<N; ++i)
        cin >> WINE[i];

    printf("%d\n", solve(-1, 0));

    return 0;
}
#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

typedef struct{
    int weight;
    int height;
}Health;
vector<Health> arr;
vector<int> order;

int solve()
{
    for(int i=0; i!=arr.size(); ++i)
    {
        int num = 0;
        for(int j=0; j!=arr.size(); ++j)
        {
            if(arr[i].weight < arr[j].weight && arr[i].height < arr[j].height )
                ++num;
        }
        order.push_back(num+1);
    }
    return 0;
}

int main()
{
    int N=0;
    scanf("%d", &N);
    for(int i=0; i<N; ++i)
    {
        int weight=0, height=0;
        scanf("%d %d", &weight, &height);
        Health h = {weight, height};
        arr.push_back( h );
    }
    
    int ret = solve();
    for(int i=0; i<order.size(); ++i)
    {
        printf("%d ", order[i]);
    }
    return 0;
}
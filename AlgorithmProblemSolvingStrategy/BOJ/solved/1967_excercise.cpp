#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>
#include <algorithm>

using namespace std;

#define MAX_V 401
int Adj[MAX_V][MAX_V] = {0,};
int Distance[MAX_V][MAX_V] = {0,};
int V = 0;
int E = 0;


void Input()
{
    cin >> V >> E;
    
    for (int i=1; i<=V; ++i)
        for(int j=1; j<=V; ++j)
            Adj[i][j] = INT_MAX;

    for (int i = 0; i < E; ++i)
    {
        int s, e, w;
        cin >> s >> e >> w;
        Adj[s][e] = w;
    }
}

void Floyd()
{
    for(int i=1; i<=V; ++i)
    {
        for(int j=1; j<=V; ++j)
        {
            if(i == j)
                Distance[i][j] = 0;
            else 
                Distance[i][j] = Adj[i][j];
        }
    }

    for(int via=1; via<=V; ++via)
    {
        for(int from=1; from<=V; ++from)
        {
            for(int to=1; to<=V; ++to)
            {
                if(from == to)
                    continue;
                if(via == from || via == to)
                    continue;
                
                if(Distance[from][via]==INT_MAX || Distance[via][to]==INT_MAX)
                    continue;

                if(Distance[from][to] > Distance[from][via] + Distance[via][to])
                    Distance[from][to] = Distance[from][via] + Distance[via][to];
            }
        }
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    Input();
    Floyd();
    
    int shortCircleDistance = INT_MAX;
    for(int from=1; from<=V; ++from)
    {
        for(int to=1; to<=V; ++to)
        {
            if(from == to)
                continue;
            if(Distance[from][to]!=INT_MAX && Distance[to][from]!=INT_MAX)
                shortCircleDistance = min(shortCircleDistance, Distance[from][to]+Distance[to][from]);
        }
    }
    printf("%d\n", shortCircleDistance == INT_MAX ? -1 : shortCircleDistance);
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

int N = 0;
int MAP[3000][3000] = {0};
int numOfZeroSquare = 0;
int numOfOneSquare = 0;
int numOfMinusOneSquare = 0;

void Solve(int x, int y, int len)
{
    // base
    bool _AllZero = true;
    bool _AllOne = true;
    bool _AllMinusOne = true;
    for(int i=x; i<x+len; ++i)
    {
        for(int j=y; j<y+len; ++j)
        {
            if(MAP[i][j]!=0)
            {
                _AllZero = false;
            }
            if(MAP[i][j] != 1)
            {
                _AllOne = false;
            }
            if(MAP[i][j] != -1)
            {
                _AllMinusOne = false;
            }
        }
    }
    if(_AllZero)
    {
        ++numOfZeroSquare;
        return;
    }
    else if(_AllOne)
    {
        ++numOfOneSquare;
        return;
    } 
    else if(_AllMinusOne)
    {
        ++numOfMinusOneSquare;
        return;
    }
    else
    {
        Solve(x,y,len/3);
        Solve(x,y+len/3,len/3);
        Solve(x,y+len/3*2,len/3);
        Solve(x+len/3,y,len/3);
        Solve(x+len/3,y+len/3,len/3);
        Solve(x+len/3,y+len/3*2,len/3);
        Solve(x+len/3*2,y,len/3);
        Solve(x+len/3*2,y+len/3,len/3);
        Solve(x+len/3*2,y+len/3*2,len/3);
    }
}
int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        for(int j=0; j<N; ++j)
        {
            int temp;
            cin >> temp;
            MAP[i][j] = temp;
        }
    }

    Solve(0,0,N);
    printf("%d\n", numOfMinusOneSquare);
    printf("%d\n", numOfZeroSquare);
    printf("%d\n", numOfOneSquare);
    return 0;
}

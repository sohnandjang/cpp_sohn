#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
using namespace std;


int N = 0;
int pos = 0;

stack<int> myStack;
vector<char> myResult;

void myPush(int i)
{
    myStack.push(i);
    myResult.push_back('+');
}
void myPop()
{
    myStack.pop();
    myResult.push_back('-');
    ++pos;
}
int main()
{
    cin >> N;
    vector<int> target;
    for(int i=0; i<N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        target.push_back(tmp);
    }
    myStack.top();
    
    #if 1
    //for(int i=1; i<=N; ++i)
    int i=1;
    while(1)
    {  
        if(i > N)
            break;
        
        if (!myStack.empty() && (myStack.top() == target[pos]))
        {
            myPop();
            continue;
        }
        else 
        {   
            myPush(i);
            ++i;
        }
    }

    int size = myStack.size();

    for (int i = 0; i < size; ++i)
    {
        if (myStack.top() == target[pos])
        {
            myPop();
        }
    }

    if (myStack.empty())
    {
        // OK
        for(int i=0; i<myResult.size(); ++i)
        {
            printf("%c\n", myResult[i]);
        }
    }
    else
    {
        // NG
        printf("NO\n");
    }
    #endif
    return 0;
}
#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

int N = 0;
int X = 0;
vector<int> Arr;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Arr.push_back(temp);
    }
    cin >> X;
}

void Solve()
{
    sort(Arr.begin(), Arr.end());
    int left = 0;
    int right = Arr.size()-1;

    int count = 0;
    while(left!=right)
    {
        int sum = Arr[left] + Arr[right];
        if (sum == X)
        {
            ++count;
            --right;
        }
        else if (sum > X)
        {
            --right;
        }
        else
        {
            ++left;
        }
    }
    printf("%d\n", count);
}

int main()
{
    Input();
    Solve();
    return 0;
}
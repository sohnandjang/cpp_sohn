#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>
#include <algorithm>
#include <stdlib.h>
#include <string.h>

using namespace std;

#define MAX_V 101
constexpr int MAX_COST = INT_MAX;
int Adj[MAX_V][MAX_V] = {0,};
int N = 0;
int M = 0;

void Input()
{
    cin >> N >> M;
    
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=N; ++j)
        {
            if(i==j)
                Adj[i][j] = 0;
            else
                Adj[i][j] = MAX_COST;
        }
    }
    for (int i = 0; i < M; ++i)
    {
        int A, B, C;
        cin >> A >> B >> C;
        if(Adj[A][B] > C)
            Adj[A][B] = C;
    }
}

void Display(int(* _Distance)[MAX_V])
{
    //printf("-----------------\n");
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=N; ++j)
        {
            printf("%d ", _Distance[i][j] == MAX_COST ? 0 : _Distance[i][j]);
        }
        printf("\n");
    }
}
void Floyd()
{
    int _Distance[MAX_V][MAX_V] = {0,};
    
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=N; ++j)
        {
            _Distance[i][j] = Adj[i][j];
        }
    }
    
    for(int k=1; k<=N; ++k)
    {
        for(int i=1; i<=N; ++i)
        {
            for(int j=1; j<=N; ++j)
            {
                if(i == j)
                    continue;
                if(k == i || k == j)
                    continue;
                
                if(_Distance[i][k]==INT_MAX || _Distance[k][j] == INT_MAX)
                    continue;

                if(_Distance[i][j] > _Distance[i][k] + _Distance[k][j])
                    _Distance[i][j] = _Distance[i][k] + _Distance[k][j];
                
                //Display(_Distance);
            }
        }
    }
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=N; ++j)
        {
            printf("%d ", _Distance[i][j] == MAX_COST ? 0 : _Distance[i][j]);
        }
        printf("\n");
    }
    
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    Input();
    Floyd();
    
    return 0;
}
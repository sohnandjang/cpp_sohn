#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

int N = 0;
int K = 0;
deque<int> myQueue;
bool Visited[100'001] = {0,};
int Depth[100'001] = {0, };

void disp()
{
    printf("disp: ");
    for(auto it=myQueue.begin(); it!=myQueue.end(); ++it)
    {
        printf("%d >", *it);
    }
    printf("\n");
}

void BFS(int start)
{
    myQueue.push_back(start);
    Visited[start] = true;

    while(!myQueue.empty())
    {
        int front = myQueue.front();
        //disp();
        myQueue.pop_front();

        if(front == K)
        {
            //printf(" found");
            break;
        }

        int next = 0;
        next = front + 1;
        if(0<= next && next <= 100000 && !Visited[next])
        {
            myQueue.push_back(next);
            Visited[next] = true;
            Depth[next] = Depth[front] + 1;
        }
        next = front - 1;
        if(0<= next && next <= 100000 && !Visited[next])
        {
            myQueue.push_back(next);
            Visited[next] = true;
            Depth[next] = Depth[front] + 1;
        }
        next = front * 2;
        if(0<= next && next <= 100000 && !Visited[next])
        {
            myQueue.push_back(next);
            Visited[next] = true;
            Depth[next] = Depth[front] + 1;
        }
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N >> K;

    BFS(N);
    printf("%d\n", Depth[K]);  
    return 0;
}
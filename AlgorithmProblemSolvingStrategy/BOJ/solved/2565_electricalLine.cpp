#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
constexpr int MAX_NUM = 501;
int N = 0;
int A[MAX_NUM];
int B[MAX_NUM]; 

vector<int> vec;
vector<int> num;

int main()
{
    cin >> N;
    int _A = 0;
    int _B = 0;
    for(int i=0; i<N; ++i)
    {
        cin >> _A >> _B;
        B[_B] = _A;
    }
    for(int i=0; i<MAX_NUM; ++i)
    {
        if(B[i]==0)
            continue;
    
        vec.push_back(B[i]);
    }
    
    num.resize(vec.size());
    for(int i=0; i<vec.size(); ++i)
    {
        num[i] = 1;
    }

    for(int i=1; i<vec.size(); ++i)
    {
        for(int j=0; j<i; ++j)
        {
            if(vec[i] > vec[j])
            {
                num[i] = max(num[j] + 1, num[i]);
            }
        }
    }

    int maxNum = 0;
    for(int i=0; i<num.size(); ++i)
    {
        maxNum = max(maxNum, num[i]);
    }
    printf("%d\n", N-maxNum);
    return 0;
}
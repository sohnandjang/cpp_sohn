#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

constexpr int MAX_LEN = 100'001;

int N = 0;
int MAX_NUM = 0;
int MIN_NUM = INT_MAX;
//vector<int> SRC;
//priority_queue<int> SRC;
int SRC[MAX_LEN];
vector<int> SELECTED;

void solve(int n);

void input()
{
    cin >> N;

    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        solve(temp);
    }

}


void solve(int n)
{
    if (n != 0)
    {
        SRC.push(n);
    }
    else
    {
        if (SRC.empty())
            printf("0\n");
        else
        {
            printf("%d\n", SRC.top());
            SRC.pop();
        }
    }
    
}
int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    input();
    //solve();
    return 0;
}


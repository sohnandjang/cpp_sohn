#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

//6 2 10 3

int x[1000];
int y[1000];

int main()
{
    for(int i=0; i<3; ++i){
        int a=0; int b=0;
        cin >> a >> b;
        x[a]++;
        y[b]++;
    }
    int rx=0; int ry=0;
    for(int i=0; i<1000; ++i){
        if(x[i]==1)
            rx = i;
        if(y[i]==1)
            ry = i;
    }
    printf("%d %d\n", rx, ry);
    return 0;
}
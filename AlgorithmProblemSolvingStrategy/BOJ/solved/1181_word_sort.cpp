#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
#include <string>
using namespace std;

vector<string> vec;

bool cmp(const string& a, const string& b)
{
    if(a.size() < b.size())
        return true;
    else if(a.size()==b.size())
    {
        if(a < b)
            return true; 
    }
    return false;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    int N;
    cin >> N;
    
    for(int i=0; i<N; ++i)
    {
        string s="";
        cin >> s;
        vec.push_back(s);
    }

    sort(vec.begin(), vec.end(), cmp);

    for(int i=0; i<vec.size(); ++i)
    {
        if(i==0)
            printf("%s\n", vec[i].c_str());
        else if(vec[i-1] != vec[i])
            printf("%s\n", vec[i].c_str());
    }
        
    return 0;
}

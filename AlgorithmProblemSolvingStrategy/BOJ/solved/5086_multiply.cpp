#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

int N = 0;
int M = 0 ;
vector<int> factor;

int main()
{
    cin >> N >> M;

    int smallOne = min(N, M);
    int maxFactor = 1;
    for(int i=1; i<=smallOne; ++i)
    {
        if (N % i == 0 && M % i == 0)
            maxFactor = i;
    }

    int minMultiply = N * M / maxFactor;

    printf("%d\n", maxFactor);   
    printf("%d\n", minMultiply);   

    return 0;
}
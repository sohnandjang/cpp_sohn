#include <iostream>
using namespace std;

int solve(int N)
{
    if(N == 0 )
        return 0;
    else if(N == 1)
        return 1;
    return solve(N-2)+solve(N-1);
}

int main()
{
    int N;
    cin >> N;
    printf("%d\n", solve(N));
    return 0;
}
#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>

using namespace std;

typedef struct
{
    int end;
    int weight;
}VERTEX_T;
struct compare
{
    bool operator()(VERTEX_T left, VERTEX_T right)
    {
        return left.weight < right.weight;
    }
};

int numOfV = 0;
int numOfE = 0;
vector<pair<int,int>> Vertex[20001];
vector<int> ShortestPath; 
int startPoint = 0;
priority_queue<pair<int,int>> pq;

void Dijkstra(int start)
{
    ShortestPath[start] = 0;
    pq.push(make_pair(0, start)); 
    
    while(!pq.empty())
    {
        int cost = - pq.top().first;
        int curVertex = pq.top().second;
        pq.pop();

        if(cost > ShortestPath[curVertex])
            continue;

        for(int i=0; i<Vertex[curVertex].size(); ++i)
        {
            int neighbor = Vertex[curVertex][i].first;
            int neighborDistance = cost + Vertex[curVertex][i].second;

            if(ShortestPath[neighbor] > neighborDistance)
            {
                ShortestPath[neighbor] = neighborDistance;
                pq.push(make_pair(-neighborDistance, neighbor));
            }
        }    
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> numOfV >> numOfE;
    cin >> startPoint;

    ShortestPath.resize(numOfV + 1, INT_MAX);

    for(int i=0; i<numOfE; ++i)
    {
        int s;
        int e;
        int w;
        cin >> s >> e >> w;
        Vertex[s].push_back(make_pair(e, w));
    }

    Dijkstra(startPoint);
    for(int i=1; i<ShortestPath.size();++i)
    {
        if(ShortestPath[i]==INT_MAX)
            printf("INF\n");
        else
            printf("%d\n", ShortestPath[i]);
    }

    return 0;
}
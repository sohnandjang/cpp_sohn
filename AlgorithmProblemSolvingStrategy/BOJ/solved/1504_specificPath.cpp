#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>

using namespace std;
#define MAX_N 801

typedef struct
{
    int end;
    int weight;
}VERTEX_T;
struct compare
{
    bool operator()(VERTEX_T left, VERTEX_T right)
    {
        return left.weight < right.weight;
    }
};

int numOfV = 0;
int numOfE = 0;
vector<pair<int,int>> Vertex[MAX_N];
vector<int> ShortestPath[MAX_N]; 
int V1 = 0;
int V2 = 0;

void Dijkstra(int start)
{
    priority_queue<pair<int,int>> pq;
    ShortestPath[start][start] = 0;
    pq.push(make_pair(0, start)); 
    
    while(!pq.empty())
    {
        int cost = - pq.top().first;
        int curVertex = pq.top().second;
        pq.pop();

        if(cost > ShortestPath[start][curVertex])
            continue;

        for(int i=0; i<Vertex[curVertex].size(); ++i)
        {
            int neighbor = Vertex[curVertex][i].first;
            int neighborDistance = cost + Vertex[curVertex][i].second;

            if(ShortestPath[start][neighbor] > neighborDistance)
            {
                ShortestPath[start][neighbor] = neighborDistance;
                pq.push(make_pair(-neighborDistance, neighbor));
            }
        }    
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> numOfV >> numOfE;

    for(int i=0; i<numOfE; ++i)
    {
        int s;
        int e;
        int w;
        cin >> s >> e >> w;
        Vertex[s].push_back(make_pair(e, w));
        Vertex[e].push_back(make_pair(s, w));
    }
    cin >> V1 >> V2;

    ShortestPath[1].resize(numOfV + 1, INT_MAX);
    ShortestPath[V1].resize(numOfV + 1, INT_MAX);
    ShortestPath[V2].resize(numOfV + 1, INT_MAX);

    Dijkstra(1);
    Dijkstra(V1);
    Dijkstra(V2);

    int ret1 = ShortestPath[1][V1] + ShortestPath[V1][V2] + ShortestPath[V2][numOfV];
    int ret2 = ShortestPath[1][V2] + ShortestPath[V2][V1] + ShortestPath[V1][numOfV];
    int ret = min(ret1, ret2);
    if((ShortestPath[1][V1] == INT_MAX ||
        ShortestPath[V1][V2] == INT_MAX || 
        ShortestPath[V2][numOfV] == INT_MAX) &&
        (ShortestPath[1][V2] == INT_MAX ||
        ShortestPath[V2][V1] == INT_MAX || 
        ShortestPath[V1][numOfV] == INT_MAX) 
    )
        printf("-1\n");
    else
        printf("%d\n", ret);
    
    return 0;
}
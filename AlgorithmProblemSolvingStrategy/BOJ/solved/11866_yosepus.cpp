#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
int N = 0;
int K = 0;

vector<int> result;
deque<int> myQueue;

void Print(const deque<int>& q)
{
    for(auto it=q.begin(); it!=q.end(); ++it)
    {
        printf("%d\n", *it);
    }
}

int main()
{
    cin >> N;
    cin >> K;

    for(int i=1; i<=N; ++i)
    {
        myQueue.push_back(i);
    }

    deque<int>::iterator it = myQueue.begin();
    int order = 0; //-1;
    
#if 1    
    for(int i=0; i<N; ++i)
    {
        int size = myQueue.size();
        order = (order + K -1) % size;

        //printf("delete %d ", *(myQueue.begin()+order));
        result.push_back(*(myQueue.begin()+order));
        myQueue.erase(myQueue.begin()+order);
    }
#endif
    printf("<");
    for(int i=0; i<N; ++i)
    {
        if (i != N - 1)
            printf("%d, ", result[i]);
        else
            printf("%d", result[i]);
    }
    printf(">");
    return 0;
}
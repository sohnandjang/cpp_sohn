#include <iostream>
using namespace std;

int solve(int N)
{
    if(N == 0 || N == 1)
        return 1;
    return N*solve(N-1);
}

int main()
{
    int N;
    cin >> N;
    printf("%d\n", solve(N));
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

typedef struct 
{
    /* data */
    int row;
    int col;
}BOARD_T;

constexpr int MAX_N = 1001;
int M = 0; 
int N = 0;
int Depth[MAX_N][MAX_N] = {0,};
int maxDepth = 0;
int Board[MAX_N][MAX_N] = {0,};
bool Visited[MAX_N][MAX_N] = {0};
queue<BOARD_T> myQueue;

int dx[4] = {1, -1, 0, 0};
int dy[4] = {0, 0, 1, -1};

void display()
{
    printf("Display]\n");
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=M; ++j)
            printf("%d ", Board[i][j]);
        printf("\n");
    }
    printf("\n");
    printf("maxDepth = %d\n", maxDepth);
}

// return N, M 까지 이동하는 거리
void BFS(int row, int col)
{
    int count = 1;
    //myQueue.push({row,col});
    //Depth[row][col] = 0;
    //maxDepth = 0;

    while(!myQueue.empty())
    {
        BOARD_T front = myQueue.front();
        row = front.row;
        col = front.col;
        myQueue.pop();
        Visited[row][col] = true;
        //display();

        for(int i=0; i<4; ++i)
        {
            int next_row = row + dy[i];
            int next_col = col + dx[i];
            if( 1<= next_row && next_row <= N && 1<= next_col && next_col <= M)
            {
                if(Board[next_row][next_col] == -1)
                    continue;
                if(Board[next_row][next_col] == 0 && !Visited[next_row][next_col])
                //if(!Visited[next_row][next_col])
                {
                    myQueue.push({next_row, next_col});
                    Depth[next_row][next_col] = Depth[row][col] + 1;
                    maxDepth = max(maxDepth, Depth[next_row][next_col]);
                    Visited[next_row][next_col] = true;
                    Board[next_row][next_col] = 1;
                }
            }
        }
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> M >> N;
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=M; ++j)
        {
            cin >> Board[i][j]; 
        }
    }
    int row = 0;
    int col = 0;
    for(row = 1; row<=N; ++row)
    {
        for(col = 1; col<=M; ++col)
        {
            if(Board[row][col]==1)
            {
                //break;
                myQueue.push({row,col});
            }
        }
        //if(col!=M+1)
        //    break;
    }
    if(myQueue.empty())//row == N+1 && col == M+1) // not found
    {
        printf("-1\n");
        return 0;
    }
    else
        BFS(row, col);
    
    for(row=1; row<= N; ++row)
    {
        for(col=1; col<= M; ++col)
        {
            if(Board[row][col] == 0)
            {
                break;
            }
        }
        if(col != M+1)
            break;
    }
    if(!(row == N+1 && col == M+1)) // not found
        printf("-1\n");
    else
        printf("%d\n", maxDepth);

    return 0;
}
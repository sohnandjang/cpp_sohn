#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

constexpr int MAX_LEN = 1'001;

int N = 0;
int Arr[MAX_LEN] = {0,};
int DP[MAX_LEN] = {0,};
vector<int> LIS[MAX_LEN];
vector<int> Answer;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    for(int i=0; i<N; ++i)
        cin >> Arr[i];
}

#if 0
void Solve()
{
    int index_max = 0;
    LIS[0].push_back(Arr[0]);
    for(int i=1; i<N; ++i)
    {
        for(int j=0; j<i; ++j)
        {
            if(Arr[j] < Arr[i])
            {
                if(LIS[j].size() >= LIS[i].size())
                {
                    LIS[i].clear();
                    LIS[i] = LIS[j];
                    LIS[i].push_back(Arr[i]);

                    index_max = max(index_max, i);
                }
            }
        }
    }
    printf("%d\n", LIS[index_max].size());
    for(int i=0; i<LIS[index_max].size(); ++i)
        printf("%d ", LIS[index_max][i]);
    printf("\n");
}
#endif
void Solve()
{
    for(int i=0; i<N; ++i)
    {
        DP[i] = 1;
        LIS[i].push_back(Arr[i]);

        for(int j=0; j<i; ++j)
        {
            if(Arr[j] < Arr[i])
            {
                //if(DP[j]+1 > DP[i])
                if(DP[j] >= DP[i])
                {
                    LIS[i].clear();
                    LIS[i] = LIS[j];
                    DP[i] = DP[j]+1;
                    LIS[i].push_back(Arr[i]);
                }
            }
        }
        if(Answer.size() < LIS[i].size())
            Answer = LIS[i];
    }

    printf("%d\n", Answer.size());
    for(int i=0; i<Answer.size(); ++i)
        printf("%d ", Answer[i]);
    printf("\n");    
}


int main()
{
    Input();
    Solve();
    return 0;
}
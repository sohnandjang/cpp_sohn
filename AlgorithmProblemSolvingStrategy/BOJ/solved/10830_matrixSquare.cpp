#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
typedef long long ll;
typedef vector<vector<long long>> matrix;
long long N = 0;
long long B = 0;
matrix operator * (const matrix& a, const matrix& b);

matrix operator * (const matrix& a, const matrix& b)
{
    long long len = a.size();
    matrix res(len, vector<long long>(len));

    for(long long i=0; i<len; ++i)
        for(long long k=0; k<len; ++k)
        {
            for(long long j=0; j<len; ++j)
            {
                res[i][k] += a[i][j] * b[j][k];
                res[i][k] %= 1000;
            }
        }
                 
    return res;
}

#if 1
matrix power(const matrix& a, long long b)
{
    ll size = a.size();
    matrix ret(size, vector<ll>(size));
    for(ll i=0; i<size; ++i)
            ret[i][i] = 1;
    
    ret = ret * a;

    if(b == 1)
        return ret;
    
    matrix temp = power(a, b/2);
    if(b % 2 == 0)
    {
        ret = temp * temp;    
    }
    else
    {
        ret = a * temp * temp;
    }
    return ret;
}
#endif
 #if 0
matrix power(matrix a, ll n) {
    ll size = a.size();
    matrix res(size, vector<ll>(size));
    for (ll i = 0; i < size; i++) { // 단위 행렬
        res[i][i] = 1;
    }
    while (n > 0) {
        if (n % 2 == 1) {
            res = res * a;
        }
        n /= 2;
        a = a * a;
    }
    return res;
 
}
#endif
void display(const matrix& a)
{
    for(long long i=0; i<a.size(); ++i)
    {
        for(long long j=0; j<a.size(); ++j)
        {
            printf("%lld ", a[i][j]);
        }
        printf("\n");
    }
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    cin >> N;
    cin >> B;
    matrix A(N, vector<long long>(N));
    for(long long i=0; i<N; ++i)
        for(long long  j=0; j<N; ++j)
            cin >> A[i][j];

    display(power(A, B));
    return 0;

}


#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

int N = 0;
int MAX_NUM = 0;
int MIN_NUM = INT_MAX;
vector<int> SRC;
vector<int> SELECTED;

void input()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        SRC.push_back(temp);
        MAX_NUM = max(MAX_NUM, temp);
        MIN_NUM = min(MIN_NUM, temp);
    }
}


void solve()
{
    for(int i=0; i<N; ++i)
    {
        int num = SRC[i];
        if(SELECTED.empty())
        {
            SELECTED.push_back(num);
            continue;
        }
        if(SELECTED.back() < num)
        {
            SELECTED.push_back(num);
            continue;
        }
        if(SELECTED.back() == num)
        {
            continue;
        }
        //find upperbound
        std::vector<int>::iterator upper = std::lower_bound(SELECTED.begin(), SELECTED.end(), num);
        int pos = upper - SELECTED.begin();
        if (SELECTED[pos] > num)
        {
            SELECTED[pos] = num;
        }   
    }
    printf("%d\n", SELECTED.size());
}
int main()
{
    input();
    solve();
    return 0;
}


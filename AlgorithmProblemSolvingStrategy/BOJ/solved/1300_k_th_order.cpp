#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

long long N = 0;
long long K = 0;
long long MAX_NUM = 0;
long long MIN_NUM = LONG_MAX;
vector<long long> SRC(N+1, 0);
vector<long long> SELECTED;

void input()
{
    cin >> N;
    cin >> K;
#if 0
    for(long long i=1; i<=N; ++i)
    {
        long long temp = 0;
        cin >> temp;
        SRC[i] = temp;
        MAX_NUM = max(MAX_NUM, temp);
        MIN_NUM = min(MIN_NUM, temp);
    }
#endif
}

long long GetNumOfSameAndSmall(const vector<long long>& in, long long len)
{
    long long ret = 0;
    for(long long i=1; i<=N; ++i)
    {
        ret += min(N, len/i);
    }
    return ret;
}

void solve()
{
    sort(SRC.begin(), SRC.end());
    long long lo = 1;//MIN_NUM;
    long long hi = K;//MAX_NUM;
    long long result = LONG_MAX;
    while(lo <= hi)
    {
        long long mid = (long long)(lo + hi) / 2;
        long long _NumOfSameAndSmall = GetNumOfSameAndSmall(SRC, mid);
        if( _NumOfSameAndSmall < K)
        {
            lo = mid + 1;
            continue;
        }
        else
        {
            result = min(result, mid);
            hi = mid - 1;
        }
    }
    printf("%lld\n", result);
}
int main()
{
    input();
    solve();
    return 0;
}


#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;


int main()
{
    int x1=0; int y1=0; int r1=0;
    int x2=0; int y2=0; int r2=0;
    int C = 0;
    cin >> C;
    for(int i=0; i<C; ++i){
        cin >> x1 >> y1 >> r1;
        cin >> x2 >> y2 >> r2;
        int d_pow = (y2-y1)*(y2-y1) + (x2-x1)*(x2-x1);
        int r1_r2_pow = r1*r1 + r2*r2 + 2*r1*r2;
        int r1_r2_pow_min = r1*r1 + r2*r2 - 2*r1*r2;

        if(x1==x2 && y1==y2 && r1==r1)
            printf("-1\n");
        else if( d_pow > r1_r2_pow)
            printf("0\n");
        else if( d_pow == r1_r2_pow)
            printf("1\n");
        else if(d_pow == r1_r2_pow_min)
            printf("1\n");
        else if(d_pow < r1_r2_pow_min)
            printf("0\n");
        else{
            printf("2\n");
        }
    }
    return 0;
}
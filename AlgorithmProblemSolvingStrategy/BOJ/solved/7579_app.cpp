#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

int N = 0;
int M = 0;
int start = 0;
vector<int> Vertex;
vector<vector<int>> graph;
vector<int> DFS_Visited;
vector<int> BFS_Visited;
queue<int> q;

void DFS(int idx)
{
    if(idx == N+1)
        return;
    
    if(DFS_Visited[idx] == 1)
        return;
    
    DFS_Visited[idx] = 1;
    printf("%d ", idx);

    sort(graph[idx].begin(), graph[idx].end());
    for(int i=0; i<graph[idx].size(); ++i)
    {
        int next = graph[idx][i];
        DFS(next);
    }
    return;
}
void BFS(int idx)
{
    q.push(idx);
    while(!q.empty())
    {
        int front = q.front();
        q.pop();
        if(BFS_Visited[front])
            continue;

        BFS_Visited[front] = 1;
        printf("%d ", front);
        sort(graph[front].begin(), graph[front].end());
        for(int i=0; i<graph[front].size(); ++i)
        {
            int next = graph[front][i];
            q.push(next);
        }
    }
}
int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> N >> M >> start;
    graph.resize(N+1);
    for(int i=0; i<M; ++i)
    {
        int begin;
        cin >> begin;
        int end;
        cin >> end;
        graph[begin].push_back(end);
        graph[end].push_back(begin); // add
        Vertex.push_back(begin);
        Vertex.push_back(end);
    } 
    DFS_Visited.resize(N+1, 0);
    BFS_Visited.resize(N+1, 0);

    DFS(start);
    for(int i=0; i<Vertex.size(); ++i)
    {
        if(Vertex[i]!=start)
            DFS(Vertex[i]);
    }
    printf("\n");

    BFS(start);
    for(int i=0; i<Vertex.size(); ++i)
    {
        if(Vertex[i]!=start)
            BFS(Vertex[i]);
    }

    printf("\n");
    return 0;
}
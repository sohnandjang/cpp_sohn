#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;

int N = 0;

vector<int> A;
int maxSum = INT_MIN;

int main()
{
    cin >> N;
    for (int i = 0; i < N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        A.push_back(tmp);
    }

    int curSum = 0;
    for (int i = 0; i < N; ++i)
    {
        if (curSum + A[i] >= 0)
        {
            curSum = curSum + A[i];
            maxSum = max(curSum, maxSum);
        }
        else
        {
            curSum = 0;
            if(maxSum < 0)
                maxSum = max(maxSum, A[i]);
        }
    }
    printf("%d\n", maxSum);
   
    return 0;
}
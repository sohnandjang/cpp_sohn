#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
using namespace std;

int N = 0; // length <= 100
int mod = 1000000000;
int cache[101][10];

// 1 9
// 2 17

/**
 * \ret possible case number
 */
int solve(int curPos, int preVal)
{
    if(curPos == N)
        return 1;
    
    int& ret = cache[curPos][preVal];

    if(ret != -1)
       return ret;

    int numOfCase = 0;
    for(int i=0; i<=9; ++i)
    {
        if(curPos==0 && i==0)
            continue;

        if(curPos==0)
            numOfCase = ((numOfCase % mod) + (solve(curPos+1, i) % mod)) % mod;
        else if(abs(preVal - i) == 1)
            numOfCase = ((numOfCase % mod) + (solve(curPos+1, i) % mod)) % mod;
    }
    ret = numOfCase;
    return ret;
}
int main()
{
    memset(cache, -1, sizeof(cache));
    cin >> N;
    
    printf("%d\n", solve(0, 0));

    return 0;
}
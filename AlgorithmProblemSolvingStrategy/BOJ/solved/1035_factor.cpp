#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

int main()
{
    while(1)
    {
        int first = 0;
        int second = 0;
        cin >> first >> second;
        
        if (first == 0 && second == 0)
            break;

        string result;
        if (second % first == 0)
            result = "factor";
        else if (first % second == 0)
            result = "multiple";
        else
            result = "neither";
        
        printf("%s\n", result.c_str());
    }

    return 0;
}
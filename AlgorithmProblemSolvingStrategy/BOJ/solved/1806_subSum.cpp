#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

constexpr int MAX_LEN = 100'000;

int N = 0;
int S = 0;
vector<int> Arr;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N >> S;

    for (int i = 0; i < N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Arr.push_back(temp);
    }
}

void Solve()
{
    int low = 0;
    int high = 0;

    int sum = Arr[0];
    int length = MAX_LEN;

#if 1
    while(low<=high && high<N)
    {
        if(sum == S)
        {
            length = min(length, (high - low + 1));
            sum += Arr[++high];
            //break;
        } 
        else if(sum < S)
        {
            sum += Arr[++high];
        }
        else if(sum > S)
        {
            length = min(length, (high - low + 1));
            sum -= Arr[low++]; // 이 부분이 중요
        }
    }
#else
    while (low <= high && high < N)
    {
        if (sum < S)
            sum += Arr[++high];
        else if (sum == S)
        {
            length = min(length, (high - low + 1));
            sum += Arr[++high];
        }
        else if (sum > S)
        {
            length = min(length, (high - low + 1));
            sum -= Arr[low++];
        }
    }
#endif
    printf("%d\n", length == MAX_LEN ? 0 : length);
}

int main()
{
    Input();
    Solve();
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
constexpr int MAX_NUM = 1001;
int N = 0;
int M = 0;
vector<int> A;
vector<int> B;
int cache[MAX_NUM][MAX_NUM];

int lis(int n, int m)
{
    if(n >= N || m >= M)
        return 0;
    
    int& ret = cache[n][m];
    if(ret != -1)
        return ret; 

    if(A[n] == B[m])
    {
        ret = lis(n+1, m+1) + 1;
        return ret;
    }
    else
    {
        ret = max(lis(n+1, m), lis(n, m+1));
        return ret;
    }
}  

int main()
{
    string _A;
    string _B;
    cin >> _A >> _B;
    memset(cache, -1, sizeof(cache));
    N = _A.size();
    M = _B.size();
    for(int i=0; i<_A.size(); ++i)
    {
        A.push_back(_A[i]-'A');
    }
    for(int i=0; i<_B.size(); ++i)
    {
        B.push_back(_B[i]-'A');
    }
    
    printf("%d\n", lis(0,0));
    return 0;
}
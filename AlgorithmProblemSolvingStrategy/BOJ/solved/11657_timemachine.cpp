#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>
#include <algorithm>

using namespace std;

#define MAX_V 501
vector<pair<int, int>> Adj[MAX_V];
int StartPoint = 1;
int N = 0;
int M = 0;


void Input()
{
    cin >> N >> M;
    
    for (int i = 0; i < M; ++i)
    {
        int A, B, C;
        cin >> A >> B >> C;
        Adj[A].push_back(make_pair(B, C));
    }
}

pair<bool, vector<long long>> BellmanFord(int start)
{
    vector<long long> _Distance(MAX_V, LONG_MAX);
    bool minusLoopExist = false;

    _Distance[start] = 0;
    
    for(int i=1; i<=N; ++i) // edge: N-1 + 1
    {
        for(int j=1; j<=N; ++j) // vertex: N
        {
            for(auto pair_next: Adj[j])
            {
                int next = pair_next.first;
                long long next_cost = _Distance[j] + pair_next.second;
                if (_Distance[j]!=LONG_MAX && _Distance[next] > next_cost)
                {
                    _Distance[next] = next_cost;
                    if (i == N)
                        minusLoopExist = true;
                }
            }
        }
    }
    return make_pair(!minusLoopExist, _Distance);
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    Input();
    auto Result = BellmanFord(StartPoint);
    if (!Result.first)
        printf("-1\n");
    else
    {
        for(int i=2; i<=N; ++i)
            printf("%lld\n", Result.second[i] == LONG_MAX ? -1 : Result.second[i]);
    }
    
    return 0;
}














































#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
int N = 0;
int M = 0;
deque<int> myDeque;
int shiftCount = 0;

void PopFront()
{
    if(myDeque.empty())
        return;
    myDeque.pop_front();
}
void LeftShift()
{
    if(myDeque.empty())
        return;
    ++shiftCount;
    int num = myDeque.front();
    myDeque.pop_front();
    myDeque.push_back(num);
}
void RightShift()
{
    if(myDeque.empty())
        return;
    ++shiftCount;
    int num = myDeque.back();
    myDeque.pop_back();
    myDeque.push_front(num);
}
int FindOrder(int num)
{
    if(myDeque.empty())
        return -1;
    int order = 0;
    for(auto it=myDeque.begin(); it!=myDeque.end(); ++it)
    {
        if(num == *it)
            return order;
        ++order;
    }
    return -1;
}
bool IsNearFront(int num)
{
    int order = FindOrder(num);
    if(order <= myDeque.size()/2)
        return true;
    return false;
} 
bool IsFrontNumber(int num)
{
    if(num == myDeque.front())
        return true;
    return false;
}

int main()
{
    cin >> N;
    cin >> M;
    
    for(int i=0; i<N; ++i)
    {
        myDeque.push_back(i+1);
    }
    for(int i=0; i<M; ++i)
    {
        int num = 0;
        cin >> num;
        while(1)
        {
            if(IsFrontNumber(num))
            {
                PopFront();
                break;
            }
            if(IsNearFront(num))
            {
                LeftShift();
            }
            else
            {
                RightShift();
            }
        }
    }
        
    printf("%d\n", shiftCount);

    return 0;
}


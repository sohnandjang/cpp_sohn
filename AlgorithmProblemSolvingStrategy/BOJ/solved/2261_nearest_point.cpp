#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
typedef long long ll;
int N = 0;
typedef struct _Point{
    int x;
    int y;
} Point;
vector<Point> MAP;
int power(int a)
{
    return (a)*(a);
}
bool sortByX(const Point& a, const Point& b)
{
    if(a.x < b.x)
        return true;
    else
        return false;
}
bool sortByY(const Point& a, const Point& b)
{
    if(a.y < b.y)
        return true;
    else
        return false;
}
int dist(const Point& a, const Point& b)
{
    return (power(b.x - a.x) + power(b.y - a.y));
}

int brute(int start, int end)
{
    int minDiff = INT_MAX;    
    for(int i=start; i<end; ++i)
    {
        for(int j=i+1; j<=end; ++j)
        {
            minDiff = min(minDiff, dist(MAP[i], MAP[j]));
        }
    }    
    return minDiff;
}

int closest(int start, int end)
{
    if(end - start + 1 <= 3)
        return brute(start, end);
    
    int mid = (start + end) / 2;
    //int line = (it[n/2 - 1].x + it[n/2].x) / 2;
    int d = min(closest(start, mid), closest(mid+1, end));
     
    vector<Point> vmid;
    int n = end - start + 1;
    //vmid.reserve(n);
    int line = (MAP[mid+1].x + MAP[mid].x)/2;

    for (int i = start; i <= end; i++) {
        int t = line - MAP[i].x;
        if (t*t < d)
            vmid.push_back(MAP[i]);
    }
 
    sort(vmid.begin(), vmid.end(), sortByY);
     
    int mid_sz = vmid.size();
    for (int i = 0; i < mid_sz - 1; i++)
        for (int j = i + 1; j < mid_sz && power(vmid[j].y - vmid[i].y) < d; j++)
            d = min(d, dist(vmid[i], vmid[j]));
     
    return d;
}
int main()
{
    cin >> N;
    //MAP.reserve(N);
    for(int i=0; i<N; ++i)
    {
        int x = 0;
        int y = 0;
        cin >> x >> y;
        MAP.push_back({x, y});
    }
    //1. sort by X
    sort(MAP.begin(), MAP.end(), sortByX);
    printf("%d\n", closest(0, N-1));
    return 0;
}


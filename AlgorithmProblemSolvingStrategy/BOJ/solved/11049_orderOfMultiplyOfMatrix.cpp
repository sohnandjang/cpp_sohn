#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_LEN = 501;

typedef struct{
    int row;
    int col;
} MATRIX_T;

int N = 0;
int cache[MAX_LEN][MAX_LEN] = {0};
MATRIX_T Array[MAX_LEN] = {0};

/**
 * return minimum number of multiplying matrix
 */
int solve(int start, int end)
{
    int& ret = cache[start][end];

    if(ret != -1)
        return ret;

    if (start == end)
        return 0; 

    if (start + 1 == end)
    {
        ret = Array[start].row * Array[start].col * Array[end].col; //r x c(=r') x c'
        return ret;
    }

    int minVal = INT_MAX;
    for (int k = start; k < end; ++k)
    {
        int tmp = solve(start, k) + solve(k + 1, end);
        // start.row, k.col, k+1.row, end.col
        if (Array[k].col != Array[k+1].row)
            tmp = INT_MAX;
        else
        {
            tmp += Array[start].row * Array[k].col * Array[end].col;
        }
        
        minVal = min(minVal, tmp);
    }

    ret = minVal;
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N;
    memset(cache, -1, sizeof(cache));
    for (int i = 0; i < N; ++i)
    {
        int row = 0;
        int col = 0;
        cin >> row >> col;
        Array[i] = {row, col};
    }
    // Logic
    printf("%d\n", solve(0, N - 1));

    return 0;
}

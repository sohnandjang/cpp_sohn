#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
#include <string>
using namespace std;

vector<string> vec;
typedef struct{
    int order;
    int age;
    string name;
} PersonInfo_t;

vector<PersonInfo_t> PersonInfo;

bool cmp(const PersonInfo_t& a, const PersonInfo_t& b)
{
    if(a.age < b.age)
        return true;
    else if(a.age==b.age)
    {
        if(a.order < b.order)
            return true; 
    }
    return false;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    int N;
    cin >> N;
    
    for(int i=0; i<N; ++i)
    {
        int name = 0;
        string s="";
        cin >> name;
        cin >> s;
        PersonInfo.push_back({i,name,s});
    }

    sort(PersonInfo.begin(), PersonInfo.end(), cmp);

    for(int i=0; i<PersonInfo.size(); ++i)
    {
        printf("%d %s\n", PersonInfo[i].age, PersonInfo[i].name.c_str());
    }
        
    return 0;
}

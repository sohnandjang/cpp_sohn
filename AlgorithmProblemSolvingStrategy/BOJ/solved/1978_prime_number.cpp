#include <iostream>
#include <string.h>
#include <tgmath.h>
using namespace std;


bool isPrimeNumber(int num)
{
    bool divided = false;
    if(num == 1)
        return false;

    for(int i=2; i<num; ++i){
        if( num % i == 0 )
            return false;
    }
    return true;
}


int main()
{
    int C = 0;
    cin >> C;
    int num = 0;
    for(int i=0; i<C; ++i){
        int X = 0;
        cin >> X;
        if( isPrimeNumber(X) )
            num++;
    }
    printf("%d\n", num);

    return 0;
}
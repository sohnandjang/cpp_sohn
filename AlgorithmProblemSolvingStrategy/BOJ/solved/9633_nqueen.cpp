#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
#include <string>
#include <math.h>
using namespace std;

#define MAX_LEN 15

int N = 0; 
int cnt = 0;
vector<int> Arr(MAX_LEN,0);
vector<int> visited(MAX_LEN,0);

bool isPossible(int depth, int x)
{
    for(int i=depth-1; i>=0; --i)
    {
        if(Arr[i] == x)
            return false;
        
        if(abs(depth-i)==abs(x-Arr[i]))
            return false;
    }
    return true;
}

int nqueen(int depth)
{
    int ret = 0;
    if(depth == N)
    {
        /*for(int i=0; i<N; ++i)
        {
            printf("%d ", Arr[i]);
        }
        printf("\n");*/
        return 1;
    }

    for(int i=0; i<N; ++i)
    {
        Arr[depth] = i;
        if(isPossible(depth, i))
        {
            ret += nqueen(depth+1);
        }
    }
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    cin >> N;
    
    int ret = 0; 
    
    ret = nqueen(0);
      
    printf("%d\n", ret);

    return 0;
}

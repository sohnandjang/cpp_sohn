#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
using namespace std;

int N = 0;
int W = 0;

int maxValue = INT_MIN;
constexpr int MAX_N = 101;
constexpr int MAX_W = 100001;
int cache[MAX_N][MAX_W];

typedef struct{
    int weight;
    int value;
}BackPack_T;

vector<BackPack_T> bp;

int simpleBackpack(int order, int weight)
{
    if (order >= N || weight > W)
        return 0;
    
    int &ret = cache[order][weight];
    //printf("ret = %d\n", ret);
    if (ret != -1)
        return ret;
    int ret_case1 = 0;
    int ret_case2 = 0;
    //1. 포함
    int next_weight = weight + bp[order].weight;
    if (next_weight <= W)
    {
        ret_case1 = simpleBackpack(order+1, next_weight) + bp[order].value;   
    }
    
    //2. 미포함
    ret_case2 = simpleBackpack(order+1, weight);
    ret = max(ret_case1, ret_case2);
    return ret;
}

int main()
{
    cin >> N;
    cin >> W;
    for (int i = 0; i < N; ++i)
    {
        int _w = 0;
        int _v = 0;
        cin >> _w >> _v;
        bp.push_back({_w, _v});
    }
    memset(cache, -1, sizeof(cache));

    printf("%d\n", simpleBackpack(0,0));
   
    return 0;
}
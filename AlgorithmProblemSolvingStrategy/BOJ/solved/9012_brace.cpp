#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
using namespace std;

#define LEFT_BRACE '('
#define RIGHT_BRACE ')'

int N = 0;

bool isMatchBrace(string _line)
{
    int sum = 0;
    for(int i=0; i<_line.size(); ++i)
    {
        if (_line[i] == LEFT_BRACE)
            sum += 1;
        else if (_line[i] == RIGHT_BRACE)
            sum -= 1;
        if(sum < 0)
            break;
    }
    return (sum == 0 ? true : false);
}

int main()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        string _line;
        cin >> _line;
        printf("%s\n", isMatchBrace(_line) ? "YES" : "NO");
    }
    return 0;
}
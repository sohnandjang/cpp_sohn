#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
using namespace std;

constexpr int MAX_LEN = 501;
int N =0;
int MAP[MAX_LEN][MAX_LEN] = {0};
int cache[MAX_LEN][MAX_LEN] = {0};

#if 0
5
7
3 8
8 1 0
2 7 4 4
4 5 2 6 5

30 
#endif

int triangle(int row, int col)
{
    if(row == N){
        return 0;
    }
    
    int &ret = cache[row][col];
    if(ret != -1){
        return ret;
    }

    int cur = MAP[row][col];
    int sum1 = triangle(row+1, col)+cur;
    int sum2 = triangle(row+1, col+1)+cur;
    ret = max(sum1, sum2);
    //printf("r:%d, c:%d, ret:%d \n", row, col, ret);
    return ret;
}

int main()
{
    memset(cache, -1, sizeof(cache));
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        for(int j=0; j<=i; ++j)
        {
            cin >> MAP[i][j];
        }
    }

    printf("triangle(4,0,0)================\n");
    printf("%d\n", triangle(4,0));
    printf("triangle(3,0,0)================\n");
    printf("%d\n", triangle(3,0));
    printf("triangle(2,0,0)================\n");
    printf("%d\n", triangle(2,0));

    return 0;
}
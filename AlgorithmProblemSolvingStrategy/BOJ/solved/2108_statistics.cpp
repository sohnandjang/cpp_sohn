#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
using namespace std;

#define MAX_NUM 500001

/**
 *  1. 산술평균 (average)
 *  2. 중앙값 (midium)
 *  3. 최빈값 (manyShow)
 *  4. 범위 (minimum, maximum)
 */

int m_average = 0;
int m_medium = 0;
int m_range = 0;
int m_min = 4000;
int m_max = -4000;
int m_sum = 0;
int m_numberShowedALot = 0;

int arr[MAX_NUM] = {0};
map<int, int> mapHowOften; //number, howOften

bool cmp(pair<int, int>& a, pair<int, int>& b)
{
    if( a.second > b.second )
        return true;
    else if( a.second == b.second)
        if(a.first < b.first)
            return true;
    
    return false;
}



int main()
{
    int N;
    cin >> N;
    
    vector<int> vec(N);
    vector<int> vec2(8001, 0);
    bool isSecond = false;
    int maxNum = 0;
    int temp = 0;
    int mode = 0;

    for(int i=0; i<N; ++i)
    {
        
        cin >> temp;
        m_min = min(m_min, temp);
        m_max = max(m_max, temp);
        arr[i] = temp;
        m_sum += temp;
        
        auto it = mapHowOften.find(temp);
        if(it!=mapHowOften.end())
            ++mapHowOften[temp];
        else
            mapHowOften[temp] = 1;      
         
        vec[i] = temp;
        temp = (vec[i] <= 0) ? abs(vec[i]) : vec[i] + 4000;
        vec2[temp]++;
        if(vec2[temp] > maxNum)
            maxNum = vec2[temp];
    }
    
    m_average = round(m_sum / (double)N);

    sort(arr, arr+N);
    m_medium = arr[N/2];
    m_range = m_max - m_min;
 
    vector<pair<int, int>> A;
    for(auto& it : mapHowOften){
        A.push_back(it);
    }
    sort(A.begin(), A.end(), cmp);
    if(A.size() == 1)
        m_numberShowedALot = A[0].first;
    else if(A[0].second < A[1].second)
        m_numberShowedALot = A[0].first;
    if(A[0].second == A[1].second)
        m_numberShowedALot = A[1].first;
       
    
    for(int i = -4000; i < 4001; i++){
        temp = i <= 0 ? abs(i) : i+4000;
        if(vec2[temp] == maxNum){
            mode = i;
            if(isSecond)
                break;
            isSecond = true;
        }
    }

    printf("%d\n", m_average);
    printf("%d\n", m_medium);
    //cout<<vec[round(N/2)]<<'\n';
    //printf("%d\n", m_numberShowedALot);
    cout<<mode<<'\n';
    printf("%d\n", m_range);

    return 0;
}
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
using namespace std;

int N = 0;

int main()
{
    cin >> N;
    multiset<int> numbers;
    for(int i=0; i<N; ++i)
    {
        int temp;
        cin >> temp;
        numbers.insert(temp);
        
    }
    
    for(set<int>::iterator it=numbers.begin(); it!=numbers.end(); ++it)
    {
        printf("%d\n", *it);
    }
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;



int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    int N = 0;
    cin >> N; 

    priority_queue<int> maxHeap;
    priority_queue<int, vector<int>, greater<int>> minHeap;

    for(int i=0; i<N; ++i)
    {
        int num = 0;
        cin >> num;

        if (minHeap.size() < maxHeap.size())
            minHeap.push(num);
        else if (minHeap.size() == maxHeap.size())
            maxHeap.push(num);
        
        if (!maxHeap.empty() && !minHeap.empty())
        {
            if (maxHeap.top() > minHeap.top())
            {
                int maxHeapTop = maxHeap.top();
                int minHeapTop = minHeap.top();
                maxHeap.pop();
                maxHeap.push(minHeapTop);
                minHeap.pop();
                minHeap.push(maxHeapTop);
            }
        }
        printf("%d\n", maxHeap.top());
    }
    return 0;
}


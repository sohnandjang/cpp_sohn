/*6 8 10
25 52 60
5 12 13
0 0 0*/

#include <iostream>
#include <algorithm>
using namespace std;

//6 2 10 3

int main()
{
    unsigned int a=0; unsigned int b=0; unsigned int c=0;
    while(1){
        cin >> a >> b >> c;
        if( a == 0 && b == 0 && c == 0 )
            return 0;
        if( a*a + b*b == c*c )
            printf("right\n");
        else if ( a*a + c*c == b*b)
            printf("right\n");
        else if( b*b + c*c == a*a)
            printf("right\n");
        else
            printf("wrong\n");
    }
   
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
typedef long long ll;
typedef vector<vector<long long>> matrix;
long long N = 0;
long long MOD = 1'000'000'007; // 1e9 + 7; // 
matrix operator * (const matrix& a, const matrix& b);

matrix operator * (const matrix& a, const matrix& b)
{
    long long len = a.size();
    matrix res(len, vector<long long>(len));

    for(long long i=0; i<len; ++i)
    {
        for(long long k=0; k<len; ++k)
        {
            for(long long j=0; j<len; ++j)
            {
                res[i][k] += a[i][j] * b[j][k];
            }
            res[i][k] %= MOD;
        }
    }
                 
    return res;
}

#if 1
matrix power(const matrix& a, long long b)
{
    ll size = a.size();
    matrix ret(size, vector<ll>(size));
    for(ll i=0; i<size; ++i)
            ret[i][i] = 1;
    
    ret = ret * a;

    if(b == 1)
        return ret;
    
    matrix temp = power(a, b/2);
    if(b % 2 == 0)
    {
        ret = temp * temp;    
    }
    else
    {
        ret = a * temp * temp;
    }
    return ret;
}
#endif

void display(const matrix& a)
{
    for(long long i=0; i<a.size(); ++i)
    {
        for(long long j=0; j<a.size(); ++j)
        {
            printf("%lld ", a[i][j]);
        }
        printf("\n");
    }
}

long long GetPibonacci(long long n)
{
    if(n == 0)
        return 0;
    else if(n == 1)
        return 1;
    else
    {
        matrix A {{1,1}, {1,0}}; //over c++11, initialize  
        matrix Result {{0, 0}, {0,0}};
        matrix Base {{1, 0}, {0,0}};
        Result = power(A, N-1);
        Result = Result * Base;
        long long number = Result[0][0];
        return number;
    }
    
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    cin >> N;
      
    printf("%lld\n", GetPibonacci(N));
    return 0;

}


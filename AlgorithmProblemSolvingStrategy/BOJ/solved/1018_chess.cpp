#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

#define BLACK 0
#define WHITE 1
int N=0;
int M=0;
int arr[51][51];
int ref1[8][8]={
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK}
};
int ref2[8][8]={
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE},
    {WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK},
    {BLACK,WHITE,BLACK,WHITE,BLACK,WHITE,BLACK,WHITE}
};

int solve()
{
    int minDiff = 64;
    for(int i=0; i<N-8+1; ++i)
    {
        for(int j=0; j<M-8+1; ++j)
        {
            int diff1 = 0;
            int diff2 = 0;
            int diff = 0;
            for(int a=0; a<8; ++a)
            {
                for(int b=0; b<8; ++b)
                {
                    if( ref1[a][b] != arr[a+i][b+j] )
                        ++diff1;
                    if( ref2[a][b] != arr[a+i][b+j] )
                        ++diff2;    
                }
            }
            diff = min(diff1, diff2);
            minDiff = min(minDiff, diff);
        }
    }
    return minDiff;
}

int main()
{
    cin >> N >> M;
    for(int i=0; i<N; ++i)
    {
        for(int j=0; j<M; ++j)
        {
            char temp;
            cin >> temp;
            if(temp == 'B')
                arr[i][j] = BLACK;
            else if(temp == 'W')
                arr[i][j] = WHITE;
        }
    }
    
    int minDiff = solve();
    printf("%d\n", minDiff);
    return 0;
}
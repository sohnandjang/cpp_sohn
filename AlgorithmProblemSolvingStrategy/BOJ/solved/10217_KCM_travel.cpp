#include <iostream>
#include <vector>
#include <queue>
#include <functional>
#include <limits.h>
#include <algorithm>
#include <stdlib.h>
#include <string.h>

using namespace std;

constexpr int MAX_V = 101;
constexpr int MAX_COST = 10001;

typedef struct 
{
    int next;
    int cost;
    int time;
}Adj_T;

vector<Adj_T> Adj[MAX_V];
int DP[MAX_V][MAX_COST] = {0,}; //DP[vertex][cost] = time
int N = 0;
int M = 0;
int T = 0;
int K = 0;

struct Compare
{
    bool operator()(Adj_T a, Adj_T b)
    {
        return a.time > b.time;
    }
};


void Init()
{
    for(int i=1; i<MAX_V; ++i)
        Adj[i].clear();
    
    for(int i=1; i<MAX_V; ++i)
    {
        for(int j=1; j<MAX_COST; ++j)
            DP[i][j] = INT_MAX;
    }
}
void Input()
{
    Init();

    cin >> N >> M >> K;
    
    for (int i=0; i<K; ++i)
    {
        int u, v, c, d;
        cin >> u >> v >> c >> d;
        Adj[u].push_back({v, c, d});
    }
}

void Dijkstra(int startPoint)
{
    priority_queue<Adj_T, vector<Adj_T>, Compare> pq;
    DP[1][0] = 0; 
    pq.push({1, 0, 0});
    int shortestTime = INT_MAX;

    while(!pq.empty())
    {
        int cur = pq.top().next;
        int cost = pq.top().cost;
        int time = pq.top().time;
        pq.pop();
        
        if (DP[cur][cost] < time)
            continue;

        for(int i=0; i<Adj[cur].size(); ++i)
        {
            int next = Adj[cur][i].next;
            int nextCost = cost + Adj[cur][i].cost;
            int nextTime = time + Adj[cur][i].time;

            if (nextCost > M)
                continue;

            if (DP[next][nextCost] > nextTime)
            {
                DP[next][nextCost] = nextTime;
                pq.push({next, nextCost, nextTime});

                if(next == N)
                    shortestTime = min(shortestTime, nextTime);
            }   
        }
    }
    if(shortestTime == INT_MAX)
        printf("Poor KCM\n");
    else
        printf("%d\n", shortestTime);
}

int main()
{
    cin.tie(NULL);
    cout.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> T;
    for (int tc = 0; tc < T; ++tc)
    {
        Input();
        Dijkstra(1);
    }
    
    return 0;
}
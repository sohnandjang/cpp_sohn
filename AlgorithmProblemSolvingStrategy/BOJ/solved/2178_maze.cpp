#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

typedef struct 
{
    /* data */
    int row;
    int col;
}BOARD_T;

constexpr int MAX_N = 101;
int M = 0; 
int N = 0;
int Depth[MAX_N][MAX_N] = {0,};
int Board[MAX_N][MAX_N] = {0,};
bool Visited[MAX_N][MAX_N] = {0};
queue<BOARD_T> myQueue;

int dx[4] = {1, -1, 0, 0};
int dy[4] = {0, 0, 1, -1};

// return N, M 까지 이동하는 거리
void BFS(int row, int col)
{
    int count = 1;
    myQueue.push({row,col});
    Depth[row][col] = 1;

    while(!myQueue.empty())
    {
        BOARD_T front = myQueue.front();
        row = front.row;
        col = front.col;
        myQueue.pop();
        Visited[row][col] = true;
        if(row == N && col == M)
            break;

        for(int i=0; i<4; ++i)
        {
            int next_row = row + dy[i];
            int next_col = col + dx[i];
            if( 1<= next_row && next_row <= N && 1<= next_col && next_col <= M)
            {
                if(Board[next_row][next_col] && !Visited[next_row][next_col])
                {
                    myQueue.push({next_row, next_col});
                    Depth[next_row][next_col] = Depth[row][col] + 1;
                    Visited[next_row][next_col] = true;
                }
            }
        }
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> N >> M;
    for(int i=1; i<=N; ++i)
    {
        string line;
        cin >> line;
        for(int j=0; j<M; ++j)
        {
            Board[i][j+1] = line[j] - '0';
        }
    }
    BFS(1,1);
    printf("%d\n", Depth[N][M]);

    return 0;
}
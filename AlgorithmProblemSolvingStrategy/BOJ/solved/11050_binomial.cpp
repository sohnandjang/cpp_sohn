#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

int N = 0;
int K = 0;

int main()
{
    cin >> N;
    cin >> K;

    int high = 1;
    int low = 1;
    for(int i=0; i<K; ++i)
    {
        high = high * (N-i);
        low = low * (K-i);
    }

    printf("%d\n", high/low);

    return 0;
}
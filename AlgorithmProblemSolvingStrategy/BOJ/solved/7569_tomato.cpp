#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

typedef struct 
{
    /* data */
    int row;
    int col;
    int height;
}BOARD_T;

constexpr int MAX_N = 101;
int M = 0; 
int N = 0;
int H = 0;
int Depth[MAX_N][MAX_N][MAX_N] = {0,};
int maxDepth = 0;
int Board[MAX_N][MAX_N][MAX_N] = {0,};
bool Visited[MAX_N][MAX_N][MAX_N] = {0};
queue<BOARD_T> myQueue;

int dx[] = {1, -1, 0, 0, 0, 0};
int dy[] = {0, 0, 1, -1, 0, 0};
int dz[] = {0, 0, 0, 0, 1, -1};

void display()
{
    printf("Display]\n");
    for(int z=1; z<=H; ++z)
    {
        for(int i=1; i<=N; ++i)
        {
            for(int j=1; j<=M; ++j)
                printf("%d ", Board[i][j][z]);
            printf("\n");
        }
        printf("\n");
    }
    //printf("\n");
    printf("maxDepth = %d\n", maxDepth);
}

// return N, M 까지 이동하는 거리
void BFS(int row, int col, int height)
{
    int count = 1;

    while(!myQueue.empty())
    {
        BOARD_T front = myQueue.front();
        row = front.row;
        col = front.col;
        height = front.height;
        myQueue.pop();
        Visited[row][col][height] = true;
        //display();

        for(int i=0; i<6; ++i)
        {
            int next_row = row + dy[i];
            int next_col = col + dx[i];
            int next_height = height + dz[i];
            if( 1<= next_row && next_row <= N && 1<= next_col && next_col <= M && 1<= next_height && next_height <= H)
            {
                if(Board[next_row][next_col][next_height] == -1)
                    continue;

                if(Board[next_row][next_col][next_height] == 0 && !Visited[next_row][next_col][next_height])
                //if(!Visited[next_row][next_col])
                {
                    myQueue.push({next_row, next_col, next_height});
                    Depth[next_row][next_col][next_height] = Depth[row][col][height] + 1;
                    maxDepth = max(maxDepth, Depth[next_row][next_col][next_height]);
                    Visited[next_row][next_col][next_height] = true;
                    Board[next_row][next_col][next_height] = 1;
                }
            }
        }
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> M >> N >> H;
    for(int h=1; h<=H; ++h)
    {
        for(int i=1; i<=N; ++i)
        {
            for(int j=1; j<=M; ++j)
            {
                cin >> Board[i][j][h]; 
            }
        }
    }
    
    int row = 0;
    int col = 0;
    int height = 0;
    for(height = 1; height <=H; ++height)
    {
        for(row = 1; row<=N; ++row)
        {
            for(col = 1; col<=M; ++col)
            {
                if(Board[row][col][height]==1)
                {
                    //break;
                    myQueue.push({row,col,height});
                }
            }
            //if(col!=M+1)
            //    break;
        }
    }
    
    if(myQueue.empty())//row == N+1 && col == M+1) // not found
    {
        printf("-1\n");
        return 0;
    }
    else
        BFS(row, col, height);
    
    for(height = 1; height<=H; ++height)
    {
        for(row=1; row<= N; ++row)
        {
            for(col=1; col<= M; ++col)
            {
                if(Board[row][col][height] == 0)
                {
                    break;
                }
            }
            if(col != M+1)
                break;
        }
        if(row != N+1)
            break;
    }
    
    if(!(row == N+1 && col == M+1 && height == H+1)) // not found
        printf("-1\n");
    else
        printf("%d\n", maxDepth);

    return 0;
}
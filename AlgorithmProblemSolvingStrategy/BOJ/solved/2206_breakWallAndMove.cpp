#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;
constexpr int MAX_N = 1001;
int N = 0;
int M = 0;
typedef struct
{
    int row;
    int col;
    int brokenBefore;
} VISIT_T;

deque<VISIT_T> myQueue;
int Visited[MAX_N][MAX_N][2] = {
    0,
}; //row,col,breakWallbefore, return numOfMove
int Board[MAX_N][MAX_N] = {
    0,
};
int dx[] = {1, -1, 0, 0};
int dy[] = {0, 0, 1, -1};

bool isValid(int row, int col)
{
    if (1 <= row && row <= N && 1 <= col && col <= M)
        return true;
    else
        return false;
}

int BFS(int _row, int _col)
{
    myQueue.push_back({_row, _col, 0});
    Visited[_row][_col][0] = 1; //init
    Visited[_row][_col][1] = 1; //init

    int row = 0;
    int col = 0;
    int brokenBefore = 0;
    while (!myQueue.empty())
    {
        auto front = myQueue.front();
        //disp();
        myQueue.pop_front();
        row = front.row;
        col = front.col;
        brokenBefore = front.brokenBefore;
        //printf("%d,%d (%d) > ", row, col, brokenBefore);

        if (front.row == N && front.col == M)
        {
            //printf("[found]!!!\n");
            break;
        }

        for (int i = 0; i < 4; ++i)
        {
            int next_row = row + dy[i];
            int next_col = col + dx[i];
            if (!isValid(next_row, next_col))
                continue;
            // 1. we meet a wall / haven't broken / not visited
            if (Board[next_row][next_col] && !brokenBefore && !Visited[next_row][next_col][1])
            {
                myQueue.push_back({next_row, next_col, 1});
                Visited[next_row][next_col][1] = Visited[row][col][brokenBefore] + 1;
            }
            else if (Board[next_row][next_col] == 0 && !Visited[next_row][next_col][brokenBefore])
            {
                myQueue.push_back({next_row, next_col, brokenBefore});
                Visited[next_row][next_col][brokenBefore] = Visited[row][col][brokenBefore] + 1;
            }
        }
    }
    if(row == N && col == M)
        return Visited[row][col][brokenBefore];
    else
    {
        return -1;
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N >> M;
    for (int i = 1; i <= N; ++i)
    {
        string line;
        cin >> line;
        for (int j = 1; j <= M; ++j)
        {
            Board[i][j] = line[j - 1] - '0';
        }
    }

    printf("%d\n", BFS(1, 1));
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

int  N = 0;
int  K = 0;
int MOD = 1000000007;

long long factorial(long long n)
{
    /*if(n==1)
        return 1;
    else
    {
        return (n*(factorial(n-1)%MOD))%MOD;
    }*/
    long long ret = 1;
    for(int i=1; i<=n; ++i)
    {
        ret = (ret * i) % MOD;
    }
    return ret;
}
long long pow(long long a, long long b)
{
    if(b==0)
        return 1;
    if(b%2 > 0)
        return (a * (pow(a,b-1)%MOD))%MOD;

    long long temp = pow(a,b/2) %MOD;
    return (temp*temp)%MOD;
}


int main()
{
    cin >> N;
    cin >> K;

    long long A = factorial(N) % MOD;
    long long B1 = factorial(K) % MOD;
    long long B2 = factorial(N-K) % MOD;
    long long B = (B1 * B2) % MOD;
    long long B_POW = pow(B, MOD-2) % MOD;
    long long C = (A * B_POW) % MOD;
    printf("%lld\n", C); 
    return 0;

}


#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
using namespace std;

int N = 0;


#if 0

    push X: 정수 X를 스택에 넣는 연산이다.
    pop: 스택에서 가장 위에 있는 정수를 빼고, 그 수를 출력한다. 만약 스택에 들어있는 정수가 없는 경우에는 -1을 출력한다.
    size: 스택에 들어있는 정수의 개수를 출력한다.
    empty: 스택이 비어있으면 1, 아니면 0을 출력한다.
    top: 스택의 가장 위에 있는 정수를 출력한다. 만약 스택에 들어있는 정수가 없는 경우에는 -1을 출력한다.

#endif

class Stack
{
public:
    void Push(int a)
    {
        m_stack.push(a);
    }
    void Pop()
    {
        if(m_stack.empty())
        {
            printf("%d\n", -1);
            return;
        }
        int a = m_stack.top();
        printf("%d\n", a);
        m_stack.pop();
    }
    void Size()
    {
        printf("%d\n", m_stack.size());
    }
    void Empty()
    {
        if(m_stack.empty())
            printf("1\n");
        else
        {
            printf("0\n");
        }
    }
    void Top()
    {
        if(m_stack.empty())
        {
            printf("%d\n", -1);
            return;
        }
        int a = m_stack.top();
        printf("%d\n", a);
    }
private:    
    stack<int> m_stack;
};


int main()
{
    cin >> N;
    Stack myStack;
    for(int i=0; i<N; ++i)
    {
        string input;
        cin >> input;
        if(input == "push")
        {
            int tmp;
            cin >> tmp;
            myStack.Push(tmp);
        }
        else if(input == "pop")
        {
            myStack.Pop();
        }
        else if(input == "top")
        {
            myStack.Top();
        }
        else if(input == "empty")
        {
            myStack.Empty();
        }
        else if(input == "size")
        {
            myStack.Size();
        }

    }
    return 0;
}
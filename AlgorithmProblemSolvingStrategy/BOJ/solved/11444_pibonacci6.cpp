#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
typedef long long ll;
int N = 0;
vector<ll> H;

typedef enum{
    RIGHT = 0,
    LEFT,
    NO_DECISION
}EDIRECTION;

ll Solve(ll start, ll end)
{
    // base
    if (start == end)
    {
        return H[start];
    }
    ll mid = (start + end)/2;
    ll res1 = Solve(start, mid);
    ll res2 = Solve(mid+1, end);
    ll res3 = 0;
    
    ll height = H[mid];
    res3 = 1 * height;
    ll left = mid;
    ll right = mid;
    //printf("res1:res2:res3 = %lld, %lld, %lld\n", res1, res2, res3);
    
    while(1)
    {
        if(left <= start && end <= right )
            break;
        EDIRECTION _direct = NO_DECISION;
        if(left == start)
        {
            _direct = RIGHT;
            ++right;
        }
        else if(right == end)
        {
            _direct = LEFT;
            --left;
        }
        else
        {
            if(H[left-1] >= H[right+1])
            {
                _direct = LEFT;
                --left;
            }
            else
            {
                _direct = RIGHT;
                ++right;
            }
        }

        if(_direct == LEFT)
        {
            // move into left
            height = min(height, H[left]);
            ll _area = (right - left + 1) * height;
            res3 = max(res3, _area);
        }
        else
        {
            /* code */
            // move into right
            height = min(height, H[right]);
            ll _area = (right - left + 1) * height;
            res3 = max(res3, _area);
        }
        
    }
    
    ll _max = max(res1, res2);
    _max = max(_max, res3);
    //printf("max:res1:res2:res3 = %lld (%lld, %lld, %lld)\n", _max, res1, res2, res3);
    return _max;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    while(1)
    {
        cin >> N;
        if (N == 0)
            break;
        
        H.clear();
        for(int i=0; i<N; ++i)
        {
            ll temp;
            cin >> temp;
            H.push_back(temp);
        }

        printf("%lld\n", Solve(0, N-1));
    }
    
    return 0;

}


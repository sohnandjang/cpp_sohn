#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_LEN = 2'001;

typedef struct{
    int S;
    int E;
} QUESTION_T;

int N = 0;
int M = 0;
int cache[MAX_LEN][MAX_LEN] = {0};
int Array[MAX_LEN] = {0};
/**
 * return if it is pelindrome
 */
bool solve(int start, int end)
{
    int& ret = cache[start][end];

    if (ret != -1)
        return ret;

    if (start == end)
        return true;

    if (Array[start] == Array[end])
    {
        if (start + 1 == end)
        {
            ret = true;
            return ret;
        }
        else
        {
            ret = solve(start+1, end-1);
            return ret;
        }
    }
    else
    {
        ret = false;
        return ret;
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N;
    memset(cache, -1, sizeof(cache));
    for (int i = 1; i <= N; ++i)
    {
        cin >> Array[i];
    }
    // Logic
    cin >> M;
    for (int i = 0; i < M; ++i)
    {
        int start = 0;
        int end = 0;
        cin >> start;
        cin >> end;
        printf("%d\n", solve(start, end));
    }

    return 0;
}

#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_LEN = 501;

int N = 0;

/**
 * return minimum cost when merging two files
 */
int solve(int start, int end)
{
    int& ret = cache[start][end];

    if(ret != -1)
        return ret;

    if (start == end)
        return 0; //fileSize[start];

    if (start + 1 == end)
    {
        ret = (fileSize[start] + fileSize[end]);
        return ret;
    }

    int minVal = INT_MAX;
    for (int k = start; k < end; ++k)
    {
        int tmp = solve(start, k) + solve(k + 1, end) + sumArray[end] - sumArray[start - 1];
        //if (start != 0)
        //    tmp - sumArray[start];
        minVal = min(minVal, tmp);
    }

    ret = minVal;
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    
    cin >> N;
    for (int i = 0; i < N; ++i)
    {
        cin >> K;
        fileSize.clear();
        sumArray.clear();
        fileSize.push_back(0); // to change index into 1 not 0
        sumArray.push_back(0);
        memset(cache, -1, sizeof(cache));
        int sum = 0;
        for (int j = 0; j < K; ++j)
        {
            int tmp;
            cin >> tmp;
            fileSize.push_back(tmp);
            sum += tmp;
            sumArray.push_back(sum);
        }
        // Logic
        printf("%d\n", solve(1, K));
    }

    return 0;
}

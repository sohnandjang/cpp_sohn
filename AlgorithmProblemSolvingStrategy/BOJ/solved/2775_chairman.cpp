#include <iostream>
#include <string.h>
using namespace std;

int cache[15][15] = {0,};

int sol(int k, int n)
{
    //if(cache[k][n] != -1)
    //    return cache[k][n];
    if(k == 0)
        return n;
    else if(n == 1)
        return 1;
    else
        return sol(k, n-1)+sol(k-1, n);

}


int main()
{
    memset(cache, -1, sizeof(cache));
//    for(int i=1; i<14; ++i)
//        cache[0][i] = i;

    int C = 0;
    cin >> C;
    for(int i=0; i<C; ++i){
        int K=0;
        int N=0;
        cin >> K >> N;
        printf("%d\n", sol(K, N));
    }


    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <map>
using namespace std;

int N = 0;

int main()
{
    cin >> N;

    int num = N / 5;
    if((N / 125) > 0 )
        num += (N / 125);
    if((N / 25) > 0)
        num += (N / 25);

    printf("%d\n", num);

    return 0;
}
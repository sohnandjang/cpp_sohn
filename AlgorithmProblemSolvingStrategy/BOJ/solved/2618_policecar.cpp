#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
using namespace std;

constexpr int MAX_LEN = 1'001;
int N = 0;
int W = 0;

typedef struct 
{
    int x;
    int y;
}COORD;


int DP[MAX_LEN][MAX_LEN] = {0};
COORD Event[MAX_LEN];

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    cin >> W;

    for(int i=1; i<=W; ++i)
    {
        int x, y;
        cin >> x >> y;
        //Event[i] = {x, y};
        Event[i].x = x;
        Event[i].y = y;
    }
    memset(DP, -1, sizeof(DP));
}

int CalcDistance(COORD a, COORD b)
{
    return (abs(a.x-b.x)+abs(a.y-b.y));
}

int Total_Distance(int Police1, int Police2)
{
    if(Police1 == W || Police2 == W)
        return 0;

    int& ret = DP[Police1][Police2];
    if(ret != -1)
        return ret;
    
    int next_event = max(Police1, Police2) + 1;
    int distance1 = 0;
    int distance2 = 0;
    if(Police1 == 0)
        distance1 = CalcDistance({1,1}, Event[next_event]);
    else
        distance1 = CalcDistance(Event[Police1], Event[next_event]);
    if(Police2 == 0)
        distance2 = CalcDistance({N,N}, Event[next_event]);
    else
        distance2 = CalcDistance(Event[Police2], Event[next_event]);
    
    int result1 = Total_Distance(next_event, Police2) + distance1;
    int result2 = Total_Distance(Police1, next_event) + distance2;
    ret = min(result1, result2); // 최소!!!
    return ret;
}
void Route(int Police1, int Police2)
{
    if(Police1 == W || Police2 == W)
        return;
    
    int next_event = max(Police1, Police2) + 1;
    int distance1 = 0;
    int distance2 = 0;
    if(Police1 == 0)
        distance1 = CalcDistance({1,1}, Event[next_event]);
    else
        distance1 = CalcDistance(Event[Police1], Event[next_event]);
    if(Police2 == 0)
        distance2 = CalcDistance({N,N}, Event[next_event]);
    else
        distance2 = CalcDistance(Event[Police2], Event[next_event]);
    
    int result1 = DP[next_event][Police2] + distance1;
    int result2 = DP[Police1][next_event] + distance2;
    if(result1 < result2)
    {
        printf("%d\n", 1);
        Route(next_event, Police2);
    }
    else
    {
        printf("%d\n", 2);
        Route(Police1, next_event);
    }
}

void Solve()
{
    printf("%d\n", Total_Distance(0, 0));
    Route(0, 0);
}


int main()
{
    Input();
    Solve();
    return 0;
}
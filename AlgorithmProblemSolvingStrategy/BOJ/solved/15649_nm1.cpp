#include <iostream>
#include <algorithm>
#include <bits/stdc++.h> // sort
#include <vector>
#include <map>
#include <string>
using namespace std;

vector<int> Arr;
int N = 0; // 1~N
int M = 0; // select the number of M

void display(const vector<int>& Arr)
{
    for(int i=0; i<Arr.size(); ++i)
    {
        printf("%d ", Arr[i]);
    }
    printf("\n");
}
bool isAleadyUsed(vector<int>& arr, int i)
{
    int j = 0;
    vector<int>::iterator it;
    for(it = arr.begin(); it!=arr.end(); ++it)
    {
        if(i == *it)
            break;
    }
    if( it == arr.end())
        return false;
    
    return true;
}
/** 
 * printNM
 * prev 
 */
void printNM(vector<int>& Arr, int numToPrint, int prev)
{
    if(numToPrint <= 0)
    {
        display(Arr);
        return;
    }
    for(int i=1; i<=N; ++i)
    {
        if(i < prev)
            continue;

        //if(isAleadyUsed(Arr, i))
        //    continue;        

        Arr.push_back(i);
        printNM(Arr, numToPrint-1, i);
        Arr.pop_back();
    }
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    cin >> N;
    cin >> M;
    printNM(Arr, M, 0);
        
    return 0;
}

#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_N = 101;
constexpr int MAX_COSTS = 10001;

int N = 0;
int M = 0;
vector<int> Memory;
vector<int> Costs;
int maxCosts = 0;
int dp[MAX_COSTS] = {0};

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> N >> M;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Memory.push_back(temp);
    }
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Costs.push_back(temp);
        maxCosts += temp;
    }

    for(int i=0; i<N; ++i)
    {
        for(int j=maxCosts; j>=Costs[i]; --j)
        {
            dp[j] = max(dp[j], dp[j-Costs[i]] + Memory[i]);
        }
    }
    int i=0;
    for(i=0; i<maxCosts, dp[i]<M; ++i);
    printf("%d\n", i);
#if 0
    for(int i=0; i<maxCosts; ++i)
    {
        if(dp[i]>=M)
        {
            printf("%d\n", i);
            break;
        }
    }
#endif
    return 0;
}

#if 0
int N = 0;
int M = 0;
vector<int> Costs;
vector<int> Memory;
int cache[MAX_N][MAX_COSTS] = {0};

// return memory
int maxMemory(int idx, int totalCost)
{
    if(idx >= N)
    {
        return 0;
    }

    int& result = cache[idx][totalCost]; 
    if(result != -1)
        return result;

    // not choose
    result = maxMemory(idx+1, totalCost);
    // choose
    if(Costs[idx] <= totalCost)
    {
        result = max(result, maxMemory(idx+1, totalCost - Costs[idx]) + Memory[idx]);
    }
    return result;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    memset(cache, -1, sizeof(cache));

    cin >> N >> M; 
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Memory.push_back(temp);
    }
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Costs.push_back(temp);
    }
    int totalCost = 0;
    while(1)
    {
        if(maxMemory(0, totalCost) >= M)
        {
            printf("%d\n", totalCost);
            break;
        }
        ++totalCost;
    }
    return 0;
}
#endif
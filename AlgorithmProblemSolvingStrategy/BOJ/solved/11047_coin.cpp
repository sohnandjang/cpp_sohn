#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
int N = 0;
int K = 0;

vector<int> coin;
int minNum = 0;

int main()
{
    cin >> N;
    cin >> K;

    for(int i=0; i<N; ++i)
    {
        int tmp;
        cin >> tmp;
        coin.push_back(tmp);
    }
    
    for(int i=coin.size()-1; i>=0; --i)
    {
        if (K / coin[i] > 0)
        {
            minNum += K / coin[i];
            K = K % coin[i];    
        }
    }

    printf("%d\n", minNum);
    return 0;
}
#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

constexpr int MAX_LEN = 1'000'001;

int N = 0;
int Arr[MAX_LEN] = {0,};
int Index[MAX_LEN] = {0,};
vector<int> LIS;
vector<int> Answer;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    for(int i=0; i<N; ++i)
        cin >> Arr[i];
}

void Solve()
{
    int count = 0;
    for(int i=0; i<N; ++i)
    {
        if (LIS.empty() || LIS[LIS.size()-1] < Arr[i])
        {
            LIS.push_back(Arr[i]);
            Index[i] = count;
            ++count;
            //printf("push: %d (Index:%d)\n", Arr[i], count);
        }
        else
        {
            int pos = lower_bound(LIS.begin(), LIS.end(), Arr[i]) - LIS.begin();
            LIS[pos] = Arr[i];
            Index[i] = pos; // error fix
            //printf("change: %d (Index:%d)\n", Arr[i], Index[pos]);
        }
    }
    printf("%d\n", LIS.size());
#if 0
    printf("LIS: ");
    for(int i=0; i<LIS.size(); ++i)
    {
        printf("%d ", LIS[i]);
    }
    printf("\n");
    printf("Index: ");
    for(int i=0; i<N; ++i)
    {
        printf("%d ", Index[i]);
    }
    printf("\n");
#endif

    int indexToFind = LIS.size()-1;
    //printf("indexToFind = %d\n", indexToFind);
    for(int i=N-1; i>=0; --i)
    {
        //if(indexToFind < 0)
        //    break;
        //printf("i=%d, Index[%d]=%d, indexToFind = %d\n", i, i, Index[i], indexToFind);
        if(Index[i]==indexToFind)
        {
            //printf("%d ", LIS[Index[i]]);
            //printf("%d (%d<-i:%d)", LIS[Index[i]], Index[i], i);
            Answer.push_back(Arr[i]);
            --indexToFind;
        }
    }
    //printf("Answer: ");
    //for(int i=0; i<N; ++i)
    //{
    //    printf("%d ", Answer[i]);
    //}
    //printf("\n");
    //sort(Answer.begin(), Answer.end());
    //printf("Answer.size() = %d\n", Answer.size());
    for(int i=Answer.size()-1; i>=0; --i)
        printf("%d ", Answer[i]);
    printf("\n");
}

int main()
{
    Input();
    Solve();
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

int N = 0;
int firstNum = 0;
vector<int> num;

int GCD(int a, int b)
{
    if(a == 0)
        return b;
    if(b == 0)
        return a;
    
    if(a > b)
        return GCD(a%b, b);
    else
        return GCD(a, b%a);
}

int main()
{
    cin >> N;
    cin >> firstNum;

    for(int i=1; i<N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        int _gcd = GCD(firstNum, tmp);
        printf("%d/%d\n", firstNum/_gcd, tmp/_gcd);
    }

    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
using namespace std;

typedef struct 
{
    int index;
    int num;
}MyData;

int sortFunc(MyData A, MyData B)
{
    if(A.index < B.index)
        return true;
    else
        return false; 
}

int N = 0;
int main()
{
    cin >> N;

    vector<MyData> input;

    for(int i=0; i<N; ++i)
    {
        int tmp;
        cin >> tmp;
        input.push_back({i, tmp});
    }

    stack<MyData> myStack;
    vector<MyData> result;

    for(int i=0; i<input.size(); ++i)
    {
        if(myStack.empty())
            myStack.push(input[i]);
        else
        {
            if(myStack.top().num >= input[i].num)
            {          
                myStack.push(input[i]);          
            }
            else
            {
                MyData temp;
                while(!myStack.empty())
                {
                    if((myStack.top().num < input[i].num))
                    {
                        temp = myStack.top();
                        myStack.pop();
                        result.push_back({temp.index, input[i].num});
                    }
                    else
                    {
                        break;
                    }
                    
                }
                myStack.push(input[i]);
            }
        }
    }
    while(1)
    {
        if(myStack.empty())
            break;
        
        MyData temp = myStack.top();
        myStack.pop();
        result.push_back({temp.index, -1});
    }
    //result.push_back({input.size(), -1});
    sort(result.begin(), result.end(), sortFunc);
    for(int i=0; i<result.size(); ++i)
    {
        printf("%d ", result[i].num);
    }
    printf("\n");

    return 0;
}
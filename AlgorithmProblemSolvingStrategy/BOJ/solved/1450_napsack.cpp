#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
using namespace std;

int N = 0;
int C = 0;
vector<int> Arr;
vector<int> LeftSum;
vector<int> RightSum;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N >> C;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Arr.push_back(temp);
    }
}
void dfs(int start, int end, int sum, vector<int>& vec)
{
    if(sum > C)
        return;
    
    if(start >= end)
    {
        if(sum != 0)
        {
            vec.push_back(sum);
        }
        return;
    }

    dfs(start+1, end, sum, vec);
    dfs(start+1, end, sum+Arr[start], vec);
}
void Solve()
{
    dfs(0, N/2, 0, LeftSum);
    dfs(N/2, N, 0, RightSum);
    int ans = 0;
    ans = LeftSum.size() + RightSum.size();

    sort(RightSum.begin(), RightSum.end());
    int pos = 0;
    for(int i=0; i<LeftSum.size(); ++i)
    {
        auto upper = upper_bound(RightSum.begin(), RightSum.end(), C-LeftSum[i]);
        pos = std::distance(RightSum.begin(), upper);
        //int pos = upper_bound(RightSum.begin(), RightSum.end(), C-LeftSum[i]) - RightSum.begin();
        //printf("pos:%d, LeftSum:%d, C-LeftSum:%d\n", pos, LeftSum[i], C-LeftSum[i]);
        ans+=pos;
    }
    ans += 1;
    printf("%d\n", ans);
}

int main()
{
    Input();
    Solve();
    return 0;
}
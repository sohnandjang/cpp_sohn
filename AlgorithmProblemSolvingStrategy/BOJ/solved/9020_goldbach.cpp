#include <iostream>
#include <cmath>
#include <vector>
#include <utility>  
using namespace std;

#define MAX_N 10001
int arr[MAX_N] = {0,};
int N = 0;

void eratostenes()
{
    for(int i=2; i<MAX_N; ++i){
        arr[i] = 1;
    }

    int sqrtNum = (int)sqrt((double)N);
    for(int i=2; i<=sqrtNum; i++){
        for(int j=2; i*j <= N; j++){
            arr[i*j] = 0;
        }
    }
}

pair<int,int> goldbach(int n)
{
    vector<int> primeNum;
    for(int i=0; i<=n; ++i){
        if(arr[i] == 1 )
            primeNum.push_back(i);
    }

    pair<int, int> res;
    for(int i=0; i<primeNum.size(); ++i){
        if( n == (primeNum[i]*2)){
            res = res = make_pair( primeNum[i], primeNum[i]);
            return res;
        }
    }
    
    int diff = 987654321;
    for(int i=0; i<primeNum.size()-1; ++i){
        for(int j=i+1; j<primeNum.size(); ++j){
            if( n == (primeNum[i] + primeNum[j]) ){
                if( diff > primeNum[j]-primeNum[i] ){
                    diff = primeNum[j] - primeNum[i];
                    res = make_pair( primeNum[i], primeNum[j]);
                }
            }
        }
    }
    return res;
}

int main()
{

    int C = 0;
    cin >> C;
    for(int i=0; i<C; ++i){
        cin >> N;
        eratostenes();
        auto res = goldbach(N);
        printf("%d %d\n", res.first, res.second);
    }
   return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_WEIGHT = 40'002;
constexpr int MAX_NUM = 32;

int N = 0;
int M = 0;
int cache[MAX_NUM][MAX_WEIGHT] = {0};
int Array[MAX_NUM] = {0};


void solve(int order, int weight)
{
    int& ret = cache[order][weight];

    if (ret != -1)
        return;

    if (order == N+1)
        return;

    ret = 1;
    //1. pick up nothing
    solve(order + 1, weight);
    //2. pick up add
    solve(order + 1, weight + Array[order]);
    //3. pick up sub
    solve(order + 1, abs(weight - Array[order]));
    return;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);

    cin >> N;
    memset(cache, -1, sizeof(cache));
    for (int i = 0; i < N; ++i)
    {
        int temp = 0;
        cin >> temp;
        Array[i] = temp;
    }
    // Logic
    solve(0,0);
    cin >> M;
    for (int i = 0; i < M; ++i)
    {
        int temp = 0;
        cin >> temp;
        printf("%s ", cache[N][temp]==1?"Y":"N");
    }
    printf("\n");

    return 0;
}

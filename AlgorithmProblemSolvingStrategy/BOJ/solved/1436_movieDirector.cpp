#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;
#define MAX_NUM 987654321
int N = 0;

bool isSixhappen3timesContinuously(int n)
{
    int cnt = 0;
    while(n != 0)
    {
        int num = n % 10;
        n = n/10;
        if( num == 6 ){
            ++cnt;
        }
        else{
            cnt = 0;
        }
        if( cnt >= 3 )
            return true;
    }
    return false;
}

int solve(int N)
{
    int order = 0;
    int doomNum = 0;
    for(doomNum=1; doomNum<=MAX_NUM; ++doomNum)    
    {
        if (isSixhappen3timesContinuously(doomNum))
            ++order;
        
        if( order == N )
            break;
    }
    return doomNum;
}

int main()
{
    cin >> N;
    int ret = solve(N);
    printf("%d\n", ret);
    
    //printf("%c\n", isSixhappen3timesContinuously(1666)?'O':'X');
    //printf("%c\n", isSixhappen3timesContinuously(6166)?'O':'X');
    //printf("%c\n", isSixhappen3timesContinuously(6661)?'O':'X');
    return 0;
}
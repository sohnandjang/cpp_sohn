#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;
int C = 0;
int N = 0;
int M = 0;

int main()
{
    
    int test_case = 0;
    cin >> test_case;
    
    for(int i=0; i<test_case; ++i)
    {
        cin >> N >> M;
        
        queue<pair<int,int>> q;
        priority_queue<int> pq;
        int count = 0;
        for(int j=0; j<N; ++j)
        {
            int priority = 0;
            cin >> priority;
            q.push({j, priority});
            pq.push(priority);
        }
        
        while(!q.empty())
        {
            int index = q.front().first;
            int priority = q.front().second;
            q.pop();

            if (priority == pq.top())
            {
                pq.pop();
                ++count;
                if (index == M)
                    printf("%d\n", count);
                else
                {
                    
                }
            }
            else
            {
                q.push({index, priority});
            }
            
        }

    }


    return 0;
}


#if 0
int result;
typedef struct 
{
    int order;
    int number;
} Packet_T;
deque<Packet_T> myQueue;

int findMaxNumber(const deque<Packet_T>& _queue)
{
    deque<Packet_T>::const_iterator it;
    int maxNum = -1;
    for(it = _queue.begin(); it!= _queue.end(); ++it)
    {
        maxNum = max(maxNum, it->number);
    }
    return maxNum;
}

void shiftQueue(deque<Packet_T>& _queue)
{
    auto temp = _queue.front();
    _queue.pop_front();
    _queue.push_back(temp);
}
void popShiftQueue(deque<Packet_T>& _queue, int& count)
{
    auto temp = _queue.front();
    _queue.pop_front();
    ++count;
}

int main()
{
    cin >> C;
    for(int i=0; i<C; ++i)
    {
        cin >> N >> M ;
        myQueue.clear();
        for(int i=0; i<N; ++i)
        {
            int temp = 0;
            cin >> temp;
            myQueue.push_back({i,temp});
        }

        int count = 0;
        while(1)
        {
            //1. find Max Value
            int maxNum = findMaxNumber(myQueue);
            //2. compare front value with it
            if (myQueue.front().number == maxNum)
            {
                if (myQueue.front().order == M)
                {
                    // found
                    //printf("found\n");
                    ++count;
                    break;
                }
                else
                {
                    // pop shift
                    popShiftQueue(myQueue, count);                    
                }
            }
            else
            {
                // shift
                shiftQueue(myQueue);
            }
            
        }
        printf("%d\n", count);
    }
    return 0;
}
#if 0
deque<Packet_T> myQueue;
vector<int> BigOrderArray;

int comp(int a, int b)
{
    return (a >= b);
}

int main()
{
    cin >> C;
    for(int i=0; i<C; ++i)
    {
        cin >> N >> M ;
        myQueue.clear();
        BigOrderArray.clear();
        for(int i=0; i<N; ++i)
        {
            int temp = 0;
            cin >> temp;
            myQueue.push_back({i,temp});
            BigOrderArray.push_back(temp);
        }
        sort(BigOrderArray.begin(), BigOrderArray.end(), comp);

        int count = 1;
        int arrayIndex = 0;
        while(!myQueue.empty())
        {
            if(myQueue.front().number >= BigOrderArray[arrayIndex])
            {
                if(M == myQueue.front().order)
                    break;
                
                myQueue.pop_front();
                count++;
                arrayIndex++;
            }       
            else
            {
                Packet_T temp = myQueue.front();
                myQueue.pop_front();
                myQueue.push_back(temp);
            }
            
        }
        printf("%d\n", count);
    }
    return 0;
}
#endif
#endif
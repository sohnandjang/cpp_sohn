#include <iostream>
#include <string.h>
#include <tgmath.h>
using namespace std;


int sol(int dist)
{
    if (dist == 1)
        return 1;
    else if (dist == 2)
        return 2;
    else if (dist == 3)
        return 3;

    double root = sqrt (dist);
    int rootInt = (int)root;
    
    int remaining = dist - rootInt*rootInt;
    int addTurn = (remaining + (rootInt-1))/ rootInt;

    return (2*rootInt-1+addTurn);
}


int main()
{
    int C = 0;
    cin >> C;
    for(int i=0; i<C; ++i){
        int X=0;
        int Y=0;
        cin >> X >> Y;
        int distance = Y - X;
        printf("%d\n", sol(distance));
    }


    return 0;
}
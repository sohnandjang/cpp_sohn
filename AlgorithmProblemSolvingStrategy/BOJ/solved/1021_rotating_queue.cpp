#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <sstream>
using namespace std;

int N = 0;
int MAP[129][129] = {0};

int numBluePiece = 0;
int numWhitePiece = 0;

void Solve(int x, int y, int len)
{
    bool _allZero = true;
    bool _allOne = true;
    for(int i=x; i<x+len; ++i)
    {
        for(int j=y; j<y+len; ++j)
        {
            if(MAP[i][j]==0)
                _allOne = false;
            if(MAP[i][j]==1)
                _allZero = false;
        }
    }
    if(_allZero)
    {
        printf("0");
        return;
    }
    else if(_allOne)
    {
        printf("1");
        return;
    }
    else
    {
        printf("(");
        Solve(x,y,len/2);
        Solve(x,y+len/2,len/2);
        Solve(x+len/2,y,len/2);
        Solve(x+len/2,y+len/2,len/2);
        printf(")");
    }
    return;

}

int main()
{
    memset(MAP, 0, sizeof(MAP));
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        string line;
        cin >> line;
        for(int j=0; j<N; ++j)
        {
            int temp = (line[j]=='0' ? 0 : 1);
            MAP[i][j] = temp;
        }
    }
    //printf("(");
    Solve(0,0,N);
    //printf(")");
    return 0;
}


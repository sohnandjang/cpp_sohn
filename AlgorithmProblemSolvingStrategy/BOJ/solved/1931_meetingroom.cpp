#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
int N = 0;

typedef struct{
    int start;
    int end;
}MeetingTime_T;
vector<MeetingTime_T> meeting;
int maxNum = 0;

void Display(MeetingTime_T i)
{
    printf("(%d %d)\n", i.start, i.end);
}
bool sortRule (MeetingTime_T i,MeetingTime_T j) 
{ 
    if (i.start < j.start)
        return true;
    else if (i.start == j.start)
    {
        if (i.end <= j.end)
            return true;
        else
            return false;
    }
    return false;
}

int main()
{
    cin >> N;

    for(int i=0; i<N; ++i)
    {
        int _start, _end;
        cin >> _start >> _end;
        meeting.push_back({_start, _end});
    }
    
    // sort by start time
    sort(meeting.begin(), meeting.end(), sortRule);

    MeetingTime_T curr = {-1, -1};
    MeetingTime_T pre = {-1, -1};

    for (int i=0; i<N; ++i)
    {
        curr = meeting[i];
        if(pre.end <= curr.start)
        {
            ++maxNum;
            pre = curr;
            //printf("+"); Display(pre);
        }
        else if(curr.end < pre.end)
        {
            pre = curr;
            //printf("v"); Display(pre);
        }  
    }

    printf("%d\n", maxNum);
    return 0;
}
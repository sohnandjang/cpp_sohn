#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;
#define ll long long


int Num = 0;
ll N = 0;
ll M = 0 ;
vector<int> factor;



ll GetNumOfEnd0(ll number, ll standard)
{
    ll ret = 0; //number / 5;
    for(ll i=standard; i<=number; i*=standard)
    {
        ret += number / i;
    }
    return ret;
}

int main()
{
    cin >> N;
    cin >> M;
    ll A = GetNumOfEnd0(N, 5) - GetNumOfEnd0(M, 5) - GetNumOfEnd0(N-M, 5);
    ll B = GetNumOfEnd0(N, 2) - GetNumOfEnd0(M, 2) - GetNumOfEnd0(N-M, 2); 
    printf("%lld\n", min(A, B));
    return 0;
}
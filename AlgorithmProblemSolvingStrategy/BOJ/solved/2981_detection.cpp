#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
using namespace std;

int N = 0;
vector<int> number;
vector<int> M;

#if 0
int minNum = INT_MAX;
int main()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        number.push_back(tmp);
        minNum = min(minNum, tmp);
    }
    
    for(int i=2; i<=minNum; ++i)
    {
        int j = 0;
        for(j=0; j<N-1; ++j)
        {
            if(number[j]%i != number[j+1]%i)
                break;
        }
        if(j == N-1)
            M.push_back(i);
    }
    for(int i=0; i<M.size(); ++i)
    {
        printf("%d ", M[i]);
    }
    printf("\n");
      
    return 0;
}
#endif
#if 0
// 시간초과
int maxNum = INT_MIN;
int main()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int tmp = 0;
        cin >> tmp;
        number.push_back(tmp);
        maxNum = max(maxNum, tmp);
    }
    
    for(int i=2; i<=maxNum; ++i)
    {
        int j = 0;
        for(j=0; j<N-1; ++j)
        {
            if(number[j]%i != number[j+1]%i)
                break;
        }
        if(j == N-1)
            M.push_back(i);
    }
    for(int i=0; i<M.size(); ++i)
    {
        printf("%d ", M[i]);
    }
    printf("\n");
      
    return 0;
}
#endif

vector<int> diff;
int maxNum = INT_MIN;
#if 0
int GCD(int a, int b)// 시간초과
{
    int _max = max(a,b);
    int factor = 1;
    for(int i=1; i<=_max; ++i)
    {
        if(a%i == 0 && b%i == 0)
            factor = i;
    }
    return factor;
}
#endif
int GCD(int a, int b)// 정렬해서 b가 항상 크다.
{
    if(a == 0)
        return b;
    if(b == 0)
        return a;
    #if 0
    if(a > b)
        return GCD(a-b, b);
    else
        return GCD(a, b-a);
    #endif
    if(a > b)
        return GCD(a%b, b);
    else
        return GCD(a, b%a);
}
int main()
{
    cin >> N;
    int prev = 0;
    cin >> prev;
    for(int i=1; i<N; ++i)
    {
        int curr = 0;
        cin >> curr;
        int _diff = abs(curr - prev);
        prev = curr;
        diff.push_back(_diff);
        maxNum = max(maxNum, _diff);
    }

    sort(diff.begin(), diff.end());

    int maxFactor = GCD(diff[0], diff[1]);
    for(int i=2; i<diff.size(); ++i)
    {
        maxFactor = GCD(maxFactor, diff[i]);
    }
    
    #if 0
    for(int i=2; i<=maxFactor; ++i)
    {
        if(maxFactor % i == 0)
            printf("%d ", i);
    }   
    printf("\n");
    #endif
    M.push_back(maxFactor);
    for(int i=2; i*i<=maxFactor; ++i)
    {
        if(maxFactor % i == 0)
        {
            M.push_back(maxFactor/i);
            if(maxFactor/i != i)
                M.push_back(i);
        }
    }   
    sort(M.begin(), M.end());
    for(int i=0; i<M.size(); ++i)
    {
        printf("%d ", M[i]);
    }
    printf("\n");
    return 0;
}
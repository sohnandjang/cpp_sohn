#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
using namespace std;
int N = 0;

class myQueue
{
public:
    void Push(int i)
    {
        m_queue.push(i);
    }
    void Pop()
    {
        if(m_queue.empty()) 
        {
            printf("-1\n");
            return;
        }
        printf("%d\n", m_queue.front());
        m_queue.pop();
    }
    void Size()
    {
        printf("%d\n", m_queue.size());
    }
    void Empty()
    {
        printf("%d\n", m_queue.empty() ? 1 : 0);
    }
    void Front()
    {
        printf("%d\n", m_queue.empty()? -1 : m_queue.front());
    }
    void Back()
    {
        printf("%d\n", m_queue.empty()? -1 : m_queue.back());
    }
private:
    queue<int> m_queue;
};

int main()
{
    cin.tie(0);
	cin.sync_with_stdio(false);
    
    myQueue obj;
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        string opcode;
        cin >> opcode;
        if (opcode == "push")
        {
            int num;
            cin >> num;
            obj.Push(num);
        }
        else if (opcode == "pop")
            obj.Pop();
        else if (opcode == "size")
            obj.Size();
        else if (opcode == "empty")
            obj.Empty();
        else if (opcode == "front")
            obj.Front();
        else if (opcode == "back")
            obj.Back();
    }
    return 0;
}
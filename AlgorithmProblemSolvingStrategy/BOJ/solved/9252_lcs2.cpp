#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
using namespace std;

constexpr int MAX_LEN = 1'001;

char A[MAX_LEN+1] = {0,};
char B[MAX_LEN+1] = {0,};
string Ans;
char Answer[MAX_LEN+1] = {0,};
int DP[MAX_LEN][MAX_LEN] = {0};
int ASize = 0;
int BSize = 0;

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    string temp;
    cin >> temp;
    ASize = temp.size();
    for(int i=1; i<=temp.size(); ++i)
        A[i] = temp[i-1];
    cin >> temp;
    BSize = temp.size();
    for(int i=1; i<=temp.size(); ++i)
        B[i] = temp[i-1];
}

void Solve()
{
    //int ASize = strlen(A);
    //int BSize = strlen(B);
    for(int i=1; i<=ASize; ++i)
    {
        for(int j=1; j<=BSize; ++j)
        {
            if(A[i] == B[j])
                DP[i][j] = DP[i-1][j-1] + 1;
            else
            {
                DP[i][j] = max(DP[i-1][j], DP[i][j-1]);
            }
        }
    }
#if 0    
    for(int i=1; i<=ASize; ++i)
    {
        for(int j=1; j<=BSize; ++j)
        {
            printf("%d ", DP[i][j]);
        }
        printf("\n");
    }        
    printf("\n");
#endif
    int row = ASize;
    int col = BSize;
    while(row >=1 && col >=1)
    {
        if(DP[row][col]==DP[row-1][col])
            --row;
        else if(DP[row][col]==DP[row][col-1])
            --col;
        else
        {
            Answer[row] = A[row];
            --row;
            --col;
        }
    }
    printf("%d\n", DP[ASize][BSize]);
    for(int i=1; i<=ASize; ++i)
    {
        if(Answer[i]==0)
            continue;
        printf("%c", Answer[i]);
    }
    printf("\n");
}


int main()
{
    Input();
    Solve();
    return 0;
}
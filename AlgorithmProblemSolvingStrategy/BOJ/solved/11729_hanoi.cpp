#include <stdio.h>

int getTotalNum(int n)
{
    if(n == 1)
        return 1;
    if(n == 1)
        return 3;
    if(n == 3)
        return 7;
    
    return 2*getTotalNum(n-1) + 1;
}

int solve(int num, int from, int by, int to)
{
    // num == 1
    if( num == 1 )
    {
        printf("%d %d\n", from, to);
    }
    else
    {
        solve(num-1, from, to, by);
        printf("%d %d\n", from, to);
        solve(num-1, by, from, to);
    }
}


int main()
{
    int n=0;
    scanf("%d", &n);
    printf("%d\n", getTotalNum(n));
    solve(n, 1, 2, 3);
    return 0;
}

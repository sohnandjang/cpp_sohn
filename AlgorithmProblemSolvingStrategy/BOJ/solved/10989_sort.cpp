#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
#include <string.h>
#include <stdlib.h>
using namespace std;

int N = 0;
int arr[10001];

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    cin >> N;
    memset(arr, 0, sizeof(arr));
    for(int i=0; i<N; ++i)
    {
        int temp;
        cin >> temp;
        arr[temp]++;
    }
    
    for(int i=1; i<10001; ++i)
    {
        int repeat = arr[i];
        for(int j=0; j<repeat; ++j)
        {
            printf("%d\n", i);
        }
    }
    return 0;
}
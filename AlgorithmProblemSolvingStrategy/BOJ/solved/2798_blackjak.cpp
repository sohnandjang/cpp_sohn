#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

vector<int> arr;

int solve(const vector<int>& arr, int M)
{
    int ret = 0;
    for(int i=0; i<arr.size()-2; ++i)
    {
        for(int j=i+1; j<arr.size()-1; ++j)
        {
            for(int k=j+1; k<arr.size(); ++k)
            {
                int sum = arr[i]+arr[j]+arr[k];
                if( sum == M )
                    return M;
                else if( sum < M )
                {
                    ret = max(ret, sum);
                }
            }
        }
    }
    return ret;
}

int main()
{
    int N=0, M=0;
    scanf("%d %d", &N, &M);
    for(int i=0; i<N; ++i)
    {
        int temp;
        scanf("%d", &temp);
        //printf("input = %d\n", temp);
        arr.push_back(temp);
    }
    int ret = solve(arr, M);
    printf("%d\n", ret);
    return 0;
}
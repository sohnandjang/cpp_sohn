#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
using namespace std;

int N = 0; // length <= 100
int mod = 1000000000;
int dp[101][11];

// 1 9
// 2 17

/**
 * \ret possible case number
 */
int solve(int n)
{
    for (int i = 1; i <= 9; i++) {
        dp[1][i] = 1;
    }
    for (int i = 2; i <= n; i++) {
        dp[i][0] = dp[i - 1][1];
        for (int j = 1; j <= 9; j++) {
            dp[i][j] = (dp[i - 1][j - 1] + dp[i - 1][j + 1]) % 1000000000;
        }
    }
    long sum = 0;
    for (int i = 0; i < 10; i++) {
        sum += dp[n][i];
    }
    return (sum % mod);
}
int main()
{
    cin >> N;
    
    printf("%d\n", solve(N));

    return 0;
}
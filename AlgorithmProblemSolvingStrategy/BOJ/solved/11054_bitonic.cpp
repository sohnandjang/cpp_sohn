#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
using namespace std;

int N = 0; 
int A[1001] = {0};
int DP[1001] = {0};
int DP_INV[1001] = {0};
#if 0
10
1 5 2 1 4 3 4 5 2 1
7
#endif

/**
 * \n current location
 * \ret max length lis
 */
int solve()
{
    for(int i=0; i<N; ++i)
    {
        DP[i] = 1;
        DP_INV[i] = 1;
    }
        
    
    int sol = 1;
    for(int i=1; i<N; ++i)
    {
        for(int j=0; j<i; ++j)
        {
            if(A[j]<A[i])
                DP[i] = max(DP[j]+1, DP[i]);
        }
        //sol = max(sol, DP[i]);
    }
    for(int i=N-2; i>=0; --i)
    {
        for(int j=N-1; j>i; --j)
        {
            if(A[j]<A[i])
                DP_INV[i] = max(DP_INV[j]+1, DP_INV[i]);
        }
        //sol = max(sol, DP[i]);
    }
    for(int i=0; i<N; ++i)
    {
        sol = max(sol, DP[i]+DP_INV[i]-1);
    }
    return sol;
}

int main()
{
    //memset(cache, -1, sizeof(cache));
    cin >> N;
    for(int i=0; i<N; ++i)
        cin >> A[i];

    printf("%d\n", solve());

    return 0;
}
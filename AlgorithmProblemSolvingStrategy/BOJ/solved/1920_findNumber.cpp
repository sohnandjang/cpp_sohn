#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
using namespace std;

int N = 0;
int M = 0;
vector<int> SRC;
vector<int> SELECTED;

void input()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int temp = 0;
        cin >> temp;
        SRC.push_back(temp);
    }
    cin >> M;
    for(int i=0; i<M; ++i)
    {
        int temp = 0;
        cin >> temp;
        SELECTED.push_back(temp);
    }
}
bool binarySearch(int start, int end, int num)
{
    int mid = (start + end)/2;
    if(num == SRC[mid])
        return true;
    
    if(start == end)
        return false;

    bool ret = false;
    if(num < SRC[mid])
    {
        ret = binarySearch(start, mid, num);
    }
    else
    {
        ret = binarySearch(mid+1, end, num);
    }
    return ret;
    
}
void solve()
{
    sort(SRC.begin(), SRC.end());
    for(int i=0; i<M; ++i)
    {
        int numToFind = SELECTED[i];
        bool ret = binarySearch(0, N-1, numToFind);
        printf("%d\n", ret ? 1 : 0);
    }
}
int main()
{
    input();
    solve();
    return 0;
}


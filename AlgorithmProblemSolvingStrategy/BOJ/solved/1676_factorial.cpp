#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h> 
#include <vector>
#include <map>
using namespace std;

int T = 0;
int N = 0;



int main()
{
    cin >> T;
    for(int i=0; i<T; ++i)
    {
        cin >> N;
        map<string, int> MAP;
        for(int j=0; j<N; ++j)
        {
            string name;
            string kinds;
            cin >> name;
            cin >> kinds;
            if(MAP.find(kinds) != MAP.end())
            {
                ++MAP[kinds];
            }
            else
            {
                MAP[kinds] = 1;
            }
        }
        int sum = 1;
        for(auto it=MAP.begin(); it!=MAP.end(); ++it)
        {
            sum = sum * (it->second + 1);
        }
        sum -= 1;
        printf("%d\n", sum);
    }
    return 0;
}
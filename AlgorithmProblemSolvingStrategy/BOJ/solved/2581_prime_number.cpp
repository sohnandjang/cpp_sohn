#include <iostream>
using namespace std;

int main()
{
    int M = 0;
    int N = 0;
    cin >> M >> N;
    int sum = 0;
    bool isMinNum = false;
    int minNum = 0;
    for(int i=M; i<=N; ++i){
        int number = 0;
        for(int j=1; j<=i; j++){
            if( i % j == 0 ){
                number++;
            }
        }
        if(number != 2)
            continue;
        
        if(!isMinNum){
            minNum = i;
            isMinNum = true;
        }
        sum+=i;
    }

    if( sum == 0){
        printf("-1\n");
    }
    else{
        printf("%d\n", sum);
        printf("%d\n", minNum);
    }
    return 0;
}
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <bitset>
#include <cstring>
#include <vector>
#include <queue>
#include <algorithm>
#define MAX 101
#define INF 987654321
using namespace std;

typedef pair<int, int> pii;

int n,m;
int u,v,w;

int adj[MAX][MAX];
int dist[MAX][MAX];

void floyd(){
    // K=0 인 경우 초기화
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            if(i==j) continue;
            
            if(adj[i][j] != INF){
                dist[i][j] = adj[i][j];
            }else{
                dist[i][j] = INF;
            }
        }
    }
    
    // Floyd
    for(int k=1; k<=n; k++){
        for(int i=1; i<=n; i++){
            for(int j=1; j<=n; j++){
                if(i==j) continue;
                if(k==i || k==j) continue;
                
                if(dist[i][j] > dist[i][k] + dist[k][j]){
                    dist[i][j] = dist[i][k] + dist[k][j];
                }
            }
        }
    }
    
    // Print
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++){
            if(dist[i][j] == INF) cout << 0 << " ";
            else cout << dist[i][j] << " ";
        }
        cout << "\n";
    }
}

int main(int argc, const char * argv[]) {
    // cin,cout 속도향상
    ios_base::sync_with_stdio(false);
    cin.tie(NULL); cout.tie(NULL);
    
    cin >> n;
    cin >> m;
    
    for(int i=0; i<MAX; i++){
        for(int j=0; j<MAX; j++){
            if(i==j) adj[i][j] = 0;
            else adj[i][j] = INF;
        }
    }
    
    for(int i=0; i<m; i++){
        cin >> u >> v >> w;
        
        if(adj[u][v] > w) adj[u][v] = w;
    }
    
    floyd();
}

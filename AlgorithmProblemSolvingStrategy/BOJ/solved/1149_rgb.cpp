#include <iostream>
#include <algorithm>
#include <limits.h>
using namespace std;

int N =0;
const int MAX_LEN =1001;
int ARR[MAX_LEN][3]={0};
int DP[MAX_LEN][3]={0};
int MIN_NUM = INT_MAX;

int main()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        cin >> ARR[i][0];
        cin >> ARR[i][1];
        cin >> ARR[i][2];
    }

    DP[0][0] = ARR[0][0];
    DP[0][1] = ARR[0][1];
    DP[0][2] = ARR[0][2];

    for(int i=0; i<N; ++i)
    {
        DP[i][0] = min(DP[i-1][1], DP[i-1][2]) + ARR[i][0];
        DP[i][1] = min(DP[i-1][0], DP[i-1][2]) + ARR[i][1];
        DP[i][2] = min(DP[i-1][0], DP[i-1][1]) + ARR[i][2];
    }
    
    int ret = min(min(DP[N-1][0], DP[N-1][1]), DP[N-1][2]);
    printf("%d\n", ret);;
    return 0;
}
#include <iostream>
#include <algorithm>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <stack>
#include <string>
#include <queue>
#include <deque>
#include <list>
#include <type_traits>
using namespace std;

constexpr int MAX_N = 51;
int T = 0;
int M = 0; 
int N = 0;
int numOfAdj = 0;
int Adj[MAX_N][MAX_N];
bool Visited[MAX_N][MAX_N] = {0};

int dx[4] = {1, -1, 0, 0};
int dy[4] = {0, 0, 1, -1};

// return num of Vertex
int DFS(int row, int col)
{
    if(row < 0 || row >= N || col < 0 || col >= M)
        return 0;

    if(!Adj[row][col])
        return 0;

    Visited[row][col] = true;
    //printf("[%d][%d] > ", row, col);

    int ret = 1;
    for(int i=0; i<4; ++i)
    {
        int next_row = row + dy[i];
        int next_col = col + dx[i];
        if(0 <= next_row && next_row < N && 0<= next_col && next_col < M)
        {
            if(!Visited[next_row][next_col] && Adj[next_row][next_col])
            {
                ret += DFS(next_row, next_col); 
            }
        }
    }
    return ret;
}

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false); 

    cin >> T;
    for(int t=0; t<T; ++t)
    {
        cin >> M >> N >> numOfAdj;
        memset(Adj, 0, sizeof(Adj));
        memset(Visited, 0, sizeof(Visited));
        for(int i=0; i<numOfAdj; ++i)
        {
            int row;
            int col;
            cin >> col >> row;
            Adj[row][col] = 1;
        }

        int numOfBugs = 0;
        for(int i=0; i<N; ++i)
        {
            for(int j=0; j<M; ++j)
            {
                int numOfCabages = 0;
                if(!Visited[i][j])
                {
                    if(Adj[i][j]==1)
                    {
                        //printf("\nnew... \n");
                        numOfCabages = DFS(i,j);
                        //printf("numOfCabages:%d \n", numOfCabages);
                    
                        if(numOfCabages)
                        {
                            ++numOfBugs;
                            //printf("numOfBugs: %d\n", numOfBugs);
                        }
                        
                    }
                }
            }
        }
        printf("%d\n", numOfBugs);
    }
    
    return 0;
}
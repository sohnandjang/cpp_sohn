#include <iostream>
#include <cmath>
using namespace std;

#define MAX_N 1000001
int arr[MAX_N] = {0,};
int M = 0;
int N = 0;

void eratostenes()
{
    for(int i=2; i<MAX_N; ++i){
        arr[i] = 1;
    }

    int sqrtNum = (int)sqrt((double)N);
    for(int i=2; i<=sqrtNum; i++){
        for(int j=2; i*j <= N; j++){
            arr[i*j] = 0;
        }
    }
}


int main()
{

    while(1){
        cin >> M;
        if(M == 0)
            break;
        N = M*2;
        eratostenes();
        int num = 0;
        for(int i=M+1; i<=N; ++i){
            if(arr[i] == 1)
                num++;
        }
        printf("%d\n", num);
    }
   return 0;
}
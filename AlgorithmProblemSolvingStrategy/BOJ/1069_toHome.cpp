#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>
#include <math.h>

using namespace std;

#define endl "\n"

double getDistance(int x1, int y1, int x2, int y2)
{
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    cout << fixed;
    cout.precision(9);

	int X, Y, D, T;
	cin >> X >> Y >> D >> T;
	double L = getDistance(0, 0, X, Y);
	double res = 0;
	if(L >= D)
	{
		int jump = L / D;
		res = min(jump*T + L - jump*D, min((double)L, (double)(jump+1)*T));
	}
	else
	{
		res = min((double)T + D - L, min((double)2*T , (double)L)); 
	}
	cout << res << endl;

	return 0;
}
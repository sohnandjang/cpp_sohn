#include <iostream>
#include <deque>
#include <vector>
using namespace std;

#define UINT8 unsigned char

typedef struct{
    int msgid;
    vector<UINT8> vec;
}MSG_T;

class MST_C{
public:
    MST_C(int _msgid, const vector<UINT8>& _vec)
    {
        msgid = _msgid;

    }
    int msgid;
    vector<UINT8> vec;
private:
};

UINT8 _getOpcode(const vector<UINT8>& _vec)
{
    //return (_vec.size()==1 ? _vec[0] : (_vec.size()>1?_vec[1]:0));
    if(_vec.size() == 1){
        printf("[%s:%d] OPCODE = 0x%X", __func__, __LINE__, _vec[0]);
        return _vec[0];
    }
    else if(_vec.size() > 1){
        printf("[%s:%d] OPCODE = 0x%X", __func__, __LINE__, _vec[1]);
        return _vec[1];
    }
    printf("[%s:%d] OPCODE = 0x%X", __func__, __LINE__, 0);
    
}

int main()
{
    deque<MSG_T> msg;
    msg.push_back({0, {0x12,0x13}});
    msg.push_back({1, {0x14,0x15}});
    msg.push_back({2, {0x16}});

    auto it = msg.begin();
    for(;it!=msg.end();++it)
    {
        printf("opcode=0x%x\n", it->vec.size()==1 ? it->vec[0] : (it->vec.size()>1?it->vec[1]:0));
        printf("opcode=0x%x\n", _getOpcode(it->vec));
    }



    return 0;
}

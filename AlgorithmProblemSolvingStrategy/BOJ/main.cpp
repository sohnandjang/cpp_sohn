#include <iostream>
using namespace std;

int main()
{
    int A=0;
    int B=0;
    int V=0;
    cin >> A >> B >> V;
    if( A >= V ){
        printf("1\n");
        return 0;
    }
    
    int day = ((V-A)+(A-B-1))/(A-B);
    printf("%d\n", day);
    return 0;
}
/*
int main()
{
    int n = 0;
    cin>> n;

    int m = 1;    
    while( 1 ){
        if( n > m ){
            n -= m;
            m++;
        }
        else{
            break;
        }
    }

    int a = 0;
    int b = 0;
    if( m % 2 == 1 ){
        a = m+1-n;
        b = n;
    }
    else{
        a = n;
        b = m+1-n;
    }

    printf("%d/%d\n", a, b);
    return 0;
}
*/

/*#include <iostream>
#include <algorithm>
using namespace std;
#define INF 987654321 

int main()
{
    int n = 0;
    cin>> n;
    
    int result = INF;
    
    if( n % 3 == 0){
        result = min( n/3, result );
    }
    if( n % 5 == 0){
        result = min( n/5, result );
    }
    
    int num3 = 0;
    int num5 = 0;
    num5 = n / 5;
    n = n - 5*num5;
    
    if(n % 3 == 0){
        num3 = n / 3;
        result = min( result, num3+num5 );
    }
    
    printf("%d\n", result==INF?-1 : result);
    return 0;
}*/
/*#include <iostream>
#include <string>
using namespace std;

int main()
{
    int C;
    cin >> C;
    int count = 0;
    for(int i=0; i<C; ++i){
        
        string s;
        cin >> s;
        char pre = -1;
        char cur = -1;
        int visited[26] = {0,};
        int j=0;
        for(j=0; j<s.length(); ++j){
            cur = s[j];
            if(visited[cur-'a'] && (pre!=cur)){
                break;
            }
            visited[cur-'a'] = 1;
            pre = cur;
        }
        if(j == s.length() ){
            count++;
        }
    }
    printf("%d\n", count);
    return 0;
}*/
/*
#include <iostream>
#include <string>
#include <vector>
using namespace std;

vector<string> cand = {
  "c=", "c-", "dz=", "d-", "lj", "nj", "s=", "z="  
};

int main()
{
    string s;
    cin >> s;
    int count = 0;
    int pos = 0;
    while(pos < s.length()){
        int i=0;
        for( i=0; i<cand.size();++i){
            if(!cand[i].compare(0, cand[i].length(), s.substr(pos, cand[i].length()))){
                pos+=cand[i].length();
                break;
            }
        }
        if( i == cand.size() ){
            pos++; 
        }
        count++;
    }
    cout << count << '\n';
    return 0;
}*/
/*#include <iostream>
using namespace std;

int timeConsume[26]={
    3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,8,9,9,9,10,10,10,10
};

int main()
{
    string s;
    cin >> s;
    int count = 0;
    for(int i=0; i<s.length(); ++i){
        count+=(timeConsume[s[i]-'A']);
    }
    cout << count << '\n';
    return 0;
}*/
/*#include <iostream>
#include <bits/stdc++.h> 
#include <string>
#include <vector>
using namespace std; 


vector<string> vec;

void removeDupWord(string str) 
{ 
    // Used to split string around spaces. 
    istringstream ss(str); 
  
    // Traverse through all words 
    do { 
        // Read a word 
        string word; 
        ss >> word; 
  
        // Print the read word 
        //cout << word << endl; 
        vec.push_back(word);
  
        // While there is more to read 
    } while (ss); 
} 

int main()
{
    string s;
    std::getline (std::cin,s);
    removeDupWord( s );
    printf("%d\n", vec.size()-1);
    return 0;
}
*/
//10 5
//1 10 4 9 2 3 8 5 7 6

/*#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main()
{
    string s;
    cin >> s;
    pair<int, int> theMostExist = {0,0}; // num, order
    int numberExist[26] = {0,};
    int i=0;
    for(i=0; i<s.length(); ++i){
        int order = 0;
        if( 'a' <= s[i] && s[i] <= 'z' )
            order = s[i] - 'a';
        else 
            order = s[i] - 'A';
        
        numberExist[order]++;
        if(theMostExist.first < numberExist[order]){
            theMostExist = make_pair( numberExist[order], order);
        }
    }

    int numOfMostExist = 0;
    for(int i=0; i<26; ++i){
        
        if( numberExist[i] == theMostExist.first )
            numOfMostExist++;
    }
    if( numOfMostExist == 1)
        printf("%c\n", theMostExist.second+'A');
    else
    {
        printf("?\n");
    }
    
    
    return 0;
}*/
/*#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main()
{
    int C=0;
    cin >> C;
    for(int i=0; i<C; ++i){
        int len = 0;
        cin >> len;
        string s;
        cin >> s;

        string res;
        for(int j=0; j<s.length(); ++j){
            res.append(len, s[j]); 
        }
        cout << res << '\n';
    }
    
    return 0;
}*/
/*
#include <iostream>
#include <string>
#include <stdlib.h>
#include <string.h>
using namespace std;

int main()
{
    string s;
    cin >> s;
    char arr[26];
    memset(arr, -1, sizeof(arr));

    for(int i=0; i<s.length(); ++i){
        int idx = s[i] - 'a';
        if( arr[ idx ] != -1 )
            continue;

        arr[idx] = i; 
    }
    for(int i=0; i<26; ++i){
        if(arr[i] != -1)
            printf("%d ", (arr[i]));
        else
            printf("-1 ");
    }    
    printf("\n");
    return 0;
}*/
/*#include <iostream>
#include <string>
using namespace std;

int main()
{
    int N = 0;
    string s;
    cin >> N;
    cin >> s;
    int sum = 0;
    for(int i=0; i<N; ++i){
        sum += (s[i] - '0');
    }
    printf("%d\n", sum);
        
    return 0;
}*/
/*#include <iostream>
using namespace std;

int main()
{
    int N = 0;
    cin >> N;
    int num = 0;
    if( N < 100 )
        num = N;
    else{
        if(N == 1000)
            N = 999;
        num = 99;
        while(N >= 100){
            int c = N % 10;
            int b = (N/10) % 10;
            int a = (N/100) % 10;
            if((a-b) == (b-c))
                num++;

             N--;
        }
    }
    cout << num << '\n';
    
    return 0;
}*/

/*
#include <iostream>
using namespace std;
constexpr int N = 10001;
int arr[N] = {0,};

int getSum(int n){
    int num = n;
    int sum = 0;
    while(num > 0){
        sum += num % 10;
        num /= 10;        
    }
    return sum;
}


void checkDN(int i)
{
    int nextNum = i;
    while( nextNum <= N-1 ){
        nextNum += getSum( nextNum );
        if(arr[nextNum] == 1)
            return;
        else
            arr[nextNum] = 1;
    }    
}

int main()
{
    for(int i=1; i<N; ++i){
        checkDN( i );
    }
    
    for(int i=1; i<N; ++i){
        if( arr[i] == 0)
            printf("%d\n", i);
    }
    return 0;
}
*/
/*
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 0;
    cin >> C;
    for(int i = 0; i <C; ++i){
        int N = 0;
        cin >> N;
        int arr[1000] = {0,};
        int total = 0;  
        double average = 0;
        for(int j = 0; j<N; ++j){
            int num = 0;
            cin >> num;
            arr[j] = num;
            total += num;
        }
        average = total / (double)N;
        
        int numOverAverage = 0;
        for(int j = 0; j<N; ++j){
            if( (double)arr[j] > average )
                numOverAverage++;
        }
        double ratioOverAverage = numOverAverage / (double)N * 100;
        double sol = round(ratioOverAverage * 10000) / (double)10000;
        cout.fixed;
        cout.precision(3);
        //cout << sol << endl;
        printf("%.3f\n", sol);
    }
    return 0;
}*/

/*
#include <iostream>
#include <algorithm>
using namespace std;

typedef enum{
    SHAPE_DEF = 0,
    SHAPE_O = 1,
    SHAPE_X = 2
}ESHAPE;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 0;
    cin >> C;
    for(int i = 0; i <C; ++i){
        string s = "";
        cin >> s;
        ESHAPE pre = SHAPE_DEF;
        ESHAPE curr = SHAPE_DEF;
        int count = 0;
        int acc = 0;
        for(int j=0; j<s.size(); ++j){
            if( s[j] == 'O')
                curr = SHAPE_O;
            else
                curr = SHAPE_X;
            if(curr == SHAPE_X )
                acc = 0;
            else if(curr == SHAPE_O ){
                if(pre == curr)
                    acc++;
                else
                    acc = 1;
            }
            pre = curr;
            count += acc;
        }
        cout << count << '\n';
    }
    return 0;
}
*/
/*
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 3;
    int total = 0;
    int maxNum = 0;
    for(int i = 0; i <C; ++i){
        int num = 0;
        cin >> num;
        total += num;
        maxNum = max(maxNum, num);
    }
    
    double average = total/(double)3/maxNum*100;
    cout << average << '\n';
    
    return 0;
}*/
/*
#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 3;
    int total = 0;
    for(int i = 0; i <C; ++i){
        int num = 0;
        cin >> num;
        total *= num;
    }
    int arr[10] = {0,};
    while( total != 0){
        int a = total % 10;
        arr[a]++;
        total /= 10;
    }
    for(int i=0; i<10; ++i){
        cout << arr[i] << '\n';
    }
    return 0;
}*/
/*
#include <iostream>
using namespace std;

int main()
{
    int num = 0; 
    int count = 0;
    cin >> num;
    int curr = num;
    do{
        int sum = (curr%10) + (curr/10);
        curr = (curr%10)*10+(sum%10);
        count++;
        
    }while( num != curr );
    
    cout << count << '\n';
    return 0;
}
*/


/*



#include <iostream>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 0;
    scanf("%d", &C);
    for(int i = 0; i <C; ++i){
        for(int j=0; j<C; ++j){
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

#include <iostream>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 0;
    cin >> C;
    for(int i = 0; i <C; ++i){
        for(int j=0; j<C; ++j){
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    cin.tie(NULL);
    ios::sync_with_stdio(false);
    int C = 0;
    cin >> C;
    int minValue = 1000000;
    int maxValue =  -1000000;
    for(int i = 0; i <C; ++i){
        int num = 0;
       
        cin >> num;
        minValue = min(minValue, num);
        maxValue = max(maxValue, num);
        
    }
    cout << minValue << " " << maxValue << '\n';
    return 0;
}

*/

1. 입출력 속도 높이기
    ``` c++ 
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    ```
2. 개행:  `cout << ‘\n’`
3. 4673 셀프 넘버   
    N = 10,000 -> O($N^2$) 인 경우 1초를 넘을 수 있음  
    가지치기 방법을 사용하여 solve
4. Read 1 line with blank  
   ``` std::getline (std::cin, (std::string) s); ```
5. how to transform from string to integer  
    ```stoi(a, nullptr, 10)```
6. 1712 손익분기점  
   * 수학계산 직접 해서 정답 구하기. while로 찾으면 시간초과  
7. 2839 설탕배달  
   * 5의 배수 가능한 개수까지 for문 돌면서 최소값 찾기  
8. 2775 부녀회장 (수학)
    * ```f(0,n)=n, f(k,1)=1, f(k,n)=f(k,n-1)+f(k-1,n) ```  
9. 1011 Fly me to (수학)  
    * 아래 원리를 찾은 후 코딩
    ```
    // ex           총거리(i^2)  횟수(i) 최고속도(i)
    1               1           1       1
    121             4           3       2
    12321           9           5       3
    1234321         16          7       4
    123454321       25          9       5

    횟수 = i x 2 - 1
    ```  
10. 1929 에라토스테네스의 체(Sieve of Eratosthenes)  
    * 일반적인 방법 O($N^2$)으로는 풀기 어려움
    * 1~N 수에서 임의의 수 m = axb 라고 할 때, 한 수는 $\sqrt{N}$ 이하이므로  
    에라토스테네스의 체는 $\sqrt{N}$ 까지만 수행하면 된다.  
    * array를 선언해두고 구현진행  
11. 9020 골드바흐  
    * 10 > 2,3,5,7 여기서 5+5인 경우, 즉 자기자신의 합에 대한 고려 먼저 필요  
12. (수학2) 3009 네번째점
    * XOR를 활용  
13. (수학2) 3053 택시기하학
    * https://sihyungyou.github.io/baekjoon-3053/  
14. (수학2) 1002 터렛  
    * case를 나누어서 생각. 두 개 원이 서로 떨어져 있는 경우, 외접, 내접, 한 원 안에.
    * 다른 방법) 두 원이 떨어져있음 > (r1+r2)^2, 한 원 안에 있음 > (r1-r2)^2  
15. (재귀) 2447 별찍기 10 , 매우 어려움
    * solve(int x, int y, int num)  
        * 초기시작은 solve (0,0,num) 으로 시작  
        * int div = num / 3;
        * solve (y+(i* div), x+(j* div), div)  
        * num = 3일 때, div = 1이고, x, y = 0이고 이 때, i, j값이 0,1,2로 변경하면서  
        mat[0][0], mat[0][1], mat[0][2], mat[1][0], ... mat[2][2]를 채운다.  
        이후 num = 9일 때, div = 3이고, x, y = 0이고 이 때, i, j값에 따라
        solve(0,0,3), solve(3,0,3), solve(6,0,3),   
        solve(0,3,3), solve(0,6,3),  
        solve(3,3,3), solve(6,6,3)  
        solve(0,0,3)은 0, 0 기준으로 mat[0][0]~ mat[2][2]까지 입력
        solve(6,6,3)은 6, 6 기준으로 mat[6][6]~ mat[8][8]까지 입력  
16. (재귀) 11729 하노이의 탑 (이동순서 출력은 어려웠음)  
    * 총 이동횟수 계산  
        * 2개 원반이 있다고 하면,  
            * 맨 아래 원반이 마지막 장대로 이동해야 하므로,  
            맨 위 원반은 2번 장대로 이동, 맨 아래 원반은 3번 장대로 이동  
            다시 2번에 있던 원반을 3번 장대로 이동.
            1 + 1 + 1 = 3  
            * 이를 정리하면,  
            f(N) = 2*f(N-1) + 1  
            맨 아래 원판을 제외한 f(N-1)를 2번 장대로 이동. 맨 아래 원판 3번 장대로 이동.  
            다시 2번 장대 f(N-1)를 3번 장대로 이동  
    * 이동 순서 출력  
        * 함수 정의가 중요함  
        solve(int n, int from, int by, int to)로 정의함  
        n == 1일 때는 printf("%d %d\n", from, to)  
        else 일 때는  
        ```
        solve(num-1, from, to, by);
        printf("%d %d\n", from, to);
        solve(num-1, by, from, to);
        ```  
17. (brute force)  
    * 단순하게 생각해서 O(N), O(N^2) 으로 1부터 N까지 순차적으로 전체를 테스트(모든 케이스)한다고 접근  

18. (정렬: 2108번 통계학)  
    * 최빈값 구하는 것이 어려웠음.  
    * 처음 생각한 대로, -4000~4000 을 vector[0~8000]으로 잡아두고 입력값에 따라 vector 업데이트 후에, 최빈값 구하면 됨  
    * 참고) map 으로 key, value 업데이트 후, map을 vector로 변환. 정렬해서 풀어보려고 하였는데 잘되지 않음  
19. (정렬:1428 소트인사이드)    
    * 나는 숫자로 입력받고 1자리씩 추출해서 정렬후 print로 품  
    * (타1) 숫자를 문자열로 입력받고 char 문자로 인식해서, qsort 사용해서 푸는 답안 인상적임
    * (타2) 배열을 0~9 준비하고, 입력된 숫자에 대해서 count 증가  
    * 9부터 각 자리수 개수만큼 출력을 함  
20. (정렬:11650 좌표정렬하기)  
    * 나는 struct로 x, y 좌표 저장하는 구조체를 만들어서 진행함  
    * (타1) pair<int,int> 를 사용  
    * sort를 사용하는 것은 동일하지만, 나는 함수를 사용  
    * (타1) [](const pair<int,int &a>,...b) 와 같이 람다함수씀  
    * 나는 for(int i=0;i<?; ++i) 와 같이 for문 사용했으나  
    * (타1) for(auto &p : A) 로 간단히 작성함  
21. (정렬:1181 단어정렬)  
    * 사전순 정렬은 string a, b; -> a < b 로 간단히 표현가능  
    * 나의 경우, a.substr(i,1)로 한 단어씩 비교하려다가  
    * a, b의 size가 다른 경우 고려를 못해 segmentation fault 발생함  
22. (정렬:10814 나이정렬)  
    * 나의 경우, struct를 이용해서 정보를 넣고 비교하였는데,,,  
    * (타) vector, queue 2차원배열을 만든 후에,,,  queue<string> q[201], vector<string> v[201];
    * (타) vec[age].push_back, que[age].push_back 으로 나이에 대해서 입력순서대로 저장함  
23. (백트래킹:15649 N과M1)  
    * (나) func(vector<int>& Arr, int depth) 정의하여, Arr에 원소추가하면서  
    * depth를 줄여나가면서 depth가 0이 되면 (base) print하기로 함  
    * Arr에 원소 추가 + 재귀 + 원소 제거  
    * (타) dfs를 이용. dfs 코드가 이 문제에 적용이 됨을 알면 됨.  
    * 각 정점에 대해서 순회하고, 정점부터 시작해서 전체 정점까지 순서대로 순회함.  
    * 모든 정점을 돌면 depth == M 이면 print를 함  
    * 한번 방문한 정점은 visited array에 저장하여 재방문하지 않음.  
    * 다른 정점 방문할 때, array에 순회하고 있는 정점들을 저장함.  
    * 결과적으로 dfs만 코드로 잘 짜면 해결되는 문제임  
    * (차이) 나는 한 번 방문한 정점에 대해서, vector를 순회하면서 찾았기 때문에 시간 소요됨  
    * 다른 사람은 int visited[MAX]로 메모리를 미리 잡아두기 때문에 나중에 찾을 때 시간 save가 됨. 메모리 사용.  
24. (백트래킹: 9633 N-Queen)  
    * (나) 2차원배열을 만들고, NxN 을 모두 순회하고,   
    * 방문한 점은 2차원배열에 저장해 두고,,, 재귀함수 진입하기 전/후에 이전 2차원배열을 저장해 두어야 했다.  
    * 점점 구현이 복잡해져서,,, 가능할 건 같은데, 시간이 많이 소요될 듯했다.  
    * (타) 퀸은 같은 행에 1개만 놓일 수 있다는 정보를 이용해서,,,  
    * 1차원배열 이용해서 퀸이 놓이는 위치를 저장할 수 있었다.  
    * (나: 문제이해) 퀸 N개를 놓을 수 있는 방법의 수 찾는 문제였음.  
    * 퀸 N개라는 정보를 뒤늦게 알게 됨. ^^;;  1,2개라도 놓으면 되는 줄 알았음.  
25. (동적계획법: 1149 RGB거리)  
    * 논리를 찾아서 수식으로 만드는 법이 중요함.  
26. (동적계획법: 1932 삼각형 )  
    * 처음에 int triange(int row, int col, int sum)  
    * sum을 parameter로 하고 return도 sum이라 중복이 되고 있었음  
    * 그리고 int sum1 = triange(row+1, col, sum+cur) 로 하다보니,  
    * 맨 아래 행의 경우 해당 행에 대한 값만 계산되어야 하는데 누적이 되고  
    * cache값이 잘못되어 있었음  
    * (수정) int triange(int row, int col) 으로 변경하고  
    * int sum1 = triange(row+1, col) + cur 로 변경하여, 개선하게 됨.  
    * point는 맨 아래 행의 값이 가장 작아야 하므로 sum값이 누적되지 않아야 함  
26. (동적계획법: 1579 계단)  
    * (나: 처음) 함수를 int stair(int height, int numSeries)로 만들었고,  
    * mistake는 파라미터가 2개이므로 cache[][] 2차원배열로 만들었어야 하는데,  
    * cache[]만들 때, height만 고려하여 만들었기 때문에 logic의 오류가 발생가능성 있음  
    * cache[][] 로 만들어서 OK!!!  
    * (타) dp[i] = max((dp[i-2]+stair[i]), dp[i-3]+stair(i-1)+stair(i))  
    * 그러니까 i번째 가는 방법은 전전 (i-2) 계단에서 가거나, 전 (i-1) 계단에서 가거나  
    * 그런데 i-1 계단에서 가려면 1칸씩 중복 3번 갈 수 없으므로 i-2 계단은 밟을 수 없음  
    * 그러면 i-3 계단을 밟아야 함.  
    * 그러니까 i-2, i-3 계단에서 시작해서 i번째 가는 경우의 수만 존재하며  
    * dp[i]는 dp[i-2], dp[i-3] 을 이용해 점화식을 만들 수 있다.  
    * (결론) 2가지 방법 모두 괜찮다.  
27. (동적계획법: 10844 쉬운 계단수)  
    * (나:) int solve(int curPos, int preVal) \ret 가능한 계단수의 개수  
    * cache[][] 해서 진행하였는데 예제는 통과하였는데, 틀렸다고 나옴.  
    * 정답과 조금 비교해 보아야 겠음  
    * (타:) 나와 비슷한데,,,  
    * DP[N][L] = DP[N-1][L-1] + DP[N-1][L+1] 이고 L이 1~8 일때,  
    * DP[N][L] = DP[N-1][L+1] L이 0일 때,  
    * DP[N][L] = DP[N-1][L-1] L이 9일 때,  
    * 그러니까 나는 함수로만 계속 생각을 하고 있었는데,  
    * 수식으로 정리를 하는 것이 중요할 수 있겠다.  
    * N번의 계산으로 완료되므로 O(N) 임  
    * N = 50까지는 내 방법도 맞는데, 숫자가 더 커지면,,, 음...  
    * 내 방식도 맞는데, mod가 제대로 안되었다.음 수정해서 OK됨.  
    * 지금 생각해보니, 내 함수와 다른 사람 수식은 동일한 논리라 생각됩  
28. (동적계획법: 2156 포도주시식)  
    * (나: ) case1 = solve(n+1, numSeries+1)+WINE[n+1]  
    * case2 = solve(n+2, 1)+WINE[n+2];  
    * ret = max(case1, case2);  
    * 시간초과 -> 틀림  
    * (타: ) https://mygumi.tistory.com/98  
    * 100, 500, 2, 1, 4, 200 일 때 701 이 아닌 704가 나와야 함  
    * 2번 계속 안 먹을 경우 고려 필요함.  
    * 이런 케이스를 찾는 것이 중요하다고 생각됨.  
    * dp[n] = max(dp[n-1], dp[n]) 를 추가해줘야 한다는데,,,  
    * (타: ) https://limkydev.tistory.com/112  
    * 그러니까 0번 연속마심, 1번 연속마심, 2번 연속마심. 이렇게 풀면 됨.  
    * 여기서 0번 연속마심이 dp[n] = dp[n-1] 로 볼 수 있음.  
29. (동적계획법: 11053 lis)  
    * N = 1000 이므로 O(n^2)으로도 해결할 수 있다.  
    * for, for 2개를 이용해서 푸는데,,,  
    * DP[i] 에 대해서만 max를 구하여서 처음에 실패하였고,,  
    * 이후 모든 DP[0~i-1]에 대해서 max 구해서 OK 됨  
30. (동적계획법: 11054 BITONIC LIS)  
    * 순방향, 역방향 lis를 구한 뒤 더해서 구하면 됨  
    * (나: ) for, for를 2번 사용하였으나  2xO(n^2)  
    * (타: ) O(n^2)로 계산함  
    * 이런 식으로 index를 i, j 대신에 N-i, N-j 로 넣어주는 방식 사용  
    * if ( arr[N-i+1] > arr[N-j+1] )  
    * 초기 DP[0~N-1]도 1로 초기화해야 하는데, 이것도 같이 for문 1개에 넣을 수도 있음.  
31. (동적계획법: 2565 전깃줄)  
    * (나: ) 이것은 정답을 참고하였는데, 오른쪽 선에 연결된 왼쪽 선 번호 숫자 배열에서  
    * lis를 구하는 문제가 된다.  
    * answer = whole electric line number - num of lis  
32. (동적계획법: 9251 LCS)  
    ``` 
    if A[n-1] == B[m-1]
        lis(n, m) = lis(n-1, m-1) + 1
    else
        lis(n, m) = max(lis(n-1, m), lis(n, m-1))      
    ```  
33. (동적계획법: 1912 연속합)  
    * O(n^2) 으로는 불가능함  
    * 연속으로 더해가면서, 가장 큰 수를 찾으면 되는데,  
    * 더해나갈 때, 양수이면 계속 더해나가면 되는데,  
    * 음수가 되면 그 수는 제외하여야 하고, 새로 시작해야 한다.  
    * 음수 나오기 전에 max값, 그리고 이후 max값을 또 찾고. 그 중에서 max를 찾으면 된다.  
    * for문으로 1칸씩 움직이는데,  
    ```
    if (curSum + A[i] >= 0)
        maxSum = max(maxSum, curSum + A[i])
    else
        curSum = 0
    그런데, 한가지 확인할 부분은 최소한 1개의 값은 선택해야 한다는 것. 이 부분만 고려를 추가하면 됨
    ```   
34. (동적계획법: 12865 평범한배낭)  
    * backpack(int n, int amount, int weight)  
    * //n: 현재위치, amount: 선택한 배낭수, weight: 배낭무게, return: 최대가치   
    * (수정 참고:타인) v = bp(int n, w)  
    * v: 가치, n: 순서, w: 무게, n번째 선택/선택x 중 max를 취함    
    * 처음 공식에서는 amount까지 포함했었는데, 문제는 없으나 3차원 배열 cache가 필요하고  
    * 없이도 가능할 것으로 보임  
35. (Greedy: 1931 회의실배정)  
    * (my) 시작시간 순서로 정렬하고,  
    * pre.end <= curr.start 이면 +1,  
    * pre.end > curr.end 이면 pre = curr 한다.  
    * 로직은 문제 없는 것 같은데, fail이 발생함. ㅠㅠ  
    * 내 생각에 시작시간 == 끝시간 이 경우 고려가 안되어서,,,?  
    * 다시,,, sort 부분만 재고려해서 OK, pass됨. logic은 OK였었네.^^    
    * sort 기준1. i.start < j.start  
    * sort 기준2. i.start == j.start 인 경우, i.end < j.end  
    * (타) 끝시간 기준으로 정렬하고,  
    * 대신 끝시간이 간은 경우에는, 시작시간 오름차순 정렬  
36. (Greedy: 1541 잃어버린 괄호)  
    * (my) - 부호만 나오면 이후는 모두 -로 계산하면 될 것 같음.  
    * 그런데, 문자열을 parsing하는 것이 작업 필요함 
37. (Greedy: 13305 주유소)  
    * (my) 주유소가격을 pre 변수로 업데이트 해 나가면서,  
    * pre 변수값이 작으면 계속 그 값으로 진행해나가고,  
    * cur 변수값이 더 작으면 업데이트해 나감.  
    * 그런데, 틀림...? 어디서...? 예제는 맞았는데...  
    * price, station <= 1000 000 000 이므로 integer로 cover되나,,  
    * sum은 integer를 벗어나므로 long long으로 변경.  
    * 이때는 틀렸음. 왜...? 그런데, price, station도 long long으로 변경시 pass 됨  
    * int -2147483648 to 2 147 483 647 int로도 충분한데...  
    * 문제는,,, int x int 를 long long에 저장을 해야 해서,,, 이 부분이 문제!!!  
    * int로 하려면, (long long) 처리를 해야 함  
38. (1037: 약수)  
    * 처음과 끝을 곱하면 됨  
39. (2609: 최대공약수, 최소공배수)  
    * 1~ 작은수까지 증가하면서 두 수 모두 % == 0 인수를 찾음  
40. (정수 2971: 검문)  
    * (my) 최소값을 찾은 후, 1~최소값으로 for 돌면서, 전체 숫자에 대해 계산  
    * O(NxM) 임. 시간초과  (100 x 1,000,000,000)  
    * 잘못된 이유는,,, 6, 16이 있는 경우 10도 답이될 수 있을 것 같음.  
    * (my: 시간초과) 같은 수가 없다는 가정하에,,,  
    * max값을 찾고, 1~max 를 for돌면서, 전체 숫자에 대해 계산 -> 시간초과
    * (other)  
    * A % M = A - (A/M)*A  
    * B % M = B - (B/M)*B  
    * C % M = C - (C/M)*C  
    * B - A = (B/M - A/M) * M  
    * C - B = (C/M - B/M) * M  
    * 각 수의 차이의 최대공약수를 구한 뒤 > 약수를 구하면 됨  
    * 최대공약수 구하는 방법. GCD(a, b)  
    * a, b 둘 중 하나 0이면 exit  
    * GCD(a%b, b) 또는 GCD(a, b%a) 로  구하면 된다.  
    * 이제 약수 구해야 하는데,,, 여기서 발목!  
    * 1~최대공약수까지 loop돌면 안되고, 제곱근까지 구하고 n, 최대공약수/n > pass  
    * 그리고, i != Factor/i 두개 비교해서 겹치는 수는 포함안되도록 함  
41. (정수 11501 이항계수2)  
    * (n k) = (n! / k!(n-k)!)  
    * 예를 들어 (5 2) = (5x4 / 2x1)  
    * (n k) + (n k+1) = (n+1 k+1) 과 같다. 수식 풀면됨  
    * 점화식 이용해서 동적계획법으로 계산처리  
    * (n k) = (n-1 k-1) + (n-1 k)  -> 이것을 구현. cache 활용. OK!  
42. (정수 패션왕신해빈 경우의 수)  
    * (A + 1 ) * (B + 1) ... -1 이렇게 구할 수 있음. 중등수학  
    * (my) 구현시, map<string, int> MAP 선언해서,,, 종류가 나오면 it->second 1씩 증가하는 것으로 구현하였음  
    * (other) 나랑 비슷하게 했네...  
43. (정수 1676 팩토리얼)  
    * (my) 그냥 N / 5 했는데, 틀림. 5, 10 숫자있으면 뒤에 0이 붙을 것 같음.  
    * (other)  내 의도도 맞았는데,,, 25와 같은 숫자 5^2 제곱수  
    * 예외처리만 추가해주면 될 것 같음.  
    * 5^2, 5^3 예외처리 후, 통과됨.  
    * 나와 같이 무식하게 /125, /25 해줘도 되는데, 아래가 조금 세련됨.  
    ```
    for (i = 5; i <= n; i *= 5) {
        five += n / i;
    }
    ```  
44. (정수 2004 조합0의개수)  
    * (my) (N M) = n! / k!(n-k)! 따라서 나는 팩토리얼0의개수 func라할 때, func(n) - func(k) - func(n-k) 하면 될 것 같은데,,,  
    * 왜 틀렸을까...?  
    * 방향은 맞았음.  나는 5에 대한 것만 고려하였는데,,, (타) 2, 5 모두 고려. 이것 때문인가..? 둘다 고려해야 함.  
    * 그리고 long long 변수 사용필요. int를 쓰면 왜 안되는 걸까...?  
    * int를 쓸 때, 런타임에러 (integer overflow) 발생하는데,,,  
    * 내 생각에 for(int i=5; i<=N; i*=5) 여기서 i*=5 로직에서 i가 2,000,000,000 이면 범위를 벗어나므로  
    * int 보다는 long long이 안전하게 됨. int를 쓰려면, 이 부분 로직 수정이 필요.  
45. (스택 10828)  
    * (my) c++ stack STL을 사용했는데, 사실 문제의 의도는 아닌 것 같음.  
    * c언어로 구현한 다른 사람들 글을 보니, array를 구성하고, top을 이용해서 현재 위치를 가리킴. top은 초기값이 -1이고  
    * push하면 +1, pop하면 -1, empty면 top이 -1인지 체크  
46. (스택 9012)  
    * (my) 그냥 ( ) (이면 +1, )이면 -1 로 하되, ) 음수가 되면 false 처리하도록 하여, stack을 사용하지 않음  
    * (other) ( 이면 push, ) 일 때 top값과 일치하면 pop, 없으면 false, 짝 찾기에는 stack이 좋음.  
47. (스택 4949 균형잡힌세상)  
    * (my) getline을 사용해서 한 줄씩 read  
    * 그런데 틀림 나옴. 예제는 맞았는데,,,  
    * (other) ( ], [ ) 이런 경우 처리가 내가 빠트린 것 같음  -> OK
48. (스택 1874 스택수열)  
    * stack push할 때, 1~N까지 수를 모두 push할 때까지 시도해야 함  
    * stack이 비어있을 때, myStack.top()을 시도하면 문제가 생김...? empty체크  
    * stack.size() 만큼 비교시, pop을 하면 size값이 변경되므로 주의  
    * (타) 무조건 push를 해야하므로,,, push는 무조건 수행하고  
    * 그리고 top과 비교를 해서 pop을 결정   
49. (스택 17298 오큰수)  
    * (my) 스택에 넣고, 빼면서, 뺀 수는 vector에 넣고,,,  
    * 스택에 빼면서, vector의 upper_bound를 찾음  
    * N * logN 이라 생각했는데, 시간초과가 나왔음  
    * map을 이용하면 더 나으려나...?  
    * 정렬은 자동으로 수행할 수 있음. 그런데 찾으려면...?  
    * (문제 잘못 이해)  자기보다 큰 수 중 가장 왼쪽에 있는 수...  
    * 스택에 top 보다 입력값이 크면 그 수는 무조건 오큰수임  
    * (알고리즘) 무조건 입력은 스택에 넣음(이 때, index를 같이 저장하면 편리할 듯). 그리고, 다음 수와 크기 비교  
    * 다음수가 크면, 기존 값 제거, 다음수 넣음.  결과값 업데이트.  
    * 다음수 작으면, 스택에 또 넣음. 끝까지 가거나, 다음수가 크면,  
    * 기존 값 제거, 
50. (18258 큐2)  
    * 시간초과를 막기위해 개행을 '\n'로 표현하고, 7,8행을 선언해야합니다  
    ```
    cin.tie(0);
	cin.sync_with_stdio(false);
    ```  
51. (11866 요세푸스)  
    * 삭제를 해야 하는데, 어떤 자료구조를 써야 될지 고민.  
    * 삭제 쉬우려면, list를 써야함.  
    * list는 몇 칸 이동하는 것이 어려움. next, next로 이동해야 해서,,,  
    * queue 변경. erase사용.  
    * queue는 iterator를 쓸 수 없어서, deque를 사용.  
    ```
    order = (order + K -1) % size;
    myQueue.erase(myQueue.begin()+order);
    ```  
    * (타) queue를 선택. pop, push 하다가 마지막 순서에 pop을 수행함. 개수가 0이 될 때까지 진행.  
52. (1966 print queue)  
    * (나) BigOrderArray를 만들고 (4,3,2,1)  비교해서 현재 queue에 있는 값이 더 크거가 같으면 pop을 하지만 작으면 맨 뒤로 이동.  
    * 시간초과가 나옴.  
    * 논리는 문제가 없는데,시간을 단축시켜야 함  
    * 구조체 (순서 , 우선순위)  
    * (나: 2nd) BigOrderArray는 삭제하고, 매번 큰 값을 찾아서 비교  
    * OK. 맞았음  
    * (타: 우선순위큐) 
    * 우선순위큐를 하나 더 사용해서, 가장 우선순위 높은 값을 미리 찾아냄.  
    * 우선순위큐는 우선순위에 따라 정렬되므로...  
    * 그러니까 전체 큐에서 가장 높은 값을 찾는 로직이 줄어들게 됨.  
    * 그 외에는 동일함  
53. (1021 회전하는 큐)  
    * (나) 왼쪽 shift, 오른쪽 shift 어느 쪽이 빠른 지 정하는 방법 고민됨.  
    * 그냥 현재 뽑으려는 수가 전체 수의 절반보다 큰지, 작은지로 판단...?  
    * 무식하게 순서를 찾으면 된다.  
    * 예제 4개 통과, 마지막 실패.  
    * 이유는 order 계산시, +1 잘못 계산함. 수정. OK  
54. (5430 AC)  
    * (나) 풀었는데, 시간초과가 ㄴ왔다.  
    * 뒤집기를 비효율적으로 구현해서 그런 것 같다.  
    * vector에 현재값 넣고, 이를 push_front해서 O(2*N) 걸림  
    * 덱을 활용해서 시간복잡도를 향상시켜야 한다는데...  
    * (2nd 나)  뒤집기하면, flag에 표기. 제거 나오면 pop_front, pop_end수행  
    * 출력시,,, 순서/거꾸로 출력함.  
    * 그런데,, [] 입력받으면, 프로그램 수행시 dead가 발생하고 있음. ^^;;  
    * (타) 예외처리를 좀 꼼꼼히 해줘야 되는 문제입니다. 빈 배열을 입력받을 때라든지, 빈 배열에서 D나 R을 수행할 때 체크를 잘 해줘야 합니다. ㅠㅠ  
    * (3rd 나) print할 때, it+1 == myDeque.end() 해주었는데,,, 다른 방법으로
    * 그렇게 해서 통과. while(!myDeque.empty()) 로 작업함.  
55. (2630 색종이)  
    * printf 사용시 stdio.h 추가하지 않으면 컴파일 에러  
56. (1992 쿼드트리)  
    * error: reference to ‘array’ is ambiguous 컴파일에러  
    * array라는 변수는 다른 곳에서 사용되므로,,, 모호... MAP으로 변경. OK  
57. (1780 종이개수)  
    * F(x,y,len) > F(x,y+len/3, len/3),,,, 이렇게 표현했어야했는데,,,  
    * F(x,y/3,len/3)으로 잘못 표현해서 시간좀씀 ^^;;  
58. (1629 곱셈)  
    * 틀림. 
    * (Solve(a,b/2)%C * Solve(a,b/2)%C)%C -> Solve(a,b/2)%C를 temp로 저장  
    * temp * temp %C 하지만 여전히 틀림  
    * base condition을 잘못 적음.
    * if(k==0) return 1 을 적었어야 했는데,,,  (O)
    * if(k==1) return a 를 적었음.           (X)  
59. (11401 이항계수3)  
    * 페르마소정리 사용해야 함. A^-1 = A^(P-2) 
    * int x int 일 때는 long long으로 결과값 저장  
    * 그런데 메모리초과가 나옴.  
    * factorial을 재귀로 했는데, 메모리 초과가 나옴  
    * 그래서 다른사람과 같이 for문으로 1*2*....*N 으로 계산해서 통과함.  
    * (https://2ssue.github.io/common_questions_for_Web_Developer/docs/Javascript/4_prevent_recursion_stack-overflow.html#trampoline)  
60. (2740 행렬곱셈)  
    * 문제는 풀었고, cin >> temp; MAP[i][j] = temp 해왔는데, 바로  
    * cin >> MAP[i][j] 사용하기  
    * Array 크기 const int 또는 constexpr int 사용.  
61. (10830 행렬제곱)  
    * 매트릭스 연산해야 하므로 parameter받는방법이 중요함.  
    * vector<vector<long long>> 으로 parameter 선언. 초기화해주는 부분 확인  
    ``` 
    typedef vector<vector<long long>> matrix;
    matrix a(n, vector<long long>(n));  
    ```
    * 그리고 operator * 를 사용한 것도 참고
    참고:https://seokjin2.tistory.com/9  
    * 입력이 커서 long long 으로 받아야 함.  
    * B = 1일 때 A^B 리턴을 위해 matrix ret(n, vector<ll>(n)) 선언필요.  
    * ret는 단위행렬로 만들어서 B= 1일 때, ret = A*ret(단위행렬) 리턴하게 함  
62. (11444 피보나치6)   
    ```
    [Fn+1  = [1 1   * [F1
     Fn  ]    1 0]^n   F0]
    <--
    Fn+2 = Fn+1 + Fn
    Fn+1 = Fn+1 + 0*Fn  
    ```
    위와 같이 표기가능.  
    그러면 행렬제곱을 이용할 수 있다.  
    N = 17까지는 계산이 맞은 것을 보았는데, 1000일 때는 결과값이 틀렸다. 왜...?  
    * MOD = 10000000007 로 기입했는데,
    * MOD = 1'000'000'007 로 썼어야 했다. 0을 하나 더 쓴 것임 (실수)  
    * 또는 1e9 + 7; 쓸 수 있다.  
    * tip. over c++ initialize : matrix Result {{0, 0}, {0,0}};  
63. (6549 히스토그램)  
    * 틀림. 분할정복 (종만북 풀이)로 접근하였는데 틀림 (https://st-lab.tistory.com/255)  
    * left, right 값이 start, end에 도달하지 않은 경우,  
    * 어느쪽으로 이동할 지 방향설정할 때,  
    * 실제 left, right 값을 변경하기 전에 H[left-1], H[right+1]을 먼저 비교  
    * 그리고 방향 결정 후, left, right 값을 업데이트 하도록 함  
    * (타) while(lo < toLeft && toRight < hi) 인 조건1  
    * while(toRight < hi>) 인 조건 2  
    * while(lo < toLeft) 인 조건 3  
    * 이렇게 하면 됨.  OK. 이 방법과 내 방법 다르지 않음.  
64. (2261 가장 가까운 두 수)  
    * https://st-lab.tistory.com/256  
    * 1. x축 정렬  
    * 2. 분할하면서 개수 3개이하인 경우, brute force로 최소거리 구함  
    * 3. 왼쪽, 오른쪽 영역 완료한 후, 가운데 영역 추출 (mid -+ 최소거리 범위)    
    * 4. y 축으로 정렬  
    * 5. 맨 아래 점부터 y값 (y + 최소거리 범위 내 점 사이 거리 계산)  
    * 6. 최소거리, 가운데영역 최소거리 중 작은 것 추출  
    * 실수 : 제곱한다고 a^2 로 표현함 -> a * a로 수정함 (띄어쓰기)  
    * segment fault 가 발생함. 
    * pair<int, int> 대신에 struct Point {int x, int y} 변경 후,
    * segment fault 대신 시간초과 발생.  
    * 아마도 pair 사용하다가 실수해서, segment fault가 발생??? 잘 모르겠다.  
    * 계속 잘 안되어서  https://casterian.net/archives/92 복사해서 붙이니  
    * 성공하였다.  
    * 이후 다시 확인해서 성공하였다.  
    * int line = (MAP[mid+1].x + MAP[mid].x) / 2  
    * 실수한 부분은 x 축으로 크기 비교시 MAP[start~end] 값과 비교하는 것.    
    * error 나는 것은 vector<Point> v; v.reserve(end-start+1)  <- v.reserve(start-end+1) 잘못 했음  
    * 그리고 push_back을 하므로 reserve는 하지 않아도 됨  
 65. 1920 수 찾기 (이분탐색)  
    * binarySearch(int start, int end, int num) 그런데  
    * 시간초과가 나왔다.  
    * cache를 활용해야 할 듯.  
    * 그건 아니고, binarySearch 구현시 재귀 이용했는데, 이 때는 cache활용해야 하지만...  
    * while문으로 n/2 씩 찾아나가기로 함  
    * 그건 아니고,,, binarySearch에서는 mid보다 크고/작은 것으로  이미 start, mid에서 찾을 것인지, mid+1, end에서 찾을 것인지 알수 있기 때문에... 그것을 활용하면 됨  
    * (타)  while로 처리시에는 start, end를 mid값으로 치환해서 계속 찾아나가는 방식임.  
66. 10816 숫자찾기 2  
    * Upper, Lower 찾아서 계산. lo, hi 초기값 고민.  
    * lo = 0, hi = N (최대보다 +1 크게)으로  
    * while(lo != hi) 하면서 logic을 생각. 그대로 구현.  
    * logic은 다른 사람 logic을 참고하였음  
67. 1654 랜선자르기  
    * mid = (lo + hi)/2  while(lo!=hi) 마지막에  
    * return mid했는데, 틀림  
    * (수정) while(lo<=hi) 변경필요.  
    * return result로 변수 두기  
    * result = max(result, mid)  
    * (오류) 탈출되지 않고 무한루프  
    * 원인은... lo = mid+1 : ok, hi = mid로 했는데  
    * hi = mid - 1로 검색하는 것으로 수정 필요  
    * 왜냐하면 while에 = 조건이 추가되어서 나중에 mid = hi가 같아지면 탈출이 안된다. 그리고 mid일 때 woodNum이 작으므로 hi = mid-1로 하는 것이 더 reasonable하다.  
    * (오류2) 덧셈이 있어 overflow 때문에 long long사용 필요  
    * -> 예) 랜선 길이 2^31 - 1 에 N = 1이면 2개 랜선이면, int 범위를 넘어서므로  
    * 랜선의 길이는 2^31-1보다 작거나 같은 자연수
68. 1805 나무자르기  
    * 입력을 int, sum에만 long long 사용  
    * 나는 전부 long long 사용. -> 보완 필요  
69. 11066 공유기설치  
    * 집 사이의 거리가 일정하지 않다보니 적용하는데 어려움이 있다.  
    * 집 사이의 거리 최소값:1, 최대값: 마지막 - 처음집  
    * 집 사이의 거리가 mid 보다 크거가 같으면 -> 공유기 설치  
    ** previous house를 수정. 계속 해 나간다.  
70. 1300 k번째 수  
    * k번째 수가 S라고 할 때, S보다 작거나 같은 수가  
    * k보다 작으면 안된다.  
    * while(lo<=hi), lo=mid+1, hi=mid-1로 찾아나감  
71. 12015 가장 긴 증가부분수열2  
    * 틀렸습니다.  
    * upper_bound -> lower_bound  
72. 11279 최대 힙  
    * 시간초과 : priority_queue 사용 but  
    * printf 쓰기 때문에,,, 아래 호출필요
    ```
    cin.tie(NULL);
    ios::sync_with_stdio(false); 
    ```    
73. 11286 절대값을 말해요 (우선순위큐)  
    * struct cmp { bool operator()(int left, int right){}}  
    * 이것을 priority_queue 에서 활용  
74. 1655 가운데를 말해요 (우선순위큐)  
    * 최대힙, 최소힙 2개 사용  
    * 최대힙은 최소힙보다 크기가 같거나 1개 크다  
    * 최대힙의 top은 최소힙의 top보다 항상 작다  
    * mid값은 최대힙의 top이다.  
75. 11066 파일합치기 (동적계획법2)  
    * 처음엔 어렵다고 느꼈는데, 보고 나니 어렵지 않게 느껴짐.  
    * 2개씩 나누는 방법에 대해서 전부 해봐야 하므로  
    * 이 부분은 brute force + cache 활용. (시간단축)  
    * sum[end] - sum[start-1] 를 하기 위해 sum 배열을 0아닌 1부터  
    * 저장하는 것이 덜 헷갈림  
    * 실수1가지. start == end 에서는 return 0 인데, 처음에 cost[start] 로 하였음. -> 수정  
76. 1520 내리막 길 (동적계획법2)  
    * dx, dy로 표기. arr로 표기.  
77. 10942 펠린드롬 (동적계획법2)     
    * start, end 비교. 한 숫자씩 감소해가며 재귀. 재귀하지 않아도 됨.  
    * cache 이용.  
78. 2629 양팔저울 (동적계획법2)
    * solve(int order, int weight) 이렇게 만들 수 있음. 그런데,  
    * weight가 존재 유무는 어떻게 판단...?  
    * 그러면서 별도의 array에 weight를 저장해 둠.  이것으로 판단.
    * 또는 order 최대값일 때, weight값을 찾으면 됨 !!!
    * relocation truncated to fit: R_X86_64_PC32 against `.bss' 라는 오류 발생  
    * cache[40001][40001] 했을 때, 문제가 생김.  
    * cache[31][40001] 로 수정해서 문제 해제됨.
    * Basically, when you need more than
2GB of data (you can check it using the gnu size), you will have to use  
79. 2293 동전 1 (동적계획법2) 
    * 재귀를 이용한 방법 사용시, order, cost 2차원 배열. 메모리초과.  
    * 다른 사람들 가이드와 같이 dp[] 규칙을 찾아서 진행해서 통과.  
80. 7579 앱 (동적계획법2)  
    * cache[order][memory] <= 용량이 너무 커서, error  
    * 다른 곳에서 봤는데, memory 대신 cost로 변경...? 가능한가...?  
    * (타1) https://jaimemin.tistory.com/578  
    ** int maxMemory(int idx, int totalCost) <- 함수이름 배울 점  
    ** main에서 totalCost = 0 부터 1씩 증가해 나가면서  
    ** memory가 M 이상이면 totalCost 출력하는 방법이 인상적이었음.  
    ** 어려운 부분은  
    ** maxMemory(idx+1, totalCost - Costs[idx]) + Memory[idx] -> 여기서 왜 -Costs[idx] 하는지 이해가 잘 되지 않았음.  main에서 Cost값을 증가시켜 나가면서 maxMemory값을 찾고 있기 때문에, 그 때의 Cost 값 예를 들어 30일 때의 Max Memory를 찾는 것임. 그래서 
    maxMemory(0, 30) -> 이라면 30 totalCost를 줄여나가면서 memory값이 M 보다 크면 선택하는 방식이므로  :  OK  
    ** 런타임에러 : outofBounds 에서는 cache size를 +1 하지 않아서 발생함  
    * (타2) https://life-with-coding.tistory.com/316
    * (타2) https://degurii.tistory.com/155?category=755814  
    ** 타2가 메모리도 적게 쓰고, 속도도 빠르다.  
81. 1260 DFS, BFS  
    * 어떤 자료구조를 쓸 지 고민이 되었다.  
    * vector 2 dimensional 구조  
    * 그리고 가장 작은 수부터 이동하므로  
    * sort도 하였다.  
    ```
    1000 1 1000
    999 1000
    ```
    입력시, 잘못된 print 됨.  
    * Vertex가 중복될 수 있다는 내용이 있음. 수정함.  
    * 틀렸습니다. 나옴. ㅠㅠ  
    * https://blog.naver.com/hwasub1115/221183031039  
    ** cin 대신 scanf 사용 
    ** printf("\n") 대신 puts("") 사용 
82. 2606 virus  
    * vertex 개수 세는 문제  
    * return 을 개수로 하여 풀었음. global 변수도 가능.  
83. 2667 단지번호붙이기  
    * int DFS 리턴을 개수로 하였음. 이 때, ret = 1 해야함
    * ret = 0 으로 했을 때, 결과값이 계속 0 이 나옴. ^^;;  
    * arr[row][col] 과 Visited[row][col] 을 체크해야함  
84. 1012 유기농배추 (DFS, BFS)  
    * N, M 에 대한 경계 처리에 시간 소요됨.  
    * 이전 문제 N, N 으로 0<= <=N 으로 하였더니,,, 실수발생  
    * 배추심은 보드는 board[][] 로...  
    * https://bitwise.tistory.com/70  
85. 2178 미로 탐색 (DFS, BFS)  
    * 최단거리 찾기. BFS 를 활용.  
    * (나) depth 정보를 포함하는 struct 를 구성해서 풀려고 하였다. 그래도 된다. struct{ {int x, int y}, int depth}  
    * (타) depth 정보를 별도의 Array로 선언해서 정보 업데이트 해 나감. 동일한 방법  
    * 실수. Board, Visited[next_row][next_col] 썼어야 했는데, row, col 로 잘못 씀.  
    * 디버깅은 Visual Studio 도움이 많이 됨. F10  
    * queue pop한 값을 이용해서 row, col 업데이트 필요했음  
    * board index가 1부터 시작함. 그리고 line값은 0부터 string 값을 취해야함. 이런 부분 실수를 줄이기  
    * (메모리초과) 실패 발생함  
    ** 방문체크를 넣으면서 해야 함.
    * (타) https://sihyungyou.github.io/baekjoon-2178/  
    ** 여기서보면, string line 을 배열로 구지 변환하지 않았음. int Board[int][int] 대신에 string Board[]를 썼음. 이렇게 할 수도 있음  
 86. 7576 토마토 (DFS, BFS)  
    * 0인 곳은 이동하고 1로 값 변경. -1은 이동 못함.  
    * 1) !Visited, 0인 곳으로 이동 모두 하고
    * 2) 마지막으로 전체 1로 cover되어 있는지 체크  
    * 그런데 1, 1 부터 시작하는 것이 아니다.^^;;  
    * 그래도 1, 1부터 차례대로 시작하자. 기준필요.  
    * for문 2번빠질 때, break 2번 해줘야 함  
    * 예제입력3번이 틀림. 이제 이해함. 여러 개 토마토. 동시에 진행이 가능함.^^ 대~박.  
    * (타) 참고하다가,,, 1인 토마토를 먼저 Queue에 모두 넣고 시작하는 것으로 진행에 idea찾음. 동일하게 마음에 드는 것을 찾지는 못했으나... 그렇게 해서 solved  
    * 어짜피 maxDepth, Depth 는 이전 Depth + 1이므로 Queue에 들어간 Depth는 0 이고... OK 이해가 됨.  
    * 예제는 모두 통과했는데, 15%에서 틀림 표시됨  
    * 어디서 틀린 것인가...?  
    * (타) https://letsbegin.tistory.com/27  
    이것과 내 idea가 유사  
    * 운좋게 다시 코드 보고 찾음. 이전에 if(col!=M+1) break 부분이 있었는데 이 부분이 myQueue.empty()로 변경되면서 제거되었었어야 했음. 삭제 해서 통과됨.  
87. 7569 토마토2 (3D)  
    * 3D로 height만 변경했음  
88. 1697 숨바꼭질  
    * 그렇게 어렵지 않음. pop_front 와 K 가 같으면 탈출  (항상 해를 찾을 수 있다고 가정)  
    * BFS 로 찾음. 가장 빠른 최단거리  
    * bool valid(int n) 를 만들어서 경계, 조건 처리 배울 점 (https://chanhuiseok.github.io/posts/baek-14/)  
    * depth도 같이 자료구조로 처리함. 별도 배열잡지 않음.배울점.  
89. 2206 벽부수고 이동하기 (DFS, BFS)  
    * 최단경로는 알겠으나, 벽부수는 것? 이것은 어떻게 ??  => 상태정보를 저장해야 하는 부분이 필요!!!
    * 벽 부숨/안부숨 고려를 위해  
    * Visited[][][] 3차원으로 만들어서 현재상태에 대한 정보를 제공해야 된다.***  
    * Visited의 return을 depth로 해도 되지만, 별도의 Depth[][]를 잡아서 이전처럼 구해도 된다.  
90. 7562 나이트 (DFS, BFS)  
    * 너무 쉽다고 생각하고 진행했는데, 틀렸음. ㅠㅠ  
    * Visited를 초기화 누락됨.  그런데 또 틀림.  
    * myQueue도 초기화가 필요하였음.  
    * Testcase가 여러개인 문제의 경우에는 변수 초기화가 중요함  
91. 1707 이분그래프 (DFS, BFS)  
    * 내 생각에, circle이 없으면 될 것 같음  
    * circle 유/무는 어떻게 판단...?  
    * 다른 사람들 글을 보면, Red, Blue로 서로 다른 색으로 칠해 나감  
    * 이후 인접한 Vertex가 서로 다른 색으로 칠해져 있는지를 체크함  
    * 이상하게 시간이 오래 걸렸다. 푸휴. 어려운 문제 아니었는데...  
    * Adj를 vector<vector<int>> 로 작업했다. 간단하게 하려고.  
    * (타) vector<int> Adj[] 이렇게 작업하는 경우도 많았다.  
    * for(int v=1; v<V; ++v) 로 순회하면서 BFS(v)를 수행할 때 Visited를 체크하는 것이 많았다. BFS 내부에서 check보다 나은 듯  
92. 1753 최단경로 (최단경로)  
    * 메모리초과가 나왔다. 다익스트라 보고 나서 구현하였는데...  
    * 
    ``` 
    vector<vector<VERTEX_T>> Vertex;
    vector<int> ShortestPath;
    priority_queue<VERTEX_T, vector<VERTEX_T>, compare> pq;
    ```  
    * 아마도 pq쪽 logic에서 잘못되어 메모리초과 발생한 것으로 보임  
    * 가지치기 추가.  현재 weight가 ShortestPath값보다 클 경우 skip.  
    * 그래도 메모리초과  
    * 가지치기 추가. 새로 간 점에서 기존 ShortestPath보다 짧으면 update하고 pq에 push함. 이번에 시간초과  
    * Dijkstra 함수에서 처음에 나는 Adj에 대해서 모두 pq에 push를 했는데,  
    * (타) 다른사람들은 처음에는 시작점만 push를 했다. 이 부분이 차이가 있다. 해도 되지 않음. 시간 초과.  
    * 나는 push할 때, neighbor를 push하지 않고 Vertex를 다시 push한 것으로 보인다. 아무튼 이부분 실수. 확실하게 헷갈리지 않게 변수 선언이 중요  
    * (타) 참고:https://jaimemin.tistory.com/555  
    * 다른 사람한 것 참고해서 품  
93. 1504 특정 최단경로 (최단경로)  
    * 아이디어가 딱 생각나지 않음.  
    * 거쳐서 가는 순서가 정해진 것도 아니고, 그러면 거치는 순서 brute force로 돌아야 하는데,,, 한다고하면...  무식하게는 가능할 듯  
    * 1 > A > B > 4, 1 > B > A > 4 이렇게  
    * 그런데, 이경우 1 > A, A > B, B > 4 는 모든 점에 대한 모든 점의 최단거리를 구하는 것이 필요함. 그러니까 Dijkstra를 각 정점에 대해서 돌리면 구할 수 있음  
    * 다른 사람 아이디어 보자.  
    * 아... 2개 점으로 고정되어 있었다. 그러니까 위 2가지 경우에 대해서 고려하면 된다. 내가 아이디어 거의 맞추었다.  
    * 구현시, 틀렸습니다. pq 초기화했음에도...  
    * -1 조건만 다시 생각해줌. --> 통과됨!!!^^  
    * distance[][] 에서 path가 하나라도 1 > A > B > N 끊어져있다면, 실패이므로..  OK  
    * (타) dijkstra return 형이 vector<int> 로 distance를 리턴하는 것도 한 방법  
94. 1591 미확인도착지 (최단경로)  
    * 문제 이해가 어려움. 
    * 문제 이해함. 1504와 같이 접근하면 될 것 같음  
    * 1 > A > B > 목적지후보, 1 > B > A > 목적지후보 중 min을 취함  
    * (에러) VERTEX[] 만들 때, bidirectional 이므로 양방향 저장 필요!!!  
    * (에러) 초기화 변수 필요. VERTEX[], candidate  
    * (타) 나와 비슷한 코딩 : https://yabmoons.tistory.com/390  
    * (타) freopen("Input.txt", "r", stdin);  참고. 파일 read  
95. 11657 타임머신 (벨만포드)  
    * 그런데, 출력초과가 나와 당황  
    * 나만 그런 것은 아님  
    * (타) https://zangzangs.tistory.com/82  
    * 거리배열의 자료형을 long으로 선언 필요. 
96. 11404 플로이드와샬  
    * 같은 경로가 중복될 수 있고 이 때는 최소값으로 갱신 필요  
    * memset(Adj, MAX_COST, sizeof(Adj)) 는 초기화가 잘못 수행됨. 그래서 for문으로 초기값 설정함  
    * 런타임에러 : _Distance[] 를 2차원 vector로 선언시, [][] 부족한 메모리할당  
    * 다수정했는데, 이제는 틀림 나옴.  
    * (타) http://melonicedlatte.com/algorithm/2018/07/09/011740.html  
    ** 배울점: vertex, edge, wegitht, from, to 와 같이 이해쉬운 변수 사용  
    * (타) https://hibee.tistory.com/134 나와 비슷한데, 나는 왜 95%에서 틀림 ?  
    ** MAX_COST 값이 987654321 이고 나는 100001 이었다.
    * (타) https://applemango2021.tistory.com/27 에보면 98%에서 틀렸씁니다.  
    ** 하나의 거리에 최대 10만의 값이 나올 수 있기 때문에, 두 도시가 여태까지 한 번도 연결되지 않았음을 표현하기 위해서는 10만 x N(최대 100)인 1000만 초과로 해주는 게 좋다. 
97. 10217 KCM Travel  
    * Dijkstra를 활용함. 그런데, Cost, Time을 고려해야 하므로  
    * 기존 1차원 -> 2차원 DP 배열을 사용  
    * 시간초과 발생하였음  
    * -> Compare를 잘못 설정함. 오름차순, 내림차순 실수함.  < 내림, > 오름  
98. 1956 운동  
    * Floyd 알고리즘 사용.  
    * circle이므로 Adj[i][j] 존재, Adj[j][i] 존재하면 circle. 그 중에서 최소 찾기  
    * 만약 Distance를 INT_MAX로 초기화할 경우에는 + 연산시 overflow 발생하므로 미리 체크해주는 것이 필요함. Distance[from][via], Distance[via][to]가 INT_MAX가 아니어야함. 
    * INF = 987654321 로 하면은 overflow가 발생하지 않죠...? 그래서 better.  (종만북)  
99. 3273 두 수의 합 (투포인터)  
    * sort(Arr, Arr+N) 방법도 있음 (배움)  
    * sort(Arr.begin(), Arr.end()) 이렇게만 했었음  
    * while(left!=right) 또는 while(left<=right)도 괜찮음  
100. 2470 두 용액 (투포인터)  
    * 틀림. sum = 0일 때 break 추가 ? 틀렸음 ^^;  
    * 다시 고려해서 OK. 어렵지 않음  
101. 1806 부분합 (투포인터)  
    * left, right 0부터 시작  
    * S > 0 이 조건이 중요!!! ㅑㄹ
    * left, right 에서 low, high로 변경  
    * (타) https://jaimemin.tistory.com/1105  
    * sum += Arr[++high] 이지만 sum -= Arr[low--] 이다.  
    * length를 먼저 계산  
    * (큰 실수) sum==S 이면 break하는 부분이 실수였음  
    * length가 가장 적은 것을 찾아야 하므로 계속 loop 돌아야함  
102. 1644 소수의합 (투포인터)  
    * 소수찾은 후, 투포인터 적용. 소수 무식하게 찾으니 시간초과 나옴.  
    * (타) https://debuglog.tistory.com/157  
    * 에라토스테네스체 방법 이해하는데 시간 소요함 (체!!! 강조해서 이해)   
    * (나) 실패 !!! Out of bounds. 배열 범위를 벗어남.  
    * 주의 필요함
    * (너무중요!!!***)(타) 소수가 아예 없을 경우의 수를 생각하지 못해, outofbounds 런타임 에러가 계속 나타났다. https://velog.io/@kpg0518/%EB%B0%B1%EC%A4%80-1644%EB%B2%88-%EC%86%8C%EC%88%98%EC%9D%98-%EC%97%B0%EC%86%8D%ED%95%A9  
103. 1405 냅색 문제 (투포인터)  
    * 어렵게 느껴짐. 무식하게 O(2^N), DP로도 10^9 라서 메모리적용X  
    * N/2로 나누어서 무식하게 적용.O(2^N/2)  
    * (타) https://technicolour.tistory.com/8  
    * ans = L.size() + R.size()  
    * L을 for문을 돌면서 이분탐색(upper_bound)를 통해 R에서 C-L[i]값이 몇번째(pos)에 있는지 알아낸다.  
    * R[pos]까지는 가방에 넣을 수 있기 때문에 ans+=pos; 해 준다.  
    * 아무것도 넣지 않는 경우 ans+=1; 해 준다.  
    * upper_bound가 헷갈려서 다시 해본다.  
    * 찾건, 안 찾건, 개수 pos를 계산해야 한다. OK!!!  
104. 12852 1로 만들기  
    * 1로 만드는 최소해를 찾는 것뿐 아니라 배열도 같이 구해야 한다.  
    * DP를 이용해서, Before[] 배열과 같이  
    * 틀림! why? 
    * 원인 : i % 2 == 0 으로 비교해야 하는데, (i-1) % 2로 비교하였음  
    * 수정 후 10 입력 기대값 10 > 9 > 3 > 1 그런데,
    * 10 > 5 > 4 > 2 > 1 이 출력됨. 수정 필요.  
    * 원인 : -1 에 대한 처리도 같은 %2 for문 안에 들어가야 함. 나는 -1 for문 먼저 돌리고 %2 for문을 돌렸음. > OK  
105. 14002 lis4  
    * O(n^2) 으로 계산  
    * 틀림. 예제는 맞았는데,,,  
    * https://yabmoons.tistory.com/578 참고해서 통과함  
    * 비슷해 보이는데,,, 예제케이스가 더 많으면 좋을 텐데, 적은수의 예제에서는 문제를 찾기가 어려움  
    * 아마도 i에 대해서 LIS, DP를 매번 초기값 업데이트를 하는데, 나의 경우는 그 부분이 초기에 1번 이루어지고  
    * 길이비교 >= 대신 DP[j]+1 > DP[i] 비교. 이 부분이 차이남. => 영향x. 그 외에 same ->우선 진도 나감 gogo  
    *
    ```
    질문 글을 찾아보니 반례 케이스 ( 7 / 1 6 2 4 5 3 7 을 입력했을 때 5 / 1 2 4 5 7 이 나와야 한다 )가 있어서 넣고 돌려보니 실패 확인. 
    ```
106. 14003 lis5  
    * O(nlogn) 으로 upper_bound로 풀어봄? 틀림  
    * https://yabmoons.tistory.com/560 참고해서 진행함  
    * 72% 에서 틀림
    * 반례:https://www.acmicpc.net/board/view/57791  
    * 
    ```
    10
    10 9 8 7 6 5 4 3 2 1
    을 넣은 경우 1~10중에 값이 하나가 나와야하는데 필자분의 코드는 0이 나옵니다.
    ```
    * 그렇다. 나도 동일하다.  
    * for문 역으로 돌 때, 초기값 N-1인데, N으로 설정함.  
107. 9295 LCS2  
    * LCS2 DP를 구한 뒤에 역으로 찾아가기  
    * string, char [ ]로 변환해서 작업했는데,,,  
    * (타) https://huiung.tistory.com/95  
    * 입력은 string으로 받고, DP[i-1] 해야 하므로, string[i-1]로 비교  
    * DP는 1부터 index~ data는 stack<char>로 저장  
    * ans도 string으로 += a[i]로 하는 사람있음  
    * 나의 경우 배열이라서 0을 또 체크해야 하는 번거로움있었음  
108. 2618 경찰차  
    * 1번, 2번 경찰차랑 사건사이의 거리를 매번 비교하면서  
    * 가까운 것으로 진행하면 될까...? 너무 간단 ?  
    * 그 경우 O(2^n) 이다. DP를 활용해서 줄인다고 생각!  
    * DP[Police1][Police2] = distance로 정의내리는 것 중요!  
    * https://yabmoons.tistory.com/644 참고  
109. 13913 숨바꼭질  
    * BFS 로 푼다.  
    * https://jaimemin.tistory.com/584 (꾸준함) 참고   
110. 9019 DSLR  
    * BFS로 푼다. 13913과 유사  
    * https://jaimemin.tistory.com/654 (꾸준함) 참고  
111. 11779 최소비용2  
    * 가중치>=0 -> dijkstra  
    * Parent 이용.  
    * https://yabmoons.tistory.com/392 (얍문) 참고  
    * 시간초과 발생  
    * 아래 내용 추가함 // 최단거리가 아닌 경우 스킵합니다.   
    ```
    if (Dist[Cur] < CurCost)
            continue;
    ```  
    * https://blog.naver.com/ndb796/221234424646 (나동빈)  
112. 11780 플로이드2  
    * 플로이드 + 경로  
    * https://yabmoons.tistory.com/441 (얍문) 참고  
113. 11725 트리의 부모찾기  
    * 고민하다가 찾아봄. DFS를 이용. 양방향으로 입력.  
    * DFS로 돌면서 parent[] 배열을 만들어냄  
    * (꾸주함) 참고
114. 1167 트리의 지름   
    * 트리를 만들고, top까지의 거리가 가장 긴 2개를 취한다.(나)  
    * (타) 1번에서 가장 먼 점(a)을 선택. a에서 가장 먼 점(b) 선택.  
    * 2번을 통해 a-b 거리를 구함  
    * https://jaimemin.tistory.com/812 참고  
    * https://donggoolosori.github.io/2020/10/12/boj-1167/  
115. 1967 트리의 지름  
    * 1167과 똑같은 것 아닌가...? oo 입력포맷만 다름  
116. 1991 트리 순회  
    * struct Node {int left, int right}  
117. 2263 트리 순회  
    * (타) https://donggoolosori.github.io/2020/10/15/boj-2263/ (동굴오소리) 참조  
    * PostOrder의 마지막값이 root라는 것은 알았는데,,, 그이상 진도x  
    * 마지막에 GetPreOrder( , , , postEnd - 1) <- postEnd로 작성. -1 중요!!!  
118. 5639 트리 순회  
    * 전위 맨 처음은 root, left, right를 나누고, left는 다시 root/l/r로  
    * 재귀를 이용해 tree를 구성 -> PostOrder 구하기?  
    * 바로 postOrder를 출력하는 방식 또는 Tree만들고 PostOrder 출력  
    * (타1) https://jaimemin.tistory.com/1345  
    * (타2) https://kyunstudio.tistory.com/172  
    * 입력받는 부분 EOF도 어렵고,,, 무한루프 도는 것도 찾기 어렵네...  
    * EOF = Ctrl + D
    * scanf("%d", Tree+idx) > 0 이렇게 하거나,,,
    * cpp 에서 이렇게 하거나,,, https://stackoverflow.com/questions/201992/how-to-read-until-eof-from-cin-in-c/36978839   
    ```
    while(cin >> val)
    {}
    ```  
119. 4803 트리  
    * 트리의 개수, 트리가 아닌 개수.  
    * circle 유무로 트리 유/무 판단. 
    * DFS를 돌린다. 각 Virtex마다. 그리고 Visited를 통해 그룹 수를 셀 수 있다.  
    * 간선은 Virtex -1 개이면 트리가 된다.  
    * (타1) https://jaimemin.tistory.com/586  
    ```
    int numOfVertex(int nodeNum)
    {
        int cnt = 1;
        ...
        return cnt;
    }
    int numOfEdge(int nodeNum)
    {
        int cnt = Tree[nodeNum].size();
        ...
        return cnt;
    }
    ``` 
    * (타2) https://myunji.tistory.com/350  
    ** 정점으로 DFS하다가 Visited를 다시 만나면 circle이 아니게 된다.  
    ** DFS시에 한 방향으로만 이동  
    * (나) numOfVertex 구현시 오류..  
    ** Visited[nodeNum] = true를 빠트렸다. ^^;;  
    * Out Of Bounds에서 틀렸습니다. ㅠㅠ  
    * for문에서 NumOfEdge(i) 입력해야 하는데 1을 입력함. ㅠㅠ  
120. 1717 집합의 표현  
    * vector<int> Graph[MAX] 로 선언해서 그룹을 만들고  
    * 포함 유/무를 판단하면 될 것 같음  
    * https://js1jj2sk3.tistory.com/16 (종신1재정2시경3)  
    * unite, find 2개 함수를 만든다. parent 부모같은지...  
    * 시간초과가 발생 !!!  
    * int Find(int x)  
    ```
    int Find(int x)
        else
            Parent[x] = Find(Parent[x]); <--- 항상 Parent 업데이트
            return Parent[x]
    ```    
121. 1976 여행가자  
    * 동일한 disjoint set으로 풀자.  
    * https://js1jj2sk3.tistory.com/17  
    * cin 에서 고생함.  
122. 4195 친구 네트워크  
    * 무식하다면, 처음 2친구를 네트워크로 묶고, 이후 계속 처음부터 반복하며  
    * 같은 네트워크에 있는지 찾을 수 있다. But ...  
    * Map을 이용하면 효율적!!!(참고)  
    * (타) https://kuklife.tistory.com/40  --> 이게 좀 더 쉬운 것 같음 
    * (타) https://imnotabear.tistory.com/111 (어흥)
    * Out of bounds 발생!!! 
    * 2e5 = 200000 임. 200002 로 MAX설정시 통과!!!  
123. 20040 사이클게임  
    * (타) https://kth990303.tistory.com/32 참고  
    * 이미 같은 집합인데, 또 연결하면 사이클이 됨  
124. 1197 MST  
    * 주의! 가중치가 가장 작은 것으로 정렬  
    * 크루스칼 알고리즘 사용  
125. 4386 별자리 만들기  
    * 크루스칼, 단, 유리수 계산 필요  
    * 점이 주어져서 거리는 내가 계산해야 하고,,, Edge도 정해져있지 않아서,,,
    * 어려움이 있다.  
    * (타)https://steady-coding.tistory.com/119  
    * (타)https://yabmoons.tistory.com/404  
    * N = 100이므로 전체 edge를 모두 구하는 방향으로 진행  
    * ㅠㅠ i, j indexing에 오류있었음. 정신차려~  
 126. 1774 우주신과의 교감  
    * 크루스칼. 이미 연결된 것은 union하고 남은 것들을 가지고 진행!  
    * 얍문 글 보니 동일. 진행해보자.  
    * Out of bounds ㅠㅠ 입력범위 고치고  
    * 틀렸습니다. -> printf 표현 .2f 로 double 출력. OK  
    * 
    ```
    cout << fixed;
    cout.precision(2); 로 해서 소수점 이하 2자리 출력가능. OK
    ```   
127. 2887 행성터널  
    * 2차원 점의 개수가 100,000으로 늘어났다. 시간제한/메모리초과!!!  
    * 크루스칼 적용하면 될 것 같은데,,, 모든 점 대응?  
    * https://js1jj2sk3.tistory.com/27  
    * '그러면 다음으로 떠오르는 생각은 정렬을 통해 N * log n 으로 해결할 방법을 찾아보는 것이다.'  
    * https://dev-jk.tistory.com/29  
    * sort(Edge)가 잘안되었음. bool compare(~~~) 선언 해결  
128. 17472 다리만들기2  
    * 다리 최소값 찾기. 그런데, 땅모양이 점이 아니라서 연결가능한 부분이 여럿임. 그 중에서 선택해야 함.  
    * 섬 영역을 지정해야 하고, 그룹으로 나눠야 하고,,,  
    * 1번 섬과 2번 섬을 연결하는 것 x축, y축으로 찾아야 하고,,,  
    * 모든 것들을 Edge에 넣고 크루스칼 돌린다. 구현 난이도  
    * (타) https://yabmoons.tistory.com/292  
    * (타) https://dev-mb.tistory.com/269
    * (타) https://seokeeee.tistory.com/83 <-- 큰 도움
    * 참고안했으면 못풀뻔... 재미있는 문제. 구현문제. 논리flowchart 그리고 풀어야 가능함. 함수 이름 등 배울 수 있음  
129. 15681 트리와 쿼리  
    * 감이 안옴. 트리 + DP라고 함  
    * https://kibbomi.tistory.com/206  
    * 정점, 간선 100,000 이라 O(n^2)으로 어려움  
    * https://imnotabear.tistory.com/226 참고  
130. 2213 트리의 독립집합  
    * https://kibbomi.tistory.com/63  
    * https://cocoon1787.tistory.com/482 <- 이것도움  
    * 따라서 했는데, 계산값이 140 아닌 130이고  
    * 1357 이어야하는데, 1356임. 어디가 잘못?  
    * index가 1부터 시작하는데,,, V를 0부터 저장하였음  ---> 중요!!!  
131. 2533 사회망 서비스  
    * https://comyoung.tistory.com/41 참고  
132. 2166 다각형 면적 (기하)  
    * https://kibbomi.tistory.com/204  
    * https://imnotabear.tistory.com/332 (어흥)   
    * 외적 이용 (outer product) 
    * 마지막에 abs를 해줘야 함  
    * 틀림 -> int -> long으로 변환  
    * 100,000 * 100,000 = int 범위 벗어나므로  
133. 11758 ccw  
    * 외적이용  
    * https://steady-coding.tistory.com/217 (느리더라도)  
    * 신발끈공식 (안잊어버림)  
    * https://degurii.tistory.com/47 외적 자세히 ccw  
134. 17386 선분교차1  
    * 평행하지 않으면 교차?  
    * 안만나는 경우도 있음. ^^;;  
    * https://lifedev0.tistory.com/177  
    * https://hsdevelopment.tistory.com/390  
    * CCW(A,B,C)*CCW(A,B,D) <=0, CCW(C,D,A)*CCW(C,D,B)<=0이면교차
    * a<=d && c <=b 여기가 이해가 잘안됨. 
        ** a가 d보다 큰 경우 만날 수 없음. -> 이해됨  
        ** A점이 a가 보장될 수 있나...?  C가 a일 수도 있지 않나?  
    * https://edukoi.tistory.com/149 다른 정답 확인!   
    * 오케이!!! a<=d.... 이부분은 필요x -> 17387에서 필요했음  
    * https://johoonday.tistory.com/107  
    * https://hyeo-noo.tistory.com/108 CCW를 양쪽으로 하는 이유... 이 글이 가장 도움이 됨 
    * https://cocoon1787.tistory.com/488?category=831127 이것이 가장 도움이 됨. coccon!!!! 
135. 20149 선분교차 3  
    * https://velog.io/@jini_eun/%EB%B0%B1%EC%A4%80-20149-%EC%84%A0%EB%B6%84-%EA%B5%90%EC%B0%A8-3-Java-Python 도움이 됨  
    * 1. CCW(A,B,C)*CCW(A,B,D)<=0 && CCW(C,D,A)*CCW(C,D,B) <=0 교차  
    * 2. CCW(A,B,C)*CCW(A,B,D)==0 && CCW(C,D,A)*CCW(C,D,B) ==0 에서 
    네 점이 한 직선상에 존재  
    A < D && C < B 이면 교차 (A <= D && C <= B) 
    * 교차점 구하기  
    ** i. CCW(A,B,C)*CCW(A,B,D)<=0 && CCW(C,D,A)*CCW(C,D,B) ==0 또는 CCW(A,B,C)*CCW(A,B,D)==0 && CCW(C,D,A)*CCW(C,D,B) <=0 인 경우 교차. 교차점 구하기 (기울기무한/직선의방정식) 
    ** ii. CCW(A,B,C)*CCW(A,B,D)==0 && CCW(C,D,A)*CCW(C,D,B) ==0 인 경우, 
    *** iii. CCW(A,B,C)==0 && CCW(A,B,D)==0 && CCW(C,D,A) == 0 && CCW(C,D,B)==0 이면 한 직선상에 모두 4점 존재
    **** (ㄱ) A < D && C < B 이면 교차 많음
    **** (ㄴ) B < C || D < A 이면 교차 X
    **** (ㄷ) A < D && B == C 또는 A == D && C < B 이면 교차 1개  
    *** iv. (ㄹ)그 외는 한 점에서 만나는 경우. (A, B가 C, D중 어느 점과 만나는지 확인) 
    * https://kangwlgns.tistory.com/14  
    * https://velog.io/@jini_eun/%EB%B0%B1%EC%A4%80-20149-%EC%84%A0%EB%B6%84-%EA%B5%90%EC%B0%A8-3-Java-Python  
    * 풀었는데,,, 최종적으로는  
    * https://cocoon1787.tistory.com/489 풀이가 가장 Best  
 136. 2162 선분그룹  
    * 유니온파인드 + CCW 선분교차  
    * https://onecoke.tistory.com/entry/BOJ-%EB%B0%B1%EC%A4%80-2162-%EC%84%A0%EB%B6%84-%EA%B7%B8%EB%A3%B9-%EC%84%A0%EB%B6%84-%EA%B5%90%EC%B0%A8  
    * 틀렸습니다. 나올 때, 좌표를 double 형으로 변경   
    * https://hyeo-noo.tistory.com/118 를 참고하기로 함  
    * 다시 처음 onecoke를 참조. 시간초과는 parent[p] = getParent(parent[p]) 추가해서 해결  
    * (해결) unite(x, y) 에서 px = parent[x] <- getParent(x)로 수정
 137. 7869 두 원  
    * https://cocoon1787.tistory.com/491  
    * https://ca.ramel.be/140
    * 교차점 P(x,y)를 구한다.  
    * r1^2 - x^2 = rx^2 - (d-x)^2  
    * x = (r1^2 - r2^2 + d^2) / 2d  
    * 틀렸습니다.  
    * getDistance에서 입력이 int -> double로 변경필요  
138. 1069 집으로  
    * https://sosoeasy.tistory.com/292  
    * 
      





 







    


    
    


    




    



 

    

    




    






    

 
    









        




    




    





#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 510;
int N = 0;
int M = 0;
vector<int> Tree[MAX];
bool Visited[MAX];
bool Passed[MAX];

void Input()
{
    while(1)
    {
        cin >> N >> M;
        if(N == 0 && M == 0)
            break;
        for(int i=0; i<M; ++i)
        {
            int node1, node2;
            cin >> node1 >> node2;
            Tree[node1].push_back(node2);
            Tree[node2].push_back(node1);
        }
    }
}
#if 0
int NumOfVirtex(int nodeNum)
{
    int cnt = 1;
    Visited[nodeNum] = true; // <--- 이게 빠졌다.

    for(int i=0; i<Tree[nodeNum].size(); ++i)
    {
        int next = Tree[nodeNum][i];
        if(!Visited[next])
        {
            cnt += NumOfVirtex(next);
        }
    }
    return cnt;
}
#endif
int NumOfVertex(int nodeNum)
{
        int cnt = 1;
        Visited[nodeNum] = true;

        for (int i = 0; i < Tree[nodeNum].size(); i++)
        {
                 int next = Tree[nodeNum][i];
                 if (!Visited[next])
                         cnt += NumOfVertex(next);

        }
        return cnt;

}

// 양방향이라서 x2
int NumOfEdge(int nodeNum)
{
    int cnt = Tree[nodeNum].size();
    Passed[nodeNum] = true;

    for(int i=0; i<Tree[nodeNum].size(); ++i)
    {
        int next = Tree[nodeNum][i];
        if(!Passed[next])
        {
            cnt += NumOfEdge(next);
        }
    }
    return cnt;
}

void Print(int count)
{
    if (count == 0)
        cout << "No trees." << endl;
    else if(count == 1)
        cout << "There is one tree." << endl;
    else
        cout << "A forest of " << count << " trees." << endl;
}


void Solve()
{
    //Input();
    int T = 1;
    while(1)
    {
        // 꼭 비워줍니다.
        for(int i=0; i<MAX; ++i)
            Tree[i].clear();

        cin >> N >> M;
        if(N == 0 && M == 0)
            break;
        for(int i=0; i<M; ++i)
        {
            int node1, node2;
            cin >> node1 >> node2;
            Tree[node1].push_back(node2);
            Tree[node2].push_back(node1);
        }

        memset(Visited, 0, sizeof(Visited));
        memset(Passed, 0, sizeof(Passed));

        cout << "Case " << T++ << ": ";
        int result = 0;
        for(int i=1; i<=N; ++i)
        {
            if(!Visited[i])
            {
                int V = NumOfVertex(i);
                int E = NumOfEdge(i);
                if(V -1 == E/2)
                    ++result;
            }
        }
        Print(result);       
    }
}

int main()
{
    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
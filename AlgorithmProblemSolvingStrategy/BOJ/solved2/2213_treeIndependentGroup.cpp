#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 1'000'001;
int N; 
bool Visited[MAX] = {false,};
vector<int> Tree[MAX];
int DP[MAX][2]; // DP[][0]: 얼리어답터x 최소 개수, DP[][1]: 얼리어댑터O 최소개수

void DFS(int now)
{
    Visited[now] = true;
    DP[now][1] = 1;

    for(int child: Tree[now])
    {
        if(!Visited[child])
        {
            DFS(child);
            DP[now][0] += DP[child][1];
            DP[now][1] += min(DP[child][0], DP[child][1]);
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> N;
    for(int i=0; i<N-1; ++i)
    {
        int a, b;
        cin >> a >> b;
        Tree[a].push_back(b);
        Tree[b].push_back(a);
    }
    DFS(1);
    cout << min(DP[1][1], DP[1][0]) << endl;
    return 0;
}
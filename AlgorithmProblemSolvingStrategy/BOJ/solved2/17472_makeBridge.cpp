#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 10 + 10;

// 다리의 정보를 저장하기 위한 구조체
struct info{
    int src, dst, len;
};
int n, m, map[MAX][MAX], parent[7], answer, cnt;
bool visited[MAX][MAX];

const int dy[] = {-1, 0, 1, 0};
const int dx[] = {0, -1, 0, 1};

vector<info> dist; // 다리의 정보를 저장할 벡터

// 상하좌우 방향탐색 map의 범위 벗어나는지 판단
bool areYou_in_map(int y, int x)
{
    if(0<=y && y<n && 0<=x && x<m) //index 0부터
        return true;
    else
        return false;
}

bool compare(info a, info b) // 다리길이 짧은 순으로 정렬
{
    return a.len < b.len;
}

int find(int x)
{
    if(x == parent[x])
        return x;
    else
        return parent[x] = find(parent[x]);
}

void Union(int a, int b)
{
    int pa = parent[a];
    int pb = parent[b];
    if(pa < pb)
        parent[pb] =  pa;
    else if(pb < pa)
        parent[pa] = pb;
}

// 좌표와 값을 인자로 받아서 해당 좌표부터 인접한 땅을 전부
// DFS 탐색하며 섬의 번호를 매겨주는 함수
void make_island(int y, int x, int value)
{
    visited[y][x] = true; //방문처리
    map[y][x] = value; // 섬번호

    //상하좌우 네 방향탐색
    for(int i=0; i<4; ++i)
    {
        int yy = y + dy[i], xx = x + dx[i];

        // 범위 벗어나거나 바다이면 continue
        if(!(areYou_in_map(yy, xx)) || !map[yy][xx])
            continue;
        // 아직 방문하지 않은 칸에 번호 부여
        if(!visited[yy][xx])
        {
            make_island(yy, xx, value);
        }
    }
}

bool can_I_make_bridge(int y, int x)
{
    // 상하좌우 네 방향 탐색
    for(int i=0; i<4; ++i)
    {
        int yy = y + dy[i], xx = x + dx[i];
        if(!(areYou_in_map(y,x)))
            continue;
        
        if(!map[yy][xx]) // 내 주변에 바다가 하나라도 있으면, 
            return true;
    }
    return false;
}

// 상하좌우 각각 이동해가면서
// 내 섬이 아니거나 다리 길이 2이상 dist 벡터에 다리 정보 저장
void make_bridge(int y, int x, int value)
{
    for(int i=0; i<4; ++i)
    {
        int yy = y, xx = x, len = 0;
        info temp;

        // 상하좌우 네 방향 중에서 한 방향으로 쭉 직진
        while(1)
        {
            yy += dy[i], xx += dx[i];
            
            // 직진하다가 배열 범위 벗어나거나 내 섬을 만나면 break;
            if(!(areYou_in_map(yy,xx)) || map[yy][xx] == value)
                break;
            
            // 바다이면 다리길이 + 1
            if(!map[yy][xx])
            {
                ++len;
                continue;
            }
            // 내 섬이 아니고 다른 섬 만났으면
            if(map[yy][xx])
            {
                if(len > 1) // 다리길이 2이상이면
                {
                    temp.src = value, temp.dst = map[yy][xx];
                    temp.len = len;
                    dist.push_back(temp);
                    break;
                }
                break; // 다리길이 2이상이 아니어도
            }
        }
    }
}

int solve()
{
    //1. 먼저 섬 번호를 부여합니다.
    for(int i=0; i<n; ++i)
    {
        for(int j=0; j<m; ++j)
        {
            if(!visited[i][j] && map[i][j])
                make_island(i, j, ++cnt);
        }
    }
    //2. 각 섬의 부모를 자기 자신으로
    for(int i=1; i<=cnt; ++i)
        parent[i] = i;
    
    //3. 해당 좌표가 땅이고 다리를 놓을 수 있다면 다리를 놓습니다.
    for(int i=0; i<n; ++i)
    {
        for(int j=0; j<m; ++j)
        {
            if(map[i][j] && can_I_make_bridge(i, j))
            {
                make_bridge(i, j, map[i][j]);
            }
        }
    }
    //4. 다리 길이 짧은 것으로 정렬
    sort(dist.begin(), dist.end(), compare);

    //5. 부모 합치고 cost 더함
    for(int i=0; i<dist.size(); ++i)
    {
        if (find(dist[i].src) != find(dist[i].dst))
        {
            answer += dist[i].len;
            Union(dist[i].src, dist[i].dst);
        }
    }
    // 6. 1번째 섬의 부모를 찾고, 그 부모와 다른 부모를 가진 섬이 있다면
    // 모든 섬이 연결되지 않았다는 뜻
    int current = find(1);
    for(int i=2; i<=cnt; ++i)
    {
        if(current != find(i))
            return -1;
    }
    return answer;

}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> n >> m;
    for(int i=0; i<n; ++i)
    {
        for(int j=0; j<m; ++j)
        {
            cin >> map[i][j];
        }
    }
    cout << solve() << endl;
    return 0;
}
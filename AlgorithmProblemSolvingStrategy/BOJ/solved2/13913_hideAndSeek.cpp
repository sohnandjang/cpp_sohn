#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

constexpr int MAX = 1'000'001;

typedef struct 
{
    int distance;
    int time;
}DATA_T;


int N = 0;
int K = 0;
vector<int> Path;
bool Visited[MAX] = {0,};
int Parent[MAX] = {0,};

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    cin >> K;
}

int minSecond()
{
    queue<DATA_T> q;
    q.push({N, 0});
    Visited[N] = true;

    while(!q.empty())
    {
        int curLoc = q.front().distance;
        int curSec = q.front().time;
        q.pop();

        if(curLoc == K)
        {
            int idx = curLoc;
            while(idx != N)
            {
                Path.push_back(idx);
                idx = Parent[idx];
            }
            Path.push_back(N);
            return curSec;
        }

        if (curLoc + 1 < MAX && !Visited[curLoc+1])
        {
            q.push({curLoc+1, curSec+1});
            Visited[curLoc+1] = 1;
            Parent[curLoc+1] = curLoc;
        }
        if (curLoc - 1 >= 0 && !Visited[curLoc-1])
        {
            q.push({curLoc-1, curSec+1});
            Visited[curLoc-1] = 1;
            Parent[curLoc-1] = curLoc;
        }
        if (curLoc * 2 < MAX && !Visited[curLoc*2])
        {
            q.push({curLoc*2, curSec+1});
            Visited[curLoc*2] = 1;
            Parent[curLoc*2] = curLoc;
        }
    }
}


int main()
{
    Input();
    printf("%d\n", minSecond());
    for(int i=Path.size()-1; i>=0; --i)
        printf("%d ", Path[i]);
    printf("\n");
    return 0;
}
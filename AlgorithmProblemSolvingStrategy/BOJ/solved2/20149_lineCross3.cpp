#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"
#define ll long long
#define pll pair<ll,ll>


ll CCW(pll A, pll B, pll C)
{
    ll ret = A.first*B.second + B.first*C.second + C.first*A.second - B.first*A.second - C.first*B.second - A.first*C.second;
    if(ret > 0)
        return 1;
    else if(ret < 0)
        return -1;
    else
        return 0;
}

double getSlope(pll A, pll B)
{
    return (double)(B.second - A.second)/(double)(B.first - A.first);
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    pll A, B, C, D;
    //freopen("input.txt", "r", stdin);
    cin >> A.first >> A.second >> B.first >> B.second;
    cin >> C.first >> C.second >> D.first >> D.second;

    ll CCW1 = CCW(A, B, C);
    ll CCW2 = CCW(A, B, D);
    ll CCW3 = CCW(C, D, A);
    ll CCW4 = CCW(C, D, B);
    ll ans1 = CCW(A, B, C) * CCW(A, B, D);
    ll ans2 = CCW(C, D, A) * CCW(C, D, B);
    //printf("ans1=%d, ans2=%d\n", ans1, ans2);
    if(ans1 == 0 && ans2 == 0)
    {
        if(CCW1 == 0 && CCW2 == 0 && CCW3 == 0 && CCW4 == 0)
        {
            if(A > B) swap(A, B);
            if(C > D) swap(C, D);

            if(A < D && C < B) 
            {// 무수히 많음
                printf("1\n");
            }
            else if((A < D && B == C) || (A == D & C < B))
            {
                // 교점이 하나
                printf("1\n");
                if(A == C || A == D) 
                    printf("%d %d\n", A.first, A.second);
                if(B == C || B == D)
                    printf("%d %d\n", B.first, B.second);    
                } 
            else
                printf("0\n");
        }
        else
        {
            // 교점이 하나
            printf("1\n");
            if(A == C || A == D) 
                printf("%d %d\n", A.first, A.second);
            if(B == C || B == D)
                printf("%d %d\n", B.first, B.second);
        }
        
    }
    else if(ans1 <= 0 && ans2 <= 0)
    {
        // 1점 교차점 구하기
        printf("1\n");
        // 무한 또는 직선의 방정식
        if(A.first == B.first) //무한: y축 평행, x점 동일
        {
            double x = A.first;  
            double slope = getSlope(C, D);
            double y = slope*(x-C.first) + C.second;
            printf("%.15lf %.15lf\n", x, y);
        }
        else if(C.first == D.first)
        {
            double x = C.first;  
            double slope = getSlope(A, B);
            double y = slope*(x-A.first) + A.second;
            printf("%.15lf %.15lf\n", x, y);
        }
        else
        {
            double sl1 = getSlope(A, B);
            double sl2 = getSlope(C, D);
            double x = (double)(sl1*A.first - sl2*C.first + C.second - A.second) / (double)(sl1 - sl2);
            double y = sl1*(x - A.first) + A.second;
            printf("%.15lf %.15lf\n", x, y);
        }
        
    }
    else // 만나지 않음
        printf("0\n");

    return 0;
}
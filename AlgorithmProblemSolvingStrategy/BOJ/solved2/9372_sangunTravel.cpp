#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 1'000'010;
int T, N, M;
int Parent[MAX];
void Input()
{
}

int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]); //<-- 시간초과를 줄인다.
        return Parent[x];
    }
    
}

bool Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);

    if(px == py)
        return false;
    
    Parent[py] = px;
    return true;
}


void Solve()
{
    cin >> T;
    for(int t=0; t<T; ++t)
    {
        cin >> N >> M;
        memset(Parent, 0, sizeof(Parent));
        for(int i=0; i<M; ++i)
        {
            int a, b;
            cin >> a >> b;
        }
        cout << N - 1 << endl;
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
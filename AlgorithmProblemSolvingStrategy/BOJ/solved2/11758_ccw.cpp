#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"
#define ll long long

constexpr int MAX = 10001;
int N;
vector<pair<ll, ll>> Arr;
double Ans;

ll Outer_product(pair<ll, ll> x, pair<ll, ll> y)
{
    return (x.first * y.second - x.second * y.first);
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        ll x, y;
        cin >> x >> y;
        Arr.push_back(make_pair(x,y));
    }

    for(int i=1; i<N-1; ++i)
    {
        Ans += (double)Outer_product({Arr[i].first-Arr[0].first, Arr[i].second-Arr[0].second}, {Arr[i+1].first-Arr[0].first, Arr[i+1].second-Arr[0].second}) / 2.0;
    }

    printf("%.1lf\n", abs(Ans));

    return 0;
}
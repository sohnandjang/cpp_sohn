#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>
#include <math.h>

using namespace std;

#define endl "\n"

constexpr int MAX = 1010;

vector<pair<double, double>> Coord;
vector<pair<double, pair<int, int>>> Edge;
int N, M;
int Parent[MAX];
double Answer;

void Input()
{
    cin >> N >> M;
    for(int i=0; i<N; ++i)
    {
        double a, b;
        cin >> a >> b;
        Coord.push_back({a,b});
    }
}
int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]); 
        return Parent[x];
    }
    
}

bool Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);

    if(px == py)
        return false;
    
    Parent[py] = px;
    return true;
}

double GetDistance(double x1, double y1, double x2, double y2)
{
    double dx = (x2 - x1)*(x2 - x1);
    double dy = (y2 - y1)*(y2 - y1);
    return sqrt(dx + dy);
}

void Solve()
{
    Input();
    for(int i=1; i<=N; ++i)
        Parent[i] = i;

    for(int i=0; i<Coord.size(); ++i)
    {
        double x1 = Coord[i].first;
        double y1 = Coord[i].second;
        for(int j=i+1; j<Coord.size(); ++j)
        {
            double x2 = Coord[j].first;
            double y2 = Coord[j].second;

            double dist = GetDistance(x1, y1, x2, y2);
            Edge.push_back(make_pair(dist, make_pair(i, j)));
        }
    }
    for(int i=1; i<=N; ++i)
    {
        Parent[i] = i;
    }
    sort(Edge.begin(), Edge.end());

    for(int i=0; i<M; ++i)
    {
        int a, b;
        cin >> a >> b;
        Unite(a, b);
    }

    for(int i=0; i<Edge.size(); ++i)
    {
        int node1 = Edge[i].second.first + 1;
        int node2 = Edge[i].second.second + 1;
        double cost = Edge[i].first;
        if(Unite(node1, node2))
            Answer += cost;
    }
    //printf("%.2f",Answer);
    cout << fixed;
    cout.precision(2);
    cout << Answer << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
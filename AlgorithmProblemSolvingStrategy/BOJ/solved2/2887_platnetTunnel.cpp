#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>
#include <math.h>
#include <tuple>

using namespace std;

#define endl "\n"

constexpr int MAX = 100'001;

struct Planet_T
{
	int x, y, z, num;
};

struct Edge_T
{
	int w, x, y;
	#if 0
	bool operator<(const Edge_T& e)
	{
		w < e.w;
	}
	#endif
};

bool compx(const Planet_T& a, const Planet_T& b)
{
	return a.x < b.x;
}
bool compy(const Planet_T& a, const Planet_T& b)
{
	return a.y < b.y;
}
bool compz(const Planet_T& a, const Planet_T& b)
{
	return a.z < b.z;
}
bool compEdge(const Edge_T& a, const Edge_T& b)
{
	return a.w < b.w;
}

vector<Planet_T> Planet;
vector<Edge_T> Edge;
int N;
int Parent[MAX];
int Answer;

void Input()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        Planet.push_back({a,b,c,i});
    }
}
int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]); 
        return Parent[x];
    }
    
}

bool Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);

    if(px == py)
        return false;
    
    Parent[py] = px;
    return true;
}

int GetDistance(int x1, int y1, int x2, int y2)
{
    int dx = (x2 - x1)*(x2 - x1);
    int dy = (y2 - y1)*(y2 - y1);
    return sqrt(dx + dy);
}

void Solve()
{
    Input();
    for(int i=1; i<=N; ++i)
        Parent[i] = i;

	// x축 정렬
	sort(Planet.begin(), Planet.end(), compx);
	for(int i=0; i<N-1; ++i) // n ~ n+1
		Edge.push_back({abs(Planet[i+1].x-Planet[i].x), Planet[i].num, Planet[i+1].num});

	sort(Planet.begin(), Planet.end(), compy);
	for(int i=0; i<N-1; ++i) // n ~ n+1
		Edge.push_back({abs(Planet[i+1].y-Planet[i].y), Planet[i].num, Planet[i+1].num});

	sort(Planet.begin(), Planet.end(), compz);
	for(int i=0; i<N-1; ++i) // n ~ n+1
		Edge.push_back({abs(Planet[i+1].z-Planet[i].z), Planet[i].num, Planet[i+1].num});

    sort(Edge.begin(), Edge.end(), compEdge);

    for(int i=0; i<Edge.size(); ++i)
    {
        int node1 = Edge[i].x;
        int node2 = Edge[i].y;
        int cost = Edge[i].w;
        if(Unite(node1, node2))
            Answer += cost;
    }
    cout << Answer << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

#define endl "\n"

constexpr int MAX = 1e5;

int Tree[MAX];
int Size = 0;

void Input()
{
    //cout << "Input> " << endl;
    int val = 0;
    #if 1
    while( cin >> val)
    {
        //cin >> val;
        //cout << "val: " << val << endl;
        //if(val == EOF) //ctrl + D
        //    break;
        //cin >> Tree[Size];
        //if (scanf("%d", Tree+Size) <= 0)
        //    break;
        Tree[Size] = val;
        ++Size;
    }
    //cout << "Input:End" << endl;
    #endif
    //for (Size = 0; scanf("%d", Tree + Size) > 0; Size++);
}

// [start, end)
void PostOrder(int start, int end)
{
    if(start == end)
        return;
    
    if(start == end - 1)
    {
        cout << Tree[start] << endl;
        return; // 주의!!!
    }

    int idx = start + 1; //start== root
    while(idx < end && Tree[idx] < Tree[start])
        idx++;
    
    PostOrder(start+1, idx);
    PostOrder(idx, end); // 주의:  idx
    cout << Tree[start] << endl;
}

void Solution()
{
    PostOrder(0, Size);
}

void Solve()
{
    Input();
    Solution();
}
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("Input.txt", "r", stdin);
    Solve();

    return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 10001;
int N;
int V[MAX]; //가중치
bool Visited[MAX];
vector<int> Graph[MAX];
int DP[MAX][2];
vector<int> Ans;

void DFS(int now)
{
    Visited[now] = true;
    DP[now][1] = V[now]; // 포함

    for(int child: Graph[now])
    {
        if(!Visited[child])
        {
            DFS(child);
            DP[now][0] += max(DP[child][0], DP[child][1]); // 미포함
            DP[now][1] += DP[child][0];
        }
    }
    //printf("DP[%d][0] = %d\n", now, DP[now][0]);
    //printf("DP[%d][1] = %d\n", now, DP[now][1]);
}
 
void Tracking(int prev, int now, int state)
{
    if(state == 1) // 포함
    {
        Ans.push_back(now);
        for(int child: Graph[now])
        {
            // 이전과 같으면 skip
            if(prev == child) continue;

            Tracking(now, child, 0);
        }
    }
    else // 미포함
    {
        for(int child: Graph[now])
        {
            if(child == prev) continue;

            if(DP[child][0] > DP[child][1])
            {
                Tracking(now, child, 0);
            }
            else
            {
                Tracking(now, child, 1);
            }
        }
    }
    
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> N;
    for(int i=1; i<=N; ++i)
        cin >> V[i];
    
    for(int i=0; i<N-1; ++i)
    {
        int a, b;
        cin >> a >> b;
        Graph[a].push_back(b);
        Graph[b].push_back(a);
    }

    DFS(1);
    int ans1 = DP[1][0];
    int ans2 = DP[1][1];
    if(ans1 > ans2)
        Tracking(-1, 1, 0);
    else
        Tracking(-1, 1, 1);
    
    sort(Ans.begin(), Ans.end());
    cout << max(ans1, ans2) << endl;
    for(int n: Ans)
        cout << n << " ";
    cout << endl;
    return 0;
}
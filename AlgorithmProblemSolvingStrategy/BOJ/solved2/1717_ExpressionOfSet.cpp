#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 1'000'010;
int N = 0;
int M = 0;
int Parent[MAX];
void Input()
{
}

int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]); //<-- 시간초과를 줄인다.
        return Parent[x];
    }
    
}

void Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);
    Parent[px] = py;
}


void Solve()
{
    //Input();
    cin >> N >> M;

    for(int i=0; i<=N; ++i)
        Parent[i] = i;

    for(int i=0; i<M; ++i)
    {
        int op, a, b;
        cin >> op >> a >> b;
        if(op == 0)
            Unite(a, b);
        else
        {
            if(Find(a) == Find(b))
                cout << "YES" << endl;
            else
                cout << "NO" << endl;
        }    
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
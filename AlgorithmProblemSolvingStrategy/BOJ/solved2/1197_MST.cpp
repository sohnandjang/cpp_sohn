#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 10'001;

typedef struct _Edge_T
{
    int u;
    int v;
    int w;

    bool operator<(const _Edge_T& e)
    {
        return w < e.w;
    }
}Edge_T;

vector<Edge_T> Edge;
int V, E;
int Parent[MAX];
int Cost;
void Input()
{
}
int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]); 
        return Parent[x];
    }
    
}

bool Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);

    if(px == py)
        return false;
    
    Parent[py] = px;
    return true;
}


void Solve()
{
    cin >> V >> E;

    for(int i=1; i<=V; ++i)
        Parent[i] = i;

    for(int i=0; i<E; ++i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        Edge.push_back({a,b,c});
    }
    sort(Edge.begin(), Edge.end());

    for(int i=0; i<Edge.size(); ++i)
    {
        if(Unite(Edge[i].u, Edge[i].v))
            Cost += Edge[i].w;
    }
    cout << Cost << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
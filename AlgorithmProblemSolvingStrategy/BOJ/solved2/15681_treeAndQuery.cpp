#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 100'001;
int N, R, Q;
vector<int> V[MAX];
bool Visited[MAX] = {false,};
int NumSubTree[MAX] = {0,};

int DFS(int n)
{
    if(NumSubTree[n]!=0)
        return NumSubTree[n];

    int ret = 1;
    Visited[n] = true;

    for(int i=0; i<V[n].size(); ++i)
    {
        int next = V[n][i];
        if(!Visited[next])
        {
            ret += DFS(next);
        }
    }
    NumSubTree[n] = ret;
    return ret;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> N >> R >> Q;
    int s, e;
    for(int i=0; i<N-1; ++i)
    {
        cin >> s >> e;
        V[s].push_back(e);
        V[e].push_back(s);
    }
    NumSubTree[R] = DFS(R);
    for(int i=0; i<Q; ++i)
    {
        int q;
        cin >> q;
        cout << DFS(q) << endl;
    }
    return 0;
}
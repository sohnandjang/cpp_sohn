#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

#define endl "\n"

constexpr int MAX = 100'010;

int N;
int Index[MAX];
int InOrder[MAX];
int PostOrder[MAX];

void Input()
{
    cin >> N;
    for(int i=1; i<=N; ++i)
    {
        cin >> InOrder[i];
        Index[InOrder[i]] = i; //InOrder요소들의 Index저장
    }
    for(int i=1; i<=N; ++i)
    {
        cin >> PostOrder[i];
    }
}

void GetPreOrder(int inStart, int inEnd, int postStart, int postEnd)
{
    if(inStart > inEnd || postStart > postEnd)
        return;
    
    int rootIndex = Index[PostOrder[postEnd]];
    int leftSize = rootIndex - inStart;
    
    cout << InOrder[rootIndex] << ' ';
    GetPreOrder(inStart, rootIndex-1, postStart, postStart+leftSize-1);
    GetPreOrder(rootIndex+1, inEnd, postStart+leftSize, postEnd-1); //postEnd - 1 중요!!!
}

void Solution()
{
    GetPreOrder(1, N, 1, N);
}

void Solve()
{
    Input();
    Solution();
}
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("Input.txt", "r", stdin);
    Solve();

    return 0;
}
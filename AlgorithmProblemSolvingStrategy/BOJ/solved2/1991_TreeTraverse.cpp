#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

#define endl "\n"

constexpr int MAX = 27;

typedef struct
{
    char left;
    char right;
} Node;

int N;
Node Tree[MAX];

void Input()
{
    cin >> N;
    for(int i=0; i<N; ++i)
    {
        char node, left, right;
        cin >> node;
        cin >> left;
        cin >> right;
        Tree[node-'A'] = {left, right};
    }
}

void PreOrder(char node)
{
    if(node == '.')
        return;
    
    cout << node;
    PreOrder(Tree[node-'A'].left);
    PreOrder(Tree[node-'A'].right);
}

void InOrder(char node)
{
    if(node == '.')
        return;
    
    InOrder(Tree[node-'A'].left);
    cout << node;
    InOrder(Tree[node-'A'].right);
}

void PostOrder(char node)
{
    if(node == '.')
        return;
    
    PostOrder(Tree[node-'A'].left);
    PostOrder(Tree[node-'A'].right);
    cout << node;
}

void Solution()
{
    PreOrder('A');
    cout << endl;
    InOrder('A');
    cout << endl;
    PostOrder('A');
    cout << endl;
}

void Solve()
{
    Input();
    Solution();
}
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("Input.txt", "r", stdin);
    Solve();

    return 0;
}
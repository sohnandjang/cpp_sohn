#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"
#define ll long long

int N;

ll CCW(ll x1, ll y1, ll x2, ll y2, ll x3, ll y3)
{
    ll ret = x1*y2 + x2*y3 + x3*y1 - x2*y1 - x3*y2 - x1*y3;
    if(ret > 0)
        return 1;
    else if(ret < 0)
        return -1;
    else
        return 0;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    //cin >> N;
    int x1, y1, x2, y2, x3, y3, x4, y4;
    cin >> x1 >> y1 >> x2 >> y2;
    cin >> x3 >> y3 >> x4 >> y4;

    ll ans1 = CCW(x1,y1,x2,y2,x3,y3) * CCW(x1,y1,x2,y2,x4,y4);
    ll ans2 = CCW(x3,y3,x4,y4,x1,y1) * CCW(x3,y3,x4,y4,x2,y2);
    //printf("ans1=%d, ans2=%d\n", ans1, ans2);
    if(ans1 <= 0 && ans2 <= 0)
    {
        if(ans1 == 0 && ans2 == 0)
        {
            if(x1 < x2)
        }
        printf("1\n");
    }
    else
        printf("0\n");

    return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 210;

int N = 0;
int M = 0;
int Parent[MAX];
int Find(int x) {
    if (x == Parent[x])
        return x;
    else
        return Parent[x] = Find(Parent[x]);
}
 
void Unite(int x, int y) {
    x = Find(x);
    y = Find(y);
    Parent[x] = y;
}
#if 0
int Find(int x)
{
    if(x == Parent[x])
        return x;
    else
    {
        Parent[x] = Find(Parent[x]);
        return Parent[x];
    }
}

void Unite(int x, int y)
{
    int px = Find(x);
    int py = Find(y);

    Parent[px] = py;
}
#endif
//void Input()
void Solve()
{
    for(int i=1; i<=N; ++i)
        Parent[i] = i;
#if 0
    cin >> N >> M;
    int a;
    for(int i=1; i<=N; ++i)
    {
        for(int j=1; j<=N; ++j)
        {
            cin >> a;
            if(a)
                Unite(i, j);
        }
    }
    
#endif    
    //scanf("%d %d", &N, &M);
    cin >> N >> M;
    int i=0, j=0, a=0, b=0;
    for (i = 1; i <= N; ++i)
        Parent[i] = i;
 
    for (i = 1; i <= N; ++i) {
        for (j = 1; j <= N; ++j) {
            cin >> a;
            //scanf("%d", &a);
            if (a)
                Unite(i, j);
        }
    }
#if 1
    int m = 0;
    cin >> m;
    //scanf("%d", &a);
    int mp = Find(m);
    for(int i=1; i<M; ++i)
    {
        cin >> m;
        //scanf("%d", &a);
        if(mp != Find(m))
        {
            //cout << "NO" << endl;
            puts("NO");
            return;
        }
    }
    //cout << "YES" << endl;
    puts("YES");
    //return;
#endif
#if 0
    scanf("%d", &a);
    b = Find(a);
 
    for (i = 1; i < M; ++i) {
        scanf("%d", &a);
        if (b != Find(a)) {
            puts("NO");
            return;
        }
    }
 
    puts("YES");
#endif
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
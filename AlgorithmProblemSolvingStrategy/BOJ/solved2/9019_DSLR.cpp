#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

constexpr int MAX = 10'000;

int A = 0;
int B = 0;
bool Visited[MAX];

void Input()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
}

string BFS()
{
    queue<pair<int, string>> q;
    q.push(make_pair(A, ""));
    Visited[A] = true;

    while(!q.empty())
    {
        int curNum = q.front().first;
        string curChange = q.front().second;
        q.pop();

        if(curNum == B)
        {
            return curChange;
        }

        int nextNum = (curNum * 2) % 10000;
        if (!Visited[nextNum])
        {
            q.push({nextNum, curChange + "D"});
            Visited[nextNum] = true;
        }
        nextNum = (curNum == 0 ? 9999 : curNum - 1);
        if (!Visited[nextNum])
        {
            q.push({nextNum, curChange + "S"});
            Visited[nextNum] = true;
        }
        nextNum = (curNum % 1000)*10 + curNum / 1000;
        if (!Visited[nextNum])
        {
            q.push({nextNum, curChange + "L"});
            Visited[nextNum] = true;
        }
        nextNum = (curNum / 10) + (curNum % 10) * 1000;
        if (!Visited[nextNum])
        {
            q.push({nextNum, curChange + "R"});
            Visited[nextNum] = true;
        }
    }
}


int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    int T = 0;
    cin >> T;

    for(int t=0; t<T; ++t)
    {
        memset(Visited, 0, sizeof(Visited));
        cin >> A >> B;
        cout << BFS() << endl;
    }
    
    return 0;
}
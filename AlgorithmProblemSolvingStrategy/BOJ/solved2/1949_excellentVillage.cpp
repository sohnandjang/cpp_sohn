#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 10001;
int N;
int V[MAX]; //가중치
bool Visited[MAX];
vector<int> Graph[MAX];
int DP[MAX][2];

void DFS(int now)
{
    Visited[now] = true;
    DP[now][1] = V[now]; // 포함

    for(int child: Graph[now])
    {
        if(!Visited[child])
        {
            DFS(child);
            DP[now][0] += max(DP[child][0], DP[child][1]); // 미포함
            DP[now][1] += DP[child][0];
        }
    }
    //printf("DP[%d][0] = %d\n", now, DP[now][0]);
    //printf("DP[%d][1] = %d\n", now, DP[now][1]);
}
 


int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    cin >> N;
    for(int i=1; i<=N; ++i)
        cin >> V[i];
    
    for(int i=0; i<N-1; ++i)
    {
        int a, b;
        cin >> a >> b;
        Graph[a].push_back(b);
        Graph[b].push_back(a);
    }

    DFS(1);
    int ans1 = DP[1][0];
    int ans2 = DP[1][1];
    cout << max(ans1, ans2) << endl;
    return 0;
}
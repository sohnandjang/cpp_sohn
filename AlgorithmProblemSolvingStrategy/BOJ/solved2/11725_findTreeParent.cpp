#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

#define endl "\n"

constexpr int MAX = 100'010;

typedef struct{
    int index;
    int dist;
}Node;

int N;
int Visited[MAX];
vector<Node> Graph[MAX];
int MaxDist;
int MaxNode;

void Input()
{
    cin >> N;
    int from, to, dist;
    for (int i = 1; i <= N; i++)
    {
        cin >> from;
        while(1)
        {
            cin >> to;
            if(to == -1)
                break;
            cin >> dist;
            Graph[from].push_back({to, dist});
            Graph[to].push_back({from, dist});
        }
    }    
}

void DFS(int node, int dist)
{
    // 방문한 노드면 return
    if(Visited[node])
        return;
    
    // maxDist 갱신
    if (MaxDist < dist)
    {
        MaxDist = dist;
        MaxNode = node;
    }

    Visited[node] = true; // 방문 체크

    // 현재 정점에서 연결된 정점들로 DFS 수행
    for(int i=0; i<Graph[node].size(); ++i)
    {
        int nextNode = Graph[node][i].index;
        int nextDist = Graph[node][i].dist;
        DFS(nextNode, nextDist+dist); // 여기 중요!!!
    }
}

void Solution()
{
    // 임의의 점점 1에서 가장 거리가 먼 정점 찾기
    DFS(1, 0);
    MaxDist = 0;
    memset(Visited, 0, sizeof(Visited));//<-- 중요!!!
    // 1과 가장 먼 정점에서 다시 DFS수행해서 트리의 지름 찾기
    DFS(MaxNode, 0);
    cout << MaxDist << endl;
}


void Solve()
{
    Input();
    Solution();
}
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("Input.txt", "r", stdin);
    Solve();
    
    return 0;
}
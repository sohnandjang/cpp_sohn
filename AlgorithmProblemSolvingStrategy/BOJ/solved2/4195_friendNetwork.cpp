#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>
#include <map>

using namespace std;

#define endl "\n"

constexpr int MAX = 500'001; 
int N, M;
int Parent[MAX];

void Input()
{
}

int FindParent(int x)
{
    if (x == Parent[x])
        return x;
    else
    {
        Parent[x] = FindParent(Parent[x]); //<-- 시간초과를 줄인다.
        return Parent[x];
    }
}

bool MakeUnion(int x, int y)
{
    int px = FindParent(x);
    int py = FindParent(y);

    if (px == py)
        return false;

    Parent[px] = py;
    return true;
}

void Solve()
{
    cin >> N >> M;

    for(int i=1; i<=N; ++i)
    {
        Parent[i] = i;
    }

    for(int i=1; i<=M; ++i)
    {
        int a, b;
        cin >> a >> b;

        if(FindParent(a) == FindParent(b))
        {
            cout << i << endl;
            return;
        }
        MakeUnion(a, b);
    }
    cout << 0 << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    Solve();
    return 0;
}
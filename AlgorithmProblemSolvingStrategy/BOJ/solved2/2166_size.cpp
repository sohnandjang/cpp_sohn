#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"

constexpr int MAX = 10001;
int N;
vector<pair<int, int>> Arr;
int Ans;


int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    //freopen("input.txt", "r", stdin);
    int x1, x2, x3, y1, y2, y3;
    cin >> x1 >> y1;
    cin >> x2 >> y2;
    cin >> x3 >> y3;

    Ans = x1*y2+x2*y3+x3*y1 - x2*y1-x3*y2-x1*y3; 

    if(Ans > 0)
        cout << 1 << endl;
    else if(Ans == 0)
        cout << 0 << endl;
    else
    {
        cout << -1 << endl;
    }

    return 0;
}
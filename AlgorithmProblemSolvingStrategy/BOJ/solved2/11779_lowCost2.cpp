#include <iostream>
#include <algorithm>
#include <string>
#include <limits.h>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <queue>
using namespace std;

#define endl "\n"
#define MAX 1010
#define INF 987654321
using namespace std;
 
int N, M, Start, End;
int Dist[MAX];
int Route[MAX];
vector<pair<int, int>> V[MAX];
vector<int> Route_V;



void Input()
{
    cin >> N >> M;
    for (int i = 1; i <= N; i++) Dist[i] = INF;
    for (int i = 0; i < M; i++)
    {
        int a, b, c; cin >> a >> b >> c;
        V[a].push_back(make_pair(b, c));
    }
    cin >> Start >> End;

}
#if 1
void Solution()
{
    priority_queue<pair<int, int>> PQ;  
    PQ.push(make_pair(0, Start));
    Dist[Start] = 0;

    while(!PQ.empty())
    {
        int CurCost = -PQ.top().first;
        int Cur = PQ.top().second;
        PQ.pop();

        if (Dist[Cur] < CurCost)
            continue;

        for(int i=0; i<V[Cur].size(); ++i)
        {
            int Next = V[Cur][i].first;
            int NextCost = V[Cur][i].second;
            if (Dist[Next] > CurCost + NextCost)
            {
                Dist[Next] = CurCost + NextCost;
                Route[Next] = Cur;
                PQ.push(make_pair(-Dist[Next], Next));
            }
        }
    }
    //printf("%d\n", Dist[End]);
    cout << Dist[End] << endl;
    int idx = End;
    while(idx != Start)
    {
        Route_V.push_back(idx);
        idx = Route[idx];
    }
    Route_V.push_back(Start);
    //printf("%d\n", Route_V.size());
    cout << Route_V.size() << endl;
    for(int i=Route_V.size()-1; i>=0; --i)
        cout << Route_V[i] << " ";
        //printf("%d ", Route_V[i]);
    //printf("\n");
    cout << endl;
}
#endif

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    Input();
    Solution();
    
    return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"
#define ll long long
#define pll pair<ll,ll>


ll CCW(pll A, pll B, pll C)
{
    ll ret = A.first*B.second + B.first*C.second + C.first*A.second - B.first*A.second - C.first*B.second - A.first*C.second;
    if(ret > 0)
        return 1;
    else if(ret < 0)
        return -1;
    else
        return 0;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    pll A, B, C, D;
    //freopen("input.txt", "r", stdin);
    cin >> A.first >> A.second >> B.first >> B.second;
    cin >> C.first >> C.second >> D.first >> D.second;

    ll ans1 = CCW(A, B, C) * CCW(A, B, D);
    ll ans2 = CCW(C, D, A) * CCW(C, D, B);
    //printf("ans1=%d, ans2=%d\n", ans1, ans2);
    if(ans1 == 0 && ans2 == 0)
    {
        if(A > B) swap(A, B);
        if(C > D) swap(C, D);

        if(A <= D && C <= B)
            printf("1\n");
        else
            printf("0\n");
    }
    else if(ans1 <= 0 && ans2 <= 0)
    {
        printf("1\n");
    }
    else
        printf("0\n");

    return 0;
}
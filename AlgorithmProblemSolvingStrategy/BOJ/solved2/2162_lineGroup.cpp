#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <cstring>

using namespace std;

#define endl "\n"
#define ll long long
#define pll pair<ll,ll>

int N = 0;
int parent[3001] = {0,};
int num[3001] = {0,};
vector<pair<pll,pll>> V;
#if 1
int getParent(int a)
{
    if(a == parent[a])
        return a;
    else
    {
        parent[a] = getParent(parent[a]);
        return parent[a];
    }
}
void unite(int x, int y)
{
    int px = getParent(x);//parent[x];
    int py = getParent(y);//parent[y];

    if(px == py)   
        return;
    
    if(px < py)
        parent[py] = px;
    else if(py < px)
        parent[px] = py;
}
#endif
#if 0
int getParent(int p)
{
	if (parent[p] == p) return p;
	parent[p] = getParent(parent[p]);
    return parent[p];
}

void unite(int a, int b)
{
	int p_a = getParent(a);
	int p_b = getParent(b);

	if (p_a < p_b) parent[p_b] = p_a;
	else if (p_a > p_b) parent[p_a] = p_b;
}
#endif


ll CCW(pll A, pll B, pll C)
{
    ll ret = A.first*B.second + B.first*C.second + C.first*A.second - B.first*A.second - C.first*B.second - A.first*C.second;
    if(ret > 0)
        return 1;
    else if(ret < 0)
        return -1;
    else
        return 0;
}

double getSlope(pll A, pll B)
{
    return (double)(B.second - A.second)/(double)(B.first - A.first);
}

#if 0
bool checkIntersect(int i, int j)
{
    pll A = V[i].first;
    pll B = V[i].second;
    pll C = V[j].first;
    pll D = V[j].second;

    int ans1 = CCW(A,B,C) * CCW(A,B,D);
    int ans2 = CCW(C,D,A) * CCW(C,D,B);

    if(ans1 == 0 && ans2 == 0)
    {
        if(A > B)
            swap(A, B);
        if(C > D)
            swap(C, D);
        
        return (A <= D && C <= B);
    }
    return (ans1 <= 0 && ans2 <= 0);
}
#endif
bool checkIntersect(int line1, int line2)
{
	pair<int, int> p1 = V[line1].first;
	pair<int, int> p2 = V[line1].second;
	pair<int, int> p3 = V[line2].first;
	pair<int, int> p4 = V[line2].second;

	int line1_2 = CCW(p1, p2, p3) * CCW(p1, p2, p4);
	int line2_1 = CCW(p3, p4, p1) * CCW(p3, p4, p2);

	if (line1_2 == 0 && line2_1 == 0)
	{
		if (p1 > p2) swap(p1, p2);
		if (p3 > p4) swap(p3, p4);

		return p1 <= p4 && p3 <= p2;
	}
	return line1_2 <= 0 && line2_1 <= 0;
}
int main()
{
    ios::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);

    cin >> N;
    for(int i=0; i<N; ++i)
    {
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;
        V.push_back({{x1,y1},{x2,y2}});
    }
    for(int i=0; i<N; ++i) //union-find
    {
        parent[i] = i;
        num[i] = 0;
    }    
    
    for(int i=0; i<N-1; ++i)
    {
        for(int j=i+1; j<N; ++j)
        {
            if(checkIntersect(i, j))
            {
                unite(i, j);
            }
        }
    }
    

    int maxNum = 0;
	int cnt = 0;

	for (int i = 0; i < N; ++i)
	{
		if (parent[i] == i) ++cnt;
		num[getParent(i)]++;
		if (num[getParent(i)] > maxNum) maxNum = num[getParent(i)];
	}

#if 0
    int cnt = 0;
    int maxNum = 0;
    for(int i=0; i<N; ++i)
    {
        if(parent[i] == i)
            ++cnt;
        
        //++num[parent[i]];
        ++num[getParent(i)];
        //maxNum = max(maxNum, num[parent[i]]);
        maxNum = max(maxNum, num[getParent(i)]);
    }
#endif
    cout << cnt << endl;
    cout << maxNum << endl;
    
    return 0;
}

#if 0
#include <iostream>
#include <vector>
#include <memory.h>
#define SIZE 3001

using namespace std;

typedef struct _tagPoint
{
	int x, y;
}POINT;

// 약 25mb
int parent[SIZE];
int num[SIZE] = { 0 };
int n;
vector < pair<pair<int, int>, pair<int, int>> > v;

void Input()
{
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		int x1, y1, x2, y2;
		cin >> x1 >> y1 >> x2 >> y2;

		v.push_back({ {x1, y1}, {x2, y2} });

		parent[i] = i;
		num[i] = 0;
	}
}

int CCW(pair<int, int> &p1, pair<int, int> &p2, pair<int, int> &p3)
{
	int op = p1.first * p2.second + p2.first*p3.second + p3.first*p1.second;
	op -= (p1.first*p3.second + p2.first*p1.second + p3.first*p2.second);

	if (op > 0) return 1;
	else if (op == 0) return 0;
	else return -1;
}

bool CheckIntersect(int line1, int line2)
{
	pair<int, int> p1 = v[line1].first;
	pair<int, int> p2 = v[line1].second;
	pair<int, int> p3 = v[line2].first;
	pair<int, int> p4 = v[line2].second;

	int line1_2 = CCW(p1, p2, p3) * CCW(p1, p2, p4);
	int line2_1 = CCW(p3, p4, p1) * CCW(p3, p4, p2);

	if (line1_2 == 0 && line2_1 == 0)
	{
		if (p1 > p2) swap(p1, p2);
		if (p3 > p4) swap(p3, p4);

		return p1 <= p4 && p3 <= p2;
	}
	return line1_2 <= 0 && line2_1 <= 0;
}

bool visit[SIZE];

int getParent(int p)
{
	if (parent[p] == p) return p;
	parent[p] = getParent(parent[p]);
    return parent[p];
}

void unoin_find(int a, int b)
{
	int p_a = getParent(a);
	int p_b = getParent(b);

	if (p_a < p_b) parent[p_b] = p_a;
	else if (p_a > p_b) parent[p_a] = p_b;
}

void solution()
{
	for (int i = 0; i < n; ++i)
	{
		for (int j = i + 1; j < n; ++j)
		{
			if (CheckIntersect(i, j))
			{
				unoin_find(i, j);
			}
		}
	}
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(0);

	Input();
	solution();

	int iMax = 0;
	int cnt = 0;

	for (int i = 0; i < n; ++i)
	{
		if (parent[i] == i) ++cnt;
		num[getParent(i)]++;
		if (num[getParent(i)] > iMax) iMax = num[getParent(i)];
	}

	cout << cnt << '\n';
	cout << iMax << '\n';

	return 0;
}
#endif
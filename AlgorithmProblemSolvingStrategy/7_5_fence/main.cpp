#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm> //~ min

using namespace std;

//2. divide and conquer

vector<int> h; // array saving each fence's height
int maxSize = 0;

int solve(int left, int right)
{
	int mid = (left + right) / 2; // mid = right/2 (wrong)
	int ret = 0;
	//base condition
	if (left == right)
	{
		return h[left];
	}
	//~ 1.divide and choose the max between them
	ret = max(solve(left, mid), solve(mid + 1, right));
	//printf("left=%d, mid=%d, mid+1=%d, right=%d, ret = %d\n", left, mid, mid + 1, right, ret);
	//~ 2.important) find the max (among the square covers both left and right)
	int lo = mid, hi = mid + 1;
	int height = min(h[lo], h[hi]);
	ret = max(ret, height * 2); //~ first consideration ( square covering [mid,mid+1])
	while (left < lo || hi < right) //~ ex) or => it is because it can only move left not right
	{								//		and lo can be increased until left so use < not <=
		if (hi < right && (lo == left || h[lo-1] < h[hi+1]))
		{
			++hi;
			height = min(height, h[hi]);
		}
		else
		{//~ in case : hi == right(end) and lo is not left(end)
			--lo;
			height = min(height, h[lo]);
		}
		ret = max(ret, height * (hi - lo + 1));
	}
	return ret;
}

int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;

	for (int i = 0; i < c; ++i)
	{
		int num= 0;
		fs >> num;
		for (int j = 0; j < num; ++j)
		{
			int temp = 0;
			fs >> temp;
			h.push_back(temp);
		}
		maxSize = solve(0, num-1);
		h.clear();
		cout << maxSize << endl;
	}
	getchar();
	return 0;
}

#if 0 
//1. brute force
vector<int> h;
int maxSize = 0;
int bruteForce(const vector<int> &h)
{
	int ret = 0;
	int N = h.size();
	for (int left = 0; left < N; ++left)
	{
		int minHeight = h[left];
		for (int right = left; right < N; ++right)
		{
			minHeight = min(minHeight, h[right]);
			//printf("ret = %d (minHeight=%d, right=%d, left=%d)\n", ret, minHeight, right, left);
			ret = max(ret, minHeight * (right - left + 1));
		}
	}
	return ret;
}
int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;

	for (int i = 0; i < c; ++i)
	{
		int num= 0;
		fs >> num;
		for (int j = 0; j < num; ++j)
		{
			int temp = 0;
			fs >> temp;
			h.push_back(temp);
		}
		maxSize = bruteForce(h);
		h.clear();
		cout << maxSize << endl;
	}
	getchar();
	return 0;
}
#endif
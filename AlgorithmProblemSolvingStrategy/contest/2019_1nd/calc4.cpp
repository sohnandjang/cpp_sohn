#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <list>
#include <map>
#include <set>
#include <numeric>
//#define DUSEFILE
//using namespace std;

#define SIZEOFA 100000
std::vector<long long> A; // (SIZEOFA + 1, 0);
std::vector<long long> A_KEY; // (SIZEOFA + 1, 0);

struct FenwickTree {
	std::vector<long long> tree;
	FenwickTree(long long n) : tree(n + 1)
	{
	}
	long long sum(long long pos)
	{
		++pos;
		long long ret = 0;
		while (pos > 0)
		{
			ret += tree[pos];
			pos &= (pos - 1);
		}
		return ret;
	}
	void add(long long pos, long long val)
	{
		++pos;
		while ((unsigned long long)pos < tree.size())
		{
			tree[pos] += val;
			pos += (pos & -pos);
		}
	}
};

enum { ADD = 1, SUBSUM = 2, SUB = 3 };
int N;
int funcOrder;
long long key;
long long value;
long long result;
long long idx1;
long long idx2;

FenwickTree FT(SIZEOFA + 10);

void disp() {
	//std::cout << "**** DISP **** " << std::endl;
	std::cout << "A_KEY, A, SUM [ ]: " << std::endl;
	for (unsigned long long i = 0; i < A.size(); i++)
	{
		std::cout << "[ " << i << "] " << A_KEY[i] << ", " << A[i] << ", " << FT.sum(i) << std::endl;
	}
	std::cout << std::endl;
}
void Remove(int index){
	long long i = index;
	long long j = 0;
	for (i = index; i < A_KEY.size() -1; ++i) {
		j = i + 1;
		FT.add(i, -A[i] + A[j]);
		A_KEY[i] = A_KEY[j];
		A[i] = A[j];
	}
	FT.add(i, -A[i]); // remove
	A.pop_back();
	A_KEY.pop_back();
}

void insertSort() {
	for (long long i = 0; i < A_KEY.size(); ++i)
	{
		long long j = i;
		while (j > 0 && A_KEY[j - 1] > A_KEY[j]) {
			// penwick
			//std::cout << "A_KEY[" << j-1 <<"]=" << A[j - 1] * (-1) + A[j] << std::endl;
			FT.add(j-1, A[j-1]*(-1)+A[j]);
			//std::cout << "A_KEY[" << j << "]=" << A[j] * (-1) + A[j-1] << std::endl;
			FT.add(j, A[j]*(-1)+A[j-1]);
			std::swap(A_KEY[j - 1], A_KEY[j]); // key
			std::swap(A[j - 1], A[j]); // value
			--j;
		}
	}
}

#ifdef DUSEFILE
void input(std::fstream& fs)
#else
void input()
#endif
{
#ifdef DUSEFILE
	fs >> funcOrder;
#else
	std::cin >> funcOrder;
#endif
	switch (funcOrder)
	{
	case ADD:
#ifdef DUSEFILE
		fs >> key; fs >> value;
		//std::cout << "ADD: " << "( " << key << ", " << value << " )" << std::endl;
#else
		std::cin >> key; std::cin >> value;
#endif
		break;
	case SUBSUM:
#ifdef DUSEFILE
		fs >> idx1; fs >> idx2;
		//std::cout << "SUBSUM: " << "( " << idx1 << ", " << idx2<< " )" << std::endl;
#else
		std::cin >> idx1; std::cin >> idx2;
#endif
		break;
	case SUB:
#ifdef DUSEFILE
		fs >> key;
		//std::cout << "SUB: " << "( " << key << " )" << std::endl;
#else
		std::cin >> key;
#endif
		break;
	}
}

#ifdef DUSEFILE
void calc(std::fstream& fs)
#else
void calc()
#endif
{
	funcOrder = 0; key = 0; value = 0;
	idx1 = 0; idx2 = 0;
#ifdef DUSEFILE
	input(fs);
#else
	input();
#endif
	switch (funcOrder)
	{
		case ADD:
		{
			//~ if key is same?
			std::vector<long long>::iterator it = std::find(A_KEY.begin(), A_KEY.end(), key);
			if (it != A_KEY.end()) { //found
				long long index1 = 0;
				index1 = std::distance(A_KEY.begin(), it);
				A[index1] += value;
				FT.add(index1, value);
			}
			else{ // not found
				FT.add(A.size(), value);
				A.push_back(value);
				A_KEY.push_back(key);
				insertSort();
			}
			//FT.add(convertKey, value);
			std::cout << FT.sum(SIZEOFA+9) << " ";
		}
		break;
		case SUBSUM:
		{
			bool exact1 = false;
			bool exact2 = false;
			std::vector<long long>::iterator it = std::find(A_KEY.begin(), A_KEY.end(), idx1);
			long long index1 = 0;
			if (it != A_KEY.end()) { //found
				index1 = std::distance(A_KEY.begin(), it);
				exact1 = true;
			}
			else { // not found
				std::vector<long long>::iterator low = std::lower_bound(A_KEY.begin(), A_KEY.end(), idx1);
				index1 = low - A_KEY.begin();
				//std::cout << "index1=" << index1 << std::endl;
			}
			it = std::find(A_KEY.begin(), A_KEY.end(), idx2);
			long long index2 = 0;
			if (it != A_KEY.end()) { //found
				index2 = std::distance(A_KEY.begin(), it);
				exact2 = true;
			}
			else { // not found
				std::vector<long long>::iterator low = std::lower_bound(A_KEY.begin(), A_KEY.end(), idx2);
				index2 = low - A_KEY.begin();
				//std::cout << "index2=" << index2 << std::endl;
			}
			long long SUM1 = 0;
			long long SUM2 = 0;
			if (index1 == 0)
				SUM1 = 0;
			else if (index1 == 1 && exact1)
				SUM1 = 0;
			else 
				SUM1 = FT.sum(index1 - 1);
			if (index2 == 0)
				SUM2 = 0;
			else if (exact2)
				SUM2 = FT.sum(index2);
			else
				SUM2 = FT.sum(index2 - 1);
			//std::cout << "SUM2= " << SUM2 << ", " << "SUM1= " << SUM1 << "= " << SUM2 - SUM1 << std::endl;
			std::cout << SUM2 - SUM1 << " ";
		}
		break; 
		case SUB:
		{
			std::vector<long long>::iterator it = std::find(A_KEY.begin(), A_KEY.end(), key);
			if (it != A_KEY.end()) {
				//std::cout << "Element Found" << std::endl;
				long long index = std::distance(A_KEY.begin(), it);
				//FT.add(index, (-1)*A[index]);
				//A[index] = 0; // delete
				Remove(index);
				std::cout << FT.sum(SIZEOFA+9) << " ";
			}
			else{
				//std::cout << "Element Not Found" << std::endl;
				std::cout << FT.sum(SIZEOFA+9) << " ";
			}
		}
		break;
	}
#ifdef DUSEFILE
	//std::cout << std::endl;
#endif
	//disp();
}

int main()
{
#ifdef DUSEFILE
	std::fstream myfile;
	myfile.open("calc.txt");
	myfile >> N;
#else
	std::cin >> N;
#endif
	for (int i = 0; i < N; i++)
#ifdef DUSEFILE
		calc(myfile);
#else
		calc();
#endif
	getchar();
	return 0;
}

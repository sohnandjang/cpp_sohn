#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
using namespace std;
//#define DUSEFILE 1
typedef unsigned long long ulonglong;

vector<ulonglong> A;
vector<ulonglong> B;
ulonglong N;
ulonglong M;
ulonglong idxA;
ulonglong idxB;

inline ulonglong ABS(ulonglong a, ulonglong b)
{
	if (a >= b)
		return a - b;
	else
		return b - a;
}

ulonglong twoArray(ulonglong a, ulonglong b)
{
	ulonglong ret = 0;
	ulonglong result;
	ulonglong b_next = b;

	if (a > A.size() - 1)
		return 0;

	while (1)
	{
		if (B[b] >= A[a]) {
			break;
		}
		if (b + 1 > B.size() - 1)
			break;
		else
			b++;
	}
	do
	{
		if (b == 0) {
			result = B[b];
			b_next = b;
			break;
		}
		ulonglong comp1 = ABS(B[b] , A[a]);
		ulonglong comp2 = ABS(B[b - 1] , A[a]);
		if (comp1 < comp2) {
			result = B[b];
			b_next = b;
		}
		else { // same priority -> low
			result = B[b - 1];
			b_next = b - 1;
		}
	} while (0);
	ret = result + twoArray(a + 1, b_next);
	//cout << "result:" << result << endl;
	return ret;
}


#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	// init
	A.clear();
	B.clear();
	idxA = 0;
	idxB = 0;
	// input
#ifdef DUSEFILE
	fs >> N;
	fs >> M;
#else
	cin >> N;
	cin >> M;
#endif
	for (ulonglong i = 0; i < N; i++) {
		ulonglong temp = 0;
#ifdef DUSEFILE
		fs >> temp;
#else
		cin >> temp;
#endif
		//A[i] = temp;
		A.push_back(temp);
	}
	for (ulonglong i = 0; i < M; i++) {
		ulonglong temp = 0;
#ifdef DUSEFILE
		fs >> temp;
#else
		cin >> temp;
#endif
		//B[i] = temp;
		B.push_back(temp);
	}
	sort(A.begin(), A.end());
	sort(B.begin(), B.end());
	// body function
	ulonglong ret = twoArray(0, 0);
	cout << ret << endl;
}

bool caseSolve(const char* path, ulonglong i_num)
{// i_num == -1 , don't care
	ulonglong num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
#else
	cin >> num;
#endif
	num = (i_num == -1 ? num : i_num);
	for (ulonglong i = 0; i < num; i++)
	{ //~ body
#ifdef DUSEFILE
		body(myfile);
#else
		body();
#endif
	}
#ifdef DUSEFILE
	myfile.close();
	return true;
	}
return false;
#else
	return true;
#endif
}

int main()
{
	caseSolve("twoArray.txt", -1);
	getchar();
	return 0;
}
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
using namespace std;
//#define DUSEFILE 1
#define MOD 1000000007
#define CACHE_SIZE 210
int N;
vector<int> X;
int cache[CACHE_SIZE][CACHE_SIZE];
inline int makeSmall(int i)
{
	return (i + MOD) % MOD;
}

int candy(int order, int preVal)
{
	int& retCache = cache[order][preVal];
	if (retCache != -1)
		return cache[order][preVal];

	int ret = 0;
	if (order > N)
	{
		return 0;
	}

	for (int i = 1; i <= X[order]; i++) {
		if (i < preVal)
		{
			continue;
		}
		if (order == N) {
			ret++;
		}
		ret = (ret + candy(order + 1, i))%MOD;
	}
	retCache = ret;
	return ret;
}

#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	// init
#ifdef DUSEFILE
	fs >> N;
#else
	cin >> N;
#endif
	X.clear();
	X.push_back(0); // order 0
	memset(cache, -1, sizeof(cache));

	for (int i = 1; i < N + 1; i++) {
		int temp;
#ifdef DUSEFILE
		fs >> temp;
#else
		cin >> temp;
#endif
		X.push_back(temp); //from order 1
	}
	sort(X.begin(), X.end());
	// input
	// body function
	int ret = candy(1, 0);
	int temp = 0;
	for (int i = 0; i < N; i++)
		temp = (temp + ret)%MOD;
	//ret = ret * N;
	ret = temp;
	cout << ret << endl;
}

bool caseSolve(const char* path, int i_num)
{// i_num == -1 , don't care
	int num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
#else
	cin >> num;
#endif
	num = (i_num == -1 ? num : i_num);
	for (int i = 0; i < num; i++)
	{ //~ body
#ifdef DUSEFILE
		body(myfile);
#else
		body();
#endif
	}
#ifdef DUSEFILE
	myfile.close();
	return true;
	}
return false;
#else
	return true;
#endif
}

int main()
{
	caseSolve("candy.txt", -1);
	getchar();
	return 0;
}
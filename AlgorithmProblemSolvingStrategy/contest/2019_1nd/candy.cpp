#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
using namespace std;
#define DUSEFILE 1
#define MAX_CANDY_NUM 1000000007
#define CACHE_INIT_VALUE (MAX_CANDY_NUM+50000)
#define CACHE_SIZE 210
unsigned int N;
vector<unsigned int> X;
unsigned int cache[CACHE_SIZE][CACHE_SIZE];
inline unsigned int makeSmall(unsigned int i)
{
	return (i + MAX_CANDY_NUM) % MAX_CANDY_NUM;
}

unsigned int candy(unsigned int order, unsigned int preVal)
{
	unsigned int& retCache = cache[order][preVal];
	if (retCache != CACHE_INIT_VALUE) 
		return cache[order][preVal];

	unsigned int ret = 0;
	if (order > N)
	{
		return 0;
	}


	for (unsigned int i = 1; i <= X[order]; i++) {
		if (i < preVal)
		{
			continue;
		}
		/*if (order + 1 > N) {
			prunsigned intf("for: order:%d, i:%d, +1\n", order, i);
			ret++;
			continue;
		}*/
		if (order == N) {
			ret++;
		}

		ret = makeSmall(ret) + makeSmall(candy(order + 1, i));
	}
	retCache = ret;
	return ret;
}

#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	// init
#ifdef DUSEFILE
	fs >> N;
#else
	cin >> N;
#endif
	X.clear();
	X.push_back(0); // order 0
	//memset(cache, -1, sizeof(cache));
	for (unsigned int i = 0; i < CACHE_SIZE; i++)
		for (unsigned int j = 0; j < CACHE_SIZE; j++)
			cache[i][j] = CACHE_INIT_VALUE;
	for (unsigned int i = 1; i < N+1; i++){
		unsigned int temp;
#ifdef DUSEFILE
		fs >> temp;
#else
		cin >> temp;
#endif
		X.push_back(temp); //from order 1
	}
	// input
	// body function
	unsigned int ret = candy(1, 0);
	cout << "case ret:" << ret << endl;
	unsigned int temp = 0;
	for (unsigned int i = 0; i < N; i++)
		temp = makeSmall(temp + ret);
	//ret = ret * N;
	ret = temp;
	cout << ret << endl;
}

bool caseSolve(const char* path, unsigned int i_num)
{// i_num == -1 , don't care
	unsigned int num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
#else
	cin >> num;
#endif
	//num = (i_num == -1 ? num : i_num);
	for (unsigned int i = 0; i < num; i++)
	{ //~ body
#ifdef DUSEFILE
		body(myfile);
#else
		body();
#endif
	}
#ifdef DUSEFILE
	myfile.close();
	return true;
	}
return false;
#else
	return true;
#endif
}

int main()
{
	caseSolve("candy.txt", 0);
	getchar();
	return 0;
}
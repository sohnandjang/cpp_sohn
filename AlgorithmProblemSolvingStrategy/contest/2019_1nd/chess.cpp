#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <limits.h>
using namespace std;

bool map1[9][9] = {
	{ false, true, false, true, false, true, false, true },
	{ true, false, true, false, true, false, true, false }, 
	{ false, true, false, true, false, true, false, true },
	{ true, false, true, false, true, false, true, false }, 
	{ false, true, false, true, false, true, false, true },
	{ true, false, true, false, true, false, true, false }, 
	{ false, true, false, true, false, true, false, true },
	{ true, false, true, false, true, false, true, false }, 
};
bool map2[65] = { 
	false, true, false, true, false, true, false, true,
	true, false, true, false, true, false, true, false, 
	false, true, false, true, false, true, false, true,
	true, false, true, false, true, false, true, false, 
	false, true, false, true, false, true, false, true,
	true, false, true, false, true, false, true, false, 
	false, true, false, true, false, true, false, true,
	true, false, true, false, true, false, true, false, 
};

//void body(fstream& fs)
void body()
{
	// init
	// input
	std::string map1_idx;
	cin >> map1_idx;
	int map2_idx;
	cin >> map2_idx;
	map2_idx--;

	int map1_idx1 = map1_idx[0] - 'A';
	int map1_idx2 = map1_idx[1] - '1';
	//cout << "map1_idx:" << map1_idx1 << "," << map1_idx2 << endl;
	//cout << "map2_idx:" << map2_idx << endl;
	// body function
	if (map1[map1_idx1][map1_idx2] == map2[map2_idx])
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
}

bool caseSolve(const char* path, int i_num)
{// i_num == -1 , don't care
	//fstream myfile;
	int num = 0;
	//myfile.open(path);
	//if (myfile.is_open())
	{
		//myfile >> num;
		cin >> num;
		num = (i_num == -1 ? num : i_num);
		for (int i = 0; i < num; i++)
		{ //~ body
			//body(myfile);
			body();
		}
		//myfile.close();
		return true;
	}
	return false;
}

int main()
{
	caseSolve("chess.txt", -1);
	getchar();
	return 0;
}

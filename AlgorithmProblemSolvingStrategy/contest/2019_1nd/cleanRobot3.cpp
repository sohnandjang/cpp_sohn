#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
using namespace std;
#define DUSEFILE 1
#define MAPSIZE 102
bool map[MAPSIZE][MAPSIZE];
int R;
int C;
int r;
int c;
enum {
	RIGHT = 0,
	LEFT = 1,
	UP = 2,
	DOWN = 3,
};
int R_Sub[4] = { 0, 0, -1, 1 };
int C_Sub[4] = { 1, -1, 0, 0 };
std::string W[4] = { "R", "L", "U", "D" };
std::string results;

bool checkMap()
{
	bool ret = true;
	for (int i = 0; i < R; i++) {
		for (int j = 0; j < C; j++) {
			if (map[i][j] == false)
				return false;
		}
	}
	return ret;
}
bool visit(int row, int col)
{
	if (row < 0 || col < 0) return false;
	if (row >= R || col >= C) return false;
	if (map[row][col] == false) {
		map[row][col] = true;
		return true;
	}
	else
		return false;
}
bool unvisit(int row, int col)
{
	if (row < 0 || col < 0) return false;
	if (row >= R || col >= C) return false;
	if (map[row][col] == true) {
		map[row][col] = false;
		return true;
	}
	else
		return false;
}

bool cleanRobot(int row, int col)
{
	bool ret = false;
	//base
	if (row < 0 || col < 0) return false;
	if (row >= R || col >= C) return false;
	if (checkMap()) {
		cout << results << endl;
		return true;
	}
	int next_row = 0;
	int next_col = 0;

	for (int i = 0; i < 4; i++)
	{
		next_row = row + R_Sub[i];
		next_col = col + C_Sub[i];
		if (visit(next_row, next_col))
		{
			results += W[i];
			ret |= cleanRobot(next_row, next_col);
			if (ret == true) return true;
			results = results.substr(0, results.size() - 1);
			unvisit(next_row, next_col);
		}
	}
	return ret;
}


#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	bool ret = false;
	// init
	R = 0, C = 0, r = 0, c = 0;
	memset(map, -1, sizeof(map));
	results = "";
	for (int i = 0; i < MAPSIZE; i++) {
		for (int j = 0; j < MAPSIZE; j++) {
			map[i][j] = false;
		}
	}

#ifdef DUSEFILE
	fs >> R; fs >> C; fs >> r; fs >> c;
#else
	cin >> R; cin >> C; cin >> r; cin >> c;
#endif
	//map[r - 1][c - 1] = true; // first position
	visit(r - 1, c - 1);
	ret = cleanRobot(r - 1, c - 1);
	if (ret == false)
		cout << "IMPOSSIBLE" << endl;
	//else
	//	cout << results << endl;
}

bool caseSolve(const char* path, int i_num)
{// i_num == -1 , don't care
	int num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
#else
	cin >> num;
#endif
	num = (i_num == -1 ? num : i_num);
	for (int i = 0; i < num; i++)
	{ //~ body
#ifdef DUSEFILE
		body(myfile);
#else
		body();
#endif
	}
#ifdef DUSEFILE
	myfile.close();
	return true;
	}
return false;
#else
	return true;
#endif
}

int main()
{
	caseSolve("cleanRobot.txt", -1);
	getchar();
	return 0;
}
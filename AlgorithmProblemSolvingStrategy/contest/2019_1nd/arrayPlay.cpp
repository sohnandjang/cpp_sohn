#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
using namespace std;
//#define DUSEFILE 1

int A[1010][1010];
int N;
int M;

void displaySumofRow() {
	int sum = 0;
	for (int r = 0; r < N; r++){
		sum = 0;
		for (int c = 0; c < N; c++) {
			sum += A[r][c];
		}
		cout << sum << " ";
	}
	cout << endl;
}
void displaySumofCol() {
	int sum = 0;
	for (int c = 0; c < N; c++){
		sum = 0;
		for (int r = 0; r < N; r++) {
			sum += A[r][c];
		}
		cout << sum << " ";
	}
	cout << endl;
}

void arrayPlay(int r1, int c1, int r2, int c2, int v)
{
	for (int r = r1 - 1; r <= r2 - 1; r++) {
		for (int c = c1 - 1; c <= c2 - 1; c++) {
			A[r][c] += v;
		}
	}
}

void disp()
{
	cout << "disp:" << endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cout << A[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
	cout << endl;
}
#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	// init
	memset(A, 0, sizeof(A));
	// input
#ifdef DUSEFILE
	fs >> N;
	fs >> M;
#else
	cin >> N;
	cin >> M;
#endif
	//cout << "N,M=" << N << "," << M << endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			int temp;
#ifdef DUSEFILE
			fs >> temp;
#else
			cin >> temp;
#endif
			A[i][j] = temp;
		}
	}
	// body function
	//disp();
	for (int i = 0; i < M; i++)
	{
		int r1, c1, r2, c2, v;
#ifdef DUSEFILE
		fs >> r1; fs >> c1; fs >> r2; fs >> c2; fs >> v;
#else
		cin >> r1; cin >> c1; cin >> r2; cin >> c2; cin >> v;
#endif
		//cout << r1 << "," << c1 << "," << r2 << "," << c2 << "," << v << endl;
		arrayPlay(r1, c1, r2, c2, v);
		//disp();
	}
	displaySumofRow();
	displaySumofCol();
}

bool caseSolve(const char* path, int i_num)
{// i_num == -1 , don't care
	int num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		myfile >> num;
#else
		cin >> num;
#endif
		num = (i_num == -1 ? num : i_num);
		for (int i = 0; i < num; i++)
		{ //~ body
#ifdef DUSEFILE
			body(myfile);
#else
			body();
#endif
		}
#ifdef DUSEFILE
		myfile.close();
		return true;
	}
	return false;
#else
		return true;
#endif
}

int main()
{
	caseSolve("arrayPlay.txt", -1);
	getchar();
	return 0;
}
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <list>
#include <map>
#include <set>
#include <numeric>
//using namespace std;

#define SIZEOFA 100000
#define MOD     (SIZEOFA+1)
#define KEYINITIAL 100010
std::vector<long long> A(SIZEOFA + 1, 0); 
std::vector<long long> A_oriKey(SIZEOFA + 1, 0);
std::map<long long, long long>   mapKeyToConvKey;
std::map<long long, long long>::iterator it;

struct FenwickTree {
	std::vector<long long> tree;
	FenwickTree(long long n) : tree(n + 1)
	{
	}
	long long sum(long long pos)
	{
		++pos;
		long long ret = 0;
		while (pos > 0)
		{
			ret += tree[pos];
			pos &= (pos - 1);
		}
		return ret;
	}
	void add(long long pos, long long val)
	{
		++pos;
		while ((unsigned long long)pos < tree.size())
		{
			tree[pos] += val;
			pos += (pos & -pos);
		}
	}
};

enum { ADD = 1, SUBSUM = 2, SUB = 3 };
int N;
int funcOrder;
long long key;
long long value;
long long result;
long long idx1;
long long idx2;

FenwickTree FT(SIZEOFA + 1);

long long convertIndex(long long key, long long value)
{
	//long long MOD = SIZEOFA + 1;
	long long convertKey = key % MOD;
	if (A[convertKey] == 0) { // 1st
		A[convertKey] = value;
		mapKeyToConvKey[key]  = convertKey;
		return convertKey;
	}
	else { // 2nd
		if (A_oriKey[convertKey] == key) // same
			return convertKey; //false;
		else { // different
			while (1) {
				convertKey++;
				if (convertKey == 0) convertKey++;
				convertKey = convertKey % MOD;
				if (A[convertKey] == 0) {
					A[convertKey] = value;
					mapKeyToConvKey[key] =convertKey;
					return convertKey;
					//break; // found
				}
			}
		}
	}
	return convertKey;
}

void input()
{
	std::cin >> funcOrder;
	switch (funcOrder)
	{
	case ADD:
		std::cin >> key; std::cin >> value;
		break;
	case SUBSUM:
		std::cin >> idx1; std::cin >> idx2;
		break;
	case SUB:
		std::cin >> key;
		break;
	}
}
long long getConvertIdx(long long a)
{
	long long found = mapKeyToConvKey[a];
	if (found)
		return found;
	else // not found
		return (a % MOD);
}
void calc()
{
	funcOrder = 0; key = 0; value = 0;
	idx1 = 0; idx2 = 0;
	input();
	switch (funcOrder)
	{
	case ADD:
	{
		long long convertKey = convertIndex(key, value);
		FT.add(convertKey, value);
		std::cout << FT.sum(SIZEOFA) << " ";
	}
	break;
	case SUBSUM:
	{
		long long convertIdx1 = getConvertIdx(idx1);
		long long convertIdx2 = getConvertIdx(idx2);
		if (convertIdx1 == 0)
			std::cout << FT.sum(convertIdx2) << " ";
		else
			std::cout << FT.sum(convertIdx2) - FT.sum(convertIdx1 - 1) << " ";
	}
	break;
	case SUB:
	{
		long long temp = 0;
		long long convertKey = getConvertIdx(key);
		if (convertKey == 0)
			temp = FT.sum(convertKey);
		else
			temp = FT.sum(convertKey) - FT.sum(convertKey - 1);
		FT.add(convertKey, (-1)*temp);
		std::cout << FT.sum(SIZEOFA) << " ";
	}
	break;
	}
}

int main()
{
	std::cin >> N;
	for (int i = 0; i < N; i++)
		calc();
	return 0;
}

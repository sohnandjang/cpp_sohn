#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <list>
#include <numeric>
using namespace std;
#define DUSEFILE 1
//#define SIZEOFA 100001
#define SIZEOFA 100000

enum { ADD = 1, SUBSUM = 2, SUB = 3 };
int N;
//long long A[SIZEOFA+1] = { 0, };
vector<long long> A(0, SIZEOFA + 1);
int funcOrder;
long long key;
long long value;
long long result;
long long idx1;
long long idx2;

struct FenwickTree {
	std::vector<long long> tree;
	FenwickTree(int n) : tree(n + 1)
	{
		//std::iota(std::begin(tree), std::end(tree), 0);
	}
	long long sum(int pos)
	{
		++pos;
		long long ret = 0;
		while (pos > 0)
		{
			ret += tree[pos];
			pos &= (pos - 1);
		}
		return ret;
	}
	void add(long long pos, long val)
	{
		++pos;
		while ((unsigned long long)pos < tree.size())
		{
			tree[pos] += val;
			//std::cout << "pos=" << pos << "val=" << val << "tree[pos]=" << tree[pos] << std::endl;
			pos += (pos & -pos);
		}
	}
};

FenwickTree FT(SIZEOFA + 1);

//static int stNumOfCommand = 0;
#ifdef DUSEFILE
void calc(fstream& fs)
#else
void calc()
#endif
{
	// init
	funcOrder = 0; key = 0; value = 0;
	idx1 = 0; idx2 = 0;
	// input
#ifdef DUSEFILE
	fs >> funcOrder;
#else
	cin >> funcOrder;
#endif
	switch (funcOrder)
	{
	case ADD:
#ifdef DUSEFILE
		fs >> key; fs >> value;
#else
		cin >> key; cin >> value;
#endif
		break;
	case SUBSUM:
#ifdef DUSEFILE
		fs >> idx1; fs >> idx2;
#else
		cin >> idx1; cin >> idx2;
#endif
		break;
	case SUB:
#ifdef DUSEFILE
		fs >> key;
#else
		cin >> key;
#endif
		break;
	}
	switch (funcOrder)
	{
	case ADD:
		FT.add(key, value);
		cout << FT.sum(SIZEOFA) << " ";
		break;
	case SUBSUM:
		if (idx1 == 0)
			cout << FT.sum(idx2) << " ";
		else
			cout << FT.sum(idx2) - FT.sum(idx1 - 1) << " ";
		break;
	case SUB:
		long long temp = 0;
		if (key == 0)
			temp = FT.sum(key);
		else
			temp = FT.sum(key) - FT.sum(key - 1);
		//if (temp > 0)
		FT.add(key, temp*(-1));
		cout << FT.sum(SIZEOFA) << " ";
		break;
	}
}

#ifdef DUSEFILE
void body(fstream& fs)
#else
void body()
#endif
{
	bool ret = false;
	// init
#ifdef DUSEFILE
	fs >> N;
	//unsigned int n = SIZEOFA;
	for (unsigned int i = 0; i < SIZEOFA; i++)
	{
		FT.add(i, A[i]);
	}
#else
	cin >> N;
#endif
	// body
	for (int i = 0; i < N; i++)
#ifdef DUSEFILE
		calc(fs);
#else
		calc();
#endif
}

bool caseSolve(const char* path, int i_num)
{// i_num == -1 , don't care
	int num = 0;
#ifdef DUSEFILE
	fstream myfile;
	myfile.open(path);
	if (myfile.is_open())
	{
		//myfile >> num;
#else
	//cin >> num;
#endif
	//num = (i_num == -1 ? num : i_num);
	num = 1;
	for (int i = 0; i < num; i++)
	{ //~ body
#ifdef DUSEFILE
		body(myfile);
#else
		body();
#endif
	}
#ifdef DUSEFILE
	myfile.close();
	return true;
	}
return false;
#else
	return true;
#endif
}

int main()
{
	cin.tie(NULL);
	ios_base::sync_with_stdio(false);
	caseSolve("calc.txt", 1);
	getchar();
	return 0;
}
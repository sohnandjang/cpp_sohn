#include <iostream>
#include <stdio.h>
#include <algorithm>
using namespace std;

int A[] = { 1, 5, 4, 7 , 5, 4, 6, 7 };
int cache3[8+1]; //cache[100] : max
int cache2[8+1]; //cache[100] : max
int n =  8;

int lis3(int start)
{
	int& ret = cache3[start+1];
	if (ret != -1)
		return ret;
	ret = 1;
	for (int next = start+1; next < n; next++)
	{
		if ( start== -1 || A[start] < A[next])
			ret = max(ret, lis3(next) + 1);
	}
	return ret;
}

int lis2(int start)
{
	int& ret = cache2[start];
	if (ret != -1)
		return ret;
	ret = 1;
	for (int next = start; next < n; next++)
	{
		if (A[start] < A[next])
			ret = max(ret, lis2(next) + 1);
	}
	return ret;
}


/*
//return : max length of lis
int lis2(int start)
{
	int& ret = cache[start];
	if (ret != -1)
	{
		cout << "cache use : start: " <<start << endl;
		return ret;
	}
	//int ret = 1; //*** not 0 *** itself
	ret = 1;
	if (start == n - 1) // base: end
		return 1;

	//for (int i = 1; i < n-start+1; i++) //*** caution *** : i<n-start+1 not i<n 
	//{
	//	if(A[start] < A[start+i])//*** caution ***: A[start+i] not A[i]
	//		ret = max(ret, 1 + lis(start + i));
	//}
	for (int next = start + 1; next < n; next++)
	{
		if (A[start] < A[next])
			ret = max(ret, 1 + lis(next));
	}
	return ret;
}
*/

int main()
{
	memset(cache2, -1, sizeof(cache2));
	memset(cache3, -1, sizeof(cache3));
	int maxLen2 = 0;
	int maxLen3 = 0;
	//~ n >=0 assume
	for(int i=0; i<n; i++)
		maxLen2 = max(maxLen2, lis2(i));
	maxLen3 = lis3(-1)-1; //here: sub of result
	//int i = 0;
	//maxLen = lis(i);
		
	cout << "max lis2 = " << maxLen2 << endl;
	cout << "max lis3 = " << maxLen3 << endl;
	getchar();
	return 0;
}
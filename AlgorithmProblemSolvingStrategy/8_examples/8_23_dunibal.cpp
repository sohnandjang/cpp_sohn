#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <fstream>
using namespace std;

int n, d, p, q;
int connected[51][51];
int deg[51];
int retNum;
double cache[51][51];
double search2(int here, int days)
{
	//printf("[search2] here:days = %d:%d\n", here, days);
	if (days == d) //~ base
	{
		if (here == q)
			return 1.0;
		else
			return 0.0;
	}
	double& ret = cache[here][days];
	if (ret > -0.5)
		return ret;
	ret = 0.0;
	for (int there = 0; there < n; there++)
	{
		if (connected[here][there] == 1)
		{
			ret = ret + search2(there, days + 1) / deg[here];
			//printf("ret = %f\n", ret);
		}
	}
	return ret;
}

double search3(int here, int days)
{
	//printf("[here:days]=%d:%d\n", here, days);
	if (days == 0)
	{
		if (here == p)
		{
			//printf("return 1.0\n");
			return 1.0;
		}
		else
		{
			//printf("return 0.0\n");
			return 0.0;
		}
	}
	double&  ret = cache[here][days];
	if (ret > -0.5)
		return ret;
	ret = 0.0;
	for (int prev = 0; prev < n; prev++)
	{
		if (connected[prev][here] == 1)
		{
			ret +=  search3(prev, days - 1)/deg[prev];
		}
	}
	//printf("ret = %f\n", ret);
	return ret;
}

int main()
{
	const char*  DATA_FILE_NAME = "input_dunibal.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;
	//c = 1;

	for (int i = 0; i < c; ++i)
	{
		fs >> n; // number of village
		fs >> d; // date passed until now
		fs >> p; // number of village that has prison
		memset(connected, 0, sizeof(connected));
		memset(deg, 0, sizeof(deg));
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < n; k++)
			{
				fs >> connected[j][k];
				if (connected[j][k] == 1)
					deg[j]++;
			}
		}
		fs >> retNum;
		for( int i=0; i<retNum; i++)
		{
			fs >> q;
			//printf("q = %d\n", q);
			//~ search2
			memset(cache, -1, sizeof(cache));
			//double ret = search2(p, 0);
			double ret = search3(q, d);
			cout << ret << ' ';
		}
		cout << endl;
	}
	getchar();
	return 0;
}

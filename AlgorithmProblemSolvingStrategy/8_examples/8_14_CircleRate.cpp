#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;

const int INF = 987654321;
string A;
int cache[10002];

int check(int start, int len)
{
	string B = A.substr(start, len);
	//condition1
	int condition1 = 0;
	for (int i = 0; i < len; i++)
	{
		if (B[i] == B[0])
			condition1 = 1;
		else
		{
			condition1 = 0;
			break;
		}
	}
	if (condition1)
		return 1;
	//condition2
	int condition2 = 0;
	int incBy1 = (B[1] - B[0]) > 0 ? 1 : 0;
	for (int i = 0; i < len-1; i++)
	{
		if (incBy1)
		{
			if (B[i + 1] - B[i] == 1)
				condition2 = 1;
			else
			{
				condition2 = 0;
				break;
			}
		}
		else
		{
			if (B[i] - B[i+1] == 1)
				condition2 = 1;
			else
			{
				condition2 = 0;
				break;
			}

		}
	}
	if (condition2)
		return 2;

	//condition3
	int condition3 = 0;
	int first = B[0];
	int second = B[1];
	for (int i = 0; i < len; i++)
	{
		if (i % 2 == 0)
		{
			if (B[i] == B[0])
				condition3 = 1;
			else
			{
				condition3 = 0;
				break;
			}
		}
		else
		{
			if (B[i] == B[1])
				condition3 = 1;
			else
			{
				condition3 = 0;
				break;
			}
		}
	}
	if (condition3)
		return 4;
	
	//condition4
	int condition4 = 0;
	for (int i = 0; i < len - 1; i++)
	{
		if (B[i + 1] - B[i] == B[1] - B[0])
		{
			condition4 = 1;
		}
		else
		{
			condition4 = 0;
			break;
		}
	}
	if (condition4)
		return 5;

	return 10;
}

int solve(int start)
{
	int& ret = cache[start];
	if (ret != -1)
	{
		cout << "use cache[" << start << "]" << endl;
		return ret;
	}
	
	ret = INF;
	if (start == A.size())//base : arrive at the end 
		return ret = 0;

	for(int i=3; i<=5; i++)
	{
		if (start + (i - 1) <= A.size() - 1)
			ret = min(ret, check(start, i) + solve(start + i));
	}
	return ret;
}


int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input_8_14.txt";
	ifstream fs(DATA_FILE_NAME);

	int maxSize = -1;
	int c;
	int result = 0;
	fs >> c;

	for (int i = 0; i < c; ++i)
	{
		fs >> A;
		memset(cache, -1, sizeof(cache));
		result = solve(0);
		cout << result << endl;
		A.clear();
	}
	getchar();
	return 0;
}

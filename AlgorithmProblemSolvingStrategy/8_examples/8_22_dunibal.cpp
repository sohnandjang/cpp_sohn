#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <fstream>
using namespace std;

int n, d, p, q;
int connected[51][51];
int deg[51];
int retNum;

double solve(vector<int>& path)
{
	//printf("solve:path=");
	//for (int i = 0; i < path.size(); i++)
	//	printf("%d ", path[i]);
	//printf("\n\n");

	//base
	double ret = 0.0;
	int top = path.back();
	if (path.size() == d+1) //~ to do : check
	{
		if (top != q)
		{
			//printf("(top:%d, q:%d)return 0.0\n", top, q);
			return 0.0;
		}
		else
		{ 
			//printf("(top:%d, q:%d)return 1.0\n", top, q);
			return 1.0;
		}
	}
	for (int i = 0; i < n; i++)
	{
		if (connected[top][i] == 1)
		{
			//printf("[top:%d][%d]==1\n", top, i);
			path.push_back(i);
			ret += solve(path);
			//printf("(return)ret = %f\n", ret);
			path.pop_back();
		}
	}
	ret = ret / (double)deg[top];
	//printf("(return)ret = %f\n", ret);
	return ret;
}

int main()
{
	const char*  DATA_FILE_NAME = "input_dunibal.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;
	//c = 1;

	for (int i = 0; i < c; ++i)
	{
		fs >> n; // number of village
		fs >> d; // date passed until now
		fs >> p; // number of village that has prison
		memset(connected, 0, sizeof(connected));
		memset(deg, 0, sizeof(deg));
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < n; k++)
			{
				fs >> connected[j][k];
				if (connected[j][k] == 1)
					deg[j]++;
			}
		}
		fs >> retNum;
		for( int i=0; i<retNum; i++)
		{
			fs >> q;
			//~ solve
			vector<int> path;
			path.push_back(p);
			double ret = solve(path);
			cout << ret << ' ';
		}
		cout << endl;
	}
	getchar();
	return 0;
}

#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

const int INF = 987654321;
int num = 0;
int groupNum = 0;
int A[102] = { 0, };
int cache[102][12] = { -1, };

int minError(int start, int end)
{
	int sum = 0;
	int len = end - start + 1;
	int avg = 0;
	int minSum = INF;
	int ret = 0;
	int rep = 0;
	int minIndex = 0;
	for (int rep = A[start]; rep <= A[end]; rep++)
	{
		sum = 0;
		for (int j = start; j <= end; j++)
		{
			sum += (A[j] - rep) * (A[j] - rep);
		}
		minSum = min(minSum, sum);
	}
	ret = minSum;
	//find average
	/*
	for (int i = start; i <= end; i++)
	{
		sum += A[i];
	}
	avg = (in)((sum +0.5) / (float)len);
	for (int j = start; j <= end; j++)
	{
		ret += (A[j] - avg)*(A[j] - avg);
	}
	*/
	return ret;
}

int quantization(int start, int groupSize)
{
	//printf("quantization: start=%d, groupSize=%d\n", start, groupSize);
	int& ret = cache[start][groupSize]; // INF;
	if (ret != -1)
	{
		//printf("cache use!\n");
		return ret;
	}
	if (groupSize == 0)
	{
		if (start == num)
			return ret=0; //0
		else
			return ret=INF;
	}

	ret = INF;
	//for (int i = 1; i < num; i++)
	//{
	//	if( start + i <= num)
	//		ret = min(ret, minError(start, start + i - 1) + quantization(start + i, groupSize - 1));
	//}
	for (int next = start; next < num; next++)
	{
		ret = min(ret, minError(start, next) + quantization(next+1, groupSize - 1));
	}
	//printf("quantization: start=%d, groupSize=%d, ret=%d\n", start, groupSize, ret);
	return ret;
}

int compare(const void* a, const void* b)
{
	return (*(int*)a - *(int*)b);
}

int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input_8_15_1.txt";
	ifstream fs(DATA_FILE_NAME);

	int maxSize = -1;
	int c;
	int result = 0;
	fs >> c;
	//c = 1;

	for (int i = 0; i < c; ++i)
	{
		fs >> num;
		fs >> groupNum;
		//printf("num=%d, groupNum=%d\n", num, groupNum);
		memset(A, 0, sizeof(A));
		memset(cache, -1, sizeof(cache));
		for (int j = 0; j < num; ++j)
		{
			fs >> A[j];
		}
		qsort(A, num, sizeof(int), compare);
		//printf("A: ");
		//for (int k = 0; k < num; ++k)
		//	printf("%d ", A[k]);
		//printf("\n");
		result = quantization(0, groupNum);
		cout << result << endl;
	}
	getchar();
	return 0;
}

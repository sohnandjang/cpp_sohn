#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;

vector<int> A;
vector<int> B;

const long long NEGINF = numeric_limits<long long>::min();

int solve(int startA, int startB)
{
	int ret = 1;
	ret = 2;
	long long a = (startA == -1 ? NEGINF : A[startA]);
	long long b = (startB == -1 ? NEGINF : B[startB]);

	int maxElement = max(a, b);
	for (int nextA = startA + 1; nextA < A.size(); nextA++)
	{
		if (maxElement < A[nextA])
			ret = max(ret, solve(nextA, startB) + 1);
	}
	for (int nextB = startB + 1; nextB < B.size(); nextB++)
	{
		if (maxElement < B[nextB])
			ret = max(ret, solve(startA, nextB) + 1);
	}
	if(startA==-1 || startB==-1)
		printf("startA:%d, startB:%d, ret=%d\n", startA,  startB,  ret);
	else
		printf("startA:%d, startB:%d, ret=%d\n", startA,  startB,  ret);
zzzz	return ret;
}
	
int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);

	int maxSize = -1;
	int c;
	fs >> c;
	//c = 1;

	for (int i = 0; i < c; ++i)
	{
		int numA = 0, numB = 0;;
		fs >> numA;
		fs >> numB;
		for (int j = 0; j < numA; ++j) //~input: A
		{
			int temp = 0;
			fs >> temp;
			A.push_back(temp);
		}
		for (int k = 0; k < numB; ++k) //~input: B
		{
			int temp = 0;
			fs >> temp;
			B.push_back(temp);
		}
		maxSize = solve(-1, -1)-2;
		cout << maxSize << endl;
		A.clear();
		B.clear();
	}
	getchar();
	return 0;
}


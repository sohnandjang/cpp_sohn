#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

const int m = 5; // day 
const int n = 9; // height

//return : the numbers of case
int cache[102][102];
int Climb(int days, int climbed)
{
	if (days == m)
		return climbed >= n ? 1 : 0;

	int& ret = cache[days][climbed];
	if (ret != -1)
		return ret;
	return ret = Climb(days + 1, climbed + 1) + Climb(days + 1, climbed + 2);
}
int myClimb(int days, int climbed)
{
	int& ret = cache[days][climbed];
	if (ret != -1)
	{
		//cout << "cache usage" << endl;
		return ret;
	}
	if (days == 0)
		return ret = 0;
	if (days == 1 && climbed == 1 || days == 1 && climbed == 2)
		return ret = 1;
	ret = myClimb(days - 1, climbed - 1) + myClimb(days - 1, climbed - 2);
	return ret;
}

int main()
{
	memset(cache, -1, sizeof(cache));
	//int result = myClimb(5, 9); //~can't detect over : related to problem definition
	//cout << result << endl;
	int result = Climb(0, 0);
	cout << result << endl;
	getchar();
	return 0;
}

#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

const int MOD = 10 * 1000 * 1000;
int input[] = { 2, 4, 92 };
int cache[102][102];

int poly(int n, int first)
{
	if (n == first)
		return 1;
	int& ret = cache[n][first];
	if (ret != -1)
		return ret;
	ret = 0; //****** very important ******* 
	for (int second = 1; second <= n - first; second++)
	{
		ret = (ret + (first + second - 1) * poly(n - first, second)) % MOD;
	}
	return ret;
}
int main()
{
	int ret = 0;
	for (int c = 0; c < 3; c++)
	{
		ret = 0; //~ initialization
		memset(cache, -1, sizeof(cache));
		int n = input[c];
		for (int i = 1; i <= n; i++)
		{
			ret = (ret + poly(n, i))%MOD;
		}
		printf("ret = %d\n", ret);
	}
	getchar();
	return 0;
}

#include <iostream>
#include <stdio.h>
#include <algorithm>
using namespace std;

constexpr int mapSize = 5;
const int map[mapSize][mapSize] =
{
	{6, 0, 0, 0, 0},
	{1, 2, 0, 0, 0},
	{3, 7, 4, 0, 0},
	{9, 4, 1, 7, 0},
	{2, 7, 5, 9, 4},
};

int cache[mapSize][mapSize];
//2. memoization removing debug
#if 1

int solve(const int row, const int col)
{
	if (row == mapSize - 1) //base
		return map[row][col]; 
	
	int& ret = cache[row][col]; //cache check
	if (ret != -1)
		return ret;

	ret = max(solve(row + 1, col), solve(row + 1, col + 1)) + map[row][col];
	return ret;
}
int main()
{
	memset(cache, -1, sizeof(cache));
	int maxSum = solve(0, 0);
	cout << "maxSum : " << maxSum << endl;
	getchar();
}
#endif
//2. memoization
#if 0

int solve(const int row, const int col)
{
	cout << "row: " << row << " col:" << col << endl;
	int ret1 = 0, ret2 = 0;
	int retTemp = 0;
	int& ret = cache[row][col];
	if (ret != -1)
	{
		printf("*** cache use ***: row,col:%d,%d\n", row, col);
		return ret;
	}

	if (row == mapSize - 1) //base
	{
		cout << " base condition: " << " row:" << row << " col:" << col << "return:" << map[row][col]<<endl;
		ret = map[row][col]; //update cache
		return ret;
	}

	ret1 = solve(row+1, col);
	ret2 = solve(row + 1, col + 1);
	retTemp = max(ret1, ret2);
	ret = retTemp + map[row][col];
	cout << "row:"<<row<<" col:"<<col<< " ret1: " << ret1 << " ret2: " << ret2 << " retTemp: " << retTemp <<" ret: "<< ret << endl;
	return ret;
}
int main()
{
	memset(cache, -1, sizeof(cache));
	int maxSum = solve(0, 0);
	cout << "maxSum : " << maxSum << endl;
	getchar();
}
#endif
#if 0
//1. brute force
int solve(const int row, const int col)
{
	cout << "row: " << row << " col:" << col << endl;
	int ret1 = 0, ret2 = 0;
	int retTemp = 0;
	int ret = 0;
	if (row == mapSize - 1) //base
	{
		cout << " base condition: " << " row:" << row << " col:" << col << "return:" << map[row][col]<<endl;
		return map[row][col];
	}

	ret1 = solve(row+1, col);
	ret2 = solve(row + 1, col + 1);
	retTemp = max(ret1, ret2);
	ret = retTemp + map[row][col];
	cout << "row:"<<row<<" col:"<<col<< " ret1: " << ret1 << " ret2: " << ret2 << " retTemp: " << retTemp <<" ret: "<< ret << endl;
	return ret;
}
int main()
{
	int maxSum = solve(0, 0);
	cout << "maxSum : " << maxSum << endl;
	getchar();
}
#endif
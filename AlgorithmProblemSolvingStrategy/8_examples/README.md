# Code 8.13 JLIS
If I choose A[0],  
![](image/1.PNG =1x1)
problem: jlis(indexA, indexB) duplication (Both left and right exist)  

So,
![](image/2.PNG =1x1)  

1. We need a rule
    1. jlis(indexA, indexB)
        1. if A[indexA] < B[indexB]
            1. {A[indexA], B[indexB], ... }  
            ... = we have to pick up the number bigger than max(A[indexA], B[indexB])
2. index starts from -1


* input rage : 32big signed integer
    * intentional min value should be **64bit.**
        * const long long NEGINF = numeric_limits<long long>::min();


# Code 8.14 
**const int INF = 987654321;**    
The author of this book often uses 987,654,321. (refer to p68)  

1. It is enough bit near 2^30
2. Even though adding each other, overflow doesn't happen in 32bit signed integer
    1. 987,654,321 x 2 = 1,976,308,642 < 2^31-1 ~ 2,000,000,000 - 1
3. Readible about type rather than 1,000,000,000

# Snail Climb
It is very important to define function.  

1. Climb(days, climbed)  
    1. days: start from 0 (of course, it can be final day but be careful not to detect over the height of well)
    2. climbed : start from 0 (of course, it is similiar upper case)  

Climb(days, climbed) = Climb(days+1, climbed+1) + Climb(days+1, climbed+2)

* useful)
	* Climb(days, climbed) = 0.75\*Climb(days+1, climbed+1) + 0.25\*Climb(days+1, climbed+2)

# Asymmetric Tiling
** Caution: when you use cache, don't reuse it in other function and make another cache **

# When you use cache
** Caution: after you find cache value is -1(init), you need to make variable 0 (init) **
``` c
int& ret = cache[][];
if(ret != -1)
    return ret;
ret = 0; //*** very important ***
```  

** Caution: you must make sure if array is initialized before meeting new testcase **  

** Caution: if the type of cache is double, when you compare the init value after memset(cache, -1, sizeof(cache)), **  
``` c
if(cache[][] > -0.5) // double
	return cache[][]
rather thatn
if(cache[][] != -1) // int
	return cache[][]

```

# dunibal doctor
* search(path)
	* Try creating all the paths that start with p and calculate the probability that the q will appear and return their sum.
* search2(here, days)
	* when Dr.dunibal was hiding in town here on 'days', return the conditional probability of being in town q on the last day.
* search3(here, days) : solve from opposite direction
	* The probability that Dr.Dunnibal will be hiding in the village on the 'days' of the escape.

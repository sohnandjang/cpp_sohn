#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

// 2 x n : rectangular
const int MOD = 1000000007;
int cache[101];

int tiling(int n) // n = width
{
	int& ret = cache[n];
	if (ret != -1)
		return ret;
	if (n == 1)
		return ret = 1;
	if (n == 2)
		return ret = 2;
	ret = (tiling(n - 1) + tiling(n - 2))% MOD;
	printf("n=%d, ret=%d\n", n, ret);
	return ret;
}

int main()
{
	memset(cache, -1, sizeof(cache));
	int result = tiling(5);
	cout << result << endl;
	getchar();
	return 0;
}

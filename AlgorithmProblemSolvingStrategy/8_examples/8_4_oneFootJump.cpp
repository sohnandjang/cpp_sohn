#include <iostream>
#include <stdio.h>
using namespace std;

#define MAX 7

int board[MAX][MAX] = { //row, col
	{2,5,1,6,1,4,1},
	{6,1,1,2,2,9,3},
	{7,2,3,2,1,3,1},
	{1,1,3,1,7,1,2},
	{4,1,2,3,4,1,2},
	{3,3,1,2,3,4,1},
	{1,5,2,9,4,7,0},
};

int cash[MAX][MAX];

void displayCash(void)
{
	for (int i = 0; i < MAX; ++i)
		for (int j = 0; j < MAX; ++j)
			printf("cash[%d][%d]=%d\n", i, j, cash[i][j]);
}
#if 0
int jump(int row, int col)
{//return : 1 (ok), 0 (ng)
	int ret = 0;
	//1. base
	static int cnt = 0;
	cnt++;
	//cout << "row,col = " << row <<','<< col <<"cnt="<< cnt <<endl;
	printf("row,col=%d,%d, cnt=%d\n", row, col, cnt);
	do {
		if (row >= MAX || col >= MAX) //~ out of range
			break;
		if (row == MAX - 1 && col == MAX - 1)
		{
			ret = 1; //~ reach destination : OK
			break;
		}
		if (cash[row][col] != -1)
		{ 
			printf("cash is used: row,col=%d,%d", row, col);
			return cash[row][col];
		}

		int adj = board[row][col];
		ret = jump(row + adj, col) || jump(row, col + adj);
		cash[row][col] = ret;
		//displayCash();
	} while (0);
	return ret;
}
#else
int jump(int row, int col)
{
	if (row >= MAX || col >= MAX)
		return 0;
	if (row == MAX - 1 && col == MAX - 1)
		return 1;
	int& ret = cash[row][col];
	if (ret != -1) return ret;
	int jumpSize = board[row][col];
	ret = jump(row, col + jumpSize) || jump(row + jumpSize, col);
	return ret;
}
#endif


int main()
{
	memset(cash, -1, sizeof(cash));

	int ret = 0;
	ret = jump(0, 0);
	cout << "result=" << ret << endl;
	getchar();
	return 0;
}
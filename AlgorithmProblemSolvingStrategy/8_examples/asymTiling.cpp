#include <iostream>
#include <stdio.h>
#include <string>
#include <algorithm>
#include <fstream>
using namespace std;

const int C = 4;
const int n[10] = { 8, 2, 4,  92 };
const int MOD = 1000000007;
int cache[102];
int cache1[102];

int tiling(int n) // n = width
{
	int& ret = cache1[n];
	if (ret != -1)
		return ret;
	//if (n == 1)
	if (n <= 1)
		return ret = 1; //~ important: tiling(0) = 1
	//if (n == 2)
	//	return ret = 2;
	ret = (tiling(n - 1) + tiling(n - 2))% MOD;
	//printf("n=%d, ret=%d\n", n, ret);
	return ret;
}

int asymmetric2(int width)
{
	if (width <= 2)
		return 0;

	int& ret = cache[width];
	if (ret != -1)
		return ret;
	ret = asymmetric2(width - 2)%MOD; //~1
	printf("%s-1]width:%d, ret=%d\n", __FUNCTION__, width, ret);
	ret = (ret + asymmetric2(width - 4))%MOD; //~2
	printf("%s-2]width:%d, ret=%d\n", __FUNCTION__, width, ret);
	ret = (ret + tiling(width - 3))%MOD; //~3
	printf("%s-3]width:%d, ret=%d, tiling(%d)=%d\n", __FUNCTION__, width, ret, width-3, tiling(width-3));
	ret = (ret + tiling(width - 3))%MOD; //~4
	printf("%s-4]width:%d, ret=%d, tiling(%d)=%d\n", __FUNCTION__, width, ret, width-3, tiling(width-3));
	return ret;
}

int asymmetric(int width)
{
	if (width % 2 == 1) {
		return (tiling(width) - tiling(width / 2) + MOD) % MOD;
	}
	int ret = tiling(width);
	ret = (ret - tiling(width / 2) + MOD) % MOD;
	ret = (ret - tiling(width / 2 - 1) + MOD) % MOD;
	printf("%s]width:%d, ret=%d\n", __FUNCTION__, width, ret);
	return ret;
}
int myAsymtiling(int n)
{
	int sym = 0;
	//sym
	if (n % 2 == 1)
		sym = (sym + tiling((n - 1) / 2)) % MOD;
	else
	{
		sym = (sym + tiling(n / 2)) % MOD;
		sym = (n==2)? sym+1:(sym + tiling(n / 2 - 1)) % MOD; //~ unless doing like this, we have to define tiling(0)=1
	}
	int ret = (tiling(n) - sym+MOD)%MOD;
	printf("ret = %d, n = %d, sym = %d, tiling(%d)=%d\n", ret, n, sym, n, tiling(n));
	return ret;
}

int main()
{
	for (int i = 0; i < 1; i++)//C; i++)
	{
		memset(cache, -1, sizeof(cache));
		memset(cache1, -1, sizeof(cache));
		//int res = myAsymtiling(n[i]);
		//cout << res << endl;
		int res = asymmetric(n[i]);
		cout << res << endl;
		memset(cache, -1, sizeof(cache));
		memset(cache1, -1, sizeof(cache));
		 res = asymmetric2(n[i]);
		cout << res << endl;
	}
	getchar();
	return 0;
}

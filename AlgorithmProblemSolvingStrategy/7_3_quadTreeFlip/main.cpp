#include <iostream>
#include <fstream>
#include <string>
using namespace std;

string reverse(string::iterator& it)
{
	char head = *it;
	++it;
	if (head == 'b' || head == 'w')
		return string(1, head);
	string upperLeft = reverse(it);
	string upperRight = reverse(it);
	string lowerLeft = reverse(it);
	string lowerRight = reverse(it);
	//change location of pices (upsidedown)
	return string("x") + lowerLeft + lowerRight + upperLeft + upperRight;
}

int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;

	for (int i = 0; i < c; i++)
	{
		string x;
		string output;
		fs >> x;
		string::iterator it = x.begin();
		output = reverse(it);
		cout << output << endl;
	}
	getchar();
	return 0;
}
# The process finding 2 balls of 4 balls
```
 -> (a) -> (a, b)   
        <-   
        -> (a, c)   
        <-   
        -> (a, d)  
 <--------  
 -> (b) -> (b, c)  
        <-  
        -> (b, d)  
        <-  
 <--------  
 -> (c) -> (c, d)  
 <--------  
 -> (d)  
 <-  
```
        
# picnic
![](images/picnic.PNG)
![](images/picnic.JPG)

# boardcover
1. book : vector<vector<int>> board
2. me : int board[20][20]
3. question1
	1. 1,2 examples, no time difference between book's way and mine (almost 0)
	2. 3 examples(1415 case), book: 0.11 sec, mine : 0.9 sec (a little bit better)
		1. I've tested in gcc, however when I've tested in Visual Studio  
		I felt it seemed to be longer than 2~3 sec.
4. question2
	1. when I declare, 2d vector like vector<vector<int> board(20, vector<int>(20));
		1. I wanted to use board[i][j]=val, but in for/for loop, below error happened  
		'vector subscript out of range', I couldn't understand
		2. So I had to alloc memory below  
		```
		for ~~~
			board.push_back(vector<int>());
			for ~~~~
				board[i].push_back(val);
		```

# ClockSync
if clock is pushed 4 times,  
it recovers original location.  

so when you recursive,  
before you move next switch,  
if you push clock 4 times,  
you don't need to do anything.  
Because you restore the original location.  

In my case,
``` c++
push 0, 1, 2, 3
call recursive
push-revert 0, 1, 2, 3
```
However, in the book
``` c++
call recursive
push 1, 2, 3, 4 (4=revert the location)
don't need anything
```
ps. g++ is much faster than visual studio

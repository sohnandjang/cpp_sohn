#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
//#include <sys/time.h>
#include <time.h>
using namespace std;
//struct timeval  tv1, tv2;
using namespace std;

int C;
int H;
int W;
int whiteNum;
enum {
	WHITE=0,
	BLACK=1,
};
//vector<vector<int>> board;
vector<vector<int>> board(20, vector<int>(20, 0));
const int coverType[4][3][2] = 
{
	{ { 0,0}, {1,-1}, {1,0}},
	{ { 0,0}, {1,0}, {1,1}},
	{ { 0,0}, {0,1}, {1,1}},
	{ { 0,0}, {1,0}, {0,1}},
};
void init()
{
	board.clear();
	whiteNum = 0;
}
bool set(vector<vector<int>>& board, int y, int x, int type, int delta)
{
	bool ok = true;
	for (int i = 0; i < 3; ++i)
	{
		const unsigned int ny = y + coverType[type][i][0];
		const unsigned int nx = x + coverType[type][i][1];
		//printf("ny,nx = %d, %d\n", ny, nx);
		if (ny < 0 || ny >= board.size() || nx < 0 || nx >= board[0].size())
			ok = false;
		else
		{
			//printf("else\n");
			//printf("before board[%d][%d] = %d, delta=%d\n", ny, nx, board[ny][nx], delta);
			board[ny][nx] += delta;
			//if (board[ny][nx] += delta > 1)
			if (board[ny][nx] > 1)
				ok = false;
			//printf("after board[%d][%d] = %d, delta=%d\n", ny, nx, board[ny][nx], delta);
		}
	}
	return ok;
}
void disp(void)
{
	for (unsigned int i = 0; i != H; ++i)//board.size(); ++i)
	{
		for (unsigned int j = 0; j != W; ++j)//board[0].size(); ++j)
		{
			if (board[i][j] == WHITE)
				cout << '.';
			else
				cout << '#';
		}
		cout << endl << endl;
	}
	cout << endl;
}
int boardcover(vector<vector<int>>& board)
{
	//disp();
	if (whiteNum % 3 != 0) return 0;
	int y = -1;
	int x = -1;
	for (int i = 0; i < H; i++)//board.size(); i++)
	{
		for (int j = 0; j < W; j++)//board[0].size(); j++)
		{
			if (board[i][j] == WHITE)
			{
				y = i;
				x = j;
				break;
			}
		}
		if (y!=-1) break;
	}
	if (y == -1) return 1; //all black
	int ret = 0;
	for (int type = 0; type < 4; ++type)
	{
		if (set(board, y, x, type, 1))
		{
			ret += boardcover(board);
		}
		set(board, y, x, type, -1);
	}
	return ret;
}
int main()
{
	fstream myfile;
	myfile.open("boardcover.txt");
	if (myfile.is_open())
	{
		myfile >> C;
		//printf("C=%d\n", C);
		C = 1;
		for (int i = 0; i < C; i++)
		{
			//printf("start) i=%d\n", i);
			//~init
			init();
			myfile >> H; 
			myfile >> W;
			vector<vector<int>> temp(20, vector<int>(20, 0));
			//printf("boardsize=%d, board[0].size=%d\n", board.size(), board[0].size());
			for (int j = 0; j < H; j++)
			{
				//board.push_back(vector<int>());
				for (int k = 0; k < W; k++)
				{
					char ch;
					myfile >> ch;
					if (ch == '#')
					{
			printf("boardsize=%d, board[0].size=%d\n", temp.size(), temp[0].size());
						//board[j].push_back(BLACK);
						//printf("board[%d][%d]=%d\n", j, k, board[0][0]);
						//board[j][k] = BLACK;
					}
					else
					{
						//board[j].push_back(WHITE);
						//board[j][k] = WHITE;
						whiteNum++;
					}
				}
			}
			//printf("complete map\n");
			//~ ************ myCode ***************
			//gettimeofday(&tv1, NULL);
			//int ret = boardcover(board);
			//gettimeofday(&tv2, NULL);
			//printf("Time taken in execution = %f seconds\n",
			//	(double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
			//	(double)(tv2.tv_sec - tv1.tv_sec));
			int ret = 0;
			cout << ret << endl;
			//~ ***********************************
		}
		//printf("file close\n");
		myfile.close();
	}
	getchar();
	return 0;

}

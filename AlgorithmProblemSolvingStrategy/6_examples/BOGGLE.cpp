#include <iostream>
#include <string>
using namespace std;

constexpr int maxSize = 5;
constexpr char map[maxSize][maxSize] = {
	{'U','R','L','P','M'},
	{'X','P','R','E','T'},
	{'G','I','A','E','T'},
	{'X','T','N','Z','Y'},
	{'X','O','Q','R','S'},
};
int dx[] = { 0, 0, 1, -1, 1, 1, -1, -1 };
int dy[] = { -1, 1, 0, 0, -1, 1, -1, 1 };

int hasWord(int y, int x, const char* word)
{
	int ret = 0;
	//~ base condition
	//~ bound
	if (y < 0 || y >= maxSize ) return 0;
	if (x < 0 || x >= maxSize ) return 0;
	if (word[0] == 0) return 1;
	//~ check if word is different?
	if (map[y][x] != word[0])
	{
		printf("block = %c,0x%x, y,x=%d,%d)\n", word[0],word[0], y, x);
		return 0;
	}
	else
	{
		printf("pass = %c(y,x=%d,%d)\n", word[0], y, x);
	}
	//~ check one word and match?
	if (sizeof(word) == 1)
	{
		if (map[y][x] == word[0])
		{
			printf("I found it = %c\n", word[0]);
			return 1;

		}
	}
	if (word == NULL)
		printf("y,x=%d,%d, word=NULL\n", y, x);
	else
		printf("y,x=%d,%d, word=%s\n", y, x, word);
	const char* preWord = word;
	word = word + 1;
	for (int i = 0; i < sizeof(dx)/sizeof(dx[0]); i++)
	{
		ret = ret || hasWord(y + dy[i], x + dx[i], word);
	}
	//word = preWord;
	printf("ret = %d\n", ret);
	return ret;
}

int hasWord2(int y, int x, const string& word)
{
	int ret = 0;
	if (y < 0 || y >= maxSize ) return 0;
	if (x < 0 || x >= maxSize ) return 0;
	if (word.size() == 0) return 1;
	if (map[y][x] != word[0])
	{
		return 0;
	}
	if (word.size() == 1)
	{
		if (map[y][x] == word[0])
		{
			printf("I found it = %c(y,x=%d,%d)\n", word[0], y, x);
			return 1;

		}
	}

	cout << "pre: " << word << endl;
	const string nextWord = word.substr(1, string::npos);
	cout << "after: " << word << endl;
	for (int i = 0; i < sizeof(dx)/sizeof(dx[0]); i++)
	{
		ret = ret || hasWord2(y + dy[i], x + dx[i], nextWord);
	}
	if (ret == 1)
		printf("y,x = %d, %d (word=%c)\n ", y, x, word[0]);
	//printf("ret = %d\n", ret);
	return ret;
}
int main()
{
	int ret = 0;
	//ret = hasWord(1, 1, "PRETTY");// "PRE");
	ret = hasWord2(1, 1, "PRETTY");// "PRE");
	cout << "final:" << ret << endl;
	getchar();
	return 0;
}
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
//#include <limits>
using namespace std;
constexpr int CLOCKSIZE = 16;
constexpr int SWITCH_SIZE = 10;
constexpr int INF = 987654321;

int C;
int click[SWITCH_SIZE] = { 0, };
vector<int> mySwitch;
vector<int> clock;
vector< vector<int> > clockList = {
	{0, 1, 2},
{3, 7, 9, 11},
{4, 10, 14, 15},
{0, 4, 5, 6, 7},
{6, 7, 8, 10, 12},
{0, 2, 14, 15},
{3, 14, 15},
{4, 5, 7, 14, 15},
{1, 2, 3, 4, 5},
{3, 4, 5, 9, 13}
};

static int cnt = 0;
//return : among remained mySwitch, return the minimum value making all clocks 12 o'clock.
void disp()
{
	printf("clock:");
	for (int i = 0; i < CLOCKSIZE; ++i)
		printf("%d, ", clock[i]);
	printf("\n");
	printf("switch:");
	for (int i = 0; i < SWITCH_SIZE; ++i)
		printf("%d, ", click[i]);
	printf("\n");
	//printf("\n");
}
int solve(vector<int> i_mySwitch, int count)
{
	//printf("%s:%d] start...i_mySwitch size=%d,count=%d\n", __FUNCTION__, __LINE__,i_mySwitch.size(), count);
	//base
	//if (i_mySwitch.size() == 3) //CLOCKSIZE)
	if (i_mySwitch.size() == 10)//SWITCH_SIZE)//CLOCKSIZE)
	{
		bool check = true;
		//for (int i = 0; i < 3; ++i)
		for (int i = 0; i < CLOCKSIZE; ++i)
		{
			if (clock[i] != 12)
			{
				check = false;
				break;
			}
		}
		//printf("solve ret = %d\n", check == true ? count : INF);
		//disp();
		//printf("%s= %d\n", check == true ? "OK" : "NG", check == true ? count : INF);
		return (check == true ? count : INF);
	}

	//push next switch
	if (i_mySwitch.size() == 0)
		i_mySwitch.push_back(0);
	else
	{
		i_mySwitch.push_back(i_mySwitch.back() + 1);
		//printf("push switch:%d\n", i_mySwitch.back() + 1);
	}

	//printf("switch.size() = %d\n", i_mySwitch.size());
	//each up to 4 times push
	int ret = INF;
	for (int i = 0; i < 4; i++)
	{
		int curSwtch = i_mySwitch.back();
		click[curSwtch] = i;
		//printf("switch=%d,i=%d\n", i_mySwitch.size(), i);
		//ex. 1 th switch push
		//~ rotate clock location
		//printf("map(%d):", clockList[curSwtch].size());
		for (int j = 0; j < clockList[curSwtch].size(); ++j)
		{
			int temp = clock[clockList[curSwtch][j]];
			//printf("%d, ", clockList[curSwtch][j]);
			temp += 3 * i;
			temp = temp % 12;
			if (temp == 0) temp = 12;
			clock[clockList[curSwtch][j]] = temp; //i=0,1,2,3 * 3hour
		}
		//printf("\n");
		//disp();
		// call self
		//printf("******* call self : switch.size=%d, %d times, count=%d\n", i_mySwitch.size(), i, count);
		cnt++;
		int cand = solve(i_mySwitch, count + i);
		ret = min(ret, cand);

		//disp();
		//~ recover as the origin
		//i_mySwitch.pop_back();
		for (int k = 0; k < clockList[curSwtch].size(); ++k)
		{
			int temp = clock[clockList[curSwtch][k]];
			temp += 12;
			temp -= 3 * i;
			temp = temp % 12;
			if (temp == 0) temp = 12;
			clock[clockList[curSwtch][k]] = temp; //i=0,1,2,3 * 3hour
		}
	}
	return ret;
}
int main()
{
	fstream myfile;
	myfile.open("clocksync.txt");
	if (myfile.is_open())
	{
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; ++i)
		{
			//~ init
			clock.clear();
			for (int j = 0; j < CLOCKSIZE; ++j)
			{
				int temp;
				myfile >> temp;
				clock.push_back(temp);
			}
			//~ body
			//printf("first mySwitch.size()=%d\n", mySwitch.size());
			//int ret = solve(mySwitch, 0);
			//mySwitch.push_back(0);
			//mySwitch.push_back(1);
			//mySwitch.push_back(2);
			//mySwitch.push_back(3);
			//mySwitch.push_back(4);
			//mySwitch.push_back(5);
			//mySwitch.push_back(6);
			int ret = solve(mySwitch, 0);
			cout << ret << endl;
			//cout << "cnt=" << cnt << endl;
		}
		myfile.close();
	}
	getchar();
	return 0;
}
#include <vector>
#include <stdio.h>

using namespace std;

int main()
{
	vector<vector<int>> v2(3, vector<int>(3, 2));

	printf("v2[0][1]=%d, v2[1][0]=%d\n", v2[0][1], v2[1][0]);
	v2[0][1] = 3;
	for(int i=0; i<2; i++)
	printf("v2[0][1]=%d, v2[1][0]=%d\n", v2[i][1], v2[1][0]);
	getchar();
	return 0;
}
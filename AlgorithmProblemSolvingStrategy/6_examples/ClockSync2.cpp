#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void disp(const char*);
const int INF = 9999, SWITCHES = 10, CLOCKS = 16;
//vector<int> clocks = { 12, 6, 6, 6, 6, 6, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12 };
vector<int> clocks = { 12, 9, 3, 12, 6, 6, 9, 3, 12, 9, 12, 9, 12, 12, 6, 6 };
const char linked[SWITCHES][CLOCKS + 1] = {
	"xxx.............",
	"...x...x.x.x....",
	"....x.....x...xx",
	"x...xxxx........",
	"......xxx.x.x...",
	"x.x...........xx",
	"...x..........xx",
	"....xx.x......xx",
	".xxxxx..........",
	"...xxx...x...x.."
};
bool areAligned(const vector<int>& clocks) {
	bool ret = true;
	for (int i = 0; i != clocks.size(); ++i)
	{
		if (clocks[i] != 12)
		{
			ret = false;
			break;
		}
	}
	return ret;
}
void push(vector<int>& clocks, int swtch)
{
	//printf("swtch= %d\n", swtch);
	for (int clock = 0; clock < CLOCKS; ++clock)
		if (linked[swtch][clock] == 'x') {
			//printf("x found!!!(swtch=%d,clock=%d) \n", swtch, clock);
			clocks[clock] += 3;
			//disp(__FUNCTION__);
			if (clocks[clock] == 15) clocks[clock] = 3;
		}
	//disp(__FUNCTION__);
}
int solve(vector<int>& clocks, int swtch) {
	if (swtch == SWITCHES) {
		//disp(__FUNCTION__);
		return areAligned(clocks) ? 0 : INF;
	}
	int ret = INF;
	for (int cnt = 0; cnt < 4; ++cnt)//4; ++cnt)
	{
		//printf("swtch=%d, cnt=%d\n", swtch, cnt);
		ret = min(ret, cnt + solve(clocks, swtch + 1));
		push(clocks, swtch);
		//disp(__FUNCTION__);
		//printf("areAligned=%s\n", areAligned(clocks) ? "OK": "NG");
	}
	return ret;
}
void disp(const char * s)
{
	printf("%s] clocks:", s);
	for (int i = 0; i < clocks.size(); ++i)
	{
		printf("%d, ", clocks[i]);
	}
	printf("\n");
}
int main()
{
	int ret = solve(clocks, 0);
	cout << ret << endl;
	getchar();
	return 0;
}
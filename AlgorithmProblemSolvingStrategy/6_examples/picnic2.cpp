#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;

int numOfStudent;
int numOfPair;
bool areFriends[10][10] = {0, };
bool taken[10] = { 0, };
vector<pair<int, int>> record;

int countPairings(bool taken[])
{
	int ret = 0;
	int firstStudent = 0;
	int i = 0;
	int secondStudent = 0;
	for (i = 0; i < numOfStudent; i++)
	{
		if (taken[i] == false)
		{
			firstStudent = i;
			break;
		}
	}
	if (i == numOfStudent) // not found
	{
		printf("pairing work is done\n");
		for (int i = 0; i < record.size(); i++)
		{
			printf("(%d, %d), ", record[i].first, record[i].second);
		}
		printf("\n");
		return 1;
	}
	for (secondStudent = firstStudent + 1; secondStudent < numOfStudent; secondStudent++)
	{
		if (taken[secondStudent] == false && areFriends[firstStudent][secondStudent] == 1)
		{
			taken[firstStudent] = true;
			taken[secondStudent] = true;
			record.push_back(pair<int,int>(firstStudent, secondStudent));
			ret += countPairings(taken);
			taken[firstStudent] = false;
			taken[secondStudent] = false;
			record.pop_back();
		}
	}
	return ret;
}

int main()
{
	ifstream myfile;
	myfile.open("picnic.txt");
	int c;
	myfile >> c;
	//c = 1;
	for (int i = 0; i < c; i++)
	{
		myfile >> numOfStudent;
		myfile >> numOfPair;
		//~ initialization
		memset(taken, 0, sizeof(taken));
		record.clear();
		for (int i = 0; i < numOfPair; i++)
		{
			int a, b, temp;
			myfile >> a;
			myfile >> b;
			//~ make map
			areFriends[a][b] = 1;
			areFriends[b][a] = 1;
		}
		//~ body
		int ret = countPairings(taken);
		cout << ret << endl;
	}
	getchar();
	return 0;
}
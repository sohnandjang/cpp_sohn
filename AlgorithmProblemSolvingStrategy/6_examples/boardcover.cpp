#include <iostream>
#include <fstream>
#include <cstring>
//#include <sys/time.h>
#include <time.h>
using namespace std;
//struct timeval  tv1, tv2;
/* Program code to execute here */

enum {
	WHITE = 0,
	BLACK = 1,
};
int map[20][20] = { 0, };
int givenMap[20][20] = { 0, };
int whiteNum = 0;
int row;
int col;
int blockType[4][3][2] = { //from up,
	{ {0, 0}, {1, -1}, {1, 0} }, //_l
	{ {0, 0}, {1, 0}, {1, 1} }, // I_
	{ {0, 0}, {0, 1}, {1, 1} }, //-|
	{ {0, 0}, {1, 0}, {0, 1} }, // |-
};
int isAllBlack(void)
{
	int ret = 1;
	//////printf("row, col = %d, %d\n", row, col);

	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (map[i][j] == WHITE)
			{
				////printf("%s:%d] map(%d,%d)== WHITE\n", __FUNCTION__, __LINE__, i, j);
				ret = 0;
				break;
			}
		}
	}
	//////printf("%s:%d] ret = %d\n", __FUNCTION__, __LINE__, ret);
	return ret;
}
void disp(void)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			//printf("%c", map[i][j] == BLACK ? '#' : '.');

		}
		//printf("\n");
	}
	//printf("\n");
}
int boardcover(int map[20][20])
{
	//printf("%s:%d] start ...\n", __FUNCTION__, __LINE__);
	disp();
	int ret = 0;
	if (whiteNum % 3 != 0)
	{
		//printf("whiteNum is not multiply by 3 = %d\n", whiteNum);
		return 0;
	}
	//////printf("isAllBlack() = %d\n", isAllBlack());
	if (isAllBlack())
	{
		//printf("boardcover complete\n");
		return 1;
	}
	//////printf("here\n");
	int whiteRow = 0;
	int whiteCol = 0;
	for (int i= 0; i< row; i++)
	{
		int j = 0;
		for (j= 0; j< col; j++)
		{
			//////printf("whiteRow,Col=%d,%d (%s)\n", i, j, map[i][j]==WHITE?"white":"black");
			if (map[i][j] == BLACK)
				continue;
			if (map[i][j] == WHITE)
			{
				whiteRow = i;
				whiteCol = j;
				break;
			}
		}
		if (j != col)
			break;
	}
	//printf("first whiteRow,Col=%d,%d\n", whiteRow, whiteCol);
	//////printf("here\n");
	for (int i = 0; i < sizeof(blockType) / sizeof(blockType[0]); i++)
	{
		//~ choose 1, 2, 3, 4 block type
		int next_row;
		int next_col;
		int j = 0;
		for (j = 0; j < 3; j++)
		{
			next_row = whiteRow+blockType[i][j][0];
			next_col = whiteCol+blockType[i][j][1];
			if (map[next_row][next_col] == BLACK)
			{
				////printf("NG: Black exists(%d,%d)\n", next_row, next_col);
				break;
			}
			if (next_row < 0 || next_row >= row)
			{
				////printf("NG: over range (next_row=%d)\n", next_row);
				break;
			}
			if (next_col < 0 || next_col >= col)
			{
				////printf("NG: over range (next_col=%d)\n", next_col);
				break;
			}
		}
		if (j != 3) //~ find Black or range over
		{
			////printf("NG: Black exists or range over\n");
			continue;
		}
		//~ not find Black
		//~ update map
		for (int k = 0; k < 3; k++)
		{
			next_row = whiteRow+blockType[i][k][0];
			next_col = whiteCol+blockType[i][k][1];
			map[next_row][next_col] = BLACK;
			//printf("(%d, %d) update to Black idx(%d,%d)\n", next_row, next_col, i, k);
		}
		////printf("here\n");
		////printf("update(%d,%d), (%d,%d), (%d, %d)\n", whiteRow + blockType[i][0][0], whiteCol + blockType[i][0][1],
		//	whiteRow + blockType[i][1][0], whiteCol + blockType[i][1][1],
		//	whiteRow + blockType[i][2][0], whiteCol + blockType[i][2][1]);
		ret+=boardcover(map);
		//~ recover updated map
		for (int k = 0; k < 3; k++)
		{
			next_row = whiteRow+blockType[i][k][0];
			next_col = whiteCol+blockType[i][k][1];
			map[next_row][next_col] = WHITE;
		}
	}
	return ret;
}
int main()
{
	ifstream myFile;
	myFile.open("boardcover.txt");
	if (myFile.is_open())
	{
		int c;
		myFile >> c;
		//////printf("c=%d\n", c);
		//c = 1;
		for (int k = 0; k < c; k++)
		{
			//~ init
			myFile >> row;
			myFile >> col;
			whiteNum = 0;
			//printf("row=%d,col=%d\n", row, col);
			for (int i = 0; i < row; i++)
			{
				for (int j = 0; j < col; j++)
				{
					char ch;
					myFile >> ch;
					if (ch == '#')
						givenMap[i][j] = BLACK;
					else if (ch == '.')
					{
						givenMap[i][j] = WHITE;
						whiteNum++;
					}
					else
					{
						//printf("wrong input\n");
					}
				}
			}
			//////printf("whiteNum = %d\n", whiteNum);
			//~ solve the problem
			int ret = 0;
			memset(map, 0, sizeof(map));
			memcpy(map, givenMap, sizeof(map));
			//disp();
			//gettimeofday(&tv1, NULL);
			ret = boardcover(map);
			//gettimeofday(&tv2, NULL);
			//printf("Time taken in execution = %f seconds\n",
			//	(double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
			//	(double)(tv2.tv_sec - tv1.tv_sec));
			printf("%d\n", ret);
		}

		myFile.close();
	}
	getchar();
	return 0;
}
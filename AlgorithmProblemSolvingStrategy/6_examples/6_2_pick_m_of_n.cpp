#include <iostream>
#include <vector>
using namespace std;

constexpr int n = 5;// 7;// 4;// 7;
constexpr int ball[10] = { 0, 1, 2, 3, 4, 5, 6 };
int map[10];
constexpr int m = 4;
vector<int> picked;

void printPick(void)
{
	cout << "(";
	for (int i = 0; i < picked.size(); i++)
	{
		cout << picked[i] << ",  ";
		//printf("picked[%d] = %d, ", i, picked[i]);
	}
	cout << ")" << endl;
}
// n : whole number
// picked : member number collected by now
// toPick : member number to collect
void pick(int n, vector<int>& picked, int toPick) {
	if (toPick == 0) 
		printPick();
	for (int i = 0; i < n; i++)
	{
		if (map[i] == 1) // block duplication
			continue;
		int select = ball[i];
		if (picked.size() != 0)
		{
			if (select < picked.back()) //consider duplication
				continue;
			//printf("top = %d\n", picked.back());
		}
		picked.push_back(select);
		map[i] = 1;
		pick(n, picked, toPick - 1);
		picked.pop_back();
		map[i] = 0;
	}
	return;
}
void pick2(int n, vector<int>& picked, int toPick)
{
	//printf("call pick2(%d,%d)\n", n, toPick);
	if (toPick == 0) { printPick(); return; }

	int smallest = picked.empty() ? ball[0] : picked.back() + 1;

	for (int next = smallest; next < n; ++next)
	{
		picked.push_back(next);
		pick2(n, picked, toPick - 1);
		picked.pop_back();
	}
}

int main()
{
	//can you consider to order of balls ? 
	memset(map, 0, sizeof(map));
	picked.clear();
	//pick(n, picked, m);
	pick2(n, picked, m);
	getchar();
	return 0;
}
#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;

vector<pair<int, int>> givenFriendList;
vector<pair<int, int>> myFriendList;
vector<int> students;
int numOfStudent;
int numOfPair;

void init(void)
{
	givenFriendList.clear();
	students.clear();
	myFriendList.clear();
}

int compare(pair<int, int> i_pair)
{
	vector<pair<int, int>>::iterator it = givenFriendList.begin();
	for(it; it<givenFriendList.end(); it++)
	{
		if (i_pair.first == it->first && i_pair.second == it->second)
			return 1;
		if (i_pair.second == it->first && i_pair.first == it->second)
			return 1;
	}
	return 0;
}
void printStudents(void)
{
	printf("student:");
	for(int i =0; i!=students.size(); i++)
	{
		printf("%d, ", students[i]);
	}
	printf("\n");
}
void printMyFriendList(void)
{
	printf("myFriendList:");
	for(int i =0; i!=myFriendList.size(); i++)
	{
		printf("(%d, %d), ", myFriendList[i].first, myFriendList[i].second);
	}
	printf("\n");
}
// return : number of case 
int makeMate(vector<int>& i_students)
{
	printStudents();
	int ret = 0;
	if (i_students.size() == 2)
	{
		//ret = compare(pair<int,int>(i_students[0], i_students[1]));
		printf("size=2, ret = %d\n", 1);// ret);
		return 1;// ret;
	}

	//for (int i = 0; i < i_students.size()-1; i++)
	int i  = 0;
	{
		for (int j = i + 1; j < i_students.size(); j++)
		{
			// pick 2 (i, j)
			int a = i_students[i];
			int b = i_students[j];
			printf("previous :");
			printStudents();
			printf("pick=a,b=%d,%d (i,j=%d,%d)\n", a, b, i, j);

			// compare with myFriendList
			/*vector<pair<int,int>>::iterator myPos = find(myFriendList.begin(), myFriendList.end(), pair<int,int>(a,b));
			if (myPos != myFriendList.end()) //~no exist
			{
				printf("myFriendList found : skip\n");
				continue;
			}
			myFriendList.push_back(pair<int, int>(a, b));*/

			// compare with givenFriendList
			vector<pair<int,int>>::iterator givenPos = find(givenFriendList.begin(), givenFriendList.end(), pair<int,int>(a,b));
			if (givenPos == givenFriendList.end()) //~no exist
			{
				printf("givenFriendList not found : skip\n");
				continue;
			}


			vector<int>::iterator pos = find(i_students.begin(), i_students.end(), a);
			i_students.erase(pos);
			pos = find(i_students.begin(), i_students.end(), b);
			i_students.erase(pos);
			ret += makeMate(i_students);
			//i_students.push_back(a); // change to insert sorted?
			//std::vector<int>::iterator it = std::upper_bound(i_students.begin(), i_students.end(), a);
			students.insert(std::upper_bound(i_students.begin(), i_students.end(), a), a);
			students.insert(std::upper_bound(i_students.begin(), i_students.end(), b), b);
			//it = std::upper_bound(i_students.begin(), i_students.end(), b);
			//i_students.push_back(b);
		}
	}
	//printMyFriendList();
	printf("ret = %d\n", ret);
	return ret;
}

int main()
{
	ifstream myfile;
	myfile.open("picnic.txt");
	int c;
	myfile >> c;
	c = 1;
	for (int i = 0; i < c; i++)
	{
		init();
		myfile >> numOfStudent;
		myfile >> numOfPair;
		//~ initialization
		for (int i = 0; i < numOfPair; i++)
		{
			int a, b, temp;
			myfile >> a;
			myfile >> b;
			if (a > b) { temp = a; a = b; b = temp; } //~ sort
			givenFriendList.push_back(pair<int,int>(a, b));
		}
		for (int i = 0; i < numOfStudent; i++)
		{
			students.push_back(i);
		}
		//~ body
		int ret = makeMate(students);
		cout << ret << endl;
	}
	getchar();
	return 0;
}
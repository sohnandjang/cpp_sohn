#include <iostream>
using namespace std;

int recursiveSum(int n)
{
	int ret = 0;
	if (n == 0)
		return 0;
	ret = n + recursiveSum(n - 1);
	return ret;
}

int main()
{
	cout << "sum(1+..+10) = " << recursiveSum(10) << endl;
	getchar();
	return 0;
}
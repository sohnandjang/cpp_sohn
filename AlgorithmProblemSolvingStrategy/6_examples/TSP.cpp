#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>
using namespace std;
#define MAX 9
int n; // number of city
enum {
	NO_VISITED = 0,
	VISITED = 1,
};
vector<int> path;
vector<bool> visited(MAX);
double currentLength;
double dist[MAX][MAX] = {
	{0.0, 1.0, 2.0, 3.0, 1.0, 2.0, 1.0, 2.0, 3.0}, //1
	{1.0, 0.0, 1.0, 2.0, 1.0, 1.0, 2.0, 2.0, 2.0}, //2
	{2.0, 1.0, 0.0, 1.0, 2.0, 1.0, 3.0, 2.0, 2.0}, //3
	{3.0, 2.0, 1.0, 0.0, 2.0, 1.0, 3.0, 2.0, 2.0}, //4
	{1.0, 1.0, 2.0, 3.0, 0.0, 1.0, 1.0, 2.0, 2.0}, //5
	{2.0, 1.0, 2.0, 1.0, 1.0, 0.0, 2.0, 1.0, 1.0}, //6
	{1.0, 2.0, 3.0, 3.0, 1.0, 2.0, 0.0, 1.0, 2.0}, //7
	{2.0, 3.0, 2.0, 2.0, 2.0, 1.0, 1.0, 0.0, 1.0}, //8
	{3.0, 3.0, 2.0, 1.0, 2.0, 1.0, 2.0, 1.0, 0.0}, //9
}; // 

// path : route made until now
// visited
// currentLength
void disp(void)
{
	printf("path:");
	for (unsigned int i = 0; i != path.size(); ++i)
	{
		printf("%d->, ", path[i]);
	}
	printf("\n");
	printf("curLen = %f\n", currentLength);
}
double shortestPath(vector<int>&path, vector<bool>& visited, double currentLength)
{
	double ret = std::numeric_limits<double>::infinity(); 
	//base
	if (path.size() == n)
	{
		printf("len = %f\n", currentLength + dist[0][path.back()]);
		return currentLength + dist[0][path.back()];
	}
	for (int next = 0; next < n; next++)
	{
		//if visited , continue;
		if (visited[next] == VISITED)
			continue;

		//~ not visited
		//~push
		disp();
		int here= path.back();
		path.push_back(next);
		visited[next] = VISITED;
		double nextLength = currentLength + dist[here][next];
		printf("nextLen  = %f (here=%d, next=%d, dist[here][next]=%f)\n", nextLength,here,next,dist[here][next]);
		//~self call
		double cand = shortestPath(path, visited, nextLength);
		ret = min( ret, cand);
		//~pop
		path.pop_back();
		visited[next] = NO_VISITED;

	}
	return ret;
}


int main()
{
	n = 9;
	//start with 0
	path.push_back(0);
	//visited.push_back(0);
	visited[0] = VISITED;
	double ret = shortestPath(path, visited, currentLength);
	cout << ret << endl;
	getchar();
	return 0;
}
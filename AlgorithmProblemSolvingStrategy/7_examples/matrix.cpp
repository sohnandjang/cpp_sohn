#include <iostream>
using namespace std;

class SquareMatrix;

SquareMatrix identity(int n); //nxn identity matrix

SquareMatrix pow(const SquareMatrix& A, int m) {
	if (m == 0) return identity(A.size());
	if (m % 2 > 0) return pow(A, m - 1) *A;
	SquareMatrix half = pow(A, m / 2);
	return half * half;
}
int fastSum(int n)
{
	if (n == 1) return 1;
	if (n % 2 == 1) return fastSum(n - 1) + n;
	return 2 * fastSum(n / 2) + (n / 2)*(n / 2);
}
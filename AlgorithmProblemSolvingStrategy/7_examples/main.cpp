#include <iostream>
#include <vector>
using namespace std;


// multiply large two number O(n2)
void normalize(vector<int>& num)
{
	num.push_back(0);
	// do num position increase 
	for (unsigned int i = 0; i < num.size(); ++i)
	{
		if (num[i] < 0)
		{//for karazuba (subFrom)
			int borrow = (abs(num[i]) + 9) / 10;
			num[i + 1] -= borrow;
			num[i] += borrow * 10;
		}
		else {
			num[i + 1] += num[i] / 10;
			num[i] %= 10;
		}
	}
	while (num.size() > 1 && num.back() == 0) num.pop_back();
}

//ex: multiply({3, 2, 1{, {6,5,4}) = 123 * 456 = 56088 = {8,8,0,6,5}
vector<int> multiply(const vector<int>& a, const vector<int> &b)
{
	vector<int> c(a.size() + b.size() + 1, 0);
	for (unsigned int i = 0; i < a.size(); ++i)
	{
		for (unsigned int j = 0; j < b.size(); ++j)
		{
			c[i + j] += a[i] * b[j];
		}
	}
	normalize(c);
	vector<int>::iterator it;
	cout << "result = ";
	for (it = c.begin(); it != c.end(); it++)
	{
		cout << *it << ' ';
	}
	cout << endl;
	return c;
}

int main()
{
//ex: multiply({3, 2, 1{, {6,5,4}) = 123 * 456 = 56088 = {8,8,0,6,5}
	const vector<int> a{ 3,2,1 };
	const vector<int> b{ 6,5,4 };
	vector<int> c(30);
	c = multiply(a, b);


	getchar();
	return 0;
}
#include <iostream>
#include <vector>
#include <stack>
using namespace std;

vector<vector<int>> adj = {
    {1, 4},
    {0, 2}, // diff {2}
    {3},
    {1},
    {5},
    {4, 3},
    /*{1, 4},
    {0, 2},
    {3},
    {1},
    {5},
    {4, 3},*/
};
vector<int> sccId;
vector<int> discovered;
stack<int> st;
int sccCounter, vertexCounter;

int scc(int here){
    cout << "========================================" << endl;
    cout << "*** scc start: here = " << here << endl;
    int ret = discovered[here] = vertexCounter++;
    st.push(here); // here's children will be stacked on after here
    //cout << "adj[here].size = " << adj[here].size() << endl;
    for(int i=0; i<adj[here].size(); ++i){
        int there = adj[here][i];
        //cout << "discoverd[there] = " << discovered[there] << endl;
        if(discovered[there] == -1) {// not discovered
            //cout << "scc[there] : there =  " << there << endl;
            ret = min(ret, scc(there));
            cout << "ret(scc) =  " << ret << " here: "<< here << " there: " << there << endl;
        }
        else if(sccId[there] == -1) {// if it is not in other scc
            ret = min(ret, discovered[there]);
            cout << "ret(sccId) =  " << ret << " here: "<< here << " there: " << there << endl;
        }
        // if already discovered and is in other scc,
    }

    if(ret == discovered[here]){ //here is the minimum value among children
        while(true){
            int t = st.top(); // here
            st.pop();
            sccId[t] = sccCounter;
            if(t == here) break;
        } // here's children is combined with one component of here
        cout << "pop sccCounter=  " << sccCounter<< endl;
        ++sccCounter;
    }
    cout << "### return: " << ret << " here: " << here << endl;
    return ret;
}

vector<int> tarjanSCC(){
    sccId = discovered = vector<int>(adj.size(), -1); // initialization
    sccCounter = vertexCounter = 0; // init
    for(int i=0; i<adj.size(); i++) if(discovered[i] == -1) scc(i);
    return sccId;
}

void disp()
{
    for(int i=0; i<sccId.size(); ++i)
    {
        cout << "sccId["<<i<<"]= "<< sccId[i] << endl;
    }
}
int main()
{
    tarjanSCC();
    disp();
    return 0;
}

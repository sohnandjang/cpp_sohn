#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <string.h>

 
std::vector<std::vector<int>> adj;
std::vector<std::string> words;
std::vector<std::string> graph[26][26];
std::vector<int> inbound;
std::vector<int> outbound;
void makeGraph(std::vector<std::string>& words)
{
	for (unsigned int i = 0; i < words.size(); i++)
	{
		std::string word = words[i];
		int a = word[0] - 'a';
		int b = word[word.size() - 1] - 'a';
		adj[a][b]++;
		outbound[a]++;
		inbound[b]++;
		graph[a][b].push_back(word);
	}
}
void caseSolve(std::fstream& fs)
{
	//~ init
	adj = std::vector<std::vector<int>>(26,  std::vector<int>(26, 0));
	for (int i = 0; i < 26; i++)
		for (int j = 0; j < 26; j++)
			graph[i][j].clear();
	inbound.clear();
	outbound.clear();
	//~ makeGraph
	int n = 0;
	fs >> n;
	for (int i = 0; i < n; i++)
	{
		std::string temp;
		fs >> temp;
		words.push_back(temp);
	}
	makeGraph(words);
}

int main()
{
	std::fstream myfile;
	myfile.open("wordConnection.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
			caseSolve(myfile);
		myfile.close();
	}
	getchar();
	return 0;
}

#include <iostream>
#include <vector>
using namespace std;
#define MAX_V  6
int V;
vector<int> adj[MAX_V];
vector<bool> visited;
const int UNWATCHED = 0;
const int WATCHED = 1;
const int INSTALLED = 2;

int installed;

int dfs(int here){
    visited[here] = true;
    int children[3] = {0, 0, 0};
    for(size_t i = 0; i< adj[here].size(); ++i){
        int there = adj[here][i];
        if(!visited[there])
            ++children[dfs(there)];
    }

    if(children[UNWATCHED]){
        ++installed;
        printf("installed here = %d\n", here);
        return INSTALLED;
    }

    if(children[INSTALLED])
        return WATCHED;
    return UNWATCHED;
}

int installcamera(){
    installed = 0;
    visited = vector<bool>(V, false);
    for(int u= 0; u<V; ++u)
        if(!visited[u] && dfs(u) == UNWATCHED)
            ++installed;
      //dfs(4);
      //dfs(0);
    return installed;
}

// 4 - 0 - 1 - 2 - 5
// 1 - 3
int main()
{
    V = 6;
    //adj[0].push_back(1);
    //adj[0].push_back(4);
    adj[4].push_back(1);
    adj[4].push_back(0);
    //adj[1].push_back(0);
    adj[1].push_back(4);
    adj[1].push_back(2);
    adj[1].push_back(3);
    adj[2].push_back(1);
    adj[2].push_back(5);
    adj[3].push_back(1);
    //adj[4].push_back(0);
    adj[0].push_back(4);
    adj[5].push_back(2);
    printf("camera number = %d\n", installcamera() );
    return 0;
}

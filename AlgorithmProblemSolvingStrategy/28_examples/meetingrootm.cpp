#include <vector>
#include <utility>
using namespace std;

// graph adjcent list
vector<vector<int>> adj;

bool disjoint(const pair<int,int>& a, const pair<int, int>& b)
{
    return a.second <= b.first || b.second <= a.first;
}

// meetings : meeting time lists
// i-th team : meetings[2*i], meetings[2*i+1]
void makeGraph(const vector<pair<int,int>>& meetings)
{
    int vars = meetings.size();
    adj.clear(); adj.resize(vars * 2);
    for(int i=0; i<vars; i+=2)
    {
        int j = i+1;
        adj[i*2+1].push_back(j*2);
        adj[j*2+1].push_back(i*2);
        // i=0, j=1
        // adj[1] = 2 (!A0 -> A1)
        // adj[3] = 0 (!A1 -> A0)
        // i=2, j=3
        // adj[5] = 6 (!A2 -> A3)
        // adj[7] = 4 (!A3 -> A2)
        // i=4, j=5
        // adj[9] = 10(!A4 -> A5)
        // adj[11] = 8(!A5 -> A4)
    }
    for(int i=0; i<vars; ++i) // i=0, skip  :  i=1, j=0   :   i=2, j=0,1 : ... : i=5, j=0,1,2,3,4  why?
        for(int j=0; j<i; ++j)
        {
            if(!disjoint(meetings[i], meetings[j])){ //if overlaps, ~i or ~j.
                adj[i*2].push_back(j*2+1);
                adj[j*2].push_back(i*2+1);
            }
        }
}

int main()
{
    vector<pair<int,int>> meetings;
    meetings.push_back( pair<int,int>(2, 5) );
    meetings.push_back( pair<int,int>(6, 9) );
    meetings.push_back( pair<int,int>(1, 3) );
    meetings.push_back( pair<int,int>(8, 10) );
    meetings.push_back( pair<int,int>(4, 7) );
    meetings.push_back( pair<int,int>(11, 12) );
    makeGraph( meetings );
}

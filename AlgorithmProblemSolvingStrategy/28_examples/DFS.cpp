#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<vector<int>> adj;
vector<int> discovered;
vector<bool> isCutVertex;
int counter = 0;

int findCutVertex(int here, bool isRoot)
{
	discovered[here] = counter++;
	int ret = discovered[here];

	int children = 0;
	for (int i = 0; i < adj[here].size(); ++i)
	{
		int there = adj[here][i];
		if (discovered[there] == -1)
		{
			++children;
			int subtree = findCutVertex(there, false);
			if (!isRoot && subtree >= discovered[here])
				isCutVertex[here] = true;
			ret = min(ret, subtree);
		}
		else
			ret = min(ret, discovered[there]);
	}
	if (isRoot) isCutVertex[here] = (children >= 2);
	return ret;
}

int main()
{
	//vector<int> v(5);
	//for (int i = 0; i < 8; i++)
	//	adj[i] = v;
	adj.resize(8, vector<int>(0));
	discovered.resize(8);
	isCutVertex.resize(8);
	adj[0].push_back(1);
	adj[1].push_back(0);
	adj[1].push_back(2);
	adj[2].push_back(1);
	adj[1].push_back(3);
	adj[3].push_back(1);
	adj[3].push_back(4);
	adj[4].push_back(3);
	adj[3].push_back(5);
	adj[5].push_back(3);
	adj[2].push_back(3);
	adj[3].push_back(2);
	adj[5].push_back(2);
	adj[2].push_back(5);
	adj[6].push_back(5);
	adj[5].push_back(6);
	adj[5].push_back(7);
	adj[7].push_back(5);
	int ret = findCutVertex(1, true);
	cout << ret << endl;
}
#if 0
vector<vector<int>> adj;
vector<int> discovered, finished;

int counter;


void dfs2(int here)
{
	discovered[here] = counter++;
	for (int i = 0; i < adj[here].size(); ++i)
	{
		int there = adj[here][i];
		cout << "(" << here << "," << there << ") is a ";
		if (discovered[there] == -1) {
			cout << "tree edge" << endl;
			dfs2(there);
		}
		else if (discovered[here] < discovered[there])
			cout << "forward edge" << endl;
		else if (finished[there] == 0)
			cout << "back edge" << endl;
		else
			cout << "cross edge" << endl;
	}
	finished[here] = 1;
}
#endif
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <string.h>

/*
typedef std::pair<int, int> myPair;
constexpr int MAXSIZE = 30;
int visited[MAXSIZE] = { 0, };
int graph[MAXSIZE][MAXSIZE] = { 0, };
std::vector<int> phase;
void dispInput(std::vector<std::string>& i_input)
{
	for (size_t i = 0; i < i_input.size(); i++)
	{
		std::cout << i_input[i] << " ,";
	}
	std::cout << std::endl;
}

void DFS(int v)
{
	visited[v] = 1;
	for (int i=0; i<MAXSIZE; i++)
	{
		if (graph[v][i] == 1)
		{
			if (visited[i] == 0)
			{
				DFS(i);
				phase.push_back(i);
				//printf("phase: push=%d\n", i);
			}
		}
	}
}

void phaseSort(void)
{

	for (size_t i = 0; i <  MAXSIZE; i++)
		for (size_t j = 0; j < MAXSIZE; j++)
		{
			if (graph[i][j] == 1)
			{
				if (visited[i] == 0)
				{
					DFS(i);
					phase.push_back(i);
					//printf("phase: push=%d\n", i);
				}
			}
		}
		
	//std::sort(phase.begin(), phase.end(), std::greater<int>());
	std::reverse(phase.begin(), phase.end());
	//std::cout << "phase size= " << phase.size() << std::endl;
	for (size_t i = 0; i < phase.size(); i++)
	{
		//std::cout << phase[i] << " -> ";
	}
	//std::cout << std::endl;

	//~ valid check
	for (size_t i = 0; i < MAXSIZE; i++)
	{
		for (size_t j = 0; j < MAXSIZE; j++)
		{
			if (graph[i][j] == 1)
			{
				int prePhaseOrder = -1;
				int nextPhaseOrder = -1;
				for (size_t k = 0; k < phase.size(); k++)
				{
					if (i == phase[k])
						prePhaseOrder = k;
					if (j == phase[k])
						nextPhaseOrder = k;
					if (prePhaseOrder != -1 && nextPhaseOrder != -1)
						break;
				}
				if (prePhaseOrder > nextPhaseOrder)
				{
					std::cout << "INVALID HYPOTHESIS" << std::endl;
					return;
				}
			}
		}
	}
	//~ result
	int result[26] = { 0, };
	for (size_t i = 0; i < phase.size(); i++)
	{
		printf("%c", 'a' + phase[i]);
		result[phase[i]] = 1;
	}
	for (int i = 0; i < 26; i++)
	{
		if (result[i] == 1) continue;
		printf("%c", 'a' + i);
	}
	std::cout << std::endl;
}
 
void caseSolve(std::fstream& fs)
{
	//~ init
	memset(visited, 0, sizeof(visited));
	memset(graph, 0, sizeof(graph));
	phase.clear();
	int n;
	fs >> n;
	std::vector<std::string> input;
	std::string temp;
	for (int i = 0; i < n; i++)
	{
		fs >> temp;
		input.push_back(temp);
	}
	//std::vector<myPair> graph;
	for (int i = 0; i < n-1; i++)
	{
		unsigned int minlen = input[i].length();
		minlen = std::min(minlen, input[i + 1].length());
		std::string pre = input[i];
		std::string next = input[i+1];
		for (size_t j = 0; j < minlen; j++)
		{
			if (pre[j] != next[j])
			{
				int preOrder = pre[j] - 'a';
				int nextOrder = next[j] - 'a';
				//graph.push_back(myPair(preOrder, nextOrder));
				graph[preOrder][nextOrder] = 1;
				break; //stop comparing more
			}
			else
				continue;
		}
		
	}
	//dispInput(input);
	phaseSort();
}
*/

std::vector<std::string> word;
std::vector<std::vector<int>>  adj;
std::vector<int> result2;
std::vector<int> visited2;
void DFS2(int here);
void topologicalSort(void);
void caseSolve2(std::fstream& fs)
{
	//~ init
	word.clear();
	adj = std::vector<std::vector<int>>(26, std::vector<int>(26, 0));
	result2.clear();
	visited2 = std::vector<int>(26, 0);
	//~ input
	int n;
	fs >> n;
	for (int i = 0; i < n; i++)
	{
		std::string temp;
		fs >> temp;
		word.push_back(temp);
	}
	for (unsigned int i = 0; i < n-1; i++)
	{
		std::string word1 = word[i];
		std::string word2 = word[i+1];
		int len = std::min(word1.size(), word2.size());
		for (int j = 0; j < len; j++)
		{
			if (word1[j] != word2[j])
			{
				int a = word1[j] - 'a';
				int b = word2[j] - 'a';
				adj[a][b] = 1;
				break;
			}
			else
				continue;
		}
	}
	topologicalSort();
	//~ reverse
	std::reverse(result2.begin(), result2.end());
	//~ check wrong form
	for (int i = 0; i < 26; i++)
	{
		for (int j = 0; j < 26; j++)
		{
			if (adj[i][j] == 1)
			{
				int a = i;
				int b = j;
				int preOrder = 0;
				int postOrder = 0;
				for (int k = 0; k < result2.size(); k++)
				{
					if (result2[k] == a)
						preOrder = k;
					if (result2[k] == b)
						postOrder = k;
				}
				if (preOrder > postOrder)
				{
					std::cout << "Error" << std::endl;
					return;
				}
			}
		}
	}

	for (unsigned int i = 0; i < result2.size(); i++)
		printf("%c", result2[i] + 'a');
	std::cout << std::endl;
}
void topologicalSort(void)
{
	for (int i = 0; i < 26; i++)
	{
		for (int j = 0; j < 26; j++)
		{
			if (adj[i][j] == 0) continue;
			if (visited2[i] == 0)
				DFS2(i);
		}
	}
}
void DFS2(int here)
{
	visited2[here] = 1;
	for (int there = 0; there < 26; there++)
	{
		if (adj[here][there] == 1)
		{
			if (visited2[there] == 0)
			{
				DFS2(there);
			}
		}
	}
	result2.push_back(here); //~ topological sort
}

int main()
{
	std::fstream myfile;
	myfile.open("dictionary.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
			caseSolve2(myfile);
			//caseSolve(myfile);
		myfile.close();
	}
	getchar();
	return 0;
}
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;


// current vertex : here
// next vertex? here + edge
// white vertex : 0~n-1
// black vertex : n~2n-1
int append(int here, int edge, int mod){
    int there = here*10 + edge;
    if( there>= mod ) return mod + there % mod;
    return there % mod;
}

// only numbers in digits 
// C mod n == m --> find the smallest C
string gifts(string digits, int n, int m){
    // if edge's number is sorted, we can find the frontest path by dictionary order
    sort (digits.begin(), digits.end());
    // white vertex i = 0~n-1
    // black i = n~2n-1
    // parent[i] = vertex i's parent in BFS spanning tree
    // choice[i] = parent[i] -> edge's number connected with i
    vector<int> parent(2*n, -1), choice(2*n, -1);
    queue<int> q;

    // insert white 0 in queue
    parent[0] = 0;
    q.push(0);
    while(!q.empty()){
        int here = q.front();
        q.pop();
        for(int i=0; i<digits.size(); ++i){
            // digits[i]-'0'
            int there = append(here, digits[i] - '0', n);
            if(parent[there] == -1){
                parent[there] = here;
                choice[there] = digits[i] - '0';
                q.push(there);
            }
        }
    }

    if(parent[n+m] == -1) return "IMPOSSIBLE";

    string ret;
    int here = n+m;
    while(parent[here] != here){
        ret += char('0' + choice[here]);
        //printf("ret = %s\n", ret.c_str());
        here = parent[here];
    }
    reverse(ret.begin(), ret.end());
    return ret;
}

int main()
{
    string s = gifts("35", 9, 8);
    printf("s = %s\n", s.c_str());
    s = gifts("1", 7, 0);
    printf("s = %s\n", s.c_str());
    s = gifts("1", 10, 1);
    printf("s = %s\n", s.c_str());
    return 0;
}

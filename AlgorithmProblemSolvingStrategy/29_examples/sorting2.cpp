#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
using namespace std;


map<vector<int>, int> toSort;

void preCalc(int n)
{
    vector<int> perm(n);
    for(int i=0; i<n; ++i) perm[i] = i;
    queue<vector<int> > q;
    q.push(perm);
    toSort[perm] = 0;
    while(!q.empty()){
        vector<int> here = q.front();
        q.pop();
        int cost = toSort[here];
        for(int i=0; i<n; ++i){
            for(int j=i+2; j<= n; ++j){
                reverse(here.begin()+i, here.begin() +j );
                if(toSort.count(here) == 0){
                    toSort[here] = cost +1;
                    q.push(here);
                }
                reverse(here.begin()+i, here.begin() +j );
            }
        }
    }
    printf("preCalc\n");
    for(std::map<vector<int>, int>::iterator it=toSort.begin(); it!=toSort.end(); ++it)
    {
        std::string s;
        vector<int> t = it->first;
        for(int i=0; i<t.size(); ++i)
            s.append(to_string(t[i])+", ");
        printf("sohn>> {%s}, %d\n", s.c_str(), it->second);

    }
    printf("\n");
}

int solve(const vector<int>& perm){
    int n = perm.size();
    vector<int> fixed(n);
    for(int i=0; i<n; ++i){
        int smaller = 0;
        for(int j=0; j<n; ++j){
            if(perm[j] < perm[i]){
                printf("perm[%d]:%d < perm[%d]:%d -> ++samller \n", j, perm[j], i, perm[i]);
                ++smaller;
            }
            else{
                printf("skip! (perm[%d]:%d >= perm[%d]:%d) \n", j, perm[j], i, perm[i]);
            }
        }
        fixed[i] = smaller;
        printf("fixed[%d] = %d \n", i, smaller);
    }
    for(int i=0; i<n; ++i){
        printf("*** fixed[%d] = %d\n", i, fixed[i]);
    }
    return toSort[fixed];
}

int main()
{
    vector<int> perm = {30, 40, 10, 20};
    preCalc(4);
    int result = solve( perm );
    printf(" result = %d\n", result );

    return 0;
}

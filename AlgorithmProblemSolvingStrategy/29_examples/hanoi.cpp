#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <stdlib.h>
#include <string.h>
using namespace std;

const int MAX_DISCS = 12;
int get(int state, int index) {
    return (state >> (index*2)) & 3;
}

int set(int state, int index, int value){
    return (state & ~(3 << (index*2))) | (value <<(index*2));
}
int c[1<<(MAX_DISCS*2)];

int bfs(int discs, int begin, int end){
    if(begin == end) return 0;
    queue<int> q;
    memset(c, -1, sizeof(c));
    q.push(begin);
    c[begin] = 0;
    static int num=0;
    while(!q.empty()){
        int here = q.front();
        printf("%d >", here);
        q.pop();
        int top[4] = {-1, -1, -1, -1};
        for(int i=discs-1; i>=0; --i)
            top[get(here, i)] = i;
        for(int i=0; i<4; ++i)
            if(top[i] != -1 )
                for(int j=0; j<4; ++j){
                    num++;
                    if( i!=j && (top[j] == -1 || top[j] > top[i] )){
                        int there = set(here, top[i], j);
                        if(c[there] != -1) continue;
                        c[there] = c[here] + 1;
                        if(there == end) {
                            printf("num:%d\n", num);
                            return c[there];
                        }
                        q.push(there);
                    }
                }
    }
    return -1;
}

int main()
{
    int discs = 3; // 2 / 0/ 3, 1
    int begin = 0;
    begin = set( begin, 1, 0);
    begin = set( begin, 2, 2);
    begin = set( begin, 0, 2);
    int end;
    end = set( end, 2, 3);
    end = set( end, 1, 3);
    end = set( end, 0, 3);
    int num = bfs(discs, begin, end);
    // 34 >38 >46 >32 >33 >35 >36 >37 >39 >44 >45 >47 >16 >48 >41 >49 >43 >19 >40 >52 >5 >53 >7 >28 >13 >15 >31 >17 >18 >50 >51 >42 >57 >61 >num:324
    // begin :34 100010 > 10 00 10 
    // end :63 111111 >  11 11 11
    return 0;
}
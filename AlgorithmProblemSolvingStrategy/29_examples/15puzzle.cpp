#include <iostream>
#include <vector>
#include <queue>
#include <map>
using namespace std;


// express game board's status
class State{
public:
    int by, bx;
    uint64 tiles;
    int estimate;

    State(){}
    State(const int board[4][4]){
        tiles = 0;
        for(int y = 3; y>=0; --y)
            for(int x = 3; x >= 0; --x){
                tiles = (tiles * 16)+board[y][x];
                if(board[y][x] == 0){
                    by = y;
                    bx = x;
                }
            }
        estimate = calcEstimate();
    }

    State(int by, int bx, uint64 tiles, int estimate):
        by(by), bx(bx), tiles(tiles), estimate(estimate){
        }

    inline int manhattan(uint64 tiles, int y, int x ) const{
        int num = getArray(tiles, y * 4 +x);
        if(num == 0) return 0;
        int ord = (num+15) % 16;
        return abs(y - ord / 4)+abs( - ord%4);
    }

    State(const int board[4][4]){
        titles
    // return adjacent status's lists
    vector<State> getAdjacent() const;
    // comp operator to insert State in map
    bool operator < (const State& rhs) const;
    // operator to compare with finish status
    bool operator == (const State& rhs) const;
};

typedef map<State, int> StateMap;

int bfs(State start, State finish){
    if(start == finish) return 0;
    StateMap c;
    queue<State> q;
    q.push(start);
    c[start] = 0;
    while(!q.empty()){
        State here = q.front();
        q.pop();
        int cost = c[here];
        vector<State> adjacent = here.getAdjacent();
        for(int i=0; i<adjacent.size(); ++i){
            if(c.count(adjacent[i]) == 0){
                if(adjacent[i] == finish) return cost +1;
                c[adjacent[i]] = cost +1;
                q.push(adjacent[i]);
            }
        }
    }
    return -1;
}

int main()
{

    return 0;
}

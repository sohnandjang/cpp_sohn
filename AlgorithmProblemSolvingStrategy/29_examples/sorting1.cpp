#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
using namespace std;


int bfs(const vector<int>& perm)
{
    int n = perm.size();
    vector<int> sorted = perm;
    sort(sorted.begin(), sorted.end());

    // visited lists
    queue<vector<int>> q;
    // distance from start points to each points
    map<vector<int>, int> distance;

    // put start points into queue
    distance[perm] = 0;
    q.push(perm);
    while(!q.empty()){
        vector<int> here = q.front();
        q.pop();
        if (here == sorted) return distance[here];
        int cost = distance[here];

        for(int i=0; i<n; ++i)
            for(int j=i+2; j<=n; ++j){
                reverse(here.begin() + i, here.begin() + j);
                if(distance.count(here) == 0){
                    distance[here] = cost + 1;
                    q.push(here);
                }
                reverse(here.begin() + i, here.begin() + j );
            }
    }
    return -1;
}

int main()
{
    vector<int> perm = {3, 4, 1, 2};
    int result = bfs( perm );
    printf(" result = %d\n", result );

    return 0;
}

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>
using namespace std;

void disp(vector<int> in);
vector< vector<int>> adj = {
    {1, 3, 4, 7}, //0
    {0, 2, 3}, //1
    {1, 5, 6}, //2
    {0, 1, 6}, //3
    {0, 5}, //4
    {2, 4}, //5
    {2, 3, 8}, //6
    {0}, //7
    {6} //8
};

void disp_bool(vector<bool> in);
void print_queue(std::queue<int> q)
{
  while (!q.empty())
  {
    std::cout << q.front() << " ";
    q.pop();
  }
  std::cout << std::endl;
}

// start: start point
// return: visit order
vector<int> bfs(int start)
{
    vector<bool> discovered( adj.size(), false);
    queue<int> q;
    vector<int> ret;

    q.push( start );
    disp( ret);

    while( !q.empty())
    {
        int p = q.front();
        if(discovered[p] == true){ 
            q.pop(); // throw away
            continue;
        }

        ret.push_back( p); // ret update
        q.pop();
        discovered[ p ] = true; // pop -> discovered update

        for(int i=0; i< adj[p].size(); ++i){
            if( !discovered[ adj[p][i]] ){ //bug fix [p] ->[p][i]
                q.push( adj[p][i] );
            }
            else{
            }
        }

    }
    return ret;
    
}

void bfs2(int start, vector<int>& distance, vector<int>& parent)
{
    vector<bool> discovered( adj.size(), false);
    queue<int> q;
    vector<int> ret;

    q.push( start );
    distance[start] = 0;
    parent[start] = start;
    int d = 0;

    while( !q.empty())
    {
        int p = q.front();
        if(discovered[p] == true){ 
            q.pop(); // throw away
            continue;
        }

        ret.push_back( p); // ret update
        q.pop();
        discovered[ p ] = true; // pop -> discovered update
        //d++;

        for(int i=0; i< adj[p].size(); ++i){
            if( !discovered[ adj[p][i]] ){ //bug fix [p] ->[p][i]
                q.push( adj[p][i] );
                if( parent[adj[p][i]]== -1){ //**********************************!!!!!!!!!!!!!!!!!!!!
                    parent[adj[p][i]] = p;
                    distance[ adj[p][i] ] = distance[ p ] +1; // &&&&&&&&&&&&&&&&?????????????????
                }
                //printf("adj[%d][%d]=%d's parent = %d \n", p, i, adj[p][i], p);
                //distance[adj[p][i]] = d;
            }
            else{
            }
        }
    }
    printf("tree>>> \n");
    disp(ret);
}

//v로부터 시작점까지의 최단 경로를 계산한다.
vector<int> shortestPath(int v, const vector<int>& parent){
    vector<int> path (1, v);
    while(parent[v] != v){
        v = parent[v];
        path.push_back( v );
    }
    reverse(path.begin(), path.end());
    return path;
}

void disp_bool(vector<bool> in)
{
    printf("disp_bool] ");
    printf("size = %d\n", in.size());
    printf("disp_bool] ");
    for(int i=0; i<in.size(); ++i){
        printf("%d ", in[i]==true?1:0);
    }
    printf("\n");
}
void disp(vector<int> in)
{
    printf("disp]0 1 2 3 4 5 6 7 8 9");
    printf("size = %d\n", in.size());
    printf("disp]");
    for(int i=0; i<in.size(); ++i){
        printf("%d ", in[i]);
    }
    printf("\n");
}

int main()
{
    //vector<int> ret = bfs( 0 );
    vector<int> distance( 10, -1)  ;
    vector<int> parent(10, -1);
    bfs2(0, distance, parent);
    printf("distance >>>\n");
    disp( distance );
    printf("parent >>>\n");
    disp( parent );

    vector<int> path;
    path = shortestPath(8, parent);
    printf("path >>>\n");
    disp( path);

    return 0;
}


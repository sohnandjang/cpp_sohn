#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <queue>
using namespace std;

void disp(vector<int> in);
vector< vector<int>> adj = {
    {1, 3, 4, 7}, //0
    {0, 2, 3}, //1
    {1, 5, 6}, //2
    {0, 1, 6}, //3
    {0, 5}, //4
    {2, 4}, //5
    {2, 3, 8}, //6
    {0}, //7
    {6} //8
};

void disp_bool(vector<bool> in);
void print_queue(std::queue<int> q)
{
  while (!q.empty())
  {
    std::cout << q.front() << " ";
    q.pop();
  }
  std::cout << std::endl;
}

// start: start point
// return: visit order
vector<int> bfs(int start)
{
    vector<bool> discovered( adj.size(), false);
    printf(">>>discovered 1111111\n");
    disp_bool(discovered);
    queue<int> q;
    vector<int> ret;

    q.push( start );
    printf("start = %d, q.empty= %c\n", start, q.empty()?'O':'X');
    disp( ret);

    while( !q.empty())
    //for(int k=0; k<12; ++k)
    {
        int p = q.front();
        printf("$$$$$$$$$$$$$$$$ p = %d, adj size=%d\n", p, adj[p].size());
        if(discovered[p] == true){ //-----------------------------------------> when push_back, consider !!!!!!
            printf(">>> already!!!! continue %d\n", p);
            q.pop(); // throw away
            continue;
        }

        ret.push_back( p); // ret update
        printf("push_back=%d, ret.size() = %d\n", p, ret.size());
        printf(">>> ret \n");
        disp( ret);
        printf(">>> queue \n");
        print_queue( q);
        q.pop();
        //discovered[ start ] = true; // pop -> discovered update
        discovered[ p ] = true; // pop -> discovered update
        printf(">>>discovered\n");
        disp_bool(discovered);

        printf("####### p = %d, adj[%d].size = %d\n", p, p, adj[p].size());
        for(int i=0; i< adj[p].size(); ++i){
            printf("####### adj[%d][%d] = %d\n", p, i, adj[p][i]);
            printf(">>>discovered\n");
            disp_bool(discovered);
            if( !discovered[ adj[p][i]] ){ //bug fix [p] ->[p][i]
                q.push( adj[p][i] );
                printf("push_back=%d\n", adj[p][i]);
                //discovered[ p ] = true;
            }
            else{
                printf("***************** already discovered=%d\n", adj[p][i]);
            }
        }

    }
    return ret;
    
}

void disp_bool(vector<bool> in)
{
    printf("disp_bool] ");
    printf("size = %d\n", in.size());
    printf("disp_bool] ");
    for(int i=0; i<in.size(); ++i){
        printf("%d ", in[i]==true?1:0);
    }
    printf("\n");
}
void disp(vector<int> in)
{
    printf("disp] ");
    printf("size = %d\n", in.size());
    printf("disp] ");
    for(int i=0; i<in.size(); ++i){
        printf("%d ", in[i]);
    }
    printf("\n");
}

int main()
{
    vector<int> ret = bfs( 0 );
    disp( ret );

    return 0;
}


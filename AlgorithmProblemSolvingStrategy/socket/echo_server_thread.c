#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define MAXLINE 1024
#define PORTNUM 3600
pthread_t thread_id;
pthread_t thread_id_server;
int server_sockfd, client_sockfd;

void * thread_func(void *data)
{
    int sockfd = *((int *)data);
    int readn;
    socklen_t addrlen;
    char buf[MAXLINE];
    struct sockaddr_in client_addr;
    addrlen = sizeof(client_addr);
    getpeername(sockfd, (struct sockaddr *)&client_addr, &addrlen);
    printf("sockfd connected: %d\n", sockfd);
    while((readn = read(sockfd, buf, MAXLINE)) > 0 )
    {
        printf("Read Data %s(%d) :%s",
                inet_ntoa(client_addr.sin_addr),
                ntohs(client_addr.sin_port),
                buf);
        write(sockfd, buf, strlen(buf));
        memset(buf, 0x00, MAXLINE);
    }
    printf("close sockfd: %d\n", sockfd);
    close(sockfd);
    printf("worker thread end\n");
    return 0;
}

void clean_up()
{
    printf("Thread cancel Clean_up function\n");
    close(server_sockfd);
    printf("close sockfd:%d\n", server_sockfd);
}

void * thread_server(void *data)
{
    int port = *((int *)data);
    socklen_t addrlen;
    int readn;
    char buf[MAXLINE];

    struct sockaddr_in server_addr, client_addr;

    if((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
    {
        perror("socket error:");
        return 1;
    }
    memset((void*)&server_addr, 0x00, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);//PORTNUM);//atoi(argv[1]));

    if(bind(server_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1)
    {
        perror("bind error");
        printf("%s\n", strerror(errno));
        return 1;
    }
    
    if(listen(server_sockfd, 5)==-1)
    {
        perror("listen error");
        return 1;
    }

    while(1)
    {
        addrlen = sizeof(client_addr);
        client_sockfd = accept( server_sockfd,
                (struct sockaddr*)&client_addr, &addrlen);
        printf("client_sockfd:%d\n", client_sockfd);
        if( client_sockfd == -1)
        {
            printf("accept error\n");
            sleep(5);
        }
        else{
            pthread_create(&thread_id, NULL, thread_func, (void*)&client_sockfd);
            pthread_detach(thread_id);
            //close(client_sockfd);
        }
    }
    printf("server close");
}

void display()
{
    printf("# Debug menu #\n");
    printf("# 1.createServer\n");
    printf("# 2.cancelServer\n");
    printf("# 3.cancelServer (connecting)\n");
    printf("# 4.send data to client\n");
    printf("command>");
    return 0;
}

int main(int argc, char **argv)
{
    //pthread_detach(thread_id_server);

    printf("start while\n");
    while(1)
    {
        display();
        int i = 0;
        scanf("%d",&i);
        switch(i)
        {
            case 1:
                {
                    int j=0;
                    printf("Create Server (input : port)>");
                    scanf("%d",&j);
                    pthread_create(&thread_id_server, NULL, thread_server, (void*)&j);
                }
                break;
            case 2:
                if(thread_id_server != NULL){
                    printf("cancel thread_id_server\n");
                    pthread_cancel(thread_id_server);
                    clean_up();
                }
                break;
            case 3:
                if(thread_id != NULL){
                    printf("cancel thread_id\n");
                    pthread_cancel(thread_id);
                    printf("close socket: %d\n", client_sockfd);
                    close(client_sockfd);
                }
                break;
            case 4:
                {
                    printf("sockfd num:");
                    int a = 0;
                    scanf("%d", &a);
                    char buf[30] = {0};
                    snprintf(buf, sizeof(buf), "%s", "helo");
                    write(a, buf, strlen(buf));
                }

            default:
                break;
        }
    }
    return 0;
}

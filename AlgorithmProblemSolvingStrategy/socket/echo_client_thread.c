#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define MAXLINE 1024
int g_port = 3600;
pthread_t thread_id;
int server_sockfd;

void display()
{
    printf("# Debug menu #\n");
    printf("# 1.createClient\n");
    printf("# 2.cancelClient\n");
    printf("# 3.change port\n");
    printf("command>");
}

void* thread_func(void* data)
{
RECONN:
    printf("reconnect>>>\n");
    sleep(3);
    int port = g_port; //<-- test
    struct sockaddr_in serveraddr;
    int client_len;
    char buf[MAXLINE];

    if ((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 )
    {
        perror("error :");
        goto RECONN;
    }
    printf("new fd: %d\n", server_sockfd);

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serveraddr.sin_port = htons(port);//atoi(argv[1]));

    client_len = sizeof(serveraddr);

    if(connect(server_sockfd, (struct sockaddr *)&serveraddr, client_len) == -1)
    {
        perror("connect error:");
        close(server_sockfd);
        goto RECONN;
    }
    printf("Success:Connection\n");

    // select
    fd_set readfds, allfds;
    int fd_num;
    int maxfd = 0;

    FD_ZERO(&readfds);
    FD_SET(server_sockfd, &readfds);
    int sockfd;
    maxfd = server_sockfd;
    while(1){

        sleep(3);
        allfds = readfds;
        printf("Select Wait %d\n", maxfd);
        fd_num= select(maxfd+1, &allfds, (fd_set*)0, (fd_set*)0, NULL);
        for(int i=0; i<=maxfd; i++)
        {
            sockfd = i;
            printf("try: sockfd: %d\n", sockfd);
            if(FD_ISSET(sockfd, &allfds))
            {
                memset(buf, 0x00, MAXLINE);
                //read(0, buf, MAXLINE);
                //snprintf(buf, sizeof(buf), "%s", "hello!");
                //printf("write:hello!\n");
                /*if(write(sockfd, buf, MAXLINE) <= 0)
                {
                    perror("write error:");
                    goto RECONN;
                }*/
                if(read(sockfd, buf, MAXLINE) <= 0 )
                {
                    perror("read error:");
                    close(sockfd);
                    FD_CLR(sockfd, &readfds);
                    goto RECONN;
                }
                printf("read: %s", buf);
                if(--fd_num <=0)
                    break;
            }
        }

    }
    return NULL;
}


int main(int argc, char **argv)
{
    printf("Create Client (input: port)>");
    int j = 0;
    scanf("%d", &j);
    pthread_create(&thread_id, NULL, thread_func, NULL);
    while(1)
    {
        display();
        int i=0;
        scanf("%d", &i);
        switch(i)
        {
            case 1:
                {
                    int j=0;
                    printf("Create Client (input: port)>");
                    scanf("%d", &j);
                    g_port = j;
                    pthread_create(&thread_id, NULL, thread_func, NULL);
                }
                break;
            case 2:
                printf("cancel client\n");
                pthread_cancel(thread_id);
                printf("close socket:%d\n", server_sockfd);
                close(server_sockfd);
                break;
            case 3:
                {
                    printf("change port:");
                    int k=0;
                    scanf("%d", &k);
                    g_port = k;
                }
                break;
            default:
                break;
        }
    }
    return -1;
}

   

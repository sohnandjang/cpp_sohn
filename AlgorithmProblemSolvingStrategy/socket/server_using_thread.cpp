#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define MAXLINE 1024
#define PORTNUM 3600

pthread_t tController_Server;
pthread_t tController_ClientService;
int server_sockfd;


void* Controller_ClientServiceThread(void* data)
{
    int sockfd = *((int *)data);
    printf("Connected by Client (%d)\n", sockfd);
    int readn;
    socklen_t addrlen;
    char buf[MAXLINE];
    struct sockaddr_in client_addr;
    addrlen = sizeof(client_addr);

    for(;;)
    {
        readn = read(sockfd, buf, MAXLINE);
        if(readn > 0)
        {
            printf("OK <<< read:%s\n", buf);
            if(strncmp("stop", buf, 4) ==0)
            {
                printf("<<< stop(client) by Client\n");
                close(sockfd);
                break;
            }
            if(strncmp("close", buf, 5) ==0)
            {
                printf("<<< close(server) by Client\n");
                close(server_sockfd);
                break;
            }
        }
        else 
        {
            printf("Fail <<< disconnected by Client");
            close(sockfd);
            break;
        }
    }

    printf("exit <<< %s\n", __func__);
    return NULL;
}

void* Controller_ServerThread(void* param)
{
    int socket = *((int*)param);
    printf("Controller_ServerThread start (%d)\n", socket);
    int client_sockfd;
    struct sockaddr_in client_addr;
    int addrlen = sizeof(client_addr);

    if(listen(socket, 10)==-1)
    {
        close(socket);
        perror("listen error");
        return NULL;
    }

    while(1)
    {
        client_sockfd = accept( socket, (struct sockaddr*)&client_addr, (socklen_t*)&addrlen);
        printf("accepted\n");
        if( client_sockfd == -1)
        {
            printf("accept error\n");
            sleep(1);
            continue;
        }
        else{
            pthread_create(&tController_ClientService, NULL, Controller_ClientServiceThread, (void*)&client_sockfd);
            pthread_detach(tController_ClientService);
            //printf("tController_ClientService join\n");
        }
    }
    printf("exit <<< %s\n", __func__);
}

void controller_createServer()
{
    int port = 41794;
    int client_sockfd;
    socklen_t addrlen;
    int readn;
    char buf[MAXLINE];

    struct sockaddr_in server_addr, client_addr;

    if((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
    {
        perror("socket error:");
        return ;
    }
    memset((void*)&server_addr, 0x00, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);//PORTNUM);//atoi(argv[1]));

    int reuseAddr = 1;
    if (setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &reuseAddr, sizeof(int)) < 0)
    {
        printf("setsockopt(SO_REUSEADDR) failed");
    }

    if(bind(server_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1)
    {
        close(server_sockfd);
        perror("bind error");
        printf("%s\n", strerror(errno));
        return ;
    }
    
    pthread_create(&tController_Server, NULL, Controller_ServerThread, (void*)&server_sockfd);
    pthread_detach(tController_Server);
    //pthread_join(tController_Server, NULL); // join하면 멈춘다.
    printf("exit<<< %s\n", __func__);
    return;
}
    
    

void display()
{
    printf("# Debug menu #\n");
    printf("# 1.createServer\n");
    printf("# 2.cancelServer\n");
    printf("# 3.exit\n");
    printf("command>");
}
int main(int argc, char **argv)
{
    printf("start main\n");
    bool _fExit = false;
    while(1)
    {
        display();
        int i = 0;
        scanf("%d",&i);
        switch(i)
        {
            case 1:
                {
                    int j=0;
                    printf("Create Server >>>\n");
                    controller_createServer();
                    printf("Next Loop of Create Server\n");
                }
                break;
            case 2:
                printf("Stop Server >>>\n");
                if(tController_Server)
                {
                    printf("kill >>> tController_Server\n");
                    pthread_cancel(tController_Server);
                    pthread_join(tController_Server, NULL);
                }
                if(tController_ClientService)
                {
                    printf("kill >>> tController_ClientService\n");
                    pthread_cancel(tController_ClientService);
                    pthread_join(tController_ClientService, NULL);
                }
                break;
            case 3:
                {
                    printf("Exit >>> \n");
                    _fExit = true;
                    break;
                }
            default:
                break;
        }
        if(_fExit)
            break;
    }
    while(!_fExit)
    {
        sleep(1);
    }
    return 0;
}

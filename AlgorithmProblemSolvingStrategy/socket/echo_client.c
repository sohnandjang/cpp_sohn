#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define MAXLINE 1024

pthread_t tController_connectServer;
pthread_t tClientThread;

void* clientThread(void* param)
{
    struct sockaddr_in serveraddr;
    int server_sockfd = *((int*)param);
    int client_len;
    char buf[MAXLINE];

    while(1)
    {
        memset(buf, 0x00, MAXLINE);
        read(0, buf, MAXLINE);
        if(strncmp(buf, "client_stop", 11)==0)
        {
            close(server_sockfd);
            printf("exit <<< %s\n", "stop");
            return NULL;
        }
        if(strncmp(buf, "cancel", 6)==0)
        {
            printf("exit <<< %s\n", "cancel");
            pthread_cancel(tController_connectServer);
            pthread_join(tController_connectServer, NULL);
            //pthread_cancel(tController_connectServer);
            return NULL;
        }
        if(write(server_sockfd, buf, MAXLINE) <= 0)
        {
            perror("write error:");
            return NULL;
        }
    }
}

void* controller_connectServer(void* param)
{
    struct sockaddr_in serveraddr;
    int server_sockfd;
    int client_len;
    char buf[MAXLINE];
    printf("create controller_connectServer\n");
    while(1)
    {
        sleep(4);

        if ((server_sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 )
        {
            perror("error :");
            continue;
        }
        
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
        serveraddr.sin_port = htons(41794);//htons(atoi(argv[1]));

        client_len = sizeof(serveraddr);

        if(connect(server_sockfd, (struct sockaddr *)&serveraddr, client_len) == -1)
        {
            perror("connect error:");
            continue;
        }

        printf("create ClientThread\n");
        pthread_create(&tClientThread, NULL, clientThread, (void*)&server_sockfd);
        pthread_join(tClientThread, NULL);
        printf("exit <<< tClientThread >>> Go Next\n");
    }
}

int main(int argc, char **argv)
{
    
    pthread_create(&tController_connectServer, NULL, controller_connectServer, NULL);
    pthread_detach(tController_connectServer);
    
    while(1)
    {
        sleep(1);
    }

    printf("exit <<< %s\n", __func__);    
    return 0;
}

   

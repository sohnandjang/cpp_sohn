#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>
#include <fstream>
#include <queue>
using namespace std;

int K;
int N;
struct RNG {
	unsigned seed;
	RNG() : seed(1983) {}
	unsigned next() {
		unsigned ret = seed;
		seed = ((seed * 214013u) + 2531011u);
		return ret % 10000 + 1;
	}
};
//vector<int> signals;
//RNG gRNG;

/*int simple(vector<int> signals)
{
	int ret = 0;
	for (int head = 0; head != signals.size(); ++head)
	{
		static int temp; 
		temp = signals[head];
		if (temp == K)
		{
			printf("found! (K:%d, head%d, =tail:%d\n", K, head, head);
			continue;
		}
		for (int tail = head+1; tail != signals.size(); ++tail)
		{
			temp += signals[tail];
			if (temp == K)
			{
				printf("found! (K:%d, head%d, tail:%d\n", K, head, tail);
				break;
			}
		}
	}
	return ret;
}
int simple1_1(vector<int> signals)
{
	int ret = 0;
	for (int head = 0; head != signals.size(); ++head)
	{
		int sum = 0;
		for (int tail = head; tail != signals.size(); ++tail)
		{
			sum += signals[tail];
			if (sum == K) 
			{
				printf("found! (K:%d, head%d, tail:%d\n", K, head, tail);
				ret++;
			}
			if (sum >= K)
				break;
		}
	}
	return ret;
}
int simple2(vector<int> signals)
{
	int ret = 0;
	for (int head = 0; head != signals.size(); ++head)
	{
		int sum = 0;
		static int tail_next = head;
		for (int tail = tail_next; tail != signals.size(); ++tail)
		{
			sum += signals[tail];
			if (sum == K) 
			{
				printf("found! (K:%d, head%d, tail:%d\n", K, head, tail);
				ret++;
			}
			if (sum >= K)
			{
				tail_next = tail + 1;
				break;
			}
		}
	}
	return ret;
}
int optimized(vector<int>& signals, int k)
{
	int ret = 0; int rangeSum = signals[0]; int tail=0;
	for (int head = 0; head!= signals.size(); ++head)
	{
		while (rangeSum < k && tail+1 < signals.size())
			rangeSum += signals[++tail];

		if (rangeSum == k) ret++;

		rangeSum -= signals[head];
	}
	return ret;
}
*/
int countRanges(int k, int n)
{
	RNG rng;
	queue<int> range;
	int ret = 0, rangeSum = 0;// rng.seed;
	for(int i = 0; i< n; i++)
	{
		//if (i != 0)
		{
			int newSignal = rng.next();
			rangeSum += newSignal;
			range.push(newSignal);
		}

		while (rangeSum > k) {
			rangeSum -= range.front();
			range.pop();
		}

		if (rangeSum == k) ret++;
	}
	return ret;
}

int main()
{
	fstream myfile;
	myfile.open("alien.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//signals.push_back(gRNG.seed);
		//for (int i = 0; i < 10; ++i)
		//	signals.push_back(gRNG.next());
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			//~ input
			myfile >> K;
			myfile >> N;
			//~ body
			//int ret = simple2(signals);
			//int ret = optimized(signals, K);
			int ret = countRanges(K, 10);
			cout << ret << endl;
		}

		myfile.close();
	}
	getchar();
	return 1;
}

#include <iostream>
#include <stack>
#include <string>
#include <fstream>
using namespace std;

string myStr;
stack<string> myStack;

bool wellMatched(const string& formula)
{
	const string opening("({["), closing(")}]");
	stack<char> openStack;
	for (int i = 0; i < formula.size(); ++i)
		if (opening.find(formula[i]) != -1)
			openStack.push(formula[i]);
		else
		{
			if (openStack.empty()) return false;
			if (opening.find(openStack.top()) != closing.find(formula[i]))
				return false;
			openStack.pop();
		}
	return openStack.empty();
}

int main()
{
	fstream myfile;
	myfile.open("bracket.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		for (int i = 0; i < C; i++)
		{
			//~ Init
			myStr.clear();
			while(!myStack.empty())
				myStack.pop();
			//~ Body
			myfile >> myStr;
			//cout << "input: " << myStr << endl;
#if 0
			while (!myStr.empty())
			{
				string temp = myStr.substr(0, 1);
				//cout << temp << endl;
				if (!temp.compare("{") || !temp.compare("[") || !temp.compare("("))
				{
					myStack.push(temp);
					//cout << "push: " << temp << endl;
				}
				else
				{
					if (!temp.compare("}"))
					{
						//cout << "meet: }" << endl;
						if (!(myStack.top().compare("{")))
						{
							temp = myStack.top();
							myStack.pop();
							//cout << "pop(): " << temp << endl;
						}
						else
						{
							myStack.push("}");
							//cout << "push: " << "}"<< endl;
						}
					}
					else if (!temp.compare("]"))
					{
						if (!(myStack.top().compare("[")))
						{
							temp = myStack.top();
							myStack.pop();
							//cout << "pop(): " << temp << endl;
						}
						else
						{
							myStack.push("]");
							//cout << "push: " << "]"<< endl;
						}
					}
					else if (!temp.compare(")"))
					{
						//cout << "meet: )" << endl;
						if ((!myStack.top().compare("(")))
						{
							temp = myStack.top();
							myStack.pop();
							//cout << "pop(): " << temp << endl;
						}
						else
						{
							myStack.push(")");
							//cout << "push: " << ")"<< endl;
						}
					}
				}
				myStr = myStr.substr(1, string::npos);
			}
			string ret = (myStack.empty() ? "YES" : "NO");
			cout << ret << endl;
#endif
			const char* ret = wellMatched(myStr) == true ? "YES" : "NO";
			cout << ret << endl;
		}

		myfile.close();
	}

	getchar();
	return 0;
}
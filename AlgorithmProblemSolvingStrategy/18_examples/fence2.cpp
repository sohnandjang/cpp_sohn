#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
using namespace std;

vector<int> h;
int solveStack()
{
	stack<int> remaining;
	h.push_back(0);
	int ret = 0;
	for (int i = 0; i < h.size(); ++i)
	{
		while (!remaining.empty() && h[remaining.top()] >= h[i])
		{
			int j = remaining.top();
			printf("pop=%d(index)\n", j);
			remaining.pop();
			int width = -1;
			if (remaining.empty())
			{
				width = i;
				printf("width=i=%d\n", i);
			}
			else
			{
				width = (i - remaining.top() - 1);
				printf("width=%d,i(%d)-remainig.top(%d)\n", width,i, remaining.top());
			}
			printf("i=%d, width = %d, area = %d\n", i, width, h[j] * width);
			ret = max(ret, h[j] * width);
		}
		remaining.push(i);
		printf("push=%d(index)\n", i);
	}
	return ret;
}

int main()
{
	/*
	h.push_back(1);
	h.push_back(3);
	h.push_back(2);
	h.push_back(5);
	h.push_back(5);
	h.push_back(4);
	int ret = solveStack();
	cout << ret << endl;
	*/

	h.clear();
	h.push_back(7);
	h.push_back(1);
	h.push_back(5);
	h.push_back(9);
	h.push_back(6);
	h.push_back(7);
	h.push_back(3);
	int ret = solveStack();
	cout << ret << endl;
	
	getchar();
	return 0;
}
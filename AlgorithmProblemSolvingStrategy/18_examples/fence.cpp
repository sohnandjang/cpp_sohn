#include <iostream>
#include <stack>
#include <vector>
#include <algorithm>
#include <fstream>
using namespace std;

int N;
int LEFT[20010];
int RIGHT[20010];
typedef struct _data{
	int left;
	int right;
	int height;
}data;
vector<int> fence;
stack<int> st;
//stack<struct _data> st_data;
stack<pair<int,int>> st_data; //index, height
//struct _data tempData;
int maxArea = 0;

int solve(void)
{
	//~init
	int index = 0;
	LEFT[0] = -1;
	st.push(fence[index]);
	st_data.push(pair<int, int>(index, fence[index]));

	//int next_index = index + 1;
	while (index +1 < N )
	{
		if (fence[index] < fence[index + 1])
		{
			st.push(fence[index+1]);
			st_data.push(pair<int, int>(index+1, fence[index+1]));
			LEFT[index + 1] = index;
		}
		else
		{ //~ less or same
			RIGHT[index] = index + 1;
			int tempArea = (RIGHT[index] - LEFT[index] - 1)*fence[index];
			printf("tempArea = %d (index=%d, RIGHT=%d, LEFT=%d, fence=%d)\n", tempArea, index, RIGHT[index], LEFT[index], fence[index]);
			maxArea = max(maxArea, tempArea);

			st.pop();
			st_data.pop();
			st.push(fence[index + 1]);
			st_data.push(pair<int, int>(index + 1, fence[index + 1]));
			LEFT[index + 1] = LEFT[index];
		}

		++index;
	}
	//~ reach end
	while (!st.empty())
	{
		//int fixedRight = N;
		int index = (st_data.top()).first;
		int height = (st_data.top()).second;
		//find RIGHT
		int right = 0;
		if (index == N - 1)
			right = N;
		else
		{
			int i = 0;
			for (i = index+1; i < N; ++i)
			{
				printf("height=%d, fence[%d]=%d\n", height, i, fence[i]);
				if (height > fence[i])
				{
					right = i;
					printf("right update = %d(height=%d, index=%d, fence[%d]=%d)\n", right, height, i, i, fence[i]);
					break;
				}
			}
			if (i == N)
			{
				right = N;
				printf("right update = %d\n", right);
			}
		}

		int tempArea = (right-LEFT[index]-1)*height;
		printf("tempArea2 = %d (index=%d, RIGHT=%d, LEFT=%d, fence=%d)\n", tempArea, index, right, LEFT[index], height);
		maxArea = max(maxArea, tempArea);
		st.pop();
		st_data.pop();
	}
	return maxArea;
}

int main()
{
	fstream myfile;
	myfile.open("fence.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			fence.clear();
			maxArea = 0;
			memset(LEFT, 0, sizeof(LEFT));
			memset(RIGHT, 0, sizeof(RIGHT));
			//~ input
			myfile >> N;
			for (int j = 0; j < N; j++)
			{
				int temp;
				myfile >> temp;
				fence.push_back(temp);
			}

			//~ body
			int ret = solve();
			cout << ret << endl;
		}

		myfile.close();
	}
	getchar();
	return 1;
}
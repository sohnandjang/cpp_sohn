#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

void printPostOrder(vector<string>& preorder, vector<string>& inorder)
{
	if (preorder.size() == 1 && inorder.size() == 1) //~ base
	{
		cout << preorder[0] << " ";
		return;
	}
	string head = preorder[0];
	ptrdiff_t head_pos = distance(inorder.begin(), find(inorder.begin(), inorder.end(), head));

	int leftsize = head_pos;
	int rightsize = inorder.size() - leftsize - 1;
	if (leftsize >= 1)
	{
		vector<string> left_preorder; 
		for (int i = 0; i < leftsize; i++)
		{
			left_preorder.push_back(preorder[i + 1]);
		}

		vector<string> left_inorder; 
		for (int i = 0; i < leftsize; i++)
		{
			left_inorder.push_back(inorder[i]);
		}
		printPostOrder(left_preorder, left_inorder);
	}
	if (rightsize >= 1)
	{
		vector<string> right_preorder; 
		for (int i = 0; i < rightsize; i++)
		{
			right_preorder.push_back(preorder[head_pos + 1 + i]);
		}
		vector<string> right_inorder; 
		for (int i = 0; i < rightsize; i++)
		{
			right_inorder.push_back(inorder[head_pos + 1 + i]);
		}
		printPostOrder(right_preorder, right_inorder);
	}
	cout << head << " ";
}

vector<int> slice(const vector<int>& v, int a, int b)
{
	return  vector<int>(v.begin() + a, v.begin() + b);
}
void printPostOrder2(const vector<int>& preorder, const vector<int>& inorder)
{
	if (preorder.empty()) return;

	const int N = preorder.size();
	const int root = preorder[0];
	const int pos_root = find(inorder.begin(), inorder.end(), root) - inorder.begin();
	const int L = pos_root;
	const int R = N - L - 1;
	printPostOrder2(slice(preorder, 1, L+1), slice(inorder, 0, L ));
	printPostOrder2(slice(preorder, L+1, N), slice(inorder, L+1, N ));
	cout << root << ' ';
}
int main()
{
	//vector<string> preorder = { "27", "16", "9", "12", "54", "36", "72" };
	//vector<string> inorder = { "9", "12", "16", "27", "36", "54", "72" };
	//cout << preorder[0] << " " << preorder[1] << endl;
	//printPostOrder(preorder, inorder);

	vector<int> preorder = { 27, 16, 9, 12, 54, 36, 72 };
	vector<int> inorder = { 9, 12, 16, 27, 36, 54, 72 };
	printPostOrder2(preorder, inorder);
	getchar();
	return 1;
}

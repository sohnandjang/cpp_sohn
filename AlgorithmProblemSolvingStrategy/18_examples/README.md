# Fence (using Stack)
![](image/fence.PNG)

# alien
when you use function param as vector<int> ...,  
you should reference like this  
```void function(vector<int>& Vec)```

# alien _ random generator

```
struct RNG{
	unsigned seed;
	RNG() : seed(1983){}
	unsigned ret = seed;
	seed = ((seed x ..... );
	return ret % 10000 + 1;
	}
};
```

# Matching bracket
```const string opening("({["), closing(")}]");```


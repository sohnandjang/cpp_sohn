#include <iostream>
#include <list>
#include <fstream>
using namespace std;
int N;
int K;
int fInit = 0;
int shift = 0;
list<int> lt;
void solve()
{
	list<int>::iterator it = lt.begin();
	while (lt.size() > 2)
	{
		if (fInit == 0)
		{
			lt.erase(it++);
			fInit = 1;
		}
		else
		{
			//it = lt.begin();
			shift = 0;
			while (1)
			{
				++shift;
				if (shift == K)
				{
					lt.erase(it++);
					//printf("size=%d\n", lt.size());
					if (it == lt.end())
					{
						//printf("meet end() 22222");
						it = lt.begin();
					}
					break;
				}
				//printf("before ++it");
				++it;
				//printf("after++it");
				if (it == lt.end())
				{
					//printf("meet end()");
					it = lt.begin();
				}
				else
				{
					//printf("meet end()");
				}
			}
			
		}
	}
	for (it = lt.begin(); it != lt.end(); it++)
		cout << *it << ' ';
	cout << endl;
}
void josephus(int n, int k)
{
	list<int> survivors;
	for (int i = 1; i <= n; i++)
		survivors.push_back(i);

	list<int>::iterator kill = survivors.begin();
	while (n > 2)
	{
		kill = survivors.erase(kill);
		--n;
		if (kill == survivors.end())
			kill = survivors.begin();
		for (int i = 0; i < K - 1; i++)
		{
			++kill;
			if (kill == survivors.end())
				kill = survivors.begin();
		}
	}
	cout << survivors.front() << " " << survivors.back() << endl;

}
int main()
{
	fstream myfile;
	myfile.open("josephus.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		for (int i = 0; i < C; ++i)
		{
			myfile >> N;
			myfile >> K;
			//~ init
			lt.clear();
			fInit = 0;
			shift = 0;
			//~ body
			//for (int j = 1; j < N + 1; ++j)
			//	lt.push_back(j);

			//solve();
			josephus(N, K);

		}

		myfile.close();
	}
	getchar();
	return 0;
}
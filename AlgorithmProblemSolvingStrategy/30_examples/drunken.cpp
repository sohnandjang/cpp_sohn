#include <iostream>
#include <vector>
#include <queue>
#include <algorithm> // min
#include <stdlib.h>
#include <string.h>

using namespace std;

int V = 8;
constexpr int MAX_V = 8;
constexpr int INF = 1000000;
int E[MAX_V] = {8, 6, 5, 8, 3, 5, 8, 4};
/*int adj[MAX_V][MAX_V] = {
//    0,    1,   2,   3,   4,  5,  6,   7
    { INF,  3, INF, INF, INF,  9, INF, INF }, // 0
    { 3, INF,  6, INF, INF, INF,  INF,   3 }, // 1
    { INF,  6, INF,   4,   2, INF, INF, INF }, // 2
    { INF, INF,  4, INF,  7, INF, INF, INF }, // 3 
    { INF, INF,  2,   7, INF,  5, INF, INF }, // 4
    { 9,  INF, INF, INF,  5, INF,  3,  5 }, // 5
    { INF, INF, INF, INF, 1,   3, INF, 3 }, // 6
    { INF,  3, INF, INF, INF,   5, 3, INF } // 7
};*/
int adj[MAX_V][MAX_V] = {
//    0,    1,   2,   3,   4,  5,  6,   7
    { INF,  3, INF, INF, INF,  9, INF, INF }, // 0
    { INF, INF,  6, INF, INF, INF,  INF,   3 }, // 1
    { INF,  INF, INF,   4,   2, INF, INF, INF }, // 2
    { INF, INF,  INF, INF,  7, INF, INF, INF }, // 3 
    { INF, INF,  INF,   INF, INF,  INF, INF, INF }, // 4
    { INF,  INF, INF, INF,  5, INF,  3,  5 }, // 5
    { INF, INF, INF, INF, 1,   INF, INF, INF }, // 6
    { INF,  INF, INF, INF, INF,  INF, 3, INF } // 7
};

int floyd()
{
    for(int k = 0; k<MAX_V; ++k){
        for(int i=0; i<MAX_V; ++i){
            for(int j=0; j<MAX_V; ++j){
                for( int e; e<MAX_V; ++e){
                    adj[i][j] = min(adj[i][j], adj[i][e]+adj[e][j] + E[e] );
                }
            }
        }
    }
}

int delay[MAX_V] = {8, 6, 5, 8, 3, 5, 8, 4};
int W[MAX_V][MAX_V];
void solve(){
    vector<pair<int, int>> order;
    for(int i=0; i<V; ++i)
        order.push_back( make_pair(delay[i], i) );
    sort( order.begin(), order.end() );

    for(int i=0; i<V; ++i)
        for(int j=0; j<V; ++j)
            if(i==j)
                W[i][j] = 0;
            else
            {
                W[i][j] = adj[i][j];
            }
            
    int ret = INF;
    for(int k=0; k<V; ++k){
        int w = order[k].second;
        for(int i=0; i<V; ++i)
            for(int j=0; j<V; ++j){
                adj[i][j] = min(adj[i][j], adj[i][w] + adj[w][j]);
                W[i][j] = min(adj[i][w] + delay[w] + adj[w][j], W[i][j]);
            }
    }
}

int main()
{
    solve();
    return 0;
}
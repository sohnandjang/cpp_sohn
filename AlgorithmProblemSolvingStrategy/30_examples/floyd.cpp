#include <iostream>
#include <vector>
#include <queue>
#include <algorithm> // min
#include <stdlib.h>
#include <string.h>

using namespace std;

int V = 7;
constexpr int MAX_V = 7;
constexpr int INF = 1000000;
int adj[MAX_V][MAX_V] = {
//    0,  1,  2,  3,  4,  5,  6
    { INF, 5, 1, INF, INF, INF, INF },
    { 5, INF, INF, 1, INF, INF, 3 },
    { 1, INF, INF, 2, INF, INF, INF },
    { INF, 1, 3, INF, 5, 3, INF },
    { INF,INF,INF, 5, INF, INF, INF },
    { INF, 3, INF, 3, INF, INF, 2 },
    { INF, 3, INF,INF,INF, 2, INF }
};
int C[MAX_V][MAX_V][MAX_V];
void allPairShortestPAth1(){
    for(int i=0; i<V; ++i)
        for(int j=0; j<V; ++j)
            if( i!=j )
                C[0][i][j] = min(adj[i][j], adj[i][0] + adj[0][j]);
            else
                C[0][i][j] = 0;

    for(int k=1; k<V; ++k)
        for(int i=0; i<V; ++i)
            for(int j=0; j<V; ++j)
                C[k][i][j] = min(C[k-1][i][j], C[k-1][i][k]+C[k-1][k][j]);
}

void floyd(){
    for(int i=0; i<V; ++i)
        adj[i][i]=0;

    for(int k=1; k<V; ++k)
        for(int i=0; i<V; ++i)
            for(int j=0; j<V; ++j)
                adj[i][j] = min(adj[i][j], adj[i][k]+adj[k][j]);
}

int via[MAX_V][MAX_V];
void floyd2(){
    for(int i=0; i<V; ++i)
        adj[i][i]=0;
    memset(via, -1, sizeof(via));
    for(int k=1; k<V; ++k)
        for(int i=0; i<V; ++i)
            for(int j=0; j<V; ++j)
                if(adj[i][j] > adj[i][k]+adj[k][j]){
                    via[i][j] = k;
                    adj[i][j] = adj[i][k] + adj[k][j];
                }
}

void reconstruct(int u, int v, vector<int>& path)
{
    if(via[u][v] == -1){
        path.push_back(u);
        if(u != v) path.push_back(v);
    }
    else{
        int w = via[u][v];
        reconstruct(u, w, path);
        path.pop_back();
        reconstruct(w, v, path);
    }
}

int main()
{
    //allPairShortestPAth1();
    floyd2();
    vector<int> path;
    reconstruct(0, 1, path);
    //for(int i=0; i<7; ++i)
    {
         printf("%d >", adj[0][1]);
    }
    printf("\n");
    return 0;
}
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int V=4;
constexpr int MAX_V = 7;
constexpr int INF = 10000000;

vector<pair<int,int>> adj[MAX_V] ={
   {{2,-7}, {3,-4}},
   {},
   {{1,3}},
   {{2,9}}
};

vector<int> bellmanFord(int src){
    vector<int> upper(V, INF);
    upper[src] = 0;
    bool updated;

    for(int iter = 0; iter < V; ++iter){
        updated = false;
        for(int here = 0; here <V; ++here){
            for(int i=0; i<adj[here].size(); i++){
                int there = adj[here][i].first;
                int cost = adj[here][i].second;

                if(upper[there] > upper[here] + cost){
                    upper[there] = upper[here] + cost;
                    updated = true;
                }
            }
        }
        if(!updated) break;
        
    }
    if(updated) upper.clear();
    return upper;
}

int main()
{
    vector<int> path = bellmanFord(0);
    for(int i=0; i<path.size(); ++i)
        printf("%d >", path[i]);
    printf("\n");
    return 0;
}
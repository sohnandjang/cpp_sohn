#include <iostream>
#include <vector>
#include <queue>

using namespace std;

const int INF = 10000000;
const int MAX_V = 7;
int V = 7;

vector<pair<int,int>> adj[MAX_V] ={
   {{1,5}, {2,1}},
   {{0,5}, {3,1}, {6,3}},
   {{0,1}, {3,2}},
   {{2,3}, {1,1}, {4,5}, {5,3}},
   {{3,5}},
   {{3,3}, {1,3}, {6,2}},
   {{5,2}, {1,3}}
};

vector<int> dijkstra(int src){
    vector<int> dist(V, INF);
    dist[src] = 0;
    priority_queue<pair<int,int>> pq;
    pq.push(make_pair(0, src));
    while(!pq.empty()){
        int cost = -pq.top().first;
        int here = pq.top().second;
        pq.pop();

        if(dist[here] < cost) continue;

        for(int i=0; i<adj[here].size(); ++i){
            int there = adj[here][i].first;
            int nextDist = cost + adj[here][i].second;

            if(dist[there] > nextDist ){
                dist[there] = nextDist;
                pq.push(make_pair(-nextDist, there));
            }
        }
    }
    return dist;
}

int main()
{
    vector<int> path = dijkstra(0);
    for(int i=0; i<path.size(); ++i)
        printf("%d >", path[i]);
    printf("\n");
    return 0;
}
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm> //~ min

using namespace std;

int cache[101][101];

string pattern;
string word; 
vector<string> output;

static int cnt = 0;
int match(int p_pos, int w_pos)
{
	int p_size = pattern.size();
	int w_size = word.size();
	int& ret = cache[p_pos][w_pos];
	//cout << "p=" << p_pos << ", w=" << w_pos << endl;
	//cout << "p=" << pattern << ", w=" << word << endl;
	if (ret != -1) //~ cache
	{
		cnt++;
		return ret;
	}

	while (1)
	{
		if ((p_pos < p_size && w_pos < w_size)
			&& (pattern[p_pos] == '?' || pattern[p_pos] == word[w_pos])) //~ match or ?
		{
			p_pos++;
			w_pos++;
		}
		else
		{
			break;
		}

	}
	/*while ((p_pos < p_size && w_pos < w_size)
		&& (pattern[p_pos] == '?' || pattern[p_pos] == word[w_pos])) //~ match or ?
	{
		cout << "pos increae: " << p_pos<< " " << w_pos<< endl;
		p_pos++;
		w_pos++;
	}*/

	if (p_pos == p_size ) // pattern arrive at end 
	{
		if (w_pos == w_size)
		{
			return ret = 1;
		}
		else
		{
			return ret = 0;
		}
	}
	//~ in case that * is the end of pattern,
	// p_size is not the same as w_size, so it goes to below * condition
	if (pattern[p_pos] == '*')
	{
		for (int i = 0; i < w_size; ++i)
		{//~ recursive : pattern : * reduce, word : brute force
			if (match(p_pos + 1, w_pos + i))
				return ret = 1;
		}
	}
	return ret = 0; 
}

int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);


	int c;
	fs >> c;
	//c = 4;

	for (int i = 0; i < c; ++i)
	{
		pattern.clear();
		output.clear();
		fs >> pattern;

		int num = 0;
		fs >> num;
		//num = 3;
		for (int j = 0; j < num; ++j)
		{	//~ initialize
			int ret = 0;
			word.clear();
			fs >> word;

			//~ comparison
			//cout << "input: pattern=" << pattern << ", word[" << k << "]=" << word[k] << endl;
			//ret = match(pattern, word[k]);
			memset(cache, -1, sizeof(cache)); //cache initialization!!!
			ret = match(0, 0);
			if (ret == 1)
			{
				//cout << "add = " << word[k] << endl;
				output.push_back(word);
			}
		}
		//~ output (in an dictionary order)
		sort(output.begin(), output.end());
		//cout << "<result> "  << endl;
		for (unsigned int l = 0; l < output.size(); ++l)
		{
			cout << output[l] << endl;
		}
		//cout << "cnt = " << cnt << endl;
	}
	getchar();
	return 0;
}
#if 0
int match(string p, string w)
{//~ 0:false, 1:true
	int pos = 0;
	int p_size = p.size();
	int w_size = w.size();
	int ret = 0;
	//cout << "p=" << p << ", w=" << w << endl;
	while ((pos < p_size && pos < w_size)
		&& (p[pos] == '?' || p[pos] == w[pos])) //~ match or ?
		pos++;

	if (pos == p_size && pos == w_size) //~ if arrive at the end,
	{
		//cout << "match" << endl;
		return 1;
	}
	//~ in case that * is the end of pattern,
	// p_size is not the same as w_size, so it goes to below * condition

	if (p[pos] == '*')
	{
		for (int i = 0; i < w_size; ++i)
		{//~ recursive : pattern : * reduce, word : brute force
			ret = ret || match(p.substr(pos + 1), w.substr(pos + i)); //*** OR *** atleast once
		}
	}
	return ret;
}

int main(int argc, char** argv[])
{
	const char*  DATA_FILE_NAME = "input.txt";
	ifstream fs(DATA_FILE_NAME);

	int c;
	fs >> c;
	//c = 1;

	for (int i = 0; i < c; ++i)
	{
		vector<string> output;
		vector<string> word; // array 
		string pattern;
		fs >> pattern;

		int num = 0;
		fs >> num;
		//num = 1;
		for (int j = 0; j < num; ++j)
		{
			string temp;
			fs >> temp;
			word.push_back(temp);
		}
		//~ comparison
		for (int k = 0; k < num; ++k)
		{
			int ret = 0;
			//cout << "input: pattern=" << pattern << ", word[" << k << "]=" << word[k] << endl;
			ret = match(pattern, word[k]);
			if (ret == 1)
			{
				//cout << "add = " << word[k] << endl;
				output.push_back(word[k]);
			}
		}
		//~ output (in an dictionary order)
		sort(output.begin(), output.end());
		//cout << "<result> "  << endl;
		for (int l = 0; l < output.size(); ++l)
		{
			cout << output[l] << endl;
		}
	}
	getchar();
	return 0;
}
#endif

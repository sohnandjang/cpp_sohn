#include <iostream>
#include <vector>
#include <string.h>
#include <stdlib.h>
using namespace std;

const int MAX_N = 3;
const int MAX_M = 3;
int n=3, m=3;
/*bool adj[MAX_N][MAX_M] = {
    {0,1,0,1,0,1},
    {1,0,0,0,1,0},
    {0,0,0,1,0,1},
    {1,0,1,0,0,0},
    {0,1,0,0,0,0},
    {1,0,1,0,0,0}
};*/
/*bool adj[MAX_N][MAX_M] = {
    {0,1,0,1,0,1},
    {1,0,0,0,1,0},
    {0,0,0,1,0,1},
    {1,0,1,0,0,0},
    {0,1,0,0,0,1},
    {1,0,1,0,1,0}
};*/
bool adj[MAX_N][MAX_M] = {
    { 1, 1, 1},
    { 1, 0, 0},
    { 0, 1, 0}
};
vector<int> aMatch = {0, 2, 4};
vector<int> bMatch = {1, 3, 5};

vector<bool> visited;

bool dfs(int a){
    if(visited[a]) return false;
    visited[a] = true;
    for(int b = 0; b < m; ++b){
        if(adj[a][b]){
            if(bMatch[b] == -1 || dfs(bMatch[b])){
                aMatch[a] = b;
                bMatch[b] = a;
                return true;
            }
        }
    }
    return false;
}   

int bipartiteMatch(){
    aMatch = vector<int>(n, -1);
    bMatch = vector<int>(m, -1);
    int size = 0;
    for(int start = 0; start< n; ++start){
        visited = vector<bool>(n, false);
        if(dfs(start))
            ++size;
    }
    return size;
}

const int H = 4;
const int W = 6;
const int dx[4] = {1, -1, 0, 0};
const int dy[4] = {0, 0, 1, -1};

bool inRange(int y, int x){
    if(y>=0 && y<=W-1)
        if(x>=0 && x<=H-1)
            return true;

    return false;
}
//int H, W;
vector<string> board={
    "#0000#",
    "000000",
    "00#000",
    "#00000"
};
bool fillDomino(){
    vector<vector<int>> id(H, vector<int>(W, -1));
    n = m = 0;
    for(int y=0; y<H; ++y){
        for(int x = 0; x<W; ++x){
            if(board[y][x] != '#'){
                if((y+x)%2==0){
                    id[y][x] = n++;
                }
                else
                {
                    id[y][x] = m++;
                }
                
            }
        }
    }
    memset(adj, 0, sizeof(adj));
    for(int y1=0; y1<H; ++y1){
        for(int x1=0; x1<W; ++x1){
            //if((y1+x1)%2 == 0 && board[y1][x1] != '#'){
                for(int k=0; k<4; ++k){
                    int y2=y1+dy[k], x2 = x1+dx[k];
                    if(inRange(y2, x2) && board[y2][x2] != '#'){
                        adj[id[y1][x1]][id[y2][x2]] = 1;
                    }
                }
            //}
        }
    }
    return bipartiteMatch() * 2 == (n + m);
}

int main()
{
    bool ret = fillDomino();
    printf("%d", ret);
    return 0;
}
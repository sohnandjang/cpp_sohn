#include <iostream>
#include <vector>
using namespace std;

const int MAX_N = 3;
const int MAX_M = 3;
int n=3, m=3;
/*bool adj[MAX_N][MAX_M] = {
    {0,1,0,1,0,1},
    {1,0,0,0,1,0},
    {0,0,0,1,0,1},
    {1,0,1,0,0,0},
    {0,1,0,0,0,0},
    {1,0,1,0,0,0}
};*/
/*bool adj[MAX_N][MAX_M] = {
    {0,1,0,1,0,1},
    {1,0,0,0,1,0},
    {0,0,0,1,0,1},
    {1,0,1,0,0,0},
    {0,1,0,0,0,1},
    {1,0,1,0,1,0}
};*/
bool adj[MAX_N][MAX_M] = {
    { 1, 1, 1},
    { 1, 0, 0},
    { 0, 1, 0}
};
vector<int> aMatch = {0, 2, 4};
vector<int> bMatch = {1, 3, 5};

vector<bool> visited;

bool dfs(int a){
    if(visited[a]) return false;
    visited[a] = true;
    for(int b = 0; b < m; ++b){
        if(adj[a][b]){
            if(bMatch[b] == -1 || dfs(bMatch[b])){
                aMatch[a] = b;
                bMatch[b] = a;
                return true;
            }
        }
    }
    return false;
}   

int bipartiteMatch(){
    aMatch = vector<int>(n, -1);
    bMatch = vector<int>(m, -1);
    int size = 0;
    for(int start = 0; start< n; ++start){
        visited = vector<bool>(n, false);
        if(dfs(start))
            ++size;
    }
    return size;
}

int main()
{
    int ret = bipartiteMatch();
    printf("%d", ret);
    return 0;
}
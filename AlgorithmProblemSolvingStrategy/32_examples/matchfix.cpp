#include<algorithm>
#include<cstring>
#include<iostream>
#include<queue>
#include<vector>
using namespace std;
const int S = 0;
const int E = 1;
int n,m,cap[115][115],flow[115][115],score[13],match[100][2];
bool networkFlow() {
    int totalFlow = 0;
    while(1){
        queue<int> q;
        vector<int> prev = vector<int>(2+n+m,-1);
        q.push(S);
        prev[S]=S;
 
        while(!q.empty() && prev[E]==-1){
            int here = q.front();
            q.pop();
 
            for (int i = 0; i < 2 + m + n; ++i) {
                if (cap[here][i] - flow[here][i] > 0 && prev[i] == -1) {
                    prev[i] = here;
                    q.push(i);
                }
            }
        }
 
        if(prev[E]==-1) break;
 
        for(int p = E; p != S; p = prev[p]){
            flow[prev[p]][p] ++;
            flow[p][prev[p]] --;
        }
        totalFlow++;
    }
    return m==totalFlow;
}

int main(){
    int T;
    cin>>T;
    while(T--){
        cin >> n >> m;
        for (int i = 0; i < n; ++i) {
            cin >> score[i];
        }
        int count = 0;
        for (int i = 0; i < m; ++i) {
            int a,b;
            cin >> a >> b;
            if(a==0 || b==0) ++count;
            match[i][0] = a;
            match[i][1] = b;
        }
 
        int lo = score[0], hi = score[0]+count, mid = 0, ans = -1;
        while(lo <= hi){
            mid = (lo + hi )/2;
            if(*max_element(score+1,score+n) >= mid) {
                lo = mid + 1;
                continue;
            }
            memset(flow,0,sizeof(flow));
            for (int i = 0; i < m; ++i) {
                cap[S][2 + i] = 1;
                for (int j = 0; j < 2; ++j) {
                    cap[2 + i][2 + m + match[i][j]] = 1;
                }
            }
            for (int i = 0; i < n; ++i) {
                cap[2 + m + i][E] = (i==0?mid-score[0]:mid-score[i]-1);
            }
            if(networkFlow()) ans = mid, hi = mid-1;
            else lo = mid + 1;
        }
        cout<<ans<<"\n";
        memset(cap,0,sizeof(cap));
    }
}

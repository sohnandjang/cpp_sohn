#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

struct NaiveDisjointSet{
    vector<int> parent;
    NaiveDisjointSet(int n): parent(n){
        for(int i=0; i<n; ++i)
            parent[i] = i;
    }

    int find(int u) const {
        if( u == parent[u] ) return u;
        return find(parent[u]);
    }

    void merge(int u, int v)
    {
        u = find(u); v = find(v);
        if(u == v) return;
        parent[u] = v;
    }
};

struct OptimizedDisjointSet{
    vector<int> parent, rank;
    OptimizedDisjointSet( int n ) : parent(n), rank(n, 1){
        for(int i=0; i < n; ++i )
            parent[i] = i;
    }

    int find(int u)
    {
        if( u == parent[u]) return u;
        return parent[u] = find(parent[u]);
    }

    void merge( int u, int v ){
        u = find(u); v = find(v);
        if(u == v) return;
        if( rank[u] > rank[v] ) swap(u, v);
        parent[u] = v;
        if( rank[u] == rank[v] ) ++rank[v];
    }
};


#define DisjointSet OptimizedDisjointSet

const int MAX_V = 7;
int V = 7;

vector<pair<int,int>> adj[MAX_V] = {
    {{1,5}, {2,1}},
    {{0,5}, {3,1}, {6,3}, {5,3}},
    {{0,1}, {3,4}},
    {{4,5}, {5,3}, {2,4}, {1,1}},
    {{3,5}},
    {{6,2}, {1,3}, {3,3}},
    {{1,3}, {5,2}}
};

int kruskal(vector<pair<int,int>>& selected){
    int ret = 0;
    selected.clear();

    vector<pair<int, pair<int,int>>> edges;
    for(int u=0; u<V; ++u)
        for(int i=0; i<adj[u].size(); ++i){
            int v = adj[u][i].first, cost = adj[u][i].second;
            edges.push_back(make_pair(cost, make_pair(u,v)));
        }

    sort(edges.begin(), edges.end());
    DisjointSet sets(V);
    for(int i=0; i<edges.size(); ++i){
        int cost = edges[i].first;
        int u = edges[i].second.first, v = edges[i].second.second;
        if(sets.find(u) == sets.find(v)) continue;
        
        sets.merge(u, v);
        selected.push_back( make_pair(u, v));
        ret += cost;
    }
    return ret;
}

int main()
{
    vector<pair<int,int>> selected;
    int cost = kruskal( selected );
    printf("cost: %d\n", cost);
    for(int i=0; i<selected.size(); ++i){
        printf("%d -> %d\n", selected[i].first, selected[i].second);
    }
    return 0;
}
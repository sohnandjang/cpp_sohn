#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

const int INF = 987654321;
const int MAX_V = 7;
int V = 7;

vector<pair<int,int>> adj[MAX_V] = {
    {{1,5}, {2,1}},
    {{0,5}, {3,1}, {6,3}, {5,3}},
    {{0,1}, {3,4}},
    {{4,5}, {5,3}, {2,4}, {1,1}},
    {{3,5}},
    {{6,2}, {1,3}, {3,3}},
    {{1,3}, {5,2}}
};

int prim(vector<pair<int,int>>& selected){
    selected.clear();
    vector<bool> added(V, false);
    vector<int> minWeight(V, INF), parent(V, -1);
    int ret = 0;
    minWeight[0] = parent[0] = 0;
    for(int iter=0; iter<V; ++iter){
        int u = -1;
        for(int v = 0; v<V; ++v)
            if(!added[v] && (u==-1 || minWeight[u] > minWeight[v]))
                u = v;

        if(parent[u] != u)
            selected.push_back( make_pair(parent[u], u));
        ret += minWeight[u];
        added[u] = true;

        for(int i=0; i<adj[u].size(); ++i){
            int v = adj[u][i].first, weight = adj[u][i].second;
            if(!added[v] && minWeight[v] > weight) {
                parent[v] = u;
                minWeight[v] = weight;
            }
        }
    }
    return ret;
}

int main()
{
    vector<pair<int,int>> selected;
    int cost = prim( selected );
    printf("cost: %d\n", cost);
    for(int i=0; i<selected.size(); ++i){
        printf("%d -> %d\n", selected[i].first, selected[i].second);
    }
    return 0;
    return 0;
}
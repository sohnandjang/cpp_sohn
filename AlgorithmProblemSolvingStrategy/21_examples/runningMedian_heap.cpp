#include <iostream>
#include <fstream>
#include <queue>
#include <vector>

template<typename T> void print_queue(T& q) {
	while (!q.empty()) {
		std::cout << q.top() << " ";
		q.pop();
	}
	std::cout << '\n';
}
struct RNG {
	int seed;
	int a, b;
	RNG(int _a, int _b) :a(_a), b(_b), seed(1983) {}
	int next() {
		int ret = seed;
		seed = (seed * (long long)a + b) % 20090711;
		return ret;
	}
};

int runningMedian(int n, RNG rng)
{
	//std::priority_queue<int, std::vector<int>,std::greater<int>> q1;
	//std::priority_queue<int, std::vector<int>> q2;
	int ret = 0;
	std::priority_queue<int, std::vector<int>> q1;
	std::priority_queue<int, std::vector<int>,std::greater<int>> q2;
	std::priority_queue<int> q;
	for (int i = 0; i < n; i++)
	{
		int cur = rng.next();
		printf("cur=%d\n", cur);
		//~ 1.insert
		if (q1.empty() || q1.size() == q2.size())
			q1.push(cur);
		else if (q2.size() < q1.size())
			q2.push(cur);
		//printf("q1.size()=%d, q2.size()=%d\n", q1.size(), q2.size());
		//~ 2. compare
		if (!q2.empty())
		{
			//printf("q1.top()=%d, q2.top()=%d\n", q1.top(), q2.top());
			if (q1.top() > q2.top()) //~ change
			{
				int q1Val = q1.top();
				q1.pop();
				int q2Val = q2.top();
				q2.pop();
				q1.push(q2Val);
				q2.push(q1Val);
			}
		}
		ret = (ret + q1.top()) % 20090711;
		printf("ret = %d, med = %d\n", ret, q1.top());
	}
	//print_queue(q1);
	//print_queue(q2);
	//return 0;
	return ret;
}

int main()
{
	std::fstream myfile;
	myfile.open("runningMedian.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			int n, a, b;
			myfile >> n; myfile >> a; myfile >> b;
			RNG rng(a, b);
			int ret = runningMedian(n, rng);
			std::cout << ret << std::endl;
		}

		myfile.close();
	}
	getchar();
	return 0;
}

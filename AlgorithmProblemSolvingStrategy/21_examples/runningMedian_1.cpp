#include <stdlib.h>
#include <iostream>
#include <utility>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
typedef int KeyType;
int N;

struct Node {
	KeyType key;
	int priority, size;
	Node *left, *right;
	Node(const KeyType& _key) : key(_key), priority(rand()), size(1), left(NULL), right(NULL) {}
	void setLeft(Node* newLeft) {
		//printf("left=%d, newLeft=%d\n", left == NULL ? 9999 : left->key, newLeft == NULL ? 9999 : newLeft->key);
		left = newLeft; calcSize();
	}
	void setRight(Node* newRight) {
		//printf("right=%d, newRight=%d\n", left == NULL ? 9999 : left->key, newRight== NULL ? 9999 : newRight->key);
		right = newRight; calcSize();
	}
	void calcSize() {
		size = 1;
		if (left) size += left->size;
		if (right) size += right->size;
	}
};
//Node* root = NULL;

typedef pair <Node*, Node*> NodePair;

NodePair split(Node* root, KeyType key)
{
	//printf("split: root=%d, key=%d\n", root == NULL ? 9999 : root->key, key);
	if (root == NULL) {
		//printf("NodePair(NULL, NULL)");
		return NodePair(NULL, NULL);
	}

	if (root->key < key)
	{
		NodePair rs = split(root->right, key);
		root->setRight(rs.first);
		//printf("NodePair(%d,%d)\n", root->key == NULL ? 9999 : root->key, rs.second == NULL ? 9999 : rs.second->key);
		return NodePair(root, rs.second);
	}

	NodePair ls = split(root->left, key);
	root->setLeft(ls.second);
	//printf("NodePair(%d,%d)\n", ls.first == NULL ? 9999 : ls.first->key, root->key == NULL ? 9999 : root->key);
	return NodePair(ls.first, root);
}

Node* insert(Node* root, Node* node)
{
	//printf("insert: root=%d, node=%d\n", root == NULL ? 9999 : root->key, node==NULL? 9999: node->key);
	if (root == NULL)
	{
		//printf("root->key=NULL, node->key=%d\n", node->key);
		return node;
	}

	if (root->priority < node->priority)
	{
		NodePair splitted = split(root, node->key);
		//printf("node->key=%d: root->key=%d, split.first=%d, split.second=%d (setLeft, setRight)\n", node->key, root == NULL ? 9999 : root->key, splitted.first == NULL ? 9999 : splitted.first->key, splitted.second == NULL ? 9999 : splitted.second->key);
		node->setLeft(splitted.first);
		node->setRight(splitted.second);
		return node;
	}
	else if (root->key < node->key)
	{
		Node* temp = insert(root->right, node);
		root->setRight(temp);
		//printf("root->setRight(root=%d, right=%d)\n", root->key, temp->key);
	}
	else
	{
		Node* temp = insert(root->left, node);
		root->setLeft(temp);
		//printf("root->setLeft(root=%d, left=%d)\n", root->key, temp->key);
	}
	return root;
}

Node* merge(Node* a, Node* b)
{
	if (a == NULL) return b;
	if (b == NULL) return a;
	if (a->priority > b->priority)
	{
		a->setRight(merge(a->right, b));
		return a;
	}
	b->setLeft(merge(a, b->left));
	return b;
}

Node* erase(Node* root, KeyType key)
{
	if (root == NULL) return NULL;

	if (root->key == key)
	{
		Node* ret = merge(root->left, root->right);
		delete root;
		return ret;
	}
	else if (root->key > key)
	{
		root->setLeft(erase(root->left, key));
		return root;
	}
	else
	{
		root->setRight(erase(root->right, key));
		return root;
	}
}

/*Node* kth(Node* root, int k)
{
	if (root == NULL) return NULL;
	int leftSize = 0;
	if (root->left != NULL)
		leftSize = root->left->size;
	//if (root->size <= k)
	if (k == leftSize + 1)
	{
		//printf("root->key:%d\n", root->key);
		return root;
	}
	if (k <= leftSize)
	{//~ leftside
		Node* ret = kth(root->left, k);
		//printf("ret->key:%d\n", ret->key);
		return ret;
	}
	//printf("k=%d, leftSize = %d, val=%d\n", k, leftSize, k - leftSize - 1);
	Node* ret = kth(root->right, k - leftSize - 1);
	//printf("ret->key:%d\n", ret->key);
	return ret;
}*/
Node * kth(Node* root, int k) {
	int leftSize = 0;
	if (root->left != NULL) leftSize = root->left->size;
	if (k <= leftSize) return kth(root->left, k);
	if (k == leftSize + 1) return root;
	return kth(root->right, k - leftSize - 1);
}

int countLessThan(Node* root, KeyType key)
{
	if (root == NULL) return 0;
	int leftSize = 0;
	if (root->left)
		leftSize = root->left->size;
	//if (root->key == key) {
	//	return leftSize;
	//}
	else if (root->key >= key)
	{
		return countLessThan(root->left, key);
	}
	return 1 + leftSize + countLessThan(root->right, key);
}

void disp(Node* root)
{
	if (root == NULL) return;
	if (root->left != NULL)
		disp(root->left);
	printf("root:%d->, ", root->key);
	if (root->right != NULL)
		disp(root->right);

}

struct RNG {
	int seed, a, b;
	RNG(int _a, int _b) : a(_a), b(_b), seed(1983)
	{}
	int next() {
		int ret = seed;
		seed = ((seed * (long long)a) + b) % 20090711; //beware!!!
		return ret;
	}
};
	
void runningMedian(int n, RNG rng) 
{
	int sum = 0;
	//n = 20;
	Node* root = NULL;
	for (int cnt = 1; cnt <= n; cnt++)
	{
		int cur = rng.next();
		printf("cur=%d\n", cur);
		root = insert(root, new Node(cur));
		int size = root->size;
		int k = (cnt + 1) / 2;

		Node* found = kth(root, k);
		//disp(root);
		//printf("k(%dth) = key(%d)\n", k,found->key);
		sum = (sum + found->key) % 20090711;
		
		printf("sum=%d, key=%d, k=%d\n", sum, found->key, k);
	}
	std::cout << sum << std::endl;
}

bool myfunction(int i, int j) { return (i > j); }

int main()
{
	fstream myfile;
	myfile.open("runningMedian.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			//~ input
			int a = 0, b = 0;
			//~ body
			myfile >> N;
			myfile >> a;
			myfile >> b;
			struct RNG rng(a, b);
			runningMedian(N, rng);
		}

		myfile.close();
	}

	getchar();
	return 0;
}

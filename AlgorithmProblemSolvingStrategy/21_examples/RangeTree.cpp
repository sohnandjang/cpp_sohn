#include <iostream>
#include <vector>
#include <algorithm>

const int INTMAX = std::numeric_limits<int>::max();
struct RMQ { //range minimum query
	int n;
	std::vector<int> rangeMin;
	RMQ(const std::vector<int>& array) {
		n = array.size();
		rangeMin.resize(n * 4);
		init(array, 0, n - 1, 1);
	}
	int init(const std::vector<int>& array, int left, int right, int node)
	{
		if (left == right)
			return rangeMin[node] = array[left];
		int mid = (left + right) / 2;
		int leftMin = init(array, left, mid, node * 2);
		int rightMin = init(array, mid+1, right, node * 2 + 1);
		return rangeMin[node] = std::min(leftMin, rightMin);
	}
	int query(int left, int right, int node, int nodeLeft, int nodeRight)
	{
		if (right < nodeLeft || nodeRight < left)
			return INTMAX;
		if (left <= nodeLeft && nodeRight <= right)
			return rangeMin[node];
		int mid = (nodeLeft + nodeRight) / 2;
		return std::min(query(left, right, 2 * node, nodeLeft, mid),
			query(left, right, 2 * node + 1, mid + 1, nodeRight));
	}
	int query(int left, int right)
	{
		return query(left, right, 1, 0, n - 1);
	}
	int update(int index, int newValue, int node, int nodeLeft, int nodeRight)
	{
		if (index < nodeLeft || nodeRight < index)
			return rangeMin[node];
		if (nodeLeft == nodeRight)
			return (rangeMin[node] = newValue);
		int mid = ( nodeLeft + nodeRight ) / 2;
		return (std::min(update(index, newValue, 2 * node, nodeLeft, mid),
			update(index, newValue, 2 * node + 1, mid + 1, nodeRight)));
	}
	int update(int index, int newValue)
	{
		update(index, newValue, 1, 0, n - 1);
	}
};

struct RangeResult {
	int size;
	int mostFrequent;
	int leftNumber, leftFreq;
	int rightNumber, rightFreq;
};

RangeResult merge(const RangeResult& a, const RangeResult& b)
{
	RangeResult ret;
	ret.size = a.size + b.size;
	ret.leftNumber = a.leftNumber;
	ret.leftFreq = a.leftFreq;
	if (a.size == a.leftFreq && a.leftNumber == b.leftNumber)
		ret.leftFreq += b.leftFreq;
	ret.rightNumber = b.rightNumber;
	ret.rightFreq = b.rightFreq;
	if (b.size == b.rightFreq && a.rightNumber == b.rightNumber)
		ret.rightFreq += a.rightFreq;
	ret.mostFrequent = std::max(a.mostFrequent, b.mostFrequent);
	if (a.rightNumber == b.leftNumber)
		ret.mostFrequent = std::max(ret.mostFrequent, a.rightFreq + b.leftFreq);
	return ret;
}

int main()
{
	return 0;
}


#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
using namespace std;

//int p[100001];
//int q[100001];
vector<pair<int, int>> pq;
int N;
int numOfNerd;
struct treeNode {
	int index;
	vector<treeNode*> children;
};
bool isIncluded(treeNode* a, treeNode* b)
{
	bool ret = true;
	//if (p[a->index] > p[b->index] && q[a->index] > q[b->index])
	if (pq[a->index].first > pq[b->index].first && pq[a->index].second > pq[b->index].second)
		ret = true;
	else
		ret = false;
	//printf("[%d,%d] vs [%d,%d] = ret(%d)\n", pq[a->index].first,pq[a->index].second ,pq[b->index].first, pq[b->index].second, ret);
	return ret;
}
void deleteTree(treeNode* root)
{
	if (root == NULL)
		return;
	if (root->children.empty())
	{
		delete root;
	}
	for (size_t i = 0; i < root->children.size(); i++)
	{
		if (root->children[i]->children.empty())
			root->children.pop_back();
		deleteTree(root->children[i]);
	}
}
void getTree(treeNode* root, int cur)
{
	treeNode* temp = new treeNode();
	temp->index = cur;
	if (root->children.empty())
		root->children.push_back(temp);
	else
	{
		int included = 0;
		for (size_t i = 0; i < root->children.size(); i++)
		{
			if (isIncluded(root->children[i], temp) || isIncluded(temp, root->children[i]))
			{
				getTree(root->children[i], cur);
				included = 1;
				break;
			}
		}
		if(included == 0)
			root->children.push_back(temp);
	}
}
size_t getNumberOfNerd(treeNode* root)
{
	return root->children.size();
}
int solveNerd(struct treeNode* rootNode)
{
	rootNode->index = 0;
	for (int i = 0; i < N; i++)
	{
		getTree(rootNode, i);
		numOfNerd += getNumberOfNerd(rootNode);
	}
	return numOfNerd;
}
int main()
{
	fstream myfile;
	myfile.open("nerd.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			// init
			//memset(p, 0, sizeof(p));
			//memset(q, 0, sizeof(q));
			pq.clear();
			numOfNerd = 0;
			struct treeNode* rootNode = NULL; 
			deleteTree(rootNode);
			rootNode = new treeNode;
			// input
			myfile >> N;
			for (int j = 0; j < N; j++)
			{
				int p_temp;
				int q_temp;
				myfile >> p_temp; // p[j];
				myfile >> q_temp; // q[j];
				pq.push_back(pair<int, int>(p_temp, q_temp));
			}
			// body
			int ret = solveNerd(rootNode);
			cout << ret << endl;
			
		}
		myfile.close();
	}
	getchar();
	return 0;
}

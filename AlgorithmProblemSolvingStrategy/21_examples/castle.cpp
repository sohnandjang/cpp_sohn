#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

int W = 0;
typedef struct _Elem{
	int idx;
	int x;
	int y;
	int r;
}Elem;
vector<Elem> castle;
vector<int> depth;
typedef struct _TreeNode {
	Elem Data;
	vector<struct _TreeNode> next;
}TreeNode;

//~ return
//~ if cand1 contains cand2, return 1
//~ if cand2 contains cand1, return -1
//~ if cand1 doesn't contain cand2, return 0
int isCovered(Elem cand1, Elem cand2)
{
	int ret = 1; //cand1 > cand2
	Elem big = cand1;
	Elem small = cand2; 
	if (cand2.r > cand1.r)
	{
		big = cand2;
		small = cand1;
		ret = -1;//cand2 > cand1
	}
	//~ distance center to center	
	unsigned diff_c = (big.x - small.x)*(big.x - small.x) + (big.y - small.y)*(big.y - small.y);
	unsigned diff_r = (big.r - small.r)*(big.r - small.r);
	if (diff_r > diff_c) //~ big
		return ret;
	else
		return 0; //~ doesn't contain
}
struct _TreeNode root;

void disp(struct _TreeNode* i_root)
{
	for (size_t i = 0; i < i_root->next.size(); i++)
	{
		printf("%d(root) -> %d(sub)\n", i_root->Data.idx, i_root->next[i].Data.idx);
		disp(&i_root->next[i]);
	}
}
//return true: insert success
bool makeTree(struct _TreeNode* i_root, Elem* cur)
{
	struct _TreeNode curTreeNode;
	curTreeNode.Data = *cur;
	if (isCovered(i_root->Data, *cur))
	{
		if (i_root->next.size() == 0)
		{
			i_root->next.push_back(curTreeNode);
			return true;
		}
		else
		{
			for (unsigned int i = 0; i < i_root->next.size(); i++)
			{
				bool ret;
				ret = makeTree(&i_root->next[i], cur);
				if (ret == true)
					return ret;
			}
			//~ cur fails to be included in root
			i_root->next.push_back(curTreeNode);
			return true;
		}
	}
	else
		return false;
}
void calcDepth(struct _TreeNode* i_root, int i_depth)
{
	if (i_root->next.size() != 0)
	{
		for (size_t i = 0; i < i_root->next.size(); i++)
		{
			calcDepth(&i_root->next[i], i_depth+1);
		}
	}
	else
	{
		depth.push_back(i_depth);
	}
}
struct myclass {
	bool operator() (int i, int j) { return (i > j); }
} myCompare;
int calcBorder(vector<Elem>& castle)
{
	root.Data = castle[0];

	for (unsigned int i = 1; i < castle.size(); i++)
	{
		makeTree( &root, &castle[i]);
	}
	int ret = 0;
	calcDepth(&root, 0);
	if (depth.size() == 1)
		ret = depth[0]; //select the max
	else if (depth.size() > 1)
	{
		sort(depth.begin(), depth.end(), myCompare);
		ret = depth[0] + depth[1]; // select the max depth 2ea
	}
	disp( &root );
	return ret;
}

int main()
{
	ifstream myfile;
	myfile.open("castle.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			castle.clear();
			memset(&root, 0, sizeof(root));
			depth.clear();
			//~ input
			myfile >> W;
			for (int j = 0; j < W; j++)
			{
				Elem  elem;
				elem.idx = j;
				myfile >> elem.x;
				myfile >> elem.y;
				myfile >> elem.r;
				castle.push_back(elem);
			}
			//~ body
			int ret = calcBorder(castle);
			cout << ret << endl;
		}
		myfile.close();
	}

	getchar();
	return 0;
}
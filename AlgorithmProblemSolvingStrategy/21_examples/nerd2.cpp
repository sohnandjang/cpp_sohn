#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
using namespace std;

int N;
map<int, int> coords;
bool isDominated(int x, int y)
{
	map<int, int>::iterator it = coords.lower_bound(x);
	if (it == coords.end()) return false;
	return y < it->second;
}

void removeDominated(int x, int y)
{
	map<int, int>::iterator it = coords.lower_bound(x);
	if (it == coords.begin()) return;
	--it;
	while (true)
	{
		if (it->second > y) break;
		if (it == coords.begin())
		{
			coords.erase(it);
			break;
		}
		else
		{
			map<int, int>::iterator jt = it;
			--jt;
			coords.erase(it);
			it = jt;
		}
	}
}

int registered(int x, int y)
{
	if (isDominated(x, y)) return coords.size();
	removeDominated(x, y);
	coords[x] = y;
	return coords.size();
}

#if 0
map<int, int> nerdMap;
int solve(int x, int y)
{
	if (nerdMap.empty())
	{
		nerdMap.insert(pair<int, int>(x, y));
		return nerdMap.size();//~ 1
	}
	map<int, int>::iterator it;
	it = nerdMap.upper_bound(x);
	if (it == nerdMap.end())
	{
		nerdMap.insert(pair<int, int>(x, y));
		it--;
		while (it != nerdMap.begin())
		{
			if (y > it->second)
			{
				it = nerdMap.erase(it);
			}
			--it;
		}
		return nerdMap.size();
	}
	else
	{
		if (y <= it->second)
		{
			//~ ignore
			return nerdMap.size();
		}
		else
		{
			nerdMap.insert(pair<int, int>(x, y));
			it--;
			while (it != nerdMap.begin())
			{
				if (y > it->second)
				{
					it = nerdMap.erase(it);
				}
				--it;
			}
			return nerdMap.size();
		}
	}
}
#endif
int main()
{
	fstream myfile;
	myfile.open("nerd.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		//C = 1;
		for (int i = 0; i < C; i++)
		{
			myfile >> N;
			int x=0, y=0;
			int ret = 0;
			//nerdMap.clear();
			for (int j = 0; j < N; j++)
			{
				myfile >> x;
				myfile >> y;
				//ret += solve(x, y);
				ret += registered(x, y);
			}

			cout << ret << endl;
		}
		myfile.close();
	}
	getchar();
	return 0;
}

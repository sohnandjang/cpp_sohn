#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
#include <numeric>

struct FenwickTree {
	std::vector<int> tree;
	FenwickTree(int n) : tree(n + 1) 
	{
		//std::iota(std::begin(tree), std::end(tree), 0);
	}
	int sum(int pos)
	{
		++pos;
		int ret = 0;
		while (pos > 0)
		{
			ret += tree[pos];
			pos &= (pos - 1);
		}
		return ret;
	}
	void add(int pos, int val)
	{
		++pos;
		while ((unsigned int)pos < tree.size())
		{
			tree[pos] += val;
			//std::cout << "pos=" << pos << "val=" << val << "tree[pos]=" << tree[pos] << std::endl;
			pos += (pos & -pos);
		}
	}
};
int main()
{
	//struct FenwickTree FT(16);
	//std::cout << "FT.sum(6) = " << FT.sum(6) << std::endl;
	//FT.add(4, 3);
	// Quiz : 2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9
	// 1. sum[5] ?
	// 2. p[3]+=6;
	//    sum[5] ? 
	std::vector<int> p = { 2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9 };
	unsigned int n = p.size();
	FenwickTree FT(n + 1);
	for (unsigned int i=0; i<p.size(); i++)
	{
		FT.add(i, p[i]);
	}
	std::cout << FT.sum(13) << std::endl;
	//FT.add(5, -6);
	//std::cout << FT.sum(1) - FT.sum(0) << std::endl;
	getchar();
	return 0;
}
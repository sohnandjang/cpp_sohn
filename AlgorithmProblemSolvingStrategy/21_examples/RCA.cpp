#include <iostream>
#include <vector>
#include <algorithm>

const int INTMAX = std::numeric_limits<int>::max();
struct RMQ { //range minimum query
	int n;
	std::vector<int> rangeMin;
	RMQ(const std::vector<int>& array) {
		n = array.size();
		rangeMin.resize(n * 4);
		init(array, 0, n - 1, 1);
	}
	int init(const std::vector<int>& array, int left, int right, int node)
	{
		if (left == right)
			return rangeMin[node] = array[left];
		int mid = (left + right) / 2;
		int leftMin = init(array, left, mid, node * 2);
		int rightMin = init(array, mid+1, right, node * 2 + 1);
		return rangeMin[node] = std::min(leftMin, rightMin);
	}
	int query(int left, int right, int node, int nodeLeft, int nodeRight)
	{
		if (right < nodeLeft || nodeRight < left)
			return INTMAX;
		if (left <= nodeLeft && nodeRight <= right)
			return rangeMin[node];
		int mid = (nodeLeft + nodeRight) / 2;
		return std::min(query(left, right, 2 * node, nodeLeft, mid),
			query(left, right, 2 * node + 1, mid + 1, nodeRight));
	}
	int query(int left, int right)
	{
		return query(left, right, 1, 0, n - 1);
	}
	int update(int index, int newValue, int node, int nodeLeft, int nodeRight)
	{
		if (index < nodeLeft || nodeRight < index)
			return rangeMin[node];
		if (nodeLeft == nodeRight)
			return (rangeMin[node] = newValue);
		int mid = ( nodeLeft + nodeRight ) / 2;
		return (std::min(update(index, newValue, 2 * node, nodeLeft, mid),
			update(index, newValue, 2 * node + 1, mid + 1, nodeRight)));
	}
	int update(int index, int newValue)
	{
		update(index, newValue, 1, 0, n - 1);
	}
};

const int MAX_N = 100000;
int n;
//const int P[MAX_N] = { 0, 1, 2, 1, 3, 4, 3, 5, 3, 1, 0, 6, 7, 6, 0, 8, 9, 10, 9, 11, 9, 8, 12, 8, 0 };
std::vector<int> child[MAX_N] = 
{
	{1, 6, 8},
	{2, 3},
	{},
	{4,5},
	{},
	{},
	{7},
	{},
	{9, 12},
	{10, 11},
	{},
	{},
	{}
};

int no2serial[MAX_N], serial2no[MAX_N];

int locInTrip[MAX_N], depth[MAX_N];

int nextSerial;

void traverse(int here, int d, std::vector<int>& trip)
{
	no2serial[here] = nextSerial;
	serial2no[nextSerial] = here;
	++nextSerial;
	depth[here] = d;
	locInTrip[here] = trip.size();
	trip.push_back(no2serial[here]);
	for (size_t i = 0; i < child[here].size(); ++i)
	{
		traverse(child[here][i], d + 1, trip);
		trip.push_back(no2serial[here]);
	}
}

RMQ* prepareRMQ() {
	nextSerial = 0;
	std::vector<int> trip;
	traverse(0, 0, trip);
	return new RMQ(trip);
}

int distance(RMQ* rmq, int u, int v) {
	int lu = locInTrip[u], lv = locInTrip[v];
	if (lu > lv) std::swap(lu, lv);
	int lca = serial2no[rmq->query(lu, lv)];
	return depth[u] + depth[v] - 2 * depth[lca];
}
int main()
{
	RMQ* rmq = prepareRMQ();
	//int ret = distance(rmq, 2, 7);
	//std::cout << ret << std::endl;
	int ret = distance(rmq, 10, 11);
	std::cout << ret << std::endl;
	/*ret = distance(rmq, 4, 11);
	std::cout << ret << std::endl;
	ret = distance(rmq, 7, 7);
	std::cout << ret << std::endl;
	ret = distance(rmq, 10, 0);
	std::cout << ret << std::endl;*/
	getchar();
	return 0;
}
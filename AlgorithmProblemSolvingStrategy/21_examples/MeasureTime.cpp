#include <iostream>
#include <vector>
#include <algorithm>
#include <list>
#include <numeric>
using namespace std;

struct FenwickTree {
	std::vector<int> tree;
	FenwickTree(int n) : tree(n + 1) 
	{
		//std::iota(std::begin(tree), std::end(tree), 0);
	}
	int sum(int pos)
	{
		++pos;
		int ret = 0;
		while (pos > 0)
		{
			ret += tree[pos];
			pos &= (pos - 1);
		}
		return ret;
	}
	void add(int pos, int val)
	{
		++pos;
		while ((unsigned int)pos < tree.size())
		{
			tree[pos] += val;
			//std::cout << "pos=" << pos << "val=" << val << "tree[pos]=" << tree[pos] << std::endl;
			pos += (pos & -pos);
		}
	}
};
long long countMoves(const std::vector<int>& A)
{
	FenwickTree tree(1000000);
	long long ret = 0;
	for (unsigned int i = 0; i < A.size(); i++)
	{
		ret += tree.sum(999999) - tree.sum(A[i]);
		tree.add(A[i], 1);
	}
	return ret;
}

typedef int KeyType;
struct Node {
	KeyType key;
	int priority, size;
	Node *left, *right;
	Node(const KeyType& _key) : key(_key), priority(rand()), size(1), left(NULL), right(NULL){}
	void setLeft(Node* newLeft) { 
		//printf("left=%d, newLeft=%d\n", left == NULL ? 9999 : left->key, newLeft == NULL ? 9999 : newLeft->key);
		left = newLeft; calcSize(); 
	}
	void setRight(Node* newRight) { 
		//printf("right=%d, newRight=%d\n", left == NULL ? 9999 : left->key, newRight== NULL ? 9999 : newRight->key);
		right = newRight; calcSize(); 
	}
	void calcSize() {
		size = 1;
		if (left) size += left->size;
		if (right) size += right->size;
	}
};
typedef pair <Node*, Node*> NodePair;
NodePair split(Node* root, KeyType key)
{
	//printf("split: root=%d, key=%d\n", root == NULL ? 9999 : root->key, key);
	if (root == NULL) {
		//printf("NodePair(NULL, NULL)");
		return NodePair(NULL, NULL);
	}

	if (root->key < key)
	{
		NodePair rs = split(root->right, key);
		root->setRight(rs.first);
		//printf("NodePair(%d,%d)\n", root->key == NULL ? 9999 : root->key, rs.second == NULL ? 9999 : rs.second->key);
		return NodePair(root, rs.second);
	}

	NodePair ls = split(root->left, key);
	root->setLeft(ls.second);
	//printf("NodePair(%d,%d)\n", ls.first == NULL ? 9999 : ls.first->key, root->key == NULL ? 9999 : root->key);
	return NodePair(ls.first, root);
}
Node* insert(Node* root, Node* node)
{
	//printf("insert: root=%d, node=%d\n", root == NULL ? 9999 : root->key, node==NULL? 9999: node->key);
	if (root == NULL)
	{
		//printf("root->key=NULL, node->key=%d\n", node->key);
		return node;
	}

	if (root->priority < node->priority)
	{
		NodePair splitted = split(root, node->key);
		//printf("node->key=%d: root->key=%d, split.first=%d, split.second=%d (setLeft, setRight)\n", node->key, root == NULL ? 9999 : root->key, splitted.first == NULL ? 9999 : splitted.first->key, splitted.second == NULL ? 9999 : splitted.second->key);
		node->setLeft(splitted.first);
		node->setRight(splitted.second);
		return node;
	}
	else if (root->key < node->key)
	{
		Node* temp = insert(root->right, node);
		root->setRight(temp);
		//printf("root->setRight(root=%d, right=%d)\n", root->key, temp->key);
	}
	else
	{
		Node* temp = insert(root->left, node);
		root->setLeft(temp);
		//printf("root->setLeft(root=%d, left=%d)\n", root->key, temp->key);
	}
	return root;
}
int countLessThan(Node* root, KeyType key)
{
	if (root == NULL) return 0;
	if (root->key >= key)
		return countLessThan(root->left, key);
	int ls = (root->left ? root->left->size : 0);
	return ls + 1 + countLessThan(root->right, key);
}
void deleteTree(Node* node)
{
	if (node == NULL) return;
	deleteTree(node->left);
	deleteTree(node->right);
	delete node;
}
void disp(Node* root);
long long countMoves2(const vector<int>& A)
{
	Node* root = NULL;
	long long ret = 0;
	for (unsigned int i = 0; i < A.size(); i++)
	{
		//printf(">>>>>>> i = %d, A[%d] = %d\n", i, i, A[i]);
		long long temp = countLessThan(root, A[i]+1 );
		ret += (i- temp);
		//cout << "sum = " << i-temp<< ", i = " << i<< ", temp = " << temp<< endl;
		root = insert(root, new Node(A[i]));
		//disp(root);
		//printf("\n");
	}
	deleteTree(root);
	return ret;
}
void disp(Node* root)
{
	if (root == NULL) return;
	if(root->left != NULL)
		disp(root->left);
	printf("root:%d->, ", root->key);
	if(root->right != NULL)
		disp(root->right);
		
}
long long countMoves3(vector<int>& A, int left, int right)
{
	if(left == right) return 0;
	int mid = (left + right) / 2;
	long long ret = countMoves3(A, left, mid) + countMoves3(A, mid + 1, right);
	vector<int> tmp(right - left + 1);
	int tmpIndex = 0, leftIndex = left, rightIndex = mid + 1;
	while (leftIndex <= mid || rightIndex <= right) {
		if (leftIndex <= mid &&
			(rightIndex > right || A[leftIndex] <= A[rightIndex]))
		{
			tmp[tmpIndex++] = A[leftIndex++];
		}
		else {
			ret += mid - leftIndex + 1;
			tmp[tmpIndex++] = A[rightIndex++];
		}
	}
	for (int i = 0; i < tmp.size(); ++i)
		A[left + i] = tmp[i];
	return ret;
}
int main()
{
	//struct FenwickTree FT(16);
	//std::cout << "FT.sum(6) = " << FT.sum(6) << std::endl;
	//FT.add(4, 3);
	/* solution1: FenwickTree
	long long ret = countMoves(A);
	std::cout << ret << std::endl; */
	// solution2 : binary search tree (heap)
	//const std::vector<int> A = { 5, 1, 4, 3, 2 };
	//long long ret = countMoves2(A);
	//std::cout << ret << std::endl; 
	//solution3 : merge sort
	std::vector<int> A = { 5, 1, 4, 3, 2 };
	long long ret = countMoves3(A, 0, 4);
	std::cout << ret << std::endl; 
	

	getchar();
	return 0;
}


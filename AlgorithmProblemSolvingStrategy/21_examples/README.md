# post order
``` c++
vector<int> slice(const vector<int>& v, int a, int b){
	return vector<int>(v.begin()+a, v.begin()+b);
}
```
``` c++
const int L = find ( inorder.begin(), inorder.end(), root) - inorder.begin();
```

```vector<int>``` usage is important  

# Castle 
code21.5 errata as I think,  
``` c++
if(heights.empty()) return 0
-->
if(root->children.empty()) return 0
```
Because it is to check if root has children or not.  
``` c++
if(heights.size() >=2 )
-->
if(root->children.size() >=2 )
```
It is the same reason like upper.

# map
cautions>  
* if you erase the element from map,  
** if you use iterator and want to use it continuously after erase
*** you had better erase(iterator)  
``` c++
iterator erase(const_iterator position);
size_type erase(const key_type& k);
```

# runningMedian
cautions>
%20170711 I should've written it.  
But I made a mistake  
%20170701  
-> Beware typo  

tips: prevent overflow>  
when multiply int x int = int x (long long) int

# RMQ
Range Minumum Query

# LCA
least/lowest commmon ancestor  

To Solve the Problem  
1. change input to RMQ  
2. make depth[ ]   
3. calc LCA from RMQ  
4. distance = depth[u]+depth[v]-2xdepth[lca]  
5. no2serial[ ] , serial2no[ ]  
I have to reorder the number, so not to make mistake, I need to make two arrays.  
# Fenwick Tree
``` c++
std::vector<int> p = { 2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9 };
unsigned int n = p.size();
FenwickTree FT(n + 1);
for (unsigned int i=0; i<p.size(); i++)
{
	FT.add(i, p[i]);
}
std::cout << FT.sum(5) << std::endl;
FT.add(5, 6);
std::cout << FT.sum(5) << std::endl;
```

# MeasureTime 24.7
when we use Fenwick Tree,  
We must set all elements of tree to 0.  

Initially, all values are zero.  
My mistake) I put 1,2,3,.....n into tree at first, so it's hard to understand how it works.












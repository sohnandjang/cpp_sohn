#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
int N;
const int root = 0;
int x[20];
int y[20];
int r[20];
struct TreeNode {
	vector<TreeNode*> children;
};
int sqr(int x)
{
	return x * x;
}
int sqrdist(int a, int b)
{
	return sqr(x[a] - x[b]) + sqr(y[a] - y[b]);
}
bool enclosed(int a, int b)
{
	bool ret = false;
	if (r[a] > r[b] && sqr(r[a]- r[b]) > sqrdist(a, b))
	{
		ret = true;
	}
	return ret;
}
bool isChild(int parent, int child)
{
	bool ret = true;
	if (!enclosed(parent, child))
		return false;

	for (int i = 0; i < N; i++)
		if (i != parent && i != child)
			if (enclosed(parent, i) && enclosed(i, child))
				ret = false;

	return ret;
}
TreeNode* getTree(int i_root)
{
	TreeNode* ret = new TreeNode;
	for (size_t i = 0; i < N; i++)
	{
		if (isChild(i_root, i))
			ret->children.push_back(getTree(i));
	}
	return ret;
}
vector<int> heights;
int longest;
int height(TreeNode* i_root)
{
	if (i_root->children.empty())
		return 0;
	for (size_t i = 0; i < i_root->children.size(); i++)
	{
		int temp = height(i_root->children[i]);
		heights.push_back(temp);
		//heights.push_back(height(i_root->children[i]));
		printf("push_back(%d), heights[%d th]\n", temp, i);
	}
	//if (heights.empty())
	//if( i)
	//	return 0;

	if (i_root->children.size() >= 2)
	{
		sort(heights.begin(), heights.end());
		longest = max(longest, 2 + heights[heights.size() - 2] + heights[heights.size() - 1]);
	}

	return heights.back() + 1;
}
int solve(TreeNode* root)
{
	longest = 0;
	int h = height(root);
	return max(longest, h);
}
int main()
{
	ifstream myfile;
	myfile.open("castle.txt");
	if (myfile.is_open())
	{
		int C;
		myfile >> C;
		C = 1;
		for (int i = 0; i < C; i++)
		{
			//~ init
			memset(x, 0, sizeof(x));
			memset(y, 0, sizeof(y));
			memset(r, 0, sizeof(r));
			heights.clear();
			longest = 0;
			//~ input
			myfile >> N;
			for (int j = 0; j < N; j++)
			{
				myfile >> x[j];
				myfile >> y[j];
				myfile >> r[j];
			}
			//~ body
			TreeNode* rootNode;
			rootNode = getTree(root);
			int ret = solve(rootNode);
			cout << ret << endl;
		
		}
		myfile.close();
	}

	getchar();
	return 0;
}